DROP FUNCTION public.sp_rpt_get_radiologi();
CREATE OR REPLACE FUNCTION public.sp_rpt_get_radiologi()
 RETURNS table(
  root_nm varchar(20),
  medicalunit_cd varchar(20),
  medicalunit_nm varchar(100)
  )
AS $function$
BEGIN
  RETURN QUERY SELECT 
  C.medicalunit_nm AS root_nm, 
  A.medicalunit_cd, 
  A.medicalunit_nm
  FROM public.trx_unitmedis_item A
  JOIN public.trx_unit_medis B On A.medunit_cd=B.medunit_cd AND B.medicalunit_tp='MEDICALUNIT_TP_3'
  LEFT JOIN public.trx_unitmedis_item C ON A.medicalunit_root=C.medicalunit_cd AND C.medicalunit_nm IS NOT NULL
  ORDER BY CASE WHEN C.medicalunit_nm IS NULL THEN A.medicalunit_cd ELSE A.medicalunit_root END,
  A.medicalunit_root,A.medicalunit_nm;
END;
$function$
LANGUAGE plpgsql;