DROP FUNCTION public.sp_rpt_get_poliklinik();
CREATE OR REPLACE FUNCTION public.sp_rpt_get_poliklinik()
 RETURNS table(
  medunit_cd varchar(20),
  medunit_nm varchar(100)
  )
AS $function$
BEGIN
  RETURN QUERY SELECT 
  A.medunit_cd, 
  A.medunit_nm
  FROM public.trx_unit_medis A
  WHERE A.medicalunit_tp='MEDICALUNIT_TP_1'
  ORDER BY A.medunit_nm;
END;
$function$
LANGUAGE plpgsql;