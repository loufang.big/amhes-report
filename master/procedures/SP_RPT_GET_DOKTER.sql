DROP FUNCTION public.SP_RPT_GET_DOKTER();
CREATE OR REPLACE FUNCTION public.SP_RPT_GET_DOKTER()
RETURNS table(
  dr_cd varchar(20),
  dr_nm varchar(100),
  spesialis_cd varchar(20),
  spesialis_nm varchar(100)
)
AS $$
BEGIN
  RETURN QUERY SELECT A.dr_cd, A.dr_nm, A.spesialis_cd, B.spesialis_nm
  FROM public.trx_dokter A
  LEFT JOIN public.trx_spesialis B ON A.spesialis_cd=B.spesialis_cd
  ORDER BY B.spesialis_nm, A.dr_nm;
END;
$$ LANGUAGE plpgsql;