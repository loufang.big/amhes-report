DROP FUNCTION public.sp_rpt_get_tindakan();
CREATE OR REPLACE FUNCTION public.sp_rpt_get_tindakan()
 RETURNS table(
  treatment_cd varchar(20),
  treatment_nm varchar(100),
  treatment_root varchar(20),
  root_nm varchar(100)
  )
AS $function$
BEGIN
  RETURN QUERY SELECT 
  A.treatment_cd, 
  A.treatment_nm, 
  A.treatment_root, 
  COALESCE(B.treatment_cd, '') AS root_nm
  FROM trx_tindakan A
  LEFT JOIN trx_tindakan B On A.treatment_root=B.treatment_cd
  ORDER BY A.treatment_root, A.treatment_cd;
END;
$function$
LANGUAGE plpgsql;
