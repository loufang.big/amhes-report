DROP FUNCTION public.sp_rpt_get_kamar();
CREATE OR REPLACE FUNCTION public.sp_rpt_get_kamar()
 RETURNS table(
  kelas_nm varchar(100),
  ruang_cd varchar(20),
  ruang_nm varchar(100),
  bangsal_nm varchar(100)
  )
AS $function$
BEGIN
  RETURN QUERY SELECT 
  B.kelas_nm, 
  A.ruang_cd, 
  A.ruang_nm, 
  C.bangsal_nm
  FROM public.trx_ruang A
  JOIN public.trx_kelas B ON A.kelas_cd=B.kelas_cd
  LEFT JOIN public.trx_bangsal C ON A.bangsal_cd=C.bangsal_cd
  ORDER BY B.kelas_nm, A.ruang_nm;
END;
$function$
LANGUAGE plpgsql;