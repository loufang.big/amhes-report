DROP FUNCTION public.sp_rpt_get_penyakit();
CREATE OR REPLACE FUNCTION public.sp_rpt_get_penyakit()
 RETURNS table(
  icd_cd varchar(20),
  icd_nm varchar(100),
  icd_root varchar(20),
  root_nm varchar(100)
  )
AS $function$
BEGIN
  RETURN QUERY SELECT 
  A.icd_cd, 
  A.icd_nm, 
  A.icd_root, 
  B.icd_cd AS root_nm
  FROM public.trx_icd A
  LEFT JOIN public.trx_icd B On A.icd_root=B.icd_cd
  ORDER BY A.icd_root, A.icd_cd;
END;
$function$
LANGUAGE plpgsql;