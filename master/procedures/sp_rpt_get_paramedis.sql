DROP FUNCTION public.sp_rpt_get_paramedis();
CREATE OR REPLACE FUNCTION public.sp_rpt_get_paramedis()
 RETURNS table(
  paramedis_cd varchar(20),
  paramedis_nm varchar(100),
  paramedis_tp varchar(10),
  tipe_nm varchar(100)
  )
AS $function$
BEGIN
  RETURN QUERY SELECT 
  A.paramedis_cd, 
  A.paramedis_nm, 
  A.paramedis_tp, 
  COMCD.code_nm AS tipe_nm
  FROM public.trx_paramedis A
  LEFT JOIN public.com_code COMCD On A.paramedis_tp=COMCD.com_cd
  ORDER BY COMCD.code_nm, A.paramedis_nm;
END;
$function$
LANGUAGE plpgsql;