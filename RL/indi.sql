DROP FUNCTION public.sp_rpt_rl_1_2_indikator_pelayanan_rumah_sakit(p_dateStart varchar, p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.sp_rpt_rl_1_2_indikator_pelayanan_rumah_sakit(p_dateStart varchar, p_dateEnd varchar)
 RETURNS TABLE(kelas_cd character varying, kelas_nm character varying, total_kamar bigint, total_pasien bigint, total_harirawat bigint, total_meninggal int, day_month integer)
AS $function$
BEGIN
  RETURN QUERY 
  SELECT 
  A.kelas_cd,
  A.kelas_nm,
  COUNT(B.ruang_cd) AS total_kamar,
  coalesce(public.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd,p_dateStart,p_dateEnd), 0) AS total_pasien,
  coalesce(public.FN_RPT_TOTAL_HARIRAWAT(A.kelas_cd,p_dateStart,p_dateEnd), 0) AS total_harirawat,
  coalesce(public.FN_RPT_TOTAL_DEATH_OUTTP11(A.kelas_cd,p_dateStart::varchar,p_dateEnd::varchar), 0) AS total_meninggal,
  to_char(p_dateEnd::timestamp, 'dd')::integer AS day_month
  FROM trx_kelas A 
  JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd 
  GROUP BY A.kelas_cd,A.kelas_nm
  ORDER BY A.kelas_nm;
END;
$function$
 LANGUAGE plpgsql;