DROP FUNCTION public.sp_rpt_rl_3_11_kegiatan_kesehatan_jiwa(p_dateStart varchar, p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.sp_rpt_rl_3_11_kegiatan_kesehatan_jiwa(p_dateStart varchar, p_dateEnd varchar)
 RETURNS TABLE(jenis_kegiatan text, jumlah bigint)
AS $function$
BEGIN
	return QUERY SELECT  
	RTRIM(LTRIM(A.medical_note)) as Jenis_Kegiatan,
	COUNT(A.medical_note) AS Jumlah
	FROM public.trx_medical_tindakan A JOIN public.trx_medical B ON A.medical_cd = B.medical_cd
	WHERE B.medunit_cd = 'POLISPJW' 
	AND B.datetime_in >= p_dateStart::timestamp AND B.datetime_in <= p_dateEnd::timestamp 
	GROUP BY A.medical_note;
END;
$function$
LANGUAGE plpgsql;

SELECT * from public.sp_rpt_rl_3_11_kegiatan_kesehatan_jiwa('2017-01-01', '2017-12-31');