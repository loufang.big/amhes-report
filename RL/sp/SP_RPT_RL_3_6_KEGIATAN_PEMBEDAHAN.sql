DROP FUNCTION public.SP_RPT_RL_3_6_KEGIATAN_PEMBEDAHAN (p_dateStart varchar,p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.SP_RPT_RL_3_6_KEGIATAN_PEMBEDAHAN (p_dateStart varchar,p_dateEnd varchar) 
RETURNS table(
	Jenis_Kegiatan text,
	Jumlah bigint
	)
AS $$
BEGIN
	RETURN QUERY SELECT
		RTRIM(LTRIM(C.treatment_nm)) AS Jenis_Kegiatan,
		COUNT(1)AS Jumlah
	FROM trx_medical_tindakan A
	JOIN trx_medical B ON A.medical_cd = B.medical_cd
	JOIN trx_tindakan C ON A.treatment_cd = C.treatment_cd
	WHERE
		B.medunit_cd = 'POLISPB'
	AND B.datetime_in >=  p_dateStart::timestamp
	AND B.datetime_in <=  p_dateEnd::timestamp
	GROUP BY
		C.treatment_nm;
END;
$$ 
LANGUAGE plpgsql;

SELECT * from public.SP_RPT_RL_3_6_KEGIATAN_PEMBEDAHAN('2017-01-01', '2017-12-31');