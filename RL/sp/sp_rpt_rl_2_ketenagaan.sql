DROP FUNCTION public.sp_rpt_rl_2_ketenagaan();
CREATE OR REPLACE FUNCTION public.sp_rpt_rl_2_ketenagaan()
 RETURNS TABLE(paramedis_cd character varying, paramedis_nm character varying, paramedis_tp character varying, tipe_nm character varying)
AS $function$
BEGIN
	RETURN QUERY SELECT 
	A.paramedis_cd, 
	A.paramedis_nm, 
	A.paramedis_tp, 
	COMCD.code_nm AS tipe_nm
	FROM trx_paramedis A
	LEFT JOIN com_code COMCD On A.paramedis_tp=COMCD.com_cd
	ORDER BY COMCD.code_nm, A.paramedis_nm;
END;
$function$
LANGUAGE plpgsql;

SELECT * from public.sp_rpt_rl_2_ketenagaan();