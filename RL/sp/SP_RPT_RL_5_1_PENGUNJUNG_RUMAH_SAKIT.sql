DROP FUNCTION public.SP_RPT_RL_5_1_PENGUNJUNG_RUMAH_SAKIT (p_dateStart Varchar(10),p_dateEnd Varchar(10));
CREATE OR REPLACE FUNCTION public.SP_RPT_RL_5_1_PENGUNJUNG_RUMAH_SAKIT (p_dateStart Varchar(10),p_dateEnd Varchar(10)) 
RETURNS table(
	jenis text,
	total bigint
)
AS $$
BEGIN

--JUMLAH KUNJUNGAN PASIEN BARU
Return Query SELECT 
	'PASIEN BARU' as jenis,
	COUNT (CASE when B.register_date = A.datetime_in THEN 'Pasien Baru' ELSE NULL END) AS Total
FROM trx_medical as  A JOIN trx_pasien as B ON A.pasien_cd = B.pasien_cd
WHERE A.datetime_in BETWEEN p_dateStart::timestamp AND p_dateEnd::timestamp

UNION
--JUMLAH KUNJUNGAN PASIEN LAMA
SELECT 
	'PASIEN LAMA' as jenis,
	COUNT (CASE WHEN B.register_date=A.datetime_in THEN NULL ELSE 'Pasien Lama' END) AS Total
FROM trx_medical as  A JOIN trx_pasien as B ON A.pasien_cd = B.pasien_cd
WHERE A.datetime_in BETWEEN p_dateStart::timestamp AND p_dateEnd::timestamp

END;
$$ LANGUAGE plpgsql;

SELECT * FROM public.SP_RPT_RL_5_1_PENGUNJUNG_RUMAH_SAKIT('2017-01-01', '2017-12-31');