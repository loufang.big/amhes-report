DROP FUNCTION public.sp_rpt_rl_3_1_kegiatan_pelayanan_rawat_inap(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
CREATE OR REPLACE FUNCTION public.sp_rpt_rl_3_1_kegiatan_pelayanan_rawat_inap(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone)
 RETURNS TABLE(medunit_cd character varying, medunit_nm character varying, pasien_awal bigint, pasien_masuk bigint, pasien_keluar_hidup bigint, keluar_mati_krg_28_jam bigint, keluar_mati_lbh_28_jam bigint, lama_dirawat bigint, pasien_akhir_satu_thn bigint, jml_hari_perawatan bigint, vvip bigint, vip bigint, kelas_1 bigint, kelas_2 bigint, kelas_3 bigint, kelas_khusus bigint)
 LANGUAGE plpgsql
AS $function$
BEGIN
	return query select 
	um.medunit_cd as medunit_cd,
	um.medunit_nm as medunit_nm,
	(select count(medical_cd) from trx_medical m where 
	fn_year(datetime_in) = fn_year(p_pdtDateStart)-1
	AND fn_month(datetime_in) = 12
	AND fn_day(datetime_in) = 31
	 and m.medunit_cd=um.medunit_cd) as pasien_awal,

	(select count(medical_cd) from trx_medical m where  
	datetime_in >= p_pdtDateStart
	AND datetime_in <=p_pdtDateEnd
	 and m.medunit_cd=um.medunit_cd) as pasien_masuk,

	(select count(medical_cd) from trx_medical m where 
	datetime_in >=p_pdtDateStart
	AND datetime_in <=p_pdtDateEnd
	 and m.medunit_cd=um.medunit_cd and out_tp<>'OUT_TP_04') as pasien_keluar_hidup,

	(select count(medical_cd) from trx_medical m where 
	datetime_in >=p_pdtDateStart
	AND datetime_in <=p_pdtDateEnd
	 and m.medunit_cd=um.medunit_cd and out_tp='OUT_TP_04' and fn_date_diff_hour(datetime_in, datetime_out) <= 48) as keluar_mati_krg_28_jam,
	
	(select count(medical_cd) from trx_medical m where 
	datetime_in >= p_pdtDateStart
	AND datetime_in <=p_pdtDateEnd and m.medunit_cd=um.medunit_cd and out_tp='OUT_TP_04' and fn_date_diff_hour(datetime_in, datetime_out) > 48) as keluar_mati_lbh_28_jam,
	
	(select SUM(fn_date_diff_hour(datetime_in, datetime_out)) from trx_medical m where m.medunit_cd=um.medunit_cd and out_tp<>'') as lama_dirawat,
	
	(select count(medical_cd) from trx_medical m where fn_year(datetime_in)=fn_year(p_pdtDateStart)
	and fn_month(datetime_in)=12 and fn_day(datetime_in)=31 and m.medunit_cd=um.medunit_cd) as pasien_akhir_satu_thn,

	(select SUM(fn_date_diff_hour(datetime_in, datetime_out)) from trx_medical m where m.medunit_cd=um.medunit_cd) as jml_hari_perawatan,
	-- 
	(select count(m.medical_cd) from trx_medical m
								join trx_ruang r on m.ruang_cd=r.ruang_cd
								join trx_kelas k on k.kelas_cd=r.kelas_cd where 
								datetime_in >=p_pdtDateStart
								AND datetime_in <=p_pdtDateEnd 
								and m.medunit_cd=um.medunit_cd and k.kelas_cd='KLVVIP') as vvip,
	(select count(m.medical_cd) from trx_medical m
								join trx_ruang r on m.ruang_cd=r.ruang_cd
								join trx_kelas k on k.kelas_cd=r.kelas_cd where 
								datetime_in >=p_pdtDateStart
								AND datetime_in <=p_pdtDateEnd
								and m.medunit_cd=um.medunit_cd and k.kelas_cd='KLVIP') as vip,
	(select count(m.medical_cd) from trx_medical m
								join trx_ruang r on m.ruang_cd=r.ruang_cd
								join trx_kelas k on k.kelas_cd=r.kelas_cd where 
								datetime_in >=p_pdtDateStart
								AND datetime_in <=p_pdtDateEnd
								and m.medunit_cd=um.medunit_cd and k.kelas_cd='KL01') as kelas_1,
	(select count(m.medical_cd) from trx_medical m
								join trx_ruang r on m.ruang_cd=r.ruang_cd
								join trx_kelas k on k.kelas_cd=r.kelas_cd where 
								datetime_in >=p_pdtDateStart
								AND datetime_in <=p_pdtDateEnd
								 and m.medunit_cd=um.medunit_cd and k.kelas_cd='KL02') as kelas_2,
	(select count(m.medical_cd) from trx_medical m
								join trx_ruang r on m.ruang_cd=r.ruang_cd
								join trx_kelas k on k.kelas_cd=r.kelas_cd where 
								datetime_in >=p_pdtDateStart
								AND datetime_in <=p_pdtDateEnd
								 and m.medunit_cd=um.medunit_cd and k.kelas_cd='KL03') as kelas_3,
	(select count(m.medical_cd) from trx_medical m
								join trx_ruang r on m.ruang_cd=r.ruang_cd
								join trx_kelas k on k.kelas_cd=r.kelas_cd where 
								datetime_in >=p_pdtDateStart
								AND datetime_in <=p_pdtDateEnd
								 and m.medunit_cd=um.medunit_cd and k.kelas_cd='KLKHUSUS') as kelas_khusus
	from 
	trx_unit_medis um;
END;
$function$

select public.SP_RPT_RL_3_1_KEGIATAN_PELAYANAN_RAWAT_INAP('2011-12-30 18:55'::timestamp, '2011-12-10 09:05'::timestamp);