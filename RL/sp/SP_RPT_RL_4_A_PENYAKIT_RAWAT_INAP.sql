DROP FUNCTION public.SP_RPT_RL_4_A_PENYAKIT_RAWAT_INAP (p_pdtDateStart timestamp,p_pdtDateEnd timestamp);
CREATE OR REPLACE FUNCTION public.SP_RPT_RL_4_A_PENYAKIT_RAWAT_INAP (p_pdtDateStart timestamp,p_pdtDateEnd timestamp)
RETURNS table(
	icd_cd Varchar(20),
	icd_nm Varchar(100),
	laki_0To6hari bigint,
	perempuan_0To6hari bigint,
	laki_7To28hari bigint,
	perempuan_7To28hari bigint,
	laki_28hariTo1th bigint,
	perempuan_28hariTo1th bigint,
	laki_1To4th bigint,
	perempuan_1To4th bigint,
	laki_5To14th bigint,
	perempuan_5To14th bigint,
	laki_15To24th bigint,
	perempuan_15To24th bigint,
	laki_25To44th bigint,
	perempuan_25To44th bigint,
	laki_45To64th bigint,
	perempuan_45To64th bigint,
	laki_lebih65th bigint,
	perempuan_lebih65th bigint,
	kosong bigint,
	total_laki bigint,
	total_perempuan bigint,
	total_hidup bigint,
	total_mati bigint,
	total bigint
)
AS $$
BEGIN
	RETURN QUERY SELECT ICD.icd_cd,ICD.icd_nm,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= 0 AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= 6 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age <= 6)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as laki_0To6hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= 0 AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= 6 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age <= 6)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as perempuan_0To6hari,	
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= 7 AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 7 AND Pas2.age <= 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as laki_7To28hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= 7 AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 7 AND Pas2.age <= 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as perempuan_7To28hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= 28 AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as laki_28hariTo1th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= 28 AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as perempuan_28hariTo1th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= 365 AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age <= (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as laki_1To4th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= 365 AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age <= (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as perempuan_1To4th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= (5*365) AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age <= (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as laki_5To14th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= (5*365) AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age <= (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as perempuan_5To14th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= (15*365) AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age <= (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as laki_15To24th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= (15*365) AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age <= (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as perempuan_15To24th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= (25*365) AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age <= (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as laki_25To44th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= (25*365) AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age <= (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as perempuan_25To44th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= (45*365) AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age <= (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as laki_45To64th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= (45*365) AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age <= (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as perempuan_45To64th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365) )
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as laki_lebih65th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND public.fn_datediff_day(Pas2.birth_date::varchar, now()::varchar) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365) )
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as perempuan_lebih65th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND ICD2.icd_cd = ICD.icd_cd
					AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp
					AND PAS2.birth_date IS NULL AND Pas2.age IS NULL) as kosong,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND Med2.out_tp IS NOT NULL
			AND Pas2.gender_tp='GENDER_TP_01'
			AND ICD2.icd_cd = icd.icd_cd
			AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as total_laki,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2 
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND Med2.out_tp IS NOT NULL
			AND Pas2.gender_tp='GENDER_TP_02'
			AND ICD2.icd_cd = icd.icd_cd
			AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as total_perempuan,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND (Med2.out_tp <> 'OUT_TP_04' AND Med2.out_tp IS NOT NULL)
			AND ICD2.icd_cd = icd.icd_cd
			AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as total_hidup,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND (Med2.out_tp = 'OUT_TP_04' AND Med2.out_tp IS NOT NULL)
			AND ICD2.icd_cd = icd.icd_cd
			AND MR2.datetime_record >=p_dateStart::timestamp AND MR2.datetime_record <= p_dateEnd::timestamp) as total_mati,	
COUNT(MR.medical_record_seqno) as total
FROM trx_medical_record MR
LEFT JOIN trx_medical Med ON Med.medical_cd = MR.medical_cd  
LEFT JOIN trx_pasien Pas ON Pas.pasien_cd = MR.pasien_cd
LEFT JOIN trx_icd ICD ON ICD.icd_cd = MR.icd_cd
WHERE med.medical_tp='MEDICAL_TP_02'
AND Med.out_tp IS NOT NULL
AND ICD.icd_cd IS NOT NULL
AND MR.datetime_record >=p_dateStart::timestamp AND MR.datetime_record <= p_dateEnd::timestamp
GROUP BY ICD.icd_cd, ICD.icd_nm
ORDER BY ICD.icd_cd;
END;
$$ 
LANGUAGE plpgsql;


SELECT * from public.SP_RPT_RL_4_A_PENYAKIT_RAWAT_INAP('2017-01-01', '2017-12-31');