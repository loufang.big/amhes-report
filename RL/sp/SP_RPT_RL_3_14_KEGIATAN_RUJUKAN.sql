DROP FUNCTION public.SP_RPT_RL_3_14_KEGIATAN_RUJUKAN (p_dateStart varchar, p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.SP_RPT_RL_3_14_KEGIATAN_RUJUKAN (p_dateStart varchar, p_dateEnd varchar)
RETURNS table(
	medunit_cd varchar(20),
	medunit_nm varchar(100),
	Diterima_Dari_Puskesmas bigint,
	Diterima_Dari_FasKes_Lain bigint,
	Diterima_Dari_RS_Lain bigint,
	Dikembalikan_Ke_Puskesmas bigint,
	Dikembalikan_Ke_FasKes_Lain bigint,
	Dikembalikan_Ke_RS_Lain bigint,
	Dirujuk bigint
)
AS $$
BEGIN
	RETURN Query SELECT 
	U.medunit_cd, 
	U.medunit_nm,
		(SELECT COUNT(X.medical_cd) 
			FROM public.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND X.reff_tp = 'REFF_TP_05' 
			AND X.out_tp='OUT_TP_07' 
			AND X.datetime_in >= p_dateStart::timestamp
			AND X.datetime_in <= p_dateEnd::timestamp
			) as Diterima_Dari_Puskesmas,
		(SELECT COUNT(X.medical_cd) 
			FROM public.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND (X.reff_tp = 'REFF_TP_01' 
			OR X.reff_tp = 'REFF_TP_02' 
			OR X.reff_tp = 'REFF_TP_07') 
			AND X.out_tp='OUT_TP_06' 
			AND X.datetime_in >= p_dateStart::timestamp
			AND X.datetime_in <= p_dateEnd::timestamp
			) as Diterima_Dari_FasKes_Lain,
		(SELECT COUNT(X.medical_cd) 
			FROM public.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND X.reff_tp = 'REFF_TP_07' 
			AND X.out_tp='OUT_TP_07' 
			AND X.datetime_in >= p_dateStart::timestamp
			AND X.datetime_in <= p_dateEnd::timestamp
			) as Diterima_Dari_RS_Lain,
		(SELECT COUNT(X.medical_cd) 
			FROM public.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND X.reff_tp = 'REFF_TP_05' 
			AND X.out_tp='OUT_TP_06' 
			AND X.datetime_in >= p_dateStart::timestamp
			AND X.datetime_in <= p_dateEnd::timestamp
			) as Dikembalikan_Ke_Puskesmas,
		(SELECT COUNT(X.medical_cd) 
			FROM public.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND (X.reff_tp = 'REFF_TP_01' 
			OR X.reff_tp = 'REFF_TP_02' 
			OR X.reff_tp = 'REFF_TP_06' 
			AND X.datetime_in >= p_dateStart::timestamp
			AND X.datetime_in <= p_dateEnd::timestamp
			AND X.out_tp='OUT_TP_06')) as Dikembalikan_Ke_FasKes_Lain,
		(SELECT COUNT(X.medical_cd) 
			FROM public.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND X.reff_tp = 'REFF_TP_07' 
			AND X.out_tp='OUT_TP_06' 
			AND X.datetime_in >= p_dateStart::timestamp
			AND X.datetime_in <= p_dateEnd::timestamp
			) as Dikembalikan_Ke_RS_Lain,
		(SELECT COUNT(X.medical_cd) 
			FROM public.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND X.out_tp ='OUT_TP_02' 
			AND X.datetime_in >= p_dateStart::timestamp
			AND X.datetime_in <= p_dateEnd::timestamp
			) as Dirujuk
	FROM public.trx_unit_medis U;  
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_RL_3_14_KEGIATAN_RUJUKAN('2017-01-01', '2017-12-31');