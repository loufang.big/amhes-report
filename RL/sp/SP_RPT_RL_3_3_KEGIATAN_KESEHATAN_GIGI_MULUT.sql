DROP FUNCTION public.SP_RPT_RL_3_3_KEGIATAN_KESEHATAN_GIGI_MULUT (p_pdtDateStart timestamp,p_pdtDateEnd timestamp); 
CREATE OR REPLACE FUNCTION public.SP_RPT_RL_3_3_KEGIATAN_KESEHATAN_GIGI_MULUT (p_pdtDateStart timestamp,p_pdtDateEnd timestamp) 
RETURNS table(
	Jenis_Kegiatan text,
	Jumlah bigint
	)
AS 
$$
BEGIN
	RETURN QUERY SELECT 
	RTRIM(LTRIM(C.treatment_nm)) AS Jenis_Kegiatan,
	COUNT(1) AS Jumlah
	FROM trx_medical_tindakan A
	JOIN trx_medical B ON A.medical_cd = B.medical_cd
	JOIN trx_tindakan C ON A.treatment_cd = C.treatment_cd
	WHERE
		B.medunit_cd = 'POLIGIGI'
	AND B.datetime_in>=  p_pdtDateStart
	AND B.datetime_in <=  p_pdtDateEnd
	GROUP BY
		C.treatment_nm;
END;
$$ 
LANGUAGE plpgsql;

SELECT * from public.SP_RPT_RL_3_3_KEGIATAN_KESEHATAN_GIGI_MULUT('2011-12-30 18:55'::timestamp, '2011-12-10 09:05'::timestamp);