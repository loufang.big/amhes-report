DROP FUNCTION public.SP_RPT_RL_3_2_KUNJUNGAN_RAWAT_DARURAT(p_dateStart varchar, p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.SP_RPT_RL_3_2_KUNJUNGAN_RAWAT_DARURAT(p_dateStart varchar, p_dateEnd varchar)
RETURNS table(
	medunit_cd Varchar(20),
	medunit_nm Varchar(100),
	Total_Pasien_Rujukan bigint,
	Total_Pasien_Non_Rujukan bigint,
	Dirawat bigint,
	Dirujuk bigint,
	Pulang bigint,
	Mati_Di_IGD bigint,
	DOA bigint
)
AS $$
BEGIN
	RETURN QUERY SELECT 
	B.medunit_cd, 
	B.medunit_nm,
		(SELECT COUNT(medical_cd) 
			FROM trx_medical Y 
			WHERE Y.datetime_in >= p_dateStart::timestamp AND Y.datetime_in <= p_dateEnd::timestamp
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.reff_tp IS NOT NULL 
			AND Y.referensi_cd IS NOT NULL) as Total_Pasien_Rujukan,
		(SELECT COUNT(medical_cd) 
			FROM trx_medical Y 
			WHERE Y.datetime_in >= p_dateStart::timestamp AND Y.datetime_in <= p_dateEnd::timestamp
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.reff_tp IS NULL 
			AND Y.referensi_cd IS NULL) as Total_Pasien_Non_Rujukan,	
		(SELECT COUNT(medical_cd) 
			FROM trx_medical Y 
			WHERE Y.datetime_in >= p_dateStart::timestamp AND Y.datetime_in <= p_dateEnd::timestamp
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.out_tp ='OUT_TP_01') as Dirawat,
		(SELECT COUNT(medical_cd) 
			FROM trx_medical Y 
			WHERE Y.datetime_in >= p_dateStart::timestamp AND Y.datetime_in <= p_dateEnd::timestamp 
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.out_tp ='OUT_TP_02') as Dirujuk,
		(SELECT COUNT(medical_cd) 
			FROM trx_medical Y 
			WHERE Y.datetime_in >= p_dateStart::timestamp AND Y.datetime_in <= p_dateEnd::timestamp
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.out_tp ='OUT_TP_03') as Pulang,
		(SELECT COUNT(medical_cd) 
			FROM trx_medical Y 
			WHERE Y.datetime_in >= p_dateStart::timestamp AND Y.datetime_in <= p_dateEnd::timestamp
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.out_tp ='OUT_TP_04') as Mati_Di_IGD,
		(SELECT COUNT(medical_cd) 
			FROM trx_medical Y 
			WHERE Y.datetime_in >= p_dateStart::timestamp AND Y.datetime_in <= p_dateEnd::timestamp
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.out_tp ='OUT_TP_05') as DOA
	FROM trx_unit_medis B 
	WHERE B.medunit_cd = 'POLISPB'
	OR B.medunit_cd = 'POLISPOG'
	OR B.medunit_cd = 'POLISPA'
	OR B.medunit_cd = 'POLISPJW'
	OR B.medunit_cd = 'POLIUGD';
END;
$$ 
LANGUAGE plpgsql;

SELECT * from public.SP_RPT_RL_3_2_KUNJUNGAN_RAWAT_DARURAT('2017-01-01', '2017-12-31');