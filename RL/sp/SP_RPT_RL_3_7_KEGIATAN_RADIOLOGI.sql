DROP FUNCTION public.SP_RPT_RL_3_7_KEGIATAN_RADIOLOGI (p_dateStart varchar,p_dateEnd varchar) ;
CREATE OR REPLACE FUNCTION public.SP_RPT_RL_3_7_KEGIATAN_RADIOLOGI (p_dateStart varchar,p_dateEnd varchar) 
RETURNS table(
	Jenis_Kegiatan varchar(100),
	Jumlah bigint
	)
AS $$
BEGIN
	RETURN QUERY SELECT
		medicalunit_nm AS Jenis_Kegiatan,
		COUNT(1)AS Jumlah
	FROM
		trx_medical_unit mast
	JOIN trx_unitmedis_item a ON a.medicalunit_cd = mast.medicalunit_cd
	AND a.medunit_cd = 'RADIO00'
	AND mast.datetime_trx >=  p_dateStart::timestamp
	AND mast.datetime_trx <=  p_dateEnd::timestamp
	GROUP BY
		medicalunit_nm;
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_RL_3_7_KEGIATAN_RADIOLOGI('2017-01-01', '2017-12-31');