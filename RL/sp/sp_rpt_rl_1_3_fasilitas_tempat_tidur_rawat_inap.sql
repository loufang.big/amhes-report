DROP FUNCTION public.sp_rpt_rl_1_3_fasilitas_tempat_tidur_rawat_inap(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
CREATE OR REPLACE FUNCTION public.sp_rpt_rl_1_3_fasilitas_tempat_tidur_rawat_inap(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone)
 RETURNS TABLE(bangsal_cd character varying, bangsal_nm character varying, jumlah_tt bigint, vvip bigint, vip bigint, kelasi bigint, kelasii bigint, kelasiii bigint, kelaslain bigint)
 LANGUAGE plpgsql
AS $function$
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	RETURN QUERY SELECT
		trx_ruang.bangsal_cd,
		trx_bangsal.bangsal_nm,
		COUNT(1) AS jumlah_tt,
		SUM(CASE WHEN trx_ruang.kelas_cd = 'KLVVIP' THEN 1 ELSE 0 END)AS vvip,
		SUM(CASE WHEN trx_ruang.kelas_cd = 'KLVIP' THEN 1 ELSE 0 END)AS vip,
		SUM(CASE WHEN trx_ruang.kelas_cd = 'KL01' THEN 1 ELSE 0 END)AS kelasI,
		SUM(CASE WHEN trx_ruang.kelas_cd = 'KL02' THEN 1 ELSE 0 END)AS kelasII,
		SUM(CASE WHEN trx_ruang.kelas_cd = 'KL03' THEN 1 ELSE 0 END)AS kelasIII,
		SUM(CASE WHEN trx_ruang.kelas_cd NOT IN ('KLVVIP','KLVIP','KL01','KL02','KL03') THEN 1 ELSE 0 END) AS kelasLAIN
	FROM trx_ruang
	JOIN trx_bangsal ON trx_ruang.bangsal_cd = trx_bangsal.bangsal_cd
	WHERE trx_ruang.modi_datetime >= p_pdtDateStart AND 
	trx_ruang.modi_datetime <= p_pdtDateEnd
	GROUP BY
		trx_ruang.bangsal_cd,
		trx_bangsal.bangsal_nm;
	
END;
$function$
