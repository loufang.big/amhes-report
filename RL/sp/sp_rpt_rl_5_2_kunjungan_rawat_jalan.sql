DROP FUNCTION public.sp_rpt_rl_5_2_kunjungan_rawat_jalan(p_dateStart varchar, p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.sp_rpt_rl_5_2_kunjungan_rawat_jalan(p_dateStart varchar, p_dateEnd varchar)
 RETURNS TABLE(jenis_kegiatan character varying, jumlah bigint)
AS 
$function$
BEGIN
	return query SELECT RJ.medunit_nm,COUNT(A.medical_cd) AS total
	FROM trx_medical A 
	JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	WHERE 
	RJ.medicalunit_tp = 'MEDICALUNIT_TP_1'
	AND A.datetime_in >= p_dateStart::timestamp
	AND A.datetime_in <= p_dateEnd::timestamp
	GROUP BY RJ.medunit_nm
	ORDER BY RJ.medunit_nm;
END;
$function$
LANGUAGE plpgsql;

SELECT * from public.sp_rpt_rl_5_2_kunjungan_rawat_jalan('2017-01-01', '2017-12-31');