DROP FUNCTION public.sp_rpt_rl_3_10_kegiatan_pelayanan_khusus(p_dateStart varchar, p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.sp_rpt_rl_3_10_kegiatan_pelayanan_khusus(p_dateStart varchar, p_dateEnd varchar)
 RETURNS TABLE(jenis_kegiatan text, jumlah bigint)
AS $function$
BEGIN
	return Query SELECT 
	RTRIM(LTRIM(A.medical_note)) as Jenis_Kegiatan,
	COUNT(A.medical_note) AS Jumlah
	FROM trx_medical_tindakan A 
	JOIN trx_medical B ON A.medical_cd = B.medical_cd
	WHERE  B.datetime_in >= p_dateStart::timestamp
	AND B.datetime_in <= p_dateEnd::timestamp
	OR A.medical_note LIKE '%elektro%'
	OR A.medical_note LIKE '%kardiographi%'
	OR A.medical_note LIKE '%myographi%' 
	OR A.medical_note LIKE '%echo%'
	OR A.medical_note LIKE '%cardiographi%'
	OR A.medical_note LIKE '%endoskopi%'
	OR A.medical_note LIKE '%hemodialisa%'
	OR A.medical_note LIKE '%densometri%'
	OR A.medical_note LIKE '%tulang%'
	OR A.medical_note LIKE '%densometri tulang%'
	OR A.medical_note LIKE '%pungsi%'
	OR A.medical_note LIKE '%spirometri%'
	OR A.medical_note LIKE '%kulit%'
	OR A.medical_note LIKE '%alergi%'
	OR A.medical_note LIKE '%histamin%'
	OR A.medical_note LIKE '%topometri%'
	OR A.medical_note LIKE '%akupuntur%'
	OR A.medical_note LIKE '%hiperbarik%'
	OR A.medical_note LIKE '%herbal%'
	OR A.medical_note LIKE '%jamu%'
	GROUP BY A.medical_note;
END;
$function$
LANGUAGE plpgsql;

SELECT * from public.sp_rpt_rl_3_10_kegiatan_pelayanan_khusus('2017-01-01', '2017-12-31');