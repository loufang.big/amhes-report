DROP FUNCTION public.SP_RPT_RL_3_4_KEGIATAN_KEBIDANAN (p_dateStart varchar, p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.SP_RPT_RL_3_4_KEGIATAN_KEBIDANAN (p_dateStart varchar, p_dateEnd varchar) 
RETURNS table(
	medical_note Varchar(100),
	R_Med_Rumah_Sakit bigint,
	R_Med_Bidan bigint,
	R_Med_Puskesmas bigint,
	R_Faskes_Lain bigint,
	R_Med_Jumlah_Hidup bigint,
	R_Med_Jumlah_Mati bigint,
	R_Med_Jumlah_Total bigint,
	R_NonMed_Jumlah_Hidup bigint,
	R_NonMed_Jumlah_Mati bigint,
	R_NonMed_Jumlah_Total bigint,
	Non_R_Jumlah_Hidup bigint,
	Non_R_Jumlah_Mati bigint,
	Non_R_Jumlah_Total bigint,
	Dirujuk bigint
)
AS $$
BEGIN
	RETURN QUERY SELECT  A.medical_note,
	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note AND Y.reff_tp = 'REFF_TP_07') as R_Med_Rumah_Sakit,
	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note AND Y.reff_tp = 'REFF_TP_03') as R_Med_Bidan,
	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note AND Y.reff_tp = 'REFF_TP_05') as R_Med_Puskesmas,
	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note AND (Y.reff_tp = 'REFF_TP_01' OR Y.reff_tp = 'REFF_TP_02' OR Y.reff_tp = 'REFF_TP_06') ) as R_Faskes_Lain,
	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note AND Y.out_tp ='OUT_TP_01'
	AND (Y.reff_tp = 'REFF_TP_01' OR Y.reff_tp = 'REFF_TP_02' OR Y.reff_tp = 'REFF_TP_03' OR Y.reff_tp = 'REFF_TP_05' OR Y.reff_tp = 'REFF_TP_06' OR Y.reff_tp = 'REFF_TP_07') ) as R_Med_Jumlah_Hidup,
	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note AND Y.out_tp ='OUT_TP_04'
	AND (Y.reff_tp = 'REFF_TP_01' OR Y.reff_tp = 'REFF_TP_02' OR Y.reff_tp = 'REFF_TP_03' OR Y.reff_tp = 'REFF_TP_05' OR Y.reff_tp = 'REFF_TP_06' OR Y.reff_tp = 'REFF_TP_07') ) as R_Med_Jumlah_Mati,
	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note 
	AND (Y.reff_tp = 'REFF_TP_01' OR Y.reff_tp = 'REFF_TP_02' OR Y.reff_tp = 'REFF_TP_03' OR Y.reff_tp = 'REFF_TP_05' OR Y.reff_tp = 'REFF_TP_06' OR Y.reff_tp = 'REFF_TP_07') ) as R_Med_Jumlah_Total,
	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note AND Y.out_tp ='OUT_TP_01' AND (Y.reff_tp = 'REFF_TP_00' OR Y.reff_tp = 'REFF_TP_04') ) as R_NonMed_Jumlah_Hidup,
	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note AND Y.out_tp ='OUT_TP_04' AND (Y.reff_tp = 'REFF_TP_00' OR Y.reff_tp = 'REFF_TP_04') ) as R_NonMed_Jumlah_Mati,
	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note AND (Y.reff_tp = 'REFF_TP_00' OR Y.reff_tp = 'REFF_TP_04') ) as R_NonMed_Jumlah_Total,
	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note AND Y.out_tp ='OUT_TP_01' AND Y.reff_tp IS NULL AND Y.referensi_cd IS NULL) as Non_R_Jumlah_Hidup,
	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note AND Y.out_tp ='OUT_TP_04' AND Y.reff_tp IS NULL AND Y.referensi_cd IS NULL) as Non_R_Jumlah_Mati,
	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note AND Y.reff_tp IS NULL AND Y.referensi_cd IS NULL) as Non_R_Jumlah_Total,

	(SELECT COUNT(x.medical_note) FROM trx_medical_tindakan X JOIN trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE x.medical_note = A.medical_note AND Y.out_tp ='OUT_TP_02') as Dirujuk
	FROM trx_medical_tindakan  A JOIN trx_medical B ON A.medical_cd =B.medical_cd 
	WHERE B.medunit_cd = 'POLISPOG' 
	AND B.datetime_in >= p_dateStart::timestamp AND B.datetime_in <= p_dateEnd::timestamp 
	GROUP BY A.medical_note;
END;
$$ 
LANGUAGE plpgsql;
SELECT * from public.SP_RPT_RL_3_4_KEGIATAN_KEBIDANAN ('2017-01-01', '2017-12-31');