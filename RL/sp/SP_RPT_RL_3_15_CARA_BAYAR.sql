DROP FUNCTION public.SP_RPT_RL_3_15_CARA_BAYAR (p_dateStart varchar, p_dateEnd varchar));
CREATE OR REPLACE FUNCTION public.SP_RPT_RL_3_15_CARA_BAYAR (p_dateStart varchar, p_dateEnd varchar))
RETURNS table(
	code_nm varchar(100),
	RI_PasienKeluar bigint,
	RI_LamDirawat bigint,
	RawatInap bigint,
	RawatJalan bigint,
	RJ_Laboratorium bigint,
	RJ_Radiologi bigint,
	RJ_Lain bigint
)
AS $$
BEGIN
	RETURN QUERY SELECT COM.code_nm,
		(SELECT COUNT(med.pasien_cd) 
			FROM public.trx_medical med 
			JOIN public.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_02' 
			AND med.out_tp IS NOT NULL 
			AND MED.datetime_in >=  p_dateStart::timestamp
			AND MED.datetime_in <= p_dateEnd::timestamp
			) as RI_PasienKeluar,
		(SELECT COUNT(med.pasien_cd) 
			FROM public.trx_medical med 
			JOIN public.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_02' 
			AND med.out_tp IS NULL 
			AND MED.datetime_in >=  p_dateStart::timestamp
			AND MED.datetime_in <= p_dateEnd::timestamp
			) as RI_LamDirawat,
		(SELECT COUNT(med.pasien_cd) 
			FROM public.trx_medical med 
			JOIN public.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_02' 
			AND MED.datetime_in >=  p_dateStart::timestamp
			AND MED.datetime_in <= p_dateEnd::timestamp
			) as RawatInap,
		(SELECT COUNT(med.pasien_cd) 
			FROM public.trx_medical med 
			JOIN public.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_01' 
			AND MED.datetime_in >=  p_dateStart::timestamp
			AND MED.datetime_in <= p_dateEnd::timestamp
			) as RawatJalan,
		(SELECT COUNT(med.pasien_cd) 
			FROM public.trx_medical med 
			JOIN public.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			JOIN public.trx_unit_medis um ON med.medunit_cd=um.medunit_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_01' 
			AND um.medicalunit_tp='MEDICALUNIT_TP_2' 
			AND MED.datetime_in >=  p_dateStart::timestamp
			AND MED.datetime_in <= p_dateEnd::timestamp
			) as RJ_Laboratorium,
		(SELECT COUNT(med.pasien_cd) 
			FROM public.trx_medical med 
			JOIN public.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			JOIN public.trx_unit_medis um ON med.medunit_cd=um.medunit_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_01' 
			AND um.medicalunit_tp='MEDICALUNIT_TP_3' 
			AND MED.datetime_in >=  p_dateStart::timestamp
			AND MED.datetime_in <= p_dateEnd::timestamp
			) as RJ_Radiologi,
		(SELECT COUNT(med.pasien_cd) 
			FROM public.trx_medical med 
			JOIN public.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			JOIN public.trx_unit_medis um ON med.medunit_cd=um.medunit_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_01' 
			AND um.medicalunit_tp='MEDICALUNIT_TP_1' 
			AND MED.datetime_in >=  p_dateStart::timestamp
			AND MED.datetime_in <= p_dateEnd::timestamp
			) as RJ_Lain 
	FROM public.com_code COM
	WHERE COM.code_group='PAYMENT_TP';
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_RL_3_15_CARA_BAYAR('2017-01-01', '2017-12-31');