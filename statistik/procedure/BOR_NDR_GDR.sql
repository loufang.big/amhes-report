DROP FUNCTION public.FN_RPT_TOTAL_PASIENKELAS (p_kelasCd Varchar(20),p_dateStart varchar,p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.FN_RPT_TOTAL_PASIENKELAS (p_kelasCd Varchar(20),p_dateStart varchar,p_dateEnd varchar)
RETURNS Bigint
AS
$$
   	DECLARE v_intResult Int;
BEGIN
	SELECT 
	COUNT(A.medical_cd) into v_intResult
	FROM trx_medical A, trx_ruang B
	WHERE A.ruang_cd=B.ruang_cd
	AND B.kelas_cd=p_kelasCd
	AND (to_char(A.datetime_in, 'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd 
	or to_char(A.datetime_out, 'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd);

	RETURN v_intResult;
END;
$$ 
LANGUAGE plpgsql;

DROP FUNCTION public.FN_RPT_TOTAL_HARIRAWAT (p_kelasCd Varchar(10),p_dateStart varchar,p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.FN_RPT_TOTAL_HARIRAWAT (p_kelasCd Varchar(10),p_dateStart varchar,p_dateEnd varchar)
RETURNS Bigint
AS
$$
   	DECLARE v_intResult Int;
BEGIN	
		SELECT SUM(fn_datediff_day(to_char(A.datetime_in, 'yyyy-mm-dd'),to_char(A.datetime_out, 'yyyy-mm-dd')) + 1) into v_intResult
		FROM trx_medical A, trx_ruang B
		WHERE A.ruang_cd=B.ruang_cd
		AND B.kelas_cd=p_kelasCd
		AND (A.datetime_in BETWEEN p_dateStart::timestamp AND p_dateEnd::timestamp  OR
		A.datetime_out BETWEEN p_dateStart::timestamp  AND p_dateEnd::timestamp);
		        		  				  
	RETURN v_intResult;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION public.FN_INTERVAL_TO_INTEGER(p_interval interval);
 CREATE OR REPLACE FUNCTION public.FN_INTERVAL_TO_INTEGER(p_interval interval)
 RETURNS Int
 as
 $$
 DECLARE v_intResult Int;
 BEGIN
 	SELECT coalesce(extract(epoch from p_interval), 0) into v_intResult;
 	Return v_intResult;
 END;
 $$ 
 LANGUAGE plpgsql;

DROP FUNCTION public.fn_last_day_month (p_date Varchar);
CREATE OR REPLACE FUNCTION public.fn_last_day_month (p_date Varchar)
RETURNS varchar
AS
$$
   	DECLARE v_intResult varchar;
BEGIN	
	SELECT (date_trunc('MONTH', p_date::timestamp) + INTERVAL '1 MONTH - 1 day')::DATE;    		  				  
	RETURN v_intResult::varchar;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION public.FN_RPT_TOTAL_DEATH_OUTTP11 (p_KelasCd Varchar(10),p_DateStart Timestamp(3),p_DateEnd Timestamp(3));
CREATE OR REPLACE FUNCTION public.FN_RPT_TOTAL_DEATH_OUTTP11 (p_KelasCd Varchar(10),p_DateStart Timestamp(3),p_DateEnd Timestamp(3))
RETURNS Int
AS
$$
	DECLARE v_intResult Int;
	-- SELECT FN_INTERVAL_TO_INTEGER(SUM(a.datetime_out::timestamp - a.datetime_in::timestamp)) + 1 into v_intResult
BEGIN
	SELECT FN_INTERVAL_TO_INTEGER(SUM(a.datetime_out::timestamp - a.datetime_in::timestamp)) into v_intResult
	FROM trx_medical A, trx_ruang B
	WHERE A.ruang_cd=B.ruang_cd
	AND A.out_tp='OUT_TP_11'
	AND B.kelas_cd=p_KelasCd
	AND (A.datetime_in BETWEEN p_dateStart::timestamp AND p_dateEnd::timestamp  OR
		A.datetime_out BETWEEN p_dateStart::timestamp  AND p_dateEnd::timestamp);   

	RETURN v_intResult;
END;
$$ 
LANGUAGE plpgsql;


DROP FUNCTION public.FN_RPT_TOTAL_DEATH (p_KelasCd Varchar,p_DateStart varchar,p_DateEnd varchar);
CREATE OR REPLACE FUNCTION public.FN_RPT_TOTAL_DEATH (p_KelasCd Varchar,p_DateStart varchar,p_DateEnd varchar)
RETURNS Int
AS
$$
	DECLARE v_intResult Int;
BEGIN
	SELECT 
	SUM(fn_datediff_day(A.datetime_in::varchar,A.datetime_out::varchar) + 1) into v_intResult
	FROM trx_medical A, trx_ruang B
	WHERE A.ruang_cd=B.ruang_cd
	AND A.out_tp IN ('OUT_TP_11','OUT_TP_12')
	AND B.kelas_cd=p_KelasCd
	AND (to_char(A.datetime_in,'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd OR
		to_char(A.datetime_out,'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd);
	        		  				  
	RETURN v_intResult;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION public.SP_RPT_GET_BOR(p_Month int,p_Year int,p_Bangsal varchar);
CREATE OR REPLACE FUNCTION public.SP_RPT_GET_BOR(p_Month int,p_Year int,p_Bangsal varchar) 
RETURNS table(
	bangsal_cd varchar,
	bangsal_nm varchar,
	kelas_nm varchar,
	total_kamar Bigint,
	total_pasien Bigint,
	total_harirawat Bigint,
	day_month integer
)
AS $$
	DECLARE v_dtDateStart varchar;
 	v_dtDateEnd varchar;
BEGIN
	v_dtDateStart := concat(p_Year::Varchar, '-', LPAD(p_Month::varchar,2,'0')::Varchar, '-', '01')::varchar;
	v_dtDateEnd := (date_trunc('MONTH', v_dtDateStart::timestamp) + INTERVAL '1 MONTH - 1 day')::DATE;

	-- execute fn_last_day_month(v_dtDateStart) into v_dtDateEnd;
	
	RETURN QUERY SELECT 
	bangsal.bangsal_cd,
	bangsal.bangsal_nm,
	kelas.kelas_nm,
	(select count(*) from trx_ruang where trx_ruang.bangsal_cd=bangsal.bangsal_cd and kelas_cd=kelas.kelas_cd) as total_kamar,
	COALESCE(public.FN_RPT_TOTAL_PASIENBANGSAL(bangsal.bangsal_cd,kelas.kelas_cd,v_dtDateStart::varchar,v_dtDateEnd::varchar),0) AS total_pasien,
	COALESCE(public.FN_RPT_TOTAL_HARIRAWAT(ruang.kelas_cd,v_dtDateStart,v_dtDateEnd),0) AS total_harirawat,
	to_char(v_dtDateEnd::timestamp, 'dd')::integer AS day_month
	FROM trx_bangsal bangsal
	left join trx_ruang ruang ON bangsal.bangsal_cd=ruang.ruang_cd AND COALESCE(ruang.ruang_tp,'')<>'1'
	join trx_kelas kelas on 1=1
	where bangsal.bangsal_cd = p_Bangsal
	GROUP BY bangsal.bangsal_cd , ruang.kelas_cd ,kelas.kelas_nm,kelas.kelas_cd
	ORDER BY bangsal.bangsal_nm, kelas.kelas_nm;
END;
$$ LANGUAGE plpgsql;

select * from public.SP_RPT_GET_BOR(2, 2017, 'NICU');

DROP FUNCTION public.SP_RPT_GET_NDR (p_Month int,p_Year int,p_Bangsal varchar);
CREATE OR REPLACE FUNCTION public.SP_RPT_GET_NDR (p_Month int,p_Year int,p_Bangsal varchar) 
RETURNS table(
	bangsal_cd varchar,
	bangsal_nm varchar,
	kelas_nm varchar,
	total_kamar Bigint,
	total_pasien Bigint,
	total_meninggal int,
	day_month integer
)
AS $$
	DECLARE v_dtDateStart Timestamp(3);
	v_dtDateEnd Timestamp(3);
BEGIN
	v_dtDateStart := concat(p_Year::Varchar, '-', LPAD(p_Month::varchar,2,'0')::Varchar, '-', '01')::varchar;
	v_dtDateEnd := (date_trunc('MONTH', v_dtDateStart::timestamp) + INTERVAL '1 MONTH - 1 day')::DATE;
	-- execute fn_last_day_month(v_dtDateStart) into v_dtDateEnd;
	RETURN QUERY SELECT 
	bangsal.bangsal_cd,
	bangsal.bangsal_nm,
	kelas.kelas_nm,
	(select count(*) from trx_ruang where trx_ruang.bangsal_cd=bangsal.bangsal_cd and trx_ruang.kelas_cd=kelas.kelas_cd) as total_ruang,
	COALESCE(public.FN_RPT_TOTAL_PASIENBANGSAL(bangsal.bangsal_cd,kelas.kelas_cd,v_dtDateStart::varchar,v_dtDateEnd::varchar),0) AS total_pasien,
	COALESCE(public.FN_RPT_TOTAL_DEATH_OUTTP11(ruang.kelas_cd,v_dtDateStart::varchar,v_dtDateEnd::varchar),0) AS total_meninggal,
	to_char(v_dtDateEnd::timestamp, 'dd')::integer AS day_month
	FROM trx_bangsal bangsal
	left join trx_ruang ruang ON bangsal.bangsal_cd=ruang.ruang_cd AND COALESCE(ruang.ruang_tp,'')<>'1'
	join trx_kelas kelas on 1=1
	where bangsal.bangsal_cd = p_Bangsal
	GROUP BY bangsal.bangsal_cd , ruang.kelas_cd ,kelas.kelas_nm,kelas.kelas_cd
	ORDER BY bangsal.bangsal_nm, kelas.kelas_nm;
END;
$$ LANGUAGE plpgsql;

select * from public.SP_RPT_GET_NDR(2, 2017,'NICU');

DROP FUNCTION public.SP_RPT_GET_GDR (p_Month int, p_Year int,p_Bangsal varchar);
CREATE OR REPLACE FUNCTION public.SP_RPT_GET_GDR (p_Month int, p_Year int,p_Bangsal varchar) 
RETURNS table(
	bangsal_cd varchar,
	bangsal_nm varchar,
	kelas_nm varchar,
	total_kamar Bigint,
	total_pasien Bigint,
	total_meninggal int,
	day_month integer
)
AS $$
	DECLARE v_dtDateStart Timestamp(3);
	v_dtDateEnd Timestamp(3);
BEGIN
	v_dtDateStart := concat(p_Year::Varchar, '-', LPAD(p_Month::varchar,2,'0')::Varchar, '-', '01')::Timestamp;
	v_dtDateEnd := (date_trunc('MONTH', v_dtDateStart::timestamp) + INTERVAL '1 MONTH - 1 day')::DATE;
	
	RETURN QUERY SELECT 
	bangsal.bangsal_cd,
	bangsal.bangsal_nm,
	kelas.kelas_nm,
	(select count(*) from trx_ruang where trx_ruang.bangsal_cd=bangsal.bangsal_cd and trx_ruang.kelas_cd=kelas.kelas_cd) as total_ruang,
	COALESCE(public.FN_RPT_TOTAL_PASIENBANGSAL(bangsal.bangsal_cd,kelas.kelas_cd,v_dtDateStart::varchar,v_dtDateEnd::varchar),0) AS total_pasien,
	COALESCE(public.FN_RPT_TOTAL_DEATH(ruang.kelas_cd,v_dtDateStart::varchar,v_dtDateEnd::varchar),0) AS total_meninggal,
	to_char(v_dtDateEnd::timestamp, 'dd')::integer AS day_month
	FROM trx_bangsal bangsal
	left join trx_ruang ruang ON bangsal.bangsal_cd=ruang.ruang_cd AND COALESCE(ruang.ruang_tp,'')<>'1'
	join trx_kelas kelas on 1=1
	where bangsal.bangsal_cd = p_Bangsal
	GROUP BY bangsal.bangsal_cd , ruang.kelas_cd ,kelas.kelas_nm,kelas.kelas_cd
	ORDER BY bangsal.bangsal_nm, kelas.kelas_nm;
END;
$$ 
LANGUAGE plpgsql;

select * from public.SP_RPT_GET_GDR(2, 2017,'NICU');

