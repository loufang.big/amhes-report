DROP FUNCTION public.SP_RPT_GET_STATISTIK_PENYAKIT (p_Month int,p_Year int); 
CREATE OR REPLACE FUNCTION public.SP_RPT_GET_STATISTIK_PENYAKIT (p_Month int,p_Year int) 
RETURNS table(
icd_cd varchar,
icd_nm varchar,
total bigint
)
AS $$
BEGIN
		RETURN QUERY SELECT 
		ICD.icd_cd,
		ICD.icd_nm,
		COUNT(A.icd_cd) AS total
		FROM trx_medical_record A 
		JOIN trx_icd ICD ON A.icd_cd=ICD.icd_cd
		WHERE to_char(A.datetime_record, 'mm') = LPAD(p_Month::varchar,2,'0') AND to_char(A.datetime_record, 'yyyy')=p_Year::varchar

		GROUP BY ICD.icd_cd,ICD.icd_nm
		ORDER BY total DESC limit 10;
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_GET_STATISTIK_PENYAKIT(2, 2017);