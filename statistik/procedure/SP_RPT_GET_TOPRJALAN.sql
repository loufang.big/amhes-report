DROP FUNCTION public.SP_RPT_GET_TOPRJALAN(p_Month int,p_Year int); 
CREATE OR REPLACE FUNCTION public.SP_RPT_GET_TOPRJALAN(p_Month int,p_Year int) 
RETURNS table(
	kelas_nm varchar,
	total bigint
)
AS $$
BEGIN
	return query SELECT 
	RJ.medunit_nm,
	COUNT(A.medical_cd) AS total
	FROM trx_medical A 
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	WHERE A.medical_tp='MEDICAL_TP_01'
	AND to_char(A.datetime_in, 'mm') = LPAD(p_Month::varchar,2,'0') AND to_char(A.datetime_in, 'yyyy')=p_Year::varchar
	GROUP BY RJ.medunit_nm
	ORDER BY RJ.medunit_nm;
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_GET_TOPRJALAN(1, 2017);