DROP FUNCTION public.SP_RPT_GET_TOPRINAP (p_Month int,p_Year int); 
CREATE OR REPLACE FUNCTION public.SP_RPT_GET_TOPRINAP (p_Month int,p_Year int) 
RETURNS table(
	kelas_nm varchar,
	total bigint
)
AS $$
BEGIN
	Return query 
	SELECT 
	coalesce(KLS.kelas_nm,'-') as kelas_nm,
	COUNT(A.medical_cd) AS total
	FROM trx_medical A 
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd AND COALESCE(KMR.ruang_tp,'')<>'1'
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	WHERE A.medical_tp='MEDICAL_TP_02'
	AND to_char(A.datetime_in, 'mm') = LPAD(p_Month::varchar,2,'0') AND to_char(A.datetime_in, 'yyyy')=p_Year::varchar
	GROUP BY KLS.kelas_nm
	ORDER BY KLS.kelas_nm;
END;
$$ 
LANGUAGE plpgsql;

SELECT * from public.SP_RPT_GET_TOPRINAP(2, 2017);