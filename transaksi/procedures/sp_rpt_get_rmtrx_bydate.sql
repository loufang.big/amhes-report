DROP FUNCTION public.sp_rpt_get_rmtrx_bydate(p_pdtdatestart text, p_pdtdateend text);
CREATE OR REPLACE FUNCTION public.sp_rpt_get_rmtrx_bydate(p_pdtdatestart text, p_pdtdateend text)
 RETURNS TABLE(
  medical_tp character varying, 
  medical_tp_nm character varying, 
  trx_datetime timestamp without time zone, 
  pasien_nm character varying, 
  no_rm character varying, 
  insurance_nm character varying, 
  dr_nm character varying, 
  medunit_nm character varying, 
  ruang_nm character varying, 
  kelas_nm character varying
  )
AS $function$
BEGIN
  RETURN QUERY SELECT A.medical_tp,
  COM.code_nm AS medical_tp_nm,
  A.datetime_in AS trx_datetime,
  PAS.pasien_nm,
  PAS.no_rm,
  INS.insurance_nm,
  DR.dr_nm,
  RJ.medunit_nm,
  KMR.ruang_nm,
  KLS.kelas_nm
  FROM trx_medical A 
  LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
  JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
  LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
  LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
  LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
  LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
  LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
  LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
  WHERE to_char(A.datetime_in, 'yyyy-mm-dd') >= p_pdtDateStart AND to_char(A.datetime_in, 'yyyy-mm-dd') <= p_pdtDateEnd
  ORDER BY  RJ.medunit_nm, A.datetime_in;
END;
$function$
LANGUAGE plpgsql;

SELECT * from public.sp_rpt_get_rmtrx_bydate('2016-01-01', '2016-12-31');


-- Gn95jF5J8W