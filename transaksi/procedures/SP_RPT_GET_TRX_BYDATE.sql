DROP FUNCTION public.SP_RPT_GET_TRX_BYDATE (p_dateStart Varchar(10),p_dateEnd Varchar(10),p_unitCd Varchar(20));
CREATE OR REPLACE FUNCTION public.SP_RPT_GET_TRX_BYDATE (p_dateStart Varchar(10),p_dateEnd Varchar(10),p_unitCd Varchar(20)) 
RETURNS table(

)
AS $$
BEGIN
	IF p_unitCd = '' OR p_unitCd IS NULL THEN 
	/*--Semua--*/
		SELECT 
		A.invoice_no,
		public.fn_formatdate(A.invoice_date::timestamp) AS invoice_date,
		A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,
		INS.insurance_nm,
		A.pay_nm,
		A.entry_nm,
		C.pasien_nm,
		C.no_rm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		DR.dr_nm,
		D.medical_tp,
		COM2.code_nm AS medical_tp_nm,
		D.datetime_in,
		D.datetime_out,
		ACC.account_nm,
		B.amount,
		A.amount_tunai,
		A.amount_nontunai
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE to_char(A.invoice_date, 'yyyy-mm-dd') >= p_dateStart
		AND to_char(A.invoice_date, 'yyyy-mm-dd') <= p_dateEnd
		AND A.payment_st='PAYMENT_ST_1'
		UNION
		SELECT 
		A.invoice_no,
		public.fn_formatdate(A.invoice_date::timestamp) AS invoice_date,
		A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,
		'' AS insurance_nm,
		A.pay_nm,
		A.entry_nm,
		CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
		CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,
		'' AS medunit_nm,
		'' AS ruang_nm,
		'' AS kelas_nm,
		CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
		'PENDAPATAN' AS medical_tp,
		'Pendapatan Lain' AS medical_tp_nm,
		A.invoice_date AS datetime_in,
		A.invoice_date AS datetime_out,
		ACC.account_nm,
		B.amount,
		A.amount_tunai,
		A.amount_nontunai
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
		LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		WHERE A.medical_cd IS NULL
		AND to_char(A.invoice_date, 'yyyy-mm-dd') >=p_dateStart
		AND to_char(A.invoice_date, 'yyyy-mm-dd') <=p_dateEnd
		AND A.payment_st='PAYMENT_ST_1'
		ORDER BY medical_tp,A.invoice_date;
	ELSE
	/*--Per unit--*/
		SELECT 
		A.invoice_no,
		public.fn_formatdate(A.invoice_date::timestamp) AS invoice_date,
		A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,
		INS.insurance_nm,
		A.pay_nm,
		A.entry_nm,
		C.pasien_nm,
		C.no_rm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		DR.dr_nm,
		D.medical_tp,
		COM2.code_nm AS medical_tp_nm,
		D.datetime_in,
		D.datetime_out,
		ACC.account_nm,
		B.amount,
		A.amount_tunai,
		A.amount_nontunai
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE to_char(A.invoice_date, 'yyyy-mm-dd') >=p_dateStart
		AND to_char(A.invoice_date, 'yyyy-mm-dd') <=p_dateEnd
		AND A.payment_st='PAYMENT_ST_1'
		AND (D.medunit_cd=p_unitCd OR BNG.bangsal_cd=p_unitCd)
		UNION
		SELECT 
		A.invoice_no,
		public.fn_formatdate(A.invoice_date::timestamp) AS invoice_date,
		A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,
		'' AS insurance_nm,
		A.pay_nm,
		A.entry_nm,
		CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
		CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,
		'' AS medunit_nm,
		'' AS ruang_nm, 
		'' AS kelas_nm,
		CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
		'PENDAPATAN' AS medical_tp,
		'Pendapatan Lain' AS medical_tp_nm,
		A.invoice_date AS datetime_in,
		A.invoice_date AS datetime_out,
		ACC.account_nm,
		B.amount,
		A.amount_tunai,
		A.amount_nontunai
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
		LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		WHERE A.medical_cd IS NULL
		AND to_char(A.invoice_date, 'yyyy-mm-dd') >=p_dateStart
		AND to_char(A.invoice_date, 'yyyy-mm-dd') <=p_dateEnd
		AND A.payment_st='PAYMENT_ST_1'
		ORDER BY medical_tp, A.invoice_date;
	END IF;
END;
$$ LANGUAGE plpgsql;