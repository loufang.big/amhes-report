DROP FUNCTION SP_RPT_GET_TRX_BYUNIT (p_pdtDateStart text,p_pdtDateEnd text);
CREATE OR REPLACE FUNCTION SP_RPT_GET_TRX_BYUNIT (p_pdtDateStart text,p_pdtDateEnd text)
RETURNS table(
	medical_tp varchar(20),
	medical_tp_nm varchar(100),
	trx_datetime timestamp,
	pasien_nm varchar(100),
	no_rm varchar(100),
	medunit_nm varchar(100)
)
AS $$
BEGIN
	RETURN QUERY SELECT 
	A.medical_tp,
	COM.code_nm AS medical_tp_nm,
	A.datetime_in AS trx_datetime,
	PAS.pasien_nm,
	PAS.no_rm,
	RJ.medunit_nm
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	WHERE A.medical_tp='MEDICAL_TP_01'
	AND to_date(to_char(A.datetime_in, 'yyyy-mm-dd'), 'yyyy-mm-dd') >=p_pdtDateStart AND to_date(to_char(A.datetime_in, 'yyyy-mm-dd'), 'yyyy-mm-dd') <= p_pdtDateEnd
	ORDER BY RJ.medunit_nm,A.datetime_in;
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_GET_TRX_BYUNIT('2016-12-12','2016-12-31');