DROP FUNCTION public.SP_RPT_GET_DEMOGRAFI_PASIEN_DETAIL(p_pdtDateStart Varchar(10),p_pdtDateEnd Varchar(10)) ;
CREATE OR REPLACE FUNCTION public.SP_RPT_GET_DEMOGRAFI_PASIEN_DETAIL(p_pdtDateStart Varchar(10),p_pdtDateEnd Varchar(10)) 
RETURNS table(
	propinsi Varchar,
	kabupaten Varchar,
	kecamatan Varchar,
	kelurahan Varchar,
	total Bigint
)
AS $$
BEGIN
	RETURN QUERY 
	SELECT 
	REGION1.region_nm AS propinsi, 
	REGION2.region_nm AS kabupaten, 
	REGION3.region_nm AS kecamatan,
	CASE WHEN COALESCE(REGION4.region_nm,'') = '' THEN '-----' ELSE REGION4.region_nm END AS kelurahan,
	COUNT(*) AS total
	FROM trx_medical A 
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	JOIN com_region REGION1 On PAS.region_prop=REGION1.region_cd 
	JOIN com_region REGION2 On PAS.region_kab=REGION2.region_cd 
	LEFT JOIN com_region REGION3 On PAS.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On PAS.region_kel=REGION4.region_cd
	WHERE to_char(A.datetime_in, 'yyyy-mm-dd') >=p_pdtDateStart AND to_char(A.datetime_in, 'yyyy-mm-dd') <= p_pdtDateEnd
	GROUP BY REGION1.region_nm,REGION2.region_nm,REGION3.region_nm,REGION4.region_nm
	-- UNION
	-- SELECT
	-- 'LAIN-LAIN' AS propinsi,
	-- 'LAIN-LAIN' AS kabupaten,
	-- 'LAIN-LAIN' AS kecamatan,
	-- 'LAIN-LAIN' AS kelurahan,
	-- COUNT(*) AS total
	-- FROM trx_medical A 
	-- JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	-- WHERE to_char(A.datetime_in, 'yyyy-mm-dd') >=p_pdtDateStart AND to_char(A.datetime_in, 'yyyy-mm-dd') <= p_pdtDateEnd
	-- ORDER BY propinsi,kabupaten,kecamatan,kelurahan
	;
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_GET_DEMOGRAFI_PASIEN_DETAIL('2017-01-01', '2017-12-31');