DROP FUNCTION public.sp_rpt_get_rmtrx_sensus_bydate(p_pdtdatestart text, p_pdtdateend text);
CREATE OR REPLACE FUNCTION public.sp_rpt_get_rmtrx_sensus_bydate(p_pdtdatestart text, p_pdtdateend text)
 RETURNS table(
  medical_cd varchar(20),
  medical_tp varchar(20),
  medical_tp_nm varchar(100),
  trx_datetime timestamp,
  datetime_in timestamp,
  pasien_nm varchar(100),
  no_rm varchar(20),
  insurance_nm varchar(100),
  age interval,
  gender_cd varchar(20),
  register_st varchar(10),
  dr_nm varchar(100,
  medunit_nm varchar(100),
  diagnosa varchar(1000)
  )
AS $function$
BEGIN
  RETURN QUERY SELECT 
  A.medical_cd,
  A.medical_tp,
  COM.code_nm AS medical_tp_nm,
  A.datetime_in AS trx_datetime,
  A.datetime_in,
  PAS.pasien_nm,
  PAS.no_rm,
  INS.insurance_nm,
  age(PAS.birth_date) AS age,
  GENDER.code_value AS gender_cd,
  CASE WHEN PAS.register_date=A.datetime_in THEN 'Baru' ELSE 'Lama' END AS register_st,
  DR.dr_nm,
  RJ.medunit_nm AS unit_nm,
  RM.medical_data AS diagnosa
  FROM trx_medical A 
  LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
  JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
  LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
  LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
  LEFT JOIN trx_medical_record RM ON A.medical_cd=RM.medical_cd
  LEFT JOIN com_code GENDER ON PAS.gender_tp=GENDER.com_cd
  LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
  LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
  LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
  LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
  WHERE A.medical_tp='MEDICAL_TP_01'
  AND RJ.medunit_nm IS NOT NULL
  AND to_char(A.datetime_in,'yyyy-mm-dd') >= p_pdtDateStart AND to_char(A.datetime_in,'yyyy-mm-dd') <= p_pdtDateEnd
  UNION
  SELECT 
  A.medical_cd,
  A.medical_tp,
  COM.code_nm AS medical_tp_nm,
  A.datetime_in AS trx_datetime,
  A.datetime_in,
  PAS.pasien_nm,
  PAS.no_rm,
  INS.insurance_nm,
  age(PAS.birth_date) AS age,
  GENDER.code_value AS gender_cd,
  CASE WHEN PAS.register_date=A.datetime_in THEN 'Baru' ELSE 'Lama' END AS register_st,
  DR.dr_nm,
  RJ.medunit_nm AS unit_nm,
  RM.medical_data AS diagnosa
  FROM trx_medical A 
  LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
  JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
  LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
  LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
  LEFT JOIN trx_medical_record RM ON A.medical_cd=RM.medical_cd
  LEFT JOIN com_code GENDER ON PAS.gender_tp=GENDER.com_cd
  LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
  LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
  LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
  LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
  WHERE A.medical_tp='MEDICAL_TP_02'
  AND KLS.kelas_nm IS NOT NULL
  AND to_char(A.datetime_in, 'yyyy-mm-dd') >= p_pdtDateStart AND to_char(A.datetime_in, 'yyyy-mm-dd')<=p_pdtDateEnd
  -- ORDER BY A.medical_tp,unit_nm,A.datetime_in
  ;
END;
$function$
LANGUAGE plpgsql;

SELECT * from public.sp_rpt_get_rmtrx_sensus_bydate('2016-01-01', '2016-12-31');