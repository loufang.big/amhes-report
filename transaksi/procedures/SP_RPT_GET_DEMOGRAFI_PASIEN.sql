DROP FUNCTION public.SP_RPT_GET_DEMOGRAFI_PASIEN (p_pdtDateStart Varchar(10),p_pdtDateEnd Varchar(10));
CREATE OR REPLACE FUNCTION public.SP_RPT_GET_DEMOGRAFI_PASIEN (p_DateStart Varchar(10),p_DateEnd Varchar(10))
RETURNS table(
	kecamatan Varchar,
	total bigint
)
AS $$
BEGIN
	Return Query 
	select 
	REGION3.region_nm AS kecamatan,
	COUNT(*) AS total
	FROM trx_medical A 
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd AND COALESCE(PAS.region_kec,'')<>''
	JOIN com_region REGION1 On PAS.region_prop=REGION1.region_cd
	JOIN com_region REGION2 On PAS.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On PAS.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On PAS.region_kel=REGION4.region_cd
	WHERE to_char(A.datetime_in, 'yyyy-mm-dd') >=p_pdtDateStart AND to_char(A.datetime_in, 'yyyy-mm-dd') <= p_pdtDateEnd
	GROUP BY REGION3.region_nm
	order by REGION3.region_nm
	;
	-- UNION
	-- SELECT 
	-- 'LAIN-LAIN' AS kecamatan,
	-- COUNT(*) AS total
	-- FROM trx_medical A 
	-- JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd AND ((PAS.region_prop<>'33' AND PAS.region_kab<>'3307') OR COALESCE(PAS.region_kec,'')='') 
	-- WHERE to_char(A.datetime_in, 'yyyy-mm-dd') >=p_pdtDateStart AND to_char(A.datetime_in, 'yyyy-mm-dd') <= p_pdtDateEnd
END;
$$ 
LANGUAGE plpgsql;

SELECT * from public.SP_RPT_GET_DEMOGRAFI_PASIEN ('2017-01-01', '2017-12-31');