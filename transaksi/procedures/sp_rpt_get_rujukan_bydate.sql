DROP FUNCTION public.sp_rpt_get_rujukan_bydate(p_pdtdatestart text, p_pdtdateend text);
CREATE OR REPLACE FUNCTION public.sp_rpt_get_rujukan_bydate(p_pdtdatestart text, p_pdtdateend text)
 RETURNS table(
  medical_tp varchar(20),
  medical_tp_nm varchar(100),
  trx_datetime timestamp,
  pasien_nm varchar(100),
  no_rm varchar(20),
  address varchar(1000),
  referensi_nm varchar(100),
  referensi_address varchar(100),
  insurance_nm varchar(100),
  dr_nm varchar(100),
  medunit_nm varchar(100),
  ruang_nm varchar(100),
  kelas_nm varchar(100)
  )
AS $function$
BEGIN
  return query SELECT 
  A.medical_tp,
  COM.code_nm AS medical_tp_nm,
  A.datetime_in AS trx_datetime,
  PAS.pasien_nm,
  PAS.no_rm,
  pas.address,
  REFF.referensi_nm,
  REFF.address AS reff_address,
  INS.insurance_nm,
  DR.dr_nm,
  RJ.medunit_nm,
  KMR.ruang_nm,
  KLS.kelas_nm
  FROM trx_medical A
  JOIN trx_referensi REFF ON A.referensi_cd=REFF.referensi_cd
  LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
  JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
  LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
  LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
  LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
  LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
  LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
  LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
  JOIN trx_settlement SETT ON A.medical_cd=SETT.medical_cd
  WHERE to_char(A.datetime_in,'yyyy-mm-dd') >= p_pdtDateStart AND to_char(A.datetime_in,'yyyy-mm-dd') <= p_pdtDateEnd
  AND SETT.payment_st='PAYMENT_ST_1'
  ORDER BY A.medical_tp,A.datetime_in;
END;
$function$
LANGUAGE plpgsql;

SELECT * from  public.sp_rpt_get_rujukan_bydate('2016-01-01', '2016-12-31');