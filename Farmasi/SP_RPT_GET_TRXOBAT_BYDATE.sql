SELECT 
	A.medical_tp,
	COM.code_nm AS medical_tp_nm,
	public.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,
	PAS.no_rm,
	PAS.address,
	CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
	DR.dr_nm,
	RJ.medunit_nm,
	KMR.ruang_nm,
	KLS.kelas_nm,
	OBAT.item_cd,
	CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
	OBAT.quantity,
	U.unit_nm,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' 
		THEN public.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) 
		ELSE public.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' 
		THEN OBAT.quantity * public.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) 
		ELSE public.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total
	--MR.medical_data
	FROM 
		trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		--LEFT JOIN trx_medical_record MR ON A.medical_cd=MR.medical_cd
		JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
		JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
		LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
		LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
	WHERE 
		RESEP.resep_date>=('2017-01-01')::timestamp AND RESEP.resep_date<=('2017-12-01')::timestamp
		AND RESEP.proses_st='1'
	ORDER BY 
		A.medical_tp,INS.insurance_nm,RESEP.resep_date