DROP FUNCTION SP_RPT_GET_TRXITEMTYPE_BYDATE(
												p_pdtDateStart Varchar(10),
												p_pdtDateEnd Varchar(10),
												p_pstrDrCd Varchar(20) );
CREATE OR REPLACE FUNCTION SP_RPT_GET_TRXITEMTYPE_BYDATE(
												p_pdtDateStart Varchar(10),
												p_pdtDateEnd Varchar(10),
												p_pstrDrCd Varchar(20) )
RETURNS TABLE
 	(
 		medical_tp character varying, 
 		medical_tp_nm character varying, 
 		trx_datetime character varying, 
 		pasien_nm character varying, 
 		no_rm character varying, 
 		address character varying, 
 		insurance_nm character varying, 
 		dr_nm character varying, 
 		medunit_nm character varying, 
 		ruang_nm character varying, 
 		kelas_nm character varying, 
 		type_cd character varying, 
 		type_nm character varying, 
 		item_cd character varying, 
 		item_nm character varying, 
 		quantity numeric, 
 		unit_nm character varying, 
 		harga numeric, 
 		total numeric, 
 		item_price_master numeric, 
 		total_price_master numeric
 	)
AS $function$
BEGIN
	IF p_pstrDrCd = ''
	THEN

	RETURN QUERY
	SELECT 
		A.type_cd,
		TP.type_nm,
		A.item_cd, 
		A.item_nm, 
		UNIT.unit_nm AS unit,
		B.quantity
	FROM 
		inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_resep_data B ON A.item_cd=B.item_cd
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
	WHERE 
		B.resep_tp='RESEP_TP_1'
		AND (public.FN_FORMATDATE(C.resep_date))::timestamp>=('')::timestamp
		AND (public.FN_FORMATDATE(C.resep_date))::timestamp<=('')::timestamp
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
	UNION ALL
	
	SELECT 
		A.type_cd,
		TP.type_nm,
		A.item_cd, 
		A.item_nm, 
		UNIT.unit_nm AS unit,
		B.quantity
	FROM 
		inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_medical_alkes B ON A.item_cd=B.item_cd
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
	WHERE 
		(public.FN_FORMATDATE(B.datetime_trx))::timestamp>=('')::timestamp
		AND (public.FN_FORMATDATE(B.datetime_trx))::timestamp<=('')::timestamp
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
	
	UNION ALL
	
	SELECT 
		A.type_cd,
		TP.type_nm,
		A.item_cd, 
		A.item_nm, 
		UNIT.unit_nm AS unit,
		RACIK.quantity
	FROM 
		inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_resep_data B ON A.item_cd=B.item_cd
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		JOIN trx_resep_racik RACIK ON B.resep_seqno=RACIK.resep_seqno
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
	WHERE 
		B.resep_tp='RESEP_TP_2'
		AND (public.FN_FORMATDATE(C.resep_date))::timestamp>=('')::timestamp
		AND (public.FN_FORMATDATE(C.resep_date))::timestamp<=('')::timestamp
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
		ORDER BY A.type_cd,TP.type_nm,A.item_cd,A.item_nm;
		

	ELSE

	RETURN QUERY
		

END;
$function$
LANGUAGE plpgsql;

select * from SP_RPT_GET_TRXITEMTYPE_BYDATE('2017-01-01','2017-12-31','');