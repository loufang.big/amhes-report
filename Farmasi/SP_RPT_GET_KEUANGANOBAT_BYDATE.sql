DROP FUNCTION SP_RPT_GET_KEUANGANOBAT_BYDATE 	(
															p_pdtDateStart Varchar(10),
															p_pdtDateEnd Varchar(10)
															) 
CREATE OR REPLACE FUNCTION public.sp_rpt_get_keuanganobat_bydate(p_pdtdatestart character varying, p_pdtdateend character varying)
 RETURNS TABLE
 	(
 		medical_tp character varying, 
 		medical_tp_nm character varying, 
 		trx_datetime character varying, 
 		pasien_nm character varying, 
 		no_rm character varying, 
 		address character varying, 
 		insurance_nm character varying, 
 		dr_nm character varying, 
 		medunit_nm character varying, 
 		ruang_nm character varying, 
 		kelas_nm character varying, 
 		type_cd character varying, 
 		type_nm character varying, 
 		item_cd character varying, 
 		item_nm character varying, 
 		quantity numeric, 
 		unit_nm character varying, 
 		harga numeric, 
 		total numeric, 
 		item_price_master numeric, 
 		total_price_master numeric
 	)
 LANGUAGE plpgsql
AS $function$
BEGIN
	RETURN QUERY
	SELECT 
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		public.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,
		PAS.no_rm,
		PAS.address,
		CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
		DR.dr_nm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		INV.type_cd,
		INVTP.type_nm,
		OBAT.item_cd,
		CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
		OBAT.quantity,
		U.unit_nm,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' 
			THEN public.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) 
			ELSE public.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' 
			THEN OBAT.quantity * public.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) 
			ELSE public.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' 
			THEN public.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) 
			ELSE public.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS item_price_master,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' 
			THEN OBAT.quantity * public.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) 
			ELSE public.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total_price_master
	FROM 
		trx_medical A 
		LEFT JOIN com_code COM 
			ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS 
			ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS 
			ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS 
			ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR 
			ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ 
			ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR 
			ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS 
			ON KMR.kelas_cd=KLS.kelas_cd
		JOIN trx_medical_resep RESEP 
			ON A.medical_cd=RESEP.medical_cd
		JOIN trx_resep_data OBAT 
			ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
		LEFT JOIN inv_item_master INV 
			ON OBAT.item_cd=INV.item_cd
		JOIN inv_item_type INVTP 
			ON INV.type_cd=INVTP.type_cd
		LEFT JOIN inv_unit U 
			ON INV.unit_cd=U.unit_cd
	WHERE 
		RESEP.resep_date>=(p_pdtDateStart)::timestamp AND RESEP.resep_date<=(p_pdtDateEnd)::timestamp
		AND RESEP.proses_st='1'
	ORDER BY 
		INV.type_cd,A.medical_tp,INS.insurance_nm,RESEP.resep_date;
END;
$function$


select * from SP_RPT_GET_KEUANGANOBAT_BYDATE('2017-01-01','2017-12-31');