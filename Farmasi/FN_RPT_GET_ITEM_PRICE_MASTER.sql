CREATE OR REPLACE FUNCTION FN_RPT_GET_ITEM_PRICE_MASTER (p_pstrItemCd Varchar(20))
RETURNS Numeric(18,0)
AS $$
     DECLARE v_numResult Numeric(18,0);
BEGIN

  
   SELECT COALESCE(SUM(item_price_buy),0) FROM inv_item_master
            WHERE item_cd=p_pstrItemCd;
  
  RETURN v_numResult;
END;
$$ LANGUAGE plpgsql;