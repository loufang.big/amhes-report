
insert into trx_medical_record(
pasien_cd,
icd_cd,
medical_cd,
datetime_record,
anamnesa,
case_tp,
modi_id,
modi_datetime
)
select
med.pasien_cd,
(select icd_cd from trx_icd order by random() limit 1) as icd,
medical_cd,
datetime_in as datetime_record,
'this is anamnesa' as anamnesa,
'RM_TP_1' as case_tp,
'admin' as modi_id,
now() as modi_datetime
from trx_medical as med;

insert into trx_medical_tindakan(
medical_cd,
treatment_cd,
datetime_trx,
dr_cd,
quantity,
medunit_cd,
modi_id,
modi_datetime
)
select
medical_cd,
(select treatment_cd from trx_tindakan order by random() limit 1) as treatment_cd,
datetime_in as datetime_trx,
dr_cd as dr_cd,
1::numeric(10,2) as quantity,
medunit_cd as medunit_cd,
'admin',
now()
from trx_medical as med;