CREATE PROCEDURE SP_RPT_GET_KEUANGANOBAT_BYDATE
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
	INV.type_cd,INVTP.type_nm,OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
	OBAT.quantity,U.unit_nm,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS item_price_master,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total_price_master
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
	JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
	LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
	JOIN inv_item_type INVTP ON INV.type_cd=INVTP.type_cd
	LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
	WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND RESEP.proses_st='1'
	ORDER BY INV.type_cd,A.medical_tp,INS.insurance_nm,RESEP.resep_date
END;

CREATE PROCEDURE SP_RPT_GET_KEUANGANOBAT_BYDOKTER
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrDrCd Varchar(20)
AS
BEGIN
	IF @pstrDrCd = '' 
	/*--Semua--*/
		/*SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		INV.type_cd,INVTP.type_nm,OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
		OBAT.quantity,U.unit_nm,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS item_price_master,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total_price_master
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
		JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
		LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
		JOIN inv_item_type INVTP ON INV.type_cd=INVTP.type_cd
		LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
		WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND RESEP.proses_st='1'
		ORDER BY DR.dr_nm,A.medical_tp,INS.insurance_nm,RESEP.resep_date*/
		SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
		CASE WHEN ISNULL(RESEP.dr_cd,'')<>'' THEN RESEPDR.dr_nm ELSE DR.dr_nm END AS dr_nm,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		INV.type_cd,INVTP.type_nm,OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
		OBAT.quantity,U.unit_nm,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS item_price_master,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total_price_master
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
		LEFT JOIN trx_dokter RESEPDR ON RESEP.dr_cd=RESEPDR.dr_cd
		JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
		LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
		JOIN inv_item_type INVTP ON INV.type_cd=INVTP.type_cd
		LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
		WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND RESEP.proses_st='1'
		ORDER BY dr_nm,A.medical_tp,INS.insurance_nm,RESEP.resep_date
	ELSE
	/*--Per dokter--*/
		/*SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		INV.type_cd,INVTP.type_nm,OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
		OBAT.quantity,U.unit_nm,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS item_price_master,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total_price_master
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
		JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
		LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
		JOIN inv_item_type INVTP ON INV.type_cd=INVTP.type_cd
		LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
		WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND DR.dr_cd=@pstrDrCd
		AND RESEP.proses_st='1'
		ORDER BY DR.dr_nm,A.medical_tp,INS.insurance_nm,RESEP.resep_date*/
		SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
		CASE WHEN ISNULL(RESEP.dr_cd,'')<>'' THEN RESEPDR.dr_nm ELSE DR.dr_nm END AS dr_nm,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		INV.type_cd,INVTP.type_nm,OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
		OBAT.quantity,U.unit_nm,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS item_price_master,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total_price_master
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
		LEFT JOIN trx_dokter RESEPDR ON RESEP.dr_cd=RESEPDR.dr_cd
		JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
		LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
		JOIN inv_item_type INVTP ON INV.type_cd=INVTP.type_cd
		LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
		WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND RESEPDR.dr_cd=@pstrDrCd
		AND RESEP.proses_st='1'
		ORDER BY dr_nm,A.medical_tp,INS.insurance_nm,RESEP.resep_date
END;

CREATE PROCEDURE SP_RPT_GET_OBATKRONIS_BYDATE
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
	OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
	OBAT.num_01 AS qty_real,OBAT.quantity AS qty_kwitansi,OBAT.num_02 AS qty_tagihan,U.unit_nm,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.num_02 * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total
	--MR.medical_data
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	--LEFT JOIN trx_medical_record MR ON A.medical_cd=MR.medical_cd
	JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
	JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
	LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
	LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
	WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND ISNULL(RESEP.case_tp,'')='1'
	--AND RESEP.proses_st='1'
	ORDER BY A.medical_tp,INS.insurance_nm,RESEP.resep_date
END;

CREATE PROCEDURE SP_RPT_GET_OBAT_BYPRINCIPAL
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrDrCd Varchar(20),
@pstrPrincipalCd Varchar(20)
AS
BEGIN
	IF @pstrDrCd = '' 
	/*--Semua--*/
		SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
		CASE WHEN ISNULL(RESEP.dr_cd,'')<>'' THEN RESEPDR.dr_nm ELSE DR.dr_nm END AS dr_nm,PAL.principal_nm,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		INV.type_cd,INVTP.type_nm,OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
		OBAT.quantity,U.unit_nm,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS item_price_master,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total_price_master
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
		LEFT JOIN trx_dokter RESEPDR ON RESEP.dr_cd=RESEPDR.dr_cd
		JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
		LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
		JOIN inv_item_type INVTP ON INV.type_cd=INVTP.type_cd
		LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
		LEFT JOIN po_principal PAL ON INV.principal_cd=PAL.principal_cd AND INV.principal_cd=@pstrPrincipalCd
		WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND RESEP.proses_st='1'
		ORDER BY dr_nm,A.medical_tp,INS.insurance_nm,RESEP.resep_date
	ELSE
	/*--Per dokter--*/
		SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
		CASE WHEN ISNULL(RESEP.dr_cd,'')<>'' THEN RESEPDR.dr_nm ELSE DR.dr_nm END AS dr_nm,PAL.principal_nm,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		INV.type_cd,INVTP.type_nm,OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
		OBAT.quantity,U.unit_nm,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS item_price_master,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total_price_master
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
		LEFT JOIN trx_dokter RESEPDR ON RESEP.dr_cd=RESEPDR.dr_cd
		JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
		LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
		JOIN inv_item_type INVTP ON INV.type_cd=INVTP.type_cd
		LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
		LEFT JOIN po_principal PAL ON INV.principal_cd=PAL.principal_cd AND INV.principal_cd=@pstrPrincipalCd
		WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND RESEPDR.dr_cd=@pstrDrCd
		AND RESEP.proses_st='1'
		ORDER BY dr_nm,A.medical_tp,INS.insurance_nm,RESEP.resep_date
END;

CREATE PROCEDURE SP_RPT_GET_TRXITEMTYPE_BYDATE
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrTypeCd Varchar(20)
AS
BEGIN
	IF @pstrTypeCd = ''
		SELECT A.type_cd,TP.type_nm,A.item_cd, A.item_nm, UNIT.unit_nm AS unit,B.quantity
		FROM inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_resep_data B ON A.item_cd=B.item_cd
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE B.resep_tp='RESEP_TP_1'
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
		UNION ALL
		SELECT A.type_cd,TP.type_nm,A.item_cd, A.item_nm, UNIT.unit_nm AS unit,B.quantity
		FROM inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_medical_alkes B ON A.item_cd=B.item_cd
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_trx))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_trx))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
		UNION ALL
		SELECT A.type_cd,TP.type_nm,A.item_cd, A.item_nm, UNIT.unit_nm AS unit,RACIK.quantity
		FROM inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_resep_data B ON A.item_cd=B.item_cd
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		JOIN trx_resep_racik RACIK ON B.resep_seqno=RACIK.resep_seqno
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE B.resep_tp='RESEP_TP_2'
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
		ORDER BY A.type_cd,TP.type_nm,A.item_cd,A.item_nm
	ELSE
		SELECT A.type_cd,TP.type_nm,A.item_cd, A.item_nm, UNIT.unit_nm AS unit,B.quantity
		FROM inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_resep_data B ON A.item_cd=B.item_cd
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE B.resep_tp='RESEP_TP_1'
		AND A.type_cd=@pstrTypeCd
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
		UNION ALL
		SELECT A.type_cd,TP.type_nm,A.item_cd, A.item_nm, UNIT.unit_nm AS unit,B.quantity
		FROM inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_medical_alkes B ON A.item_cd=B.item_cd
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE A.type_cd=@pstrTypeCd
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_trx))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_trx))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
		UNION ALL
		SELECT A.type_cd,TP.type_nm,A.item_cd, A.item_nm, UNIT.unit_nm AS unit,RACIK.quantity
		FROM inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_resep_data B ON A.item_cd=B.item_cd
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		JOIN trx_resep_racik RACIK ON B.resep_seqno=RACIK.resep_seqno
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE B.resep_tp='RESEP_TP_2'
		AND A.type_cd=@pstrTypeCd
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
		ORDER BY A.type_cd,TP.type_nm,A.item_cd,A.item_nm
END;

CREATE PROCEDURE SP_RPT_GET_TRXOBATGENERIK_BYDATE
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
	OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
	OBAT.quantity,U.unit_nm,
	--CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
	--CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
	CASE WHEN ISNULL(INV.generic_st,'')='1' THEN 1 ELSE 0 END AS generik,
	CASE WHEN ISNULL(INV.generic_st,'')<>'1' THEN 1 ELSE 0 END AS paten,
	RESEP.medical_resep_seqno
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
	JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
	LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
	LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
	WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND RESEP.proses_st='1'
	ORDER BY A.medical_tp,INS.insurance_nm,RESEP.medical_resep_seqno
END;

CREATE PROCEDURE SP_RPT_GET_TRXOBAT_BYDATE
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
	OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
	OBAT.quantity,U.unit_nm,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total
	--MR.medical_data
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	--LEFT JOIN trx_medical_record MR ON A.medical_cd=MR.medical_cd
	JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
	JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
	LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
	LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
	WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND RESEP.proses_st='1'
	ORDER BY A.medical_tp,INS.insurance_nm,RESEP.resep_date
END;
