DROP FUNCTION IF EXISTS SP_ANALISIS_DIAGNOSA(Varchar,real) CASCADE;
CREATE PROCEDURE [simrke].[SP_ANALISIS_DIAGNOSA] 
@pstrMedicalCd Varchar(10),
@pnumLimit real
AS
BEGIN
	DECLARE @numAnamnesaTotal real
	DECLARE @numStatstikTotal real
	
	SELECT TOP 1 @numAnamnesaTotal=COUNT(A.anamnesa_cd)
	FROM trx_medical_anamnesa A 
	WHERE A.medical_cd=@pstrMedicalCd
	GROUP BY A.medical_record_seqno
	ORDER BY A.medical_record_seqno DESC
	
	SELECT @numStatstikTotal=SUM(statistik)
	FROM
		(SELECT RM.icd_cd,COUNT(RM.icd_cd) AS statistik
		FROM
			(SELECT A.medical_cd,COUNT(A.medical_cd) AS total,(COUNT(A.medical_cd)/@numAnamnesaTotal)*100 AS persen
			FROM trx_medical_anamnesa A 
			WHERE A.medical_cd<>@pstrMedicalCd
			AND A.anamnesa_cd IN
				(SELECT anamnesa_cd
				FROM trx_medical_anamnesa
				WHERE medical_cd=@pstrMedicalCd
				AND medical_record_seqno=
						(SELECT TOP 1 medical_record_seqno
						FROM trx_medical_anamnesa
						WHERE medical_cd=@pstrMedicalCd
						ORDER BY medical_record_seqno DESC)
				)
			GROUP BY A.medical_cd)
		AS TEMP
		JOIN trx_medical_record RM ON TEMP.medical_cd=RM.medical_cd
		WHERE persen>=@pnumLimit
		GROUP BY RM.icd_cd
		)
	AS TEMP2
	
	SELECT RM.icd_cd,CONVERT(VARCHAR(5000),RM.medical_data) AS icd_nm,(COUNT(RM.icd_cd)/@numStatstikTotal)*100 AS statistik
	FROM
		(SELECT A.medical_cd,COUNT(A.medical_cd) AS total,(COUNT(A.medical_cd)/@numAnamnesaTotal)*100 AS persen
		FROM trx_medical_anamnesa A 
		WHERE A.medical_cd<>@pstrMedicalCd
		AND A.anamnesa_cd IN
			(SELECT anamnesa_cd
			FROM trx_medical_anamnesa
			WHERE medical_cd=@pstrMedicalCd
			AND medical_record_seqno=
					(SELECT TOP 1 medical_record_seqno
					FROM trx_medical_anamnesa
					WHERE medical_cd=@pstrMedicalCd
					ORDER BY medical_record_seqno DESC)
			)
		GROUP BY A.medical_cd)
	AS TEMP
	JOIN trx_medical_record RM ON TEMP.medical_cd=RM.medical_cd
	WHERE persen>=@pnumLimit
	GROUP BY RM.icd_cd,CONVERT(VARCHAR(5000),RM.medical_data)
	ORDER BY statistik DESC
END;

DROP FUNCTION IF EXISTS SP_ANALISIS_DIAGNOSA_HISTORY(Varchar,real) CASCADE;
CREATE PROCEDURE [simrke].[SP_ANALISIS_DIAGNOSA_HISTORY] 
@pstrMedicalCd Varchar(10),
@pnumLimit real
AS
BEGIN
	DECLARE @numAnamnesaTotal real
	
	SELECT TOP 1 @numAnamnesaTotal=COUNT(A.anamnesa_cd)
	FROM trx_medical_anamnesa A 
	WHERE A.medical_cd=@pstrMedicalCd
	GROUP BY A.medical_record_seqno
	ORDER BY A.medical_record_seqno DESC
	
	SELECT TOP 100 RM.medical_record_seqno,simrke.FN_FORMATDATE(RM.datetime_record) AS date_trx,
	DR.dr_nm,RM.icd_cd,RM.medical_data,RM.anamnesa
	FROM
		(SELECT A.medical_cd,COUNT(A.medical_cd) AS total,(COUNT(A.medical_cd)/@numAnamnesaTotal)*100 AS persen
		FROM trx_medical_anamnesa A 
		WHERE A.medical_cd<>@pstrMedicalCd
		AND A.anamnesa_cd IN
			(SELECT anamnesa_cd
			FROM trx_medical_anamnesa
			WHERE medical_cd=@pstrMedicalCd
			AND medical_record_seqno=
					(SELECT TOP 1 medical_record_seqno
					FROM trx_medical_anamnesa
					WHERE medical_cd=@pstrMedicalCd
					ORDER BY medical_record_seqno DESC)
			)
		GROUP BY A.medical_cd)
	AS TEMP
	JOIN trx_medical_record RM ON TEMP.medical_cd=RM.medical_cd
	LEFT JOIN trx_icd ICD ON RM.icd_cd=ICD.icd_cd 
	LEFT JOIN trx_dokter DR ON RM.dr_cd=DR.dr_cd 
	WHERE persen>=@pnumLimit
	ORDER BY RM.datetime_record DESC,DR.dr_nm
END;

DROP FUNCTION IF EXISTS SP_ANALISIS_OBAT(Varchar,real) CASCADE;
CREATE PROCEDURE [simrke].[SP_ANALISIS_OBAT] 
@pstrMedicalCd Varchar(10),
@pnumLimit real
AS
BEGIN
	DECLARE @numAnamnesaTotal real
	DECLARE @numStatstikTotal real
	
	SELECT TOP 1 @numAnamnesaTotal=COUNT(A.anamnesa_cd)
	FROM trx_medical_anamnesa A 
	WHERE A.medical_cd=@pstrMedicalCd
	GROUP BY A.medical_record_seqno
	ORDER BY A.medical_record_seqno DESC
	
	SELECT @numStatstikTotal=SUM(statistik)
	FROM
		(SELECT RM.item_cd,COUNT(RM.item_cd) AS statistik
		FROM
			(SELECT A.medical_cd,COUNT(A.medical_cd) AS total,(COUNT(A.medical_cd)/@numAnamnesaTotal)*100 AS persen
			FROM trx_medical_anamnesa A 
			WHERE A.medical_cd<>@pstrMedicalCd
			AND A.anamnesa_cd IN
				(SELECT anamnesa_cd
				FROM trx_medical_anamnesa
				WHERE medical_cd=@pstrMedicalCd
				AND medical_record_seqno=
						(SELECT TOP 1 medical_record_seqno
						FROM trx_medical_anamnesa
						WHERE medical_cd=@pstrMedicalCd
						ORDER BY medical_record_seqno DESC)
				)
			GROUP BY A.medical_cd)
		AS TEMP
		JOIN trx_medical_resep RESEP ON TEMP.medical_cd=RESEP.medical_cd
		JOIN trx_resep_data RM ON RESEP.medical_resep_seqno=RM.medical_resep_seqno
		WHERE persen>=@pnumLimit
		GROUP BY RM.item_cd
		)
	AS TEMP2
	
	SELECT RM.item_cd,CASE WHEN RM.resep_tp='RESEP_TP_2' THEN RM.data_nm ELSE INV.item_nm END AS item_nm,
	(COUNT(RM.item_cd)/@numStatstikTotal)*100 AS statistik
	FROM
		(SELECT A.medical_cd,COUNT(A.medical_cd) AS total,(COUNT(A.medical_cd)/@numAnamnesaTotal)*100 AS persen
		FROM trx_medical_anamnesa A 
		WHERE A.medical_cd<>@pstrMedicalCd
		AND A.anamnesa_cd IN
			(SELECT anamnesa_cd
			FROM trx_medical_anamnesa
			WHERE medical_cd=@pstrMedicalCd
			AND medical_record_seqno=
					(SELECT TOP 1 medical_record_seqno
					FROM trx_medical_anamnesa
					WHERE medical_cd=@pstrMedicalCd
					ORDER BY medical_record_seqno DESC)
			)
		GROUP BY A.medical_cd)
	AS TEMP
	JOIN trx_medical_resep RESEP ON TEMP.medical_cd=RESEP.medical_cd
	JOIN trx_resep_data RM ON RESEP.medical_resep_seqno=RM.medical_resep_seqno
	LEFT JOIN inv_item_master INV ON RM.item_cd=INV.item_cd
	WHERE persen>=@pnumLimit
	GROUP BY RM.item_cd,CASE WHEN RM.resep_tp='RESEP_TP_2' THEN RM.data_nm ELSE INV.item_nm END
	ORDER BY statistik DESC
END;

DROP FUNCTION IF EXISTS SP_ANALISIS_OBAT_HISTORY(Varchar,real) CASCADE;
CREATE PROCEDURE [simrke].[SP_ANALISIS_OBAT_HISTORY] 
@pstrMedicalCd Varchar(10),
@pnumLimit real
AS
BEGIN
	DECLARE @numAnamnesaTotal real
	
	SELECT TOP 1 @numAnamnesaTotal=COUNT(A.anamnesa_cd)
	FROM trx_medical_anamnesa A 
	WHERE A.medical_cd=@pstrMedicalCd
	GROUP BY A.medical_record_seqno
	ORDER BY A.medical_record_seqno DESC
	
	SELECT TOP 100 RM.medical_resep_seqno,simrke.FN_FORMATDATE(RESEP.resep_date) AS date_trx,
	DR.dr_nm,RM.item_cd,CASE WHEN RM.resep_tp='RESEP_TP_2' THEN RM.data_nm ELSE INV.item_nm END AS item_nm,
	RM.quantity,U.unit_nm
	FROM
		(SELECT A.medical_cd,COUNT(A.medical_cd) AS total,(COUNT(A.medical_cd)/@numAnamnesaTotal)*100 AS persen
		FROM trx_medical_anamnesa A 
		WHERE A.medical_cd<>@pstrMedicalCd
		AND A.anamnesa_cd IN
			(SELECT anamnesa_cd
			FROM trx_medical_anamnesa
			WHERE medical_cd=@pstrMedicalCd
			AND medical_record_seqno=
					(SELECT TOP 1 medical_record_seqno
					FROM trx_medical_anamnesa
					WHERE medical_cd=@pstrMedicalCd
					ORDER BY medical_record_seqno DESC)
			)
		GROUP BY A.medical_cd)
	AS TEMP
	JOIN trx_medical_resep RESEP ON TEMP.medical_cd=RESEP.medical_cd
	LEFT JOIN trx_dokter DR ON RESEP.dr_cd=DR.dr_cd
	JOIN trx_resep_data RM ON RESEP.medical_resep_seqno=RM.medical_resep_seqno
	LEFT JOIN inv_item_master INV ON RM.item_cd=INV.item_cd
	LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
	WHERE persen>=@pnumLimit
	ORDER BY RESEP.resep_date DESC,RESEP.medical_resep_seqno DESC,INV.item_nm
END;

DROP FUNCTION IF EXISTS SP_ANALISIS_TINDAKAN(Varchar,real) CASCADE;
CREATE PROCEDURE [simrke].[SP_ANALISIS_TINDAKAN] 
@pstrMedicalCd Varchar(10),
@pnumLimit real
AS
BEGIN
	DECLARE @numAnamnesaTotal real
	DECLARE @numStatstikTotal real
	
	SELECT TOP 1 @numAnamnesaTotal=COUNT(A.anamnesa_cd)
	FROM trx_medical_anamnesa A 
	WHERE A.medical_cd=@pstrMedicalCd
	GROUP BY A.medical_record_seqno
	ORDER BY A.medical_record_seqno DESC
	
	SELECT @numStatstikTotal=SUM(statistik)
	FROM
		(SELECT RM.treatment_cd,COUNT(RM.treatment_cd) AS statistik
		FROM
			(SELECT A.medical_cd,COUNT(A.medical_cd) AS total,(COUNT(A.medical_cd)/@numAnamnesaTotal)*100 AS persen
			FROM trx_medical_anamnesa A 
			WHERE A.medical_cd<>@pstrMedicalCd
			AND A.anamnesa_cd IN
				(SELECT anamnesa_cd
				FROM trx_medical_anamnesa
				WHERE medical_cd=@pstrMedicalCd
				AND medical_record_seqno=
						(SELECT TOP 1 medical_record_seqno
						FROM trx_medical_anamnesa
						WHERE medical_cd=@pstrMedicalCd
						ORDER BY medical_record_seqno DESC)
				)
			GROUP BY A.medical_cd)
		AS TEMP
		JOIN trx_medical_tindakan RM ON TEMP.medical_cd=RM.medical_cd
		WHERE persen>=@pnumLimit
		GROUP BY RM.treatment_cd
		)
	AS TEMP2
	
	SELECT RM.treatment_cd,RM.medical_note,(COUNT(RM.treatment_cd)/@numStatstikTotal)*100 AS statistik
	FROM
		(SELECT A.medical_cd,COUNT(A.medical_cd) AS total,(COUNT(A.medical_cd)/@numAnamnesaTotal)*100 AS persen
		FROM trx_medical_anamnesa A 
		WHERE A.medical_cd<>@pstrMedicalCd
		AND A.anamnesa_cd IN
			(SELECT anamnesa_cd
			FROM trx_medical_anamnesa
			WHERE medical_cd=@pstrMedicalCd
			AND medical_record_seqno=
					(SELECT TOP 1 medical_record_seqno
					FROM trx_medical_anamnesa
					WHERE medical_cd=@pstrMedicalCd
					ORDER BY medical_record_seqno DESC)
			)
		GROUP BY A.medical_cd)
	AS TEMP
	JOIN trx_medical_tindakan RM ON TEMP.medical_cd=RM.medical_cd
	WHERE persen>=@pnumLimit
	GROUP BY RM.treatment_cd,RM.medical_note
	ORDER BY statistik DESC
END;

DROP FUNCTION IF EXISTS SP_ANALISIS_TINDAKAN_HISTORY(Varchar,real) CASCADE;
CREATE PROCEDURE [simrke].[SP_ANALISIS_TINDAKAN_HISTORY] 
@pstrMedicalCd Varchar(10),
@pnumLimit real
AS
BEGIN
	DECLARE @numAnamnesaTotal real
	
	SELECT TOP 1 @numAnamnesaTotal=COUNT(A.anamnesa_cd)
	FROM trx_medical_anamnesa A 
	WHERE A.medical_cd=@pstrMedicalCd
	GROUP BY A.medical_record_seqno
	ORDER BY A.medical_record_seqno DESC
	
	SELECT TOP 100 RM.medical_tindakan_seqno,simrke.FN_FORMATDATE(RM.datetime_trx) AS date_trx,
	DR.dr_nm,RM.treatment_cd,RM.medical_note
	FROM
		(SELECT A.medical_cd,COUNT(A.medical_cd) AS total,(COUNT(A.medical_cd)/@numAnamnesaTotal)*100 AS persen
		FROM trx_medical_anamnesa A 
		WHERE A.medical_cd<>@pstrMedicalCd
		AND A.anamnesa_cd IN
			(SELECT anamnesa_cd
			FROM trx_medical_anamnesa
			WHERE medical_cd=@pstrMedicalCd
			AND medical_record_seqno=
					(SELECT TOP 1 medical_record_seqno
					FROM trx_medical_anamnesa
					WHERE medical_cd=@pstrMedicalCd
					ORDER BY medical_record_seqno DESC)
			)
		GROUP BY A.medical_cd)
	AS TEMP
	JOIN trx_medical_tindakan RM ON TEMP.medical_cd=RM.medical_cd
	LEFT JOIN trx_tindakan TK ON RM.treatment_cd=TK.treatment_cd 
	LEFT JOIN trx_dokter DR ON RM.dr_cd=DR.dr_cd 
	WHERE persen>=@pnumLimit
	ORDER BY RM.datetime_trx DESC,DR.dr_nm
END;

DROP FUNCTION IF EXISTS SP_CANCEL_INVOICE(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_CANCEL_INVOICE] 
@pintSettlementNo bigint
AS
BEGIN
	DECLARE @strMedicalCd varchar(10)
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalRootRootCd varchar(10)
	DECLARE @strMedicalChildCd varchar(10)
	
	DECLARE @strInvoiceNo varchar(20)
	
	SELECT @strMedicalCd=A.medical_cd,@strInvoiceNo=A.invoice_no
	FROM trx_settlement A 
	WHERE A.settlement_no=@pintSettlementNo
	
	SET @strMedicalRootCd = (SELECT medical_root_cd FROM trx_medical WHERE medical_cd=@strMedicalCd)
	
	SET @strMedicalRootRootCd = (SELECT A.medical_root_cd
								FROM trx_medical A 
								WHERE A.medical_cd=@strMedicalRootCd)
								
	SET @strMedicalChildCd = (SELECT TOP 1 medical_cd FROM trx_medical WHERE medical_root_cd=@strMedicalCd)
	
	BEGIN TRANSACTION
	
	--Clear settlement
	DELETE FROM trx_settlement_account
	WHERE settlement_no=@pintSettlementNo
	
	DELETE FROM trx_settlement
	WHERE settlement_no=@pintSettlementNo
	
	--Reset invoice_no
	--Update trx_medical_settlement
	UPDATE trx_medical_settlement SET invoice_no=NULL
	WHERE invoice_no=@strInvoiceNo
	
	--Update trx_medical_settlement_inv
	UPDATE trx_medical_settlement_inv SET invoice_no=NULL
	WHERE invoice_no=@strInvoiceNo
	--End Reset invoice_no
	
	--Update trx_medical medical_trx_st
	UPDATE trx_medical SET medical_trx_st='MEDICAL_TRX_ST_0',datetime_out=NULL
	WHERE medical_cd=@strMedicalCd
	
	IF @strMedicalRootCd <>'' AND @strMedicalRootCd IS NOT NULL
	BEGIN
		UPDATE trx_medical SET medical_trx_st='MEDICAL_TRX_ST_2',datetime_out=NULL
		WHERE medical_cd=@strMedicalRootCd
		
		--root of root
		UPDATE trx_medical SET medical_trx_st='MEDICAL_TRX_ST_2',datetime_out=NULL
		WHERE medical_cd=@strMedicalRootRootCd
	END
	IF @strMedicalChildCd <>'' AND @strMedicalChildCd IS NOT NULL
	BEGIN
		UPDATE trx_medical SET medical_trx_st='MEDICAL_TRX_ST_2',datetime_out=NULL
		WHERE medical_cd=@strMedicalChildCd
	END
	
	--Clear kamar
	--UPDATE trx_ruang SET ruang_st='1'
	--WHERE ruang_cd=(SELECT ruang_cd FROM trx_medical WHERE medical_cd=@strMedicalCd)
	
	IF @@ERROR<> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	ELSE
	BEGIN
		COMMIT TRANSACTION
	END	
END;

DROP FUNCTION IF EXISTS SP_CLEAR_SETTLEMENT(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_CLEAR_SETTLEMENT] 
@pstrMedicalCd Varchar(10)
AS
BEGIN
	DECLARE @strMedicalRootCd varchar(10)
	
	SET @strMedicalRootCd = (SELECT medical_root_cd FROM trx_medical WHERE medical_cd=@pstrMedicalCd)
	
	DELETE FROM trx_medical_settlement WHERE medical_cd=@pstrMedicalCd
	AND payment_st='PAYMENT_ST_0'
	AND manual_st='0'
	
	DELETE FROM trx_medical_settlement_inv WHERE medical_cd=@pstrMedicalCd
	AND payment_st='PAYMENT_ST_0'
	AND manual_st='0'
	
	DELETE FROM trx_settlement_share WHERE medical_cd=@pstrMedicalCd
	
	IF @strMedicalRootCd <>'' AND @strMedicalRootCd IS NOT NULL
	BEGIN
		EXECUTE SP_CLEAR_SETTLEMENT @strMedicalRootCd
	END
END;

DROP FUNCTION IF EXISTS SP_CLEAR_SETTLEMENT_2(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_CLEAR_SETTLEMENT_2] 
@pstrMedicalCd Varchar(10)
AS
BEGIN
	DELETE FROM trx_medical_settlement WHERE medical_cd=@pstrMedicalCd
	AND payment_st='PAYMENT_ST_0'
	AND manual_st='0'
	
	DELETE FROM trx_medical_settlement_inv WHERE medical_cd=@pstrMedicalCd
	AND payment_st='PAYMENT_ST_0'
	AND manual_st='0'
	
	DELETE FROM trx_settlement_share WHERE medical_cd=@pstrMedicalCd
END;

DROP FUNCTION IF EXISTS SP_CLEAR_SETTLEMENT_INCOME(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_CLEAR_SETTLEMENT_INCOME] 
@pintMedicalResepSeqno bigint
AS
BEGIN
	DELETE FROM trx_medical_settlement_inv
	WHERE ref_medical_resep_seqno=@pintMedicalResepSeqno
	AND payment_st='PAYMENT_ST_0'
	AND manual_st='0'
END;

DROP FUNCTION IF EXISTS SP_CLEAR_SETTLEMENT_SALE(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_CLEAR_SETTLEMENT_SALE] 
@pintMedicalResepSeqno bigint
AS
BEGIN
	DELETE FROM trx_medical_settlement_inv
	WHERE ref_medical_resep_seqno=@pintMedicalResepSeqno
	AND payment_st='PAYMENT_ST_0'
	AND manual_st='0'
END;

DROP FUNCTION IF EXISTS SP_DELETE_INVITEM(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_DELETE_INVITEM] 
@pstrItemCd Varchar(20)
AS
BEGIN
	DELETE FROM inv_item_master WHERE item_cd=@pstrItemCd
	
	DELETE FROM trx_tarif_inventori WHERE item_cd=@pstrItemCd
END;

DROP FUNCTION IF EXISTS SP_DELETE_PASIEN(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_DELETE_PASIEN] 
@pstrPasienCd Varchar(20)
AS
BEGIN
	DELETE FROM trx_pasien_identity WHERE pasien_cd=@pstrPasienCd
	DELETE FROM trx_pasien_family WHERE pasien_cd=@pstrPasienCd
	DELETE FROM trx_pasien_insurance WHERE pasien_cd=@pstrPasienCd
	
	DELETE FROM trx_pasien WHERE pasien_cd=@pstrPasienCd
END;

DROP FUNCTION IF EXISTS SP_DELETE_TRXMEDICAL(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_DELETE_TRXMEDICAL] 
@pstrMedicalCd Varchar(10)
AS
BEGIN
	--reset root & root of root
	/*UPDATE trx_medical
	SET medical_trx_st='MEDICAL_TRX_ST_0'
	WHERE medical_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical WHERE medical_cd=@pstrMedicalCd)
	
	UPDATE trx_medical
	SET medical_trx_st='MEDICAL_TRX_ST_0'
	WHERE medical_root_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical WHERE medical_cd=@pstrMedicalCd)*/
	--end reset root
		
	--UPDATE trx_medical_record SET medical_cd=NULL WHERE medical_cd=@pstrMedicalCd
	DELETE FROM trx_medical_record WHERE medical_cd=@pstrMedicalCd
	DELETE FROM trx_medical_anamnesa WHERE medical_cd=@pstrMedicalCd
	
	DELETE FROM trx_medical_paramedis WHERE ref_proses_seqno IN (SELECT medical_tindakan_seqno FROM trx_medical_tindakan WHERE medical_cd=@pstrMedicalCd)
	DELETE FROM trx_medical_tindakan WHERE medical_cd=@pstrMedicalCd
	
	DELETE FROM trx_medicalunit_property WHERE medical_unit_seqno IN (SELECT medical_unit_seqno FROM trx_medical_unit WHERE medical_cd=@pstrMedicalCd)
	DELETE FROM trx_medical_unit WHERE medical_cd=@pstrMedicalCd
	
	DELETE FROM trx_medical_ruang WHERE medical_cd=@pstrMedicalCd
	UPDATE trx_ruang SET ruang_st='0' WHERE ruang_cd=(SELECT ruang_cd FROM trx_medical WHERE medical_cd=@pstrMedicalCd)
	
	DELETE FROM trx_medical_dokter WHERE medical_cd=@pstrMedicalCd
	
	DELETE FROM trx_medical_alkes WHERE medical_cd=@pstrMedicalCd
	
	DELETE FROM trx_resep_racik WHERE resep_seqno IN (SELECT resep_seqno FROM trx_resep_data WHERE medical_resep_seqno IN (SELECT medical_resep_seqno FROM trx_medical_resep WHERE medical_cd=@pstrMedicalCd))
	DELETE FROM trx_resep_data WHERE medical_resep_seqno IN (SELECT medical_resep_seqno FROM trx_medical_resep WHERE medical_cd=@pstrMedicalCd)
	DELETE FROM trx_medicalresep_param WHERE medical_resep_seqno IN (SELECT medical_resep_seqno FROM trx_medical_resep WHERE medical_cd=@pstrMedicalCd)
	DELETE FROM trx_medical_resep WHERE medical_cd=@pstrMedicalCd
	
	DELETE FROM trx_retur_data WHERE retur_seqno IN (SELECT retur_seqno FROM trx_resep_retur WHERE medical_cd=@pstrMedicalCd)
	DELETE FROM trx_resep_retur WHERE medical_cd=@pstrMedicalCd
	
	DELETE FROM trx_deposit WHERE medical_cd=@pstrMedicalCd
	DELETE FROM trx_medical_settlement WHERE medical_cd=@pstrMedicalCd
	DELETE FROM trx_medical_settlement_inv WHERE medical_cd=@pstrMedicalCd
	
	DELETE FROM trx_settlement_account WHERE settlement_no IN (SELECT settlement_no FROM trx_settlement WHERE medical_cd=@pstrMedicalCd)
	DELETE FROM trx_settlement WHERE medical_cd=@pstrMedicalCd
	
	DELETE FROM trx_medical_insurance WHERE medical_cd=@pstrMedicalCd
	
	DELETE FROM trx_medical WHERE medical_cd=@pstrMedicalCd
END;

DROP FUNCTION IF EXISTS SP_EDIT_INVITEM(Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,numeric,numeric,Varchar,numeric,numeric,numeric,numeric,Varchar,Char,Char,Char,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_EDIT_INVITEM] 
@pstrItemCd Varchar(20),
@pstrTypeCd Varchar(20),
@pstrUnitCd Varchar(20),
@pstrItemNm Varchar(100),
@pstrBarcode Varchar(50),
@pstrCurrCd Varchar(1000),
@pnumItemPriceBuy numeric(15),
@pnumItemPrice numeric(15),
@pstrVatTp Varchar(20),
@pnumPpn numeric(5,2),
@pnumReorder numeric(10,2),
@pnumStokMin numeric(10,2),
@pnumStokMax numeric(10,2),
@pstrTarifTp Varchar(20),
@pstrGenericSt Char(1),
@pstrActiveSt Char(1),
@pstrInventorySt Char(1),
@pstrUserId Varchar(20)
AS
BEGIN
	UPDATE inv_item_master SET type_cd=@pstrTypeCd, unit_cd=@pstrUnitCd, item_nm=@pstrItemNm,
	barcode=@pstrBarcode, currency_cd=@pstrCurrCd,
	item_price_buy=@pnumItemPriceBuy, item_price=@pnumItemPrice, vat_tp=@pstrVatTp, ppn=@pnumPpn,
	reorder_point=@pnumReorder, minimum_stock=@pnumStokMin, maximum_stock=@pnumStokMax,
	tariftp_cd=@pstrTarifTp,generic_st=@pstrGenericSt,active_st=@pstrActiveSt,inventory_st=@pstrInventorySt,
	modi_id=@pstrUserId, modi_datetime=GETDATE()
	WHERE item_cd=@pstrItemCd
	
	DECLARE @strAccountCd Varchar(20)
	--HARDCODE
	SET @strAccountCd = 'AC301'
	--End HARDCODE
	
	--Tarif
	IF EXISTS(SELECT item_cd FROM trx_tarif_inventori
			  WHERE item_cd=@pstrItemCd
			  AND ISNULL(kelas_cd,'')=''
			  AND ISNULL(insurance_cd,'')='')
		UPDATE trx_tarif_inventori SET tarif=@pnumItemPrice
		WHERE item_cd=@pstrItemCd
		AND ISNULL(kelas_cd,'')=''
		AND ISNULL(insurance_cd,'')=''
	ELSE
		INSERT INTO trx_tarif_inventori (item_cd,tarif,account_cd,kelas_cd,insurance_cd,modi_id,modi_datetime) 
		VALUES (@pstrItemCd,@pnumItemPrice,@strAccountCd,'','',@pstrUserId,GETDATE())
END;

DROP FUNCTION IF EXISTS SP_EDIT_PASIEN(Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Datetime,int,Varchar,Varchar,Varchar,Varchar,numeric,numeric,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Datetime,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_EDIT_PASIEN] 
@pstrPasienCd Varchar(20),
@pstrPasienNm Varchar(100),
@pstrNoRM Varchar(10),
@pstrPasienTp Varchar(20),
@pstrAddress Varchar(1000),
@pstrProp Varchar(100),
@pstrKab Varchar(100),
@pstrKec Varchar(100),
@pstrKel Varchar(100),
@pstrKodePost Varchar(20),
@pstrBirthPlace Varchar(100),
@pdtBirthDate Datetime,
@pintUmur int,
@pstrPhone Varchar(20),
@pstrMobile Varchar(20),
@pstrEmail Varchar(100),
@pstrBloodTp Varchar(20),
@pnumBerat numeric(5,2),
@pnumTinggi numeric(5,2),
@pstrGenderTp Varchar(20),
@pstrMaritalTp Varchar(20),
@pstrNationCd Varchar(20),
@pstrAgamaCd Varchar(20),
@pstrPekerjaanCd Varchar(20),
@pstrPendidikanCd Varchar(20),
@pstrRasCd Varchar(20),
@pstrIdentitas Varchar(20),
@pstrIdNo Varchar(50),
@pstrAsuransiCd Varchar(20),
@pstrAsuransiNo Varchar(50),
@pstrAsrPerusahaanCd Varchar(20),
@pstrAsrKelasCd Varchar(20),
@pstrAyahNm Varchar(100),
@pstrIbuNm Varchar(100),
@pstrPenanggungNm Varchar(100),
@pstrPenanggungTp Varchar(20),
@pstrPenanggungAddress Varchar(1000),
@pstrPenanggungPhone Varchar(20),
@pstrPenanggungBirth Datetime,
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @strNoRM Varchar(10)
	IF @pstrNoRM <> ''
	BEGIN
		SET @strNoRM = simrke.FN_FORMAT_STRING(@pstrNoRM,'0',8)
	END
	ELSE
	BEGIN
		SET @strNoRM = ''
	END
	
	UPDATE trx_pasien SET no_rm=@strNoRM, pasien_tp=@pstrPasienTp, pasien_nm=@pstrPasienNm, 
	address=@pstrAddress, region_prop=@pstrProp, region_kab=@pstrKab, region_kec=@pstrKec, region_kel=@pstrKel, postcode=@pstrKodePost, 
	phone=@pstrPhone, mobile=@pstrMobile, email=@pstrEmail, birth_place=@pstrBirthPlace, birth_date=@pdtBirthDate, age=@pintUmur, 
	blood_tp=@pstrBloodTp, weight=@pnumBerat, height=@pnumTinggi, gender_tp=@pstrGenderTp, marital_tp=@pstrMaritalTp, 
	nation_cd=@pstrNationCd, religion_cd=@pstrAgamaCd, occupation_cd=@pstrPekerjaanCd, education_cd=@pstrPendidikanCd, race_cd=@pstrRasCd, 
	modi_id=@pstrUserId, modi_datetime=GETDATE()
	WHERE pasien_cd=@pstrPasienCd
	
	DELETE FROM trx_pasien_identity WHERE pasien_cd=@pstrPasienCd
	IF @pstrIdentitas IS NOT NULL
	BEGIN
		INSERT INTO trx_pasien_identity (pasien_cd,identity_tp,id_no,modi_id,modi_datetime)
		VALUES (@pstrPasienCd,@pstrIdentitas,@pstrIdNo,@pstrUserId,GETDATE())
	END
	
	DELETE FROM trx_pasien_family WHERE pasien_cd=@pstrPasienCd
	IF @pstrAyahNm <> ''
	BEGIN
		INSERT INTO trx_pasien_family (pasien_cd,family_tp,family_nm,modi_id,modi_datetime)
		VALUES (@pstrPasienCd,'FAMILY_TP_01',@pstrAyahNm,@pstrUserId,GETDATE())
	END
	IF @pstrIbuNm <> ''
	BEGIN
		INSERT INTO trx_pasien_family (pasien_cd,family_tp,family_nm,modi_id,modi_datetime)
		VALUES (@pstrPasienCd,'FAMILY_TP_02',@pstrIbuNm,@pstrUserId,GETDATE())
	END
	IF @pstrPenanggungNm <> ''
	BEGIN
		IF @pstrPenanggungTp IN ('FAMILY_TP_01','FAMILY_TP_02')
			SET @pstrPenanggungTp = 'FAMILY_TP_00'
			
		INSERT INTO trx_pasien_family (pasien_cd,family_tp,family_nm,address,phone,birth_date,modi_id,modi_datetime)
		VALUES (@pstrPasienCd,@pstrPenanggungTp,@pstrPenanggungNm,@pstrPenanggungAddress,@pstrPenanggungPhone,@pstrPenanggungBirth,@pstrUserId,GETDATE())
	END
	
	DELETE FROM trx_pasien_insurance WHERE pasien_cd=@pstrPasienCd
	IF @pstrAsuransiCd IS NOT NULL
	BEGIN
		INSERT INTO trx_pasien_insurance (pasien_cd,insurance_cd,insurance_no,standar_kelas_cd,default_st,modi_id,modi_datetime)
		VALUES (@pstrPasienCd,@pstrAsuransiCd,@pstrAsuransiNo,@pstrAsrKelasCd,'1',@pstrUserId,GETDATE())
	END
END;

DROP FUNCTION IF EXISTS SP_EXECUTE_QUERY(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_EXECUTE_QUERY] 
@pstrQuery Varchar(2000)
AS
BEGIN
	EXEC(@pstrQuery)
END;

DROP FUNCTION IF EXISTS SP_GET_INVOICE(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_INVOICE]
@pintSettlementNo bigint
AS
BEGIN
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
	INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,simrke.FN_GET_DEPOSIT_TOTAL(A.medical_cd) AS total_deposit,
	A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
END;

DROP FUNCTION IF EXISTS SP_GET_INVOICE_DEPOSIT(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_INVOICE_DEPOSIT]
@pintSeqno bigint
AS
BEGIN
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,
	CASE WHEN A.total=0 THEN A.note ELSE 'TITIP JAMINAN PASIEN : ' +  C.pasien_nm END AS account_nm,
	/*--'TITIP JAMINAN PASIEN : ' +  C.pasien_nm AS account_nm,--*/
	A.total AS amount,A.amount_tunai,A.amount_nontunai,
	ISNULL(simrke.FN_GET_TERBILANG(A.total),'') AS total_terbilang
	FROM trx_deposit A 
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.seq_no=@pintSeqno
END;

DROP FUNCTION IF EXISTS SP_GET_INVOICE_EXT_BYDATE(Varchar,Varchar,Varchar,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_INVOICE_EXT_BYDATE]
@pdtDateStart Varchar(20),
@pdtDateEnd Varchar(20),
@pstrParam1 Varchar(100),
@pstrParam2 Varchar(100),
@pstrUserId Varchar(20),
@pstrUserNm Varchar(100)
AS
BEGIN
	IF @pstrUserId = '' 
	/*--Semua--*/
		SELECT A.settlement_no,A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,B.pasien_nm,
		B.address + ' ' + KEL.region_nm + ' ' + KEC.region_nm AS address,A.ref_medical_resep_seqno,
		simrke.FN_FORMATDATE(A.ext_date) AS ext_date,A.ext_amount
		FROM trx_settlement A 
		LEFT JOIN trx_pasien B ON A.pasien_cd=B.pasien_cd 
		LEFT JOIN com_region KEL ON B.region_kel=KEL.region_cd 
		LEFT JOIN com_region KEC ON B.region_kec=KEC.region_cd 
		WHERE ISNULL(A.ext_st,'')='0' 
		AND ISNULL(A.medical_cd,'')<>''
		AND A.ext_date>=simrke.FN_STRINGTODATETIME(@pdtDateStart) AND A.ext_date<=simrke.FN_STRINGTODATETIME(@pdtDateEnd)
		AND (no_rm LIKE @pstrParam1 OR pasien_nm LIKE @pstrParam1)
		AND invoice_no LIKE @pstrParam2
		UNION 
		SELECT A.settlement_no,A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,B.pasien_nm,
		B.info_01 AS address,A.ref_medical_resep_seqno,
		simrke.FN_FORMATDATE(A.ext_date) AS ext_date,A.ext_amount
		FROM trx_settlement A 
		JOIN trx_medical_resep B ON A.ref_medical_resep_seqno=B.medical_resep_seqno 
		WHERE ISNULL(A.ext_st,'')='0' 
		AND ISNULL(A.medical_cd,'')='' 
		AND A.ext_date>=simrke.FN_STRINGTODATETIME(@pdtDateStart) AND A.ext_date<=simrke.FN_STRINGTODATETIME(@pdtDateEnd)
		AND invoice_no LIKE @pstrParam2
		ORDER BY ext_date,A.invoice_no
	ELSE
	/*--By User--*/
		SELECT A.settlement_no,A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,B.pasien_nm,
		B.address + ' ' + KEL.region_nm + ' ' + KEC.region_nm AS address,A.ref_medical_resep_seqno,
		simrke.FN_FORMATDATE(A.ext_date) AS ext_date,A.ext_amount
		FROM trx_settlement A 
		LEFT JOIN trx_pasien B ON A.pasien_cd=B.pasien_cd 
		LEFT JOIN com_region KEL ON B.region_kel=KEL.region_cd 
		LEFT JOIN com_region KEC ON B.region_kec=KEC.region_cd 
		WHERE ISNULL(A.ext_st,'')='0' 
		AND ISNULL(A.medical_cd,'')<>''
		AND A.ext_user=@pstrUserId
		AND A.ext_date>=simrke.FN_STRINGTODATETIME(@pdtDateStart) AND A.ext_date<=simrke.FN_STRINGTODATETIME(@pdtDateEnd)
		AND (no_rm LIKE @pstrParam1 OR pasien_nm LIKE @pstrParam1)
		AND invoice_no LIKE @pstrParam2
		UNION 
		SELECT A.settlement_no,A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,B.pasien_nm,
		B.info_01 AS address,A.ref_medical_resep_seqno,
		simrke.FN_FORMATDATE(A.ext_date) AS ext_date,A.ext_amount
		FROM trx_settlement A 
		JOIN trx_medical_resep B ON A.ref_medical_resep_seqno=B.medical_resep_seqno 
		WHERE ISNULL(A.ext_st,'')='0' 
		AND ISNULL(A.medical_cd,'')='' 
		AND A.ext_user=@pstrUserId
		AND A.ext_date>=simrke.FN_STRINGTODATETIME(@pdtDateStart) AND A.ext_date<=simrke.FN_STRINGTODATETIME(@pdtDateEnd)
		AND invoice_no LIKE @pstrParam2
		ORDER BY ext_date,A.invoice_no
END;

DROP FUNCTION IF EXISTS SP_GET_INVOICE_INCOME(bigint,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_INVOICE_INCOME] 
@pintMedicalResepNo bigint,
@pstrUserNm Varchar(100)
AS
BEGIN
	DECLARE @numTotalPrice numeric
	DECLARE @strItemCd varchar(20)
	DECLARE @numJumlah numeric
	DECLARE @numAmount numeric
	DECLARE @intTotal int
	
	DECLARE @intResepSeqno bigint
	
	SET @intTotal = 0
	SET @numTotalPrice = 0
	DECLARE cursorData CURSOR FOR
	SELECT B.item_cd,B.quantity,B.price,B.resep_seqno
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	WHERE A.medical_resep_seqno=@pintMedicalResepNo
	ORDER BY B.resep_seqno
		
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strItemCd,@numJumlah,@numAmount,@intResepSeqno
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @numTotalPrice = @numTotalPrice + (@numAmount * @numJumlah)
		
		FETCH NEXT FROM cursorData
		INTO @strItemCd,@numJumlah,@numAmount,@intResepSeqno
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	--end total price
	
	SELECT DISTINCT B.resep_seqno,B.item_cd,A.resep_date,
	C.item_nm,B.quantity,B.price AS item_price,A.medical_resep_seqno,
	@numTotalPrice AS total_price,
	ISNULL(simrke.FN_GET_TERBILANG(@numTotalPrice),'') AS total_terbilang,
	A.pasien_nm,A.info_01
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	WHERE A.medical_resep_seqno=@pintMedicalResepNo
	ORDER BY B.resep_seqno
END;

DROP FUNCTION IF EXISTS SP_GET_INVOICE_ITEMSALE(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_INVOICE_ITEMSALE]
@pintSettlementNo bigint
AS
BEGIN
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
	A.entry_nm,
	CASE WHEN ISNULL(A.pay_nm,'')<>'' THEN A.pay_nm ELSE RSALE.pasien_nm END AS pay_nm,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN C.pasien_nm ELSE RSALE.pasien_nm END AS pasien_nm,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN C.no_rm ELSE '' END AS no_rm,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN C.address ELSE RSALE.info_01 END AS address,
	RSALE.dr_nm,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,
	A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
	INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	JOIN trx_medical_settlement_inv INVSTL ON ((A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd) OR (A.ref_medical_resep_seqno=INVSTL.ref_medical_resep_seqno))
	LEFT JOIN trx_medical_resep RSALE ON A.ref_medical_resep_seqno=RSALE.medical_resep_seqno
	LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
	LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	WHERE A.settlement_no=@pintSettlementNo
	ORDER BY B.account_cd,detail_nm
END;

DROP FUNCTION IF EXISTS SP_GET_INVOICE_PERITEM(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_INVOICE_PERITEM]
@pintSettlementNo bigint
AS
BEGIN
	DECLARE @strMedicalCd varchar(10)
	
	SET @strMedicalCd = (SELECT TOP 1 medical_cd FROM trx_settlement WHERE settlement_no=@pintSettlementNo)
								
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
	INS.insurance_nm,A.pay_nm,A.entry_nm,
	C.pasien_nm,C.no_rm,C.address,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,
	A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	MEDSTL.data_cd AS detail_cd,MEDSTL.data_nm AS detail_nm,MEDSTL.quantity  AS quantity,MEDSTL.amount AS detail_amount
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	JOIN trx_medical_settlement MEDSTL ON A.invoice_no=MEDSTL.invoice_no AND B.account_cd=MEDSTL.account_cd
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	JOIN trx_medical D ON A.medical_cd=D.medical_cd
	--LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	--LEFT JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
	UNION ALL
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
	INS.insurance_nm,A.pay_nm,A.entry_nm,
	C.pasien_nm,C.no_rm,C.address,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,
	A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
	INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	JOIN trx_medical_settlement_inv INVSTL ON A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd
	--JOIN trx_medical_settlement_inv INVSTL ON ((A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd) OR (A.ref_medical_resep_seqno=INVSTL.ref_medical_resep_seqno))
	LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	JOIN trx_medical D ON A.medical_cd=D.medical_cd
	--LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	--LEFT JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
	ORDER BY B.account_cd,detail_nm
END;

DROP FUNCTION IF EXISTS SP_GET_INVOICE_TEMP(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_INVOICE_TEMP]
@pintSettlementNo bigint
AS
BEGIN
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	ACC.account_nm,B.amount,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,0 AS total_deposit,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	info_11 AS payment_bayar,info_12 AS payment_sisa,note
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
END;

DROP FUNCTION IF EXISTS SP_GET_INVOICEDETAIL(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_INVOICEDETAIL]
@pintSettlementNo bigint
AS
BEGIN
	DECLARE @strMedicalCd varchar(10)
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalRootRootCd varchar(10)
	DECLARE @strMedicalChildCd varchar(10)
	DECLARE @strDiagnosa varchar(1000)
	
	SET @strMedicalCd = (SELECT TOP 1 medical_cd FROM trx_settlement WHERE settlement_no=@pintSettlementNo)
	/*
	DECLARE @strInvoiceNo varchar(25)
	SELECT TOP 1 @strMedicalCd=medical_cd,@strInvoiceNo=invoice_no
	FROM trx_settlement
	WHERE settlement_no=@pintSettlementNo*/
	
	SET @strMedicalRootCd = (SELECT medical_root_cd FROM trx_medical WHERE medical_cd=@strMedicalCd)
	/*SET @strMedicalRootRootCd = (SELECT A.medical_cd
								FROM trx_medical A 
								WHERE A.medical_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical
													WHERE medical_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical WHERE medical_root_cd=@strMedicalRootCd)
													)
								)*/
	SET @strMedicalRootRootCd = (SELECT A.medical_root_cd
								FROM trx_medical A 
								WHERE A.medical_cd=@strMedicalRootCd)
								
	SET @strMedicalChildCd = (SELECT TOP 1 medical_cd FROM trx_medical WHERE medical_root_cd=@strMedicalCd)
	
	--Get diagnosa
	IF EXISTS(SELECT medical_data FROM trx_medical_record WHERE medical_cd=@strMedicalCd AND rm_tp='RM_TP_1')
	BEGIN
		SET @strDiagnosa = (SELECT TOP 1 medical_data FROM trx_medical_record 
						    WHERE medical_cd=@strMedicalCd AND rm_tp='RM_TP_1'
							ORDER BY medical_record_seqno DESC)
	END
	ELSE
	BEGIN
		SET @strDiagnosa = (SELECT TOP 1 medical_data FROM trx_medical_record 
						    WHERE medical_cd=@strMedicalCd
							ORDER BY medical_record_seqno DESC)
	END
	
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
	INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,
	simrke.FN_GET_DEPOSIT_TOTAL(A.medical_cd) AS total_deposit,A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	
	MEDSTL.data_cd AS detail_cd,CASE WHEN D.medical_tp='MEDICAL_TP_01' THEN MEDSTL.data_nm ELSE MEDSTL.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS detail_nm,
	--MEDSTL.data_cd AS detail_cd,MEDSTL.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') AS detail_nm,
	--MEDSTL.data_cd AS detail_cd,MEDSTL.data_nm AS detail_nm,
	/*MEDSTL.data_cd AS detail_cd,
	CASE WHEN MEDSTL.tarif_tp='TARIF_TP_02' THEN MEDSTL.data_nm + ' - ' + ISNULL(MUNITDR.dr_nm,'')
	ELSE
		CASE WHEN MEDSTL.tarif_tp='TARIF_TP_04' THEN MEDSTL.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'')
		ELSE
			MEDSTL.data_nm
		END
	END AS detail_nm,*/
	
	MEDSTL.quantity AS quantity,MEDSTL.amount AS detail_amount,MEDSTL.item_price AS item_price,
	MEDSTL.data_nm AS urutan,MTKDR.dr_nm AS mtk_dr_nm,
	CASE WHEN ISNULL(MTKUNITRJ.medunit_cd,'')<>'' THEN MTKUNITRJ.medunit_nm ELSE MTKUNITRI.bangsal_nm END AS mtk_medunit_nm,
	@strDiagnosa AS diagnosa
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	--JOIN trx_medical_settlement MEDSTL ON (A.medical_cd=MEDSTL.medical_cd AND B.account_cd=MEDSTL.account_cd)
	--									  OR (A.invoice_no=MEDSTL.invoice_no AND B.account_cd=MEDSTL.account_cd)
	JOIN trx_medical_settlement MEDSTL ON (A.invoice_no=MEDSTL.invoice_no AND B.account_cd=MEDSTL.account_cd)
	LEFT JOIN trx_medical_tindakan MTK ON MEDSTL.ref_seqno=MTK.medical_tindakan_seqno AND MEDSTL.tarif_tp='TARIF_TP_04'
	LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
	LEFT JOIN trx_unit_medis MTKUNITRJ ON MTK.medunit_cd=MTKUNITRJ.medunit_cd
	LEFT JOIN trx_bangsal MTKUNITRI ON MTK.medunit_cd=MTKUNITRI.bangsal_cd
	LEFT JOIN trx_medical_unit MUNIT ON MEDSTL.ref_seqno=MUNIT.medical_unit_seqno AND MEDSTL.tarif_tp='TARIF_TP_02'
	LEFT JOIN trx_dokter MUNITDR ON MUNIT.dr_cd=MUNITDR.dr_cd
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
	UNION ALL
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
	INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,
	simrke.FN_GET_DEPOSIT_TOTAL(A.medical_cd) AS total_deposit,A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
	INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount,INVSTL.item_price AS item_price,
	CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' 
	ELSE CASE WHEN INVSTL.item_cd IN ('NONINV101','NONINV102','NONINV103') THEN 'ZZ' + INV.item_nm ELSE INV.item_nm END
	END 
	AS urutan,
	'' AS mtk_dr_nm,'' AS mtk_medunit_nm,@strDiagnosa AS diagnosa
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	--JOIN trx_medical_settlement_inv INVSTL ON (A.medical_cd=INVSTL.medical_cd AND B.account_cd=INVSTL.account_cd)
	--										  OR (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
	JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
	LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
	ORDER BY B.account_cd,urutan
	/*
	UNION ALL
	--transaksi root
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	MEDSTL.data_cd AS detail_cd,MEDSTL.data_nm AS detail_nm,MEDSTL.quantity  AS quantity,MEDSTL.amount AS detail_amount
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	--JOIN trx_medical_settlement MEDSTL ON @strMedicalRootCd=MEDSTL.medical_cd AND B.account_cd=MEDSTL.account_cd
	JOIN trx_medical_settlement MEDSTL ON (A.invoice_no=MEDSTL.invoice_no AND @strMedicalRootCd=MEDSTL.medical_cd AND B.account_cd=MEDSTL.account_cd)
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON @strMedicalRootCd=D.medical_cd 
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
	UNION ALL
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
	INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	--JOIN trx_medical_settlement_inv INVSTL ON @strMedicalRootCd=INVSTL.medical_cd AND B.account_cd=INVSTL.account_cd
	JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
	LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON @strMedicalRootCd=D.medical_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
	
	UNION ALL
	--transaksi root of root
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	MEDSTL.data_cd AS detail_cd,MEDSTL.data_nm AS detail_nm,MEDSTL.quantity  AS quantity,MEDSTL.amount AS detail_amount
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	--JOIN trx_medical_settlement MEDSTL ON @strMedicalRootRootCd=MEDSTL.medical_cd AND B.account_cd=MEDSTL.account_cd
	JOIN trx_medical_settlement MEDSTL ON (A.invoice_no=MEDSTL.invoice_no AND A.invoice_no=MEDSTL.invoice_no AND B.account_cd=MEDSTL.account_cd)
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON @strMedicalRootRootCd=D.medical_cd 
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
	UNION ALL
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
	INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	--JOIN trx_medical_settlement_inv INVSTL ON @strMedicalRootRootCd=INVSTL.medical_cd AND B.account_cd=INVSTL.account_cd
	JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
	LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON @strMedicalRootRootCd=D.medical_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
	
	--PROSES TRANSAKSI CHILD
	UNION ALL
	--transaksi child
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	MEDSTL.data_cd AS detail_cd,MEDSTL.data_nm AS detail_nm,MEDSTL.quantity  AS quantity,MEDSTL.amount AS detail_amount
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	--JOIN trx_medical_settlement MEDSTL ON @strMedicalChildCd=MEDSTL.medical_cd AND B.account_cd=MEDSTL.account_cd
	JOIN trx_medical_settlement MEDSTL ON (A.invoice_no=MEDSTL.invoice_no AND A.invoice_no=MEDSTL.invoice_no AND B.account_cd=MEDSTL.account_cd)
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON @strMedicalChildCd=D.medical_cd 
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
	UNION ALL
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
	INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	--JOIN trx_medical_settlement_inv INVSTL ON @strMedicalChildCd=INVSTL.medical_cd AND B.account_cd=INVSTL.account_cd
	JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
	LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON @strMedicalChildCd=D.medical_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
	--END PROSES TRANSAKSI CHILD
	*/
	--ORDER BY B.account_cd,detail_nm
END;

DROP FUNCTION IF EXISTS SP_GET_INVOICEDETAIL_KEUANGAN(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_INVOICEDETAIL_KEUANGAN]
@pintSettlementNo bigint
AS
BEGIN
	DECLARE @strMedicalCd varchar(10)
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalRootRootCd varchar(10)
	DECLARE @strMedicalChildCd varchar(10)
	
	SET @strMedicalCd = (SELECT TOP 1 medical_cd FROM trx_settlement WHERE settlement_no=@pintSettlementNo)
	
	/*
	SET @strMedicalRootCd = (SELECT medical_root_cd FROM trx_medical WHERE medical_cd=@strMedicalCd)
	SET @strMedicalRootRootCd = (SELECT A.medical_root_cd
								FROM trx_medical A 
								WHERE A.medical_cd=@strMedicalRootCd)
								
	SET @strMedicalChildCd = (SELECT TOP 1 medical_cd FROM trx_medical WHERE medical_root_cd=@strMedicalCd)
	*/
								
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
	INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,
	simrke.FN_GET_DEPOSIT_TOTAL(A.medical_cd) AS total_deposit,A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	
	MEDSTL.data_cd AS detail_cd,CASE WHEN D.medical_tp='MEDICAL_TP_01' THEN MEDSTL.data_nm ELSE MEDSTL.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS detail_nm,
	--MEDSTL.data_cd AS detail_cd,MEDSTL.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') AS detail_nm,
	--MEDSTL.data_cd AS detail_cd,MEDSTL.data_nm AS detail_nm,
	/*MEDSTL.data_cd AS detail_cd,
	CASE WHEN MEDSTL.tarif_tp='TARIF_TP_02' THEN MEDSTL.data_nm + ' - ' + ISNULL(MUNITDR.dr_nm,'')
	ELSE
		CASE WHEN MEDSTL.tarif_tp='TARIF_TP_04' THEN MEDSTL.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'')
		ELSE
			MEDSTL.data_nm
		END
	END AS detail_nm,*/
	
	MEDSTL.quantity AS quantity,MEDSTL.amount AS detail_amount,MEDSTL.item_price AS item_price,
	MEDSTL.data_nm AS urutan,MTKDR.dr_nm AS mtk_dr_nm,
	CASE WHEN ISNULL(MTKUNITRJ.medunit_cd,'')<>'' THEN MTKUNITRJ.medunit_nm ELSE MTKUNITRI.bangsal_nm END AS mtk_medunit_nm,
	simrke.FN_RPT_GET_TARIFTP(MEDSTL.tarif_tp,'1',MEDSTL.data_cd,@strMedicalCd,MEDSTL.ref_seqno,MEDSTL.item_price) AS JS,
	simrke.FN_RPT_GET_TARIFTP(MEDSTL.tarif_tp,'2',MEDSTL.data_cd,@strMedicalCd,MEDSTL.ref_seqno,MEDSTL.item_price) AS JM,
	simrke.FN_RPT_GET_TARIFTP(MEDSTL.tarif_tp,'5',MEDSTL.data_cd,@strMedicalCd,MEDSTL.ref_seqno,MEDSTL.item_price) AS BHP
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	--JOIN trx_medical_settlement MEDSTL ON (A.medical_cd=MEDSTL.medical_cd AND B.account_cd=MEDSTL.account_cd)
	--									  OR (A.invoice_no=MEDSTL.invoice_no AND B.account_cd=MEDSTL.account_cd)
	JOIN trx_medical_settlement MEDSTL ON (A.invoice_no=MEDSTL.invoice_no AND B.account_cd=MEDSTL.account_cd)
	LEFT JOIN trx_medical_tindakan MTK ON MEDSTL.ref_seqno=MTK.medical_tindakan_seqno AND MEDSTL.tarif_tp='TARIF_TP_04'
	LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
	LEFT JOIN trx_unit_medis MTKUNITRJ ON MTK.medunit_cd=MTKUNITRJ.medunit_cd
	LEFT JOIN trx_bangsal MTKUNITRI ON MTK.medunit_cd=MTKUNITRI.bangsal_cd
	LEFT JOIN trx_medical_unit MUNIT ON MEDSTL.ref_seqno=MUNIT.medical_unit_seqno AND MEDSTL.tarif_tp='TARIF_TP_02'
	LEFT JOIN trx_dokter MUNITDR ON MUNIT.dr_cd=MUNITDR.dr_cd
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
	UNION ALL
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
	INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,
	simrke.FN_FORMATDATE(D.datetime_in) AS datetime_in,simrke.FN_FORMATDATE(D.datetime_out) AS datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo) AS total_invoice,
	simrke.FN_GET_DEPOSIT_TOTAL(A.medical_cd) AS total_deposit,A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
	ISNULL(simrke.FN_GET_TERBILANG(simrke.FN_GET_INVOICE_TOTAL(@pintSettlementNo)),'') AS total_terbilang,
	INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
	INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount,INVSTL.item_price AS item_price,
	CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' 
	ELSE CASE WHEN INVSTL.item_cd IN ('NONINV101','NONINV102','NONINV103') THEN 'ZZ' + INV.item_nm ELSE INV.item_nm END
	END 
	AS urutan,
	'' AS mtk_dr_nm,'' AS mtk_medunit_nm,
	simrke.FN_RPT_GET_TARIFTP('TARIF_TP_05','1',INVSTL.item_cd,@strMedicalCd,0,INVSTL.item_price) AS JS,
	simrke.FN_RPT_GET_TARIFTP('TARIF_TP_05','2',INVSTL.item_cd,@strMedicalCd,0,INVSTL.item_price) AS JM,
	simrke.FN_RPT_GET_TARIFTP('TARIF_TP_05','5',INVSTL.item_cd,@strMedicalCd,0,INVSTL.item_price) AS BHP
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	--JOIN trx_medical_settlement_inv INVSTL ON (A.medical_cd=INVSTL.medical_cd AND B.account_cd=INVSTL.account_cd)
	--										  OR (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
	JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
	LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.settlement_no=@pintSettlementNo
	ORDER BY B.account_cd,urutan
END;

DROP FUNCTION IF EXISTS SP_GET_SETTLEMENT(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_SETTLEMENT] 
@pstrMedicalCd Varchar(10)
AS
SET NOCOUNT ON
BEGIN
	DECLARE @strMedicalTp varchar(20)
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalRootRootCd varchar(10)
	DECLARE @strMedicalChildCd varchar(10)
	
	--SET @strMedicalRootCd = (SELECT medical_root_cd FROM trx_medical WHERE medical_cd=@pstrMedicalCd)
	SELECT @strMedicalRootCd=medical_root_cd,@strMedicalTp=medical_tp
	FROM trx_medical WHERE medical_cd=@pstrMedicalCd
	
	/*SET @strMedicalRootRootCd = (SELECT A.medical_cd
								FROM trx_medical A 
								WHERE A.medical_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical
													WHERE medical_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical WHERE medical_root_cd=@strMedicalRootCd)
													)
								)*/
	SET @strMedicalRootRootCd = (SELECT A.medical_root_cd
								FROM trx_medical A 
								WHERE A.medical_cd=@strMedicalRootCd)
	
	SET @strMedicalChildCd = (SELECT TOP 1 medical_cd FROM trx_medical WHERE medical_root_cd=@pstrMedicalCd)
	
	SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
	--A.data_cd,/*A.data_nm,*/A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') AS data_nm,
	A.data_cd,CASE WHEN @strMedicalTp='MEDICAL_TP_01' THEN A.data_nm ELSE A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS data_nm,
	A.amount,COM.code_nm AS tarif_tp,A.note,
	A.item_price,A.quantity, '' AS root,ref_seqno
	FROM trx_medical_settlement A 
	LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd 
	LEFT JOIN trx_medical_tindakan MTK ON A.ref_seqno=MTK.medical_tindakan_seqno AND A.tarif_tp='TARIF_TP_04'
	LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
	WHERE A.medical_cd=@pstrMedicalCd
	AND A.payment_st='PAYMENT_ST_0'
	UNION ALL 
	SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
	A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
	A.total_price,COM.code_nm AS tarif_tp,A.note,
	A.item_price,A.quantity, '' AS root,
	CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
	FROM trx_medical_settlement_inv A 
	LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
	LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
	WHERE A.medical_cd=@pstrMedicalCd
	AND A.payment_st='PAYMENT_ST_0'
	--+transaksi root (medical_root_cd)    
	UNION ALL 
	SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
	--A.data_cd,/*A.data_nm,*/A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') AS data_nm,
	A.data_cd,CASE WHEN @strMedicalTp='MEDICAL_TP_01' THEN A.data_nm ELSE A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS data_nm,
	A.amount,COM.code_nm AS tarif_tp,A.note,
	A.item_price,A.quantity,@strMedicalRootCd AS root,ref_seqno
	FROM trx_medical_settlement A 
	LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd
	LEFT JOIN trx_medical_tindakan MTK ON A.ref_seqno=MTK.medical_tindakan_seqno AND A.tarif_tp='TARIF_TP_04'
	LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
	WHERE A.medical_cd=@strMedicalRootCd
	AND A.payment_st='PAYMENT_ST_0'
	UNION ALL 
	SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
	A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
	A.total_price,COM.code_nm AS tarif_tp,A.note,
	A.item_price,A.quantity,@strMedicalRootCd AS root,
	CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
	FROM trx_medical_settlement_inv A 
	LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
	LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
	WHERE A.medical_cd=@strMedicalRootCd
	AND A.payment_st='PAYMENT_ST_0'
	--+transaksi root of root   
	UNION ALL 
	SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
	--A.data_cd,/*A.data_nm,*/A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') AS data_nm,
	A.data_cd,CASE WHEN @strMedicalTp='MEDICAL_TP_01' THEN A.data_nm ELSE A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS data_nm,
	A.amount,COM.code_nm AS tarif_tp,A.note,
	A.item_price,A.quantity,@strMedicalRootRootCd AS root,ref_seqno
	FROM trx_medical_settlement A 
	LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd
	LEFT JOIN trx_medical_tindakan MTK ON A.ref_seqno=MTK.medical_tindakan_seqno AND A.tarif_tp='TARIF_TP_04'
	LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
	WHERE A.medical_cd=@strMedicalRootRootCd
	AND A.payment_st='PAYMENT_ST_0'
	UNION ALL 
	SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
	A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
	A.total_price,COM.code_nm AS tarif_tp,A.note,
	A.item_price,A.quantity,@strMedicalRootRootCd AS root,
	CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
	FROM trx_medical_settlement_inv A 
	LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
	LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
	WHERE A.medical_cd=@strMedicalRootRootCd
	AND A.payment_st='PAYMENT_ST_0'
	
	--PROSES TRANSAKSI CHILD
	--+transaksi child  
	UNION ALL 
	SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
	--A.data_cd,/*A.data_nm,*/A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') AS data_nm,
	A.data_cd,CASE WHEN @strMedicalTp='MEDICAL_TP_01' THEN A.data_nm ELSE A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS data_nm,
	A.amount,COM.code_nm AS tarif_tp,A.note,
	A.item_price,A.quantity,'' AS root,ref_seqno
	FROM trx_medical_settlement A 
	LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd
	LEFT JOIN trx_medical_tindakan MTK ON A.ref_seqno=MTK.medical_tindakan_seqno AND A.tarif_tp='TARIF_TP_04'
	LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
	WHERE A.medical_cd=@strMedicalChildCd
	AND A.payment_st='PAYMENT_ST_0'
	UNION ALL 
	SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
	A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
	A.total_price,COM.code_nm AS tarif_tp,A.note,
	A.item_price,A.quantity,'' AS root,
	CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
	FROM trx_medical_settlement_inv A 
	LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
	LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
	WHERE A.medical_cd=@strMedicalChildCd
	AND A.payment_st='PAYMENT_ST_0'
	--END PROSES TRANSAKSI CHILD
	
	ORDER BY date_trx DESC,root DESC,tarif_tp
END
SET NOCOUNT OFF;
DROP FUNCTION IF EXISTS SP_GET_SETTLEMENT_BYPARAM(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_SETTLEMENT_BYPARAM] 
@pstrMedicalCd Varchar(10),
@pstrTarifTp Varchar(20)
AS
SET NOCOUNT ON
BEGIN
	DECLARE @strMedicalTp varchar(20)
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalRootRootCd varchar(10)
	DECLARE @strMedicalChildCd varchar(10)
		
	--SET @strMedicalRootCd = (SELECT medical_root_cd FROM trx_medical WHERE medical_cd=@pstrMedicalCd)
	SELECT @strMedicalRootCd=medical_root_cd,@strMedicalTp=medical_tp
	FROM trx_medical WHERE medical_cd=@pstrMedicalCd
	
	/*SET @strMedicalRootRootCd = (SELECT A.medical_cd
								FROM trx_medical A 
								WHERE A.medical_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical
													WHERE medical_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical WHERE medical_root_cd=@strMedicalRootCd)
													)
								)*/
	SET @strMedicalRootRootCd = (SELECT A.medical_root_cd
								FROM trx_medical A 
								WHERE A.medical_cd=@strMedicalRootCd)
								
	SET @strMedicalChildCd = (SELECT TOP 1 medical_cd FROM trx_medical WHERE medical_root_cd=@pstrMedicalCd)
								
	IF @pstrTarifTp = '' 
		SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
		--A.data_cd,/*A.data_nm,*/A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') AS data_nm,
		A.data_cd,CASE WHEN @strMedicalTp='MEDICAL_TP_01' THEN A.data_nm ELSE A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS data_nm,
		A.amount,COM.code_nm AS tarif_tp,A.note,
		A.item_price,A.quantity, '' AS root,ref_seqno
		FROM trx_medical_settlement A 
		LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd 
		LEFT JOIN trx_medical_tindakan MTK ON A.ref_seqno=MTK.medical_tindakan_seqno AND A.tarif_tp='TARIF_TP_04'
		LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
		WHERE A.medical_cd=@pstrMedicalCd
		AND A.payment_st='PAYMENT_ST_0'
		UNION ALL 
		SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
		A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
		A.total_price,COM.code_nm AS tarif_tp,A.note,
		A.item_price,A.quantity, '' AS root,
		CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
		FROM trx_medical_settlement_inv A 
		LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
		LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
		WHERE A.medical_cd=@pstrMedicalCd
		AND A.payment_st='PAYMENT_ST_0'
		--+transaksi root (medical_root_cd)
		UNION ALL 
		SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
		--A.data_cd,/*A.data_nm,*/A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') AS data_nm,
		A.data_cd,CASE WHEN @strMedicalTp='MEDICAL_TP_01' THEN A.data_nm ELSE A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS data_nm,
		A.amount,COM.code_nm AS tarif_tp,A.note,
		A.item_price,A.quantity,@strMedicalRootCd AS root,ref_seqno
		FROM trx_medical_settlement A 
		LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd
		LEFT JOIN trx_medical_tindakan MTK ON A.ref_seqno=MTK.medical_tindakan_seqno AND A.tarif_tp='TARIF_TP_04'
		LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
		WHERE A.medical_cd=@strMedicalRootCd
		AND A.payment_st='PAYMENT_ST_0'
		UNION ALL 
		SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
		A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
		A.total_price,COM.code_nm AS tarif_tp,A.note,
		A.item_price,A.quantity,@strMedicalRootCd AS root,
		CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
		FROM trx_medical_settlement_inv A 
		LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
		LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
		WHERE A.medical_cd=@strMedicalRootCd
		AND A.payment_st='PAYMENT_ST_0'
		--+transaksi root of root   
		UNION ALL 
		SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
		--A.data_cd,/*A.data_nm,*/A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') AS data_nm,
		A.data_cd,CASE WHEN @strMedicalTp='MEDICAL_TP_01' THEN A.data_nm ELSE A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS data_nm,
		A.amount,COM.code_nm AS tarif_tp,A.note,
		A.item_price,A.quantity,@strMedicalRootRootCd AS root,ref_seqno
		FROM trx_medical_settlement A 
		LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd 
		LEFT JOIN trx_medical_tindakan MTK ON A.ref_seqno=MTK.medical_tindakan_seqno AND A.tarif_tp='TARIF_TP_04'
		LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
		WHERE A.medical_cd=@strMedicalRootRootCd
		AND A.payment_st='PAYMENT_ST_0'
		UNION ALL 
		SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
		A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
		A.total_price,COM.code_nm AS tarif_tp,A.note,
		A.item_price,A.quantity,@strMedicalRootRootCd AS root,
		CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
		FROM trx_medical_settlement_inv A 
		LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
		LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
		WHERE A.medical_cd=@strMedicalRootRootCd
		AND A.payment_st='PAYMENT_ST_0'
		
		--PROSES TRANSAKSI CHILD
		--+transaksi child  
		UNION ALL 
		SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
		--A.data_cd,/*A.data_nm,*/A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') AS data_nm,
		A.data_cd,CASE WHEN @strMedicalTp='MEDICAL_TP_01' THEN A.data_nm ELSE A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS data_nm,
		A.amount,COM.code_nm AS tarif_tp,A.note,
		A.item_price,A.quantity,'' AS root,ref_seqno
		FROM trx_medical_settlement A 
		LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd 
		LEFT JOIN trx_medical_tindakan MTK ON A.ref_seqno=MTK.medical_tindakan_seqno AND A.tarif_tp='TARIF_TP_04'
		LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
		WHERE A.medical_cd=@strMedicalChildCd
		AND A.payment_st='PAYMENT_ST_0'
		UNION ALL 
		SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
		A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
		A.total_price,COM.code_nm AS tarif_tp,A.note,
		A.item_price,A.quantity,'' AS root,
		CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
		FROM trx_medical_settlement_inv A 
		LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
		LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
		WHERE A.medical_cd=@strMedicalChildCd
		AND A.payment_st='PAYMENT_ST_0'
		--END PROSES TRANSAKSI CHILD
		
		ORDER BY date_trx DESC,root DESC,tarif_tp
	ELSE
	BEGIN
		IF @pstrTarifTp = 'TARIF_TP_05' OR @pstrTarifTp = 'TARIF_TP_05B'
		BEGIN
			IF @pstrTarifTp = 'TARIF_TP_05'
				SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
				A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
				A.total_price,COM.code_nm AS tarif_tp,A.note,
				A.item_price,A.quantity, '' AS root,
				CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
				FROM trx_medical_settlement_inv A 
				LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
				LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05'
				WHERE A.medical_cd=@pstrMedicalCd
				AND COM.com_cd=@pstrTarifTp
				--AND ISNULL(A.ref_medical_resep_seqno,0)<>0
				AND (ISNULL(A.ref_medical_resep_seqno,0)<>0 OR A.item_cd='NONINV900')
				AND A.payment_st='PAYMENT_ST_0'
				UNION ALL 
				SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
				A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
				A.total_price,COM.code_nm AS tarif_tp,A.note,
				A.item_price,A.quantity,@strMedicalRootCd AS root,
				CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
				FROM trx_medical_settlement_inv A 
				LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
				LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
				WHERE A.medical_cd=@strMedicalRootCd
				AND COM.com_cd=@pstrTarifTp
				--AND ISNULL(A.ref_medical_resep_seqno,0)<>0
				AND (ISNULL(A.ref_medical_resep_seqno,0)<>0 OR A.item_cd='NONINV900')
				AND A.payment_st='PAYMENT_ST_0'
				UNION ALL 
				SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
				A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
				A.total_price,COM.code_nm AS tarif_tp,A.note,
				A.item_price,A.quantity,@strMedicalRootRootCd AS root,
				CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
				FROM trx_medical_settlement_inv A 
				LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
				LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
				WHERE A.medical_cd=@strMedicalRootRootCd
				AND COM.com_cd=@pstrTarifTp
				--AND ISNULL(A.ref_medical_resep_seqno,0)<>0
				AND (ISNULL(A.ref_medical_resep_seqno,0)<>0 OR A.item_cd='NONINV900')
				AND A.payment_st='PAYMENT_ST_0'
				
				--PROSES TRANSAKSI CHILD
				UNION ALL 
				SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
				A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
				A.total_price,COM.code_nm AS tarif_tp,A.note,
				A.item_price,A.quantity,'' AS root,
				CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
				FROM trx_medical_settlement_inv A 
				LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
				LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
				WHERE A.medical_cd=@strMedicalChildCd
				AND COM.com_cd=@pstrTarifTp
				--AND ISNULL(A.ref_medical_resep_seqno,0)<>0
				AND (ISNULL(A.ref_medical_resep_seqno,0)<>0 OR A.item_cd='NONINV900')
				AND A.payment_st='PAYMENT_ST_0'
				--END PROSES TRANSAKSI CHILD
				
				ORDER BY date_trx DESC,root DESC,tarif_tp
			ELSE
				SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
				A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
				A.total_price,COM.code_nm AS tarif_tp,A.note,
				A.item_price,A.quantity, '' AS root,
				CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
				FROM trx_medical_settlement_inv A 
				LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
				LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05'
				WHERE A.medical_cd=@pstrMedicalCd
				AND COM.com_cd='TARIF_TP_05'
				AND ISNULL(A.ref_medical_alkes_seqno,0)<>0
				AND A.payment_st='PAYMENT_ST_0'
				UNION ALL 
				SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
				A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
				A.total_price,COM.code_nm AS tarif_tp,A.note,
				A.item_price,A.quantity,@strMedicalRootCd AS root,
				CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
				FROM trx_medical_settlement_inv A 
				LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
				LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
				WHERE A.medical_cd=@strMedicalRootCd
				AND COM.com_cd='TARIF_TP_05'
				AND ISNULL(A.ref_medical_alkes_seqno,0)<>0
				AND A.payment_st='PAYMENT_ST_0'
				UNION ALL 
				SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
				A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
				A.total_price,COM.code_nm AS tarif_tp,A.note,
				A.item_price,A.quantity,@strMedicalRootRootCd AS root,
				CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
				FROM trx_medical_settlement_inv A 
				LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
				LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
				WHERE A.medical_cd=@strMedicalRootRootCd
				AND COM.com_cd='TARIF_TP_05'
				AND ISNULL(A.ref_medical_alkes_seqno,0)<>0
				AND A.payment_st='PAYMENT_ST_0'
				
				--PROSES TRANSAKSI CHILD
				UNION ALL 
				SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
				A.item_cd AS data_cd,CASE WHEN A.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS data_nm,
				A.total_price,COM.code_nm AS tarif_tp,A.note,
				A.item_price,A.quantity,'' AS root,
				CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno
				FROM trx_medical_settlement_inv A 
				LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
				LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
				WHERE A.medical_cd=@strMedicalChildCd
				AND COM.com_cd='TARIF_TP_05'
				AND ISNULL(A.ref_medical_alkes_seqno,0)<>0
				AND A.payment_st='PAYMENT_ST_0'
				--END PROSES TRANSAKSI CHILD
				
				ORDER BY date_trx DESC,root DESC,tarif_tp
		END	
		ELSE
			SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
			--A.data_cd,/*A.data_nm,*/A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') AS data_nm,
			A.data_cd,CASE WHEN @strMedicalTp='MEDICAL_TP_01' THEN A.data_nm ELSE A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS data_nm,
			A.amount,COM.code_nm AS tarif_tp,A.note,
			A.item_price,A.quantity, '' AS root,ref_seqno
			FROM trx_medical_settlement A 
			LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd 
			LEFT JOIN trx_medical_tindakan MTK ON A.ref_seqno=MTK.medical_tindakan_seqno AND A.tarif_tp='TARIF_TP_04'
			LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
			WHERE A.medical_cd=@pstrMedicalCd
			AND COM.com_cd=@pstrTarifTp
			AND A.payment_st='PAYMENT_ST_0'
			UNION ALL 
			SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
			A.data_cd,A.data_nm,A.amount,COM.code_nm AS tarif_tp,A.note,
			A.item_price,A.quantity,@strMedicalRootCd AS root,ref_seqno
			FROM trx_medical_settlement A 
			LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd 
			WHERE A.medical_cd=@strMedicalRootCd
			AND COM.com_cd=@pstrTarifTp
			AND A.payment_st='PAYMENT_ST_0'
			UNION ALL 
			SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
			A.data_cd,A.data_nm,A.amount,COM.code_nm AS tarif_tp,A.note,
			A.item_price,A.quantity,@strMedicalRootRootCd AS root,ref_seqno
			FROM trx_medical_settlement A 
			LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd 
			WHERE A.medical_cd=@strMedicalRootRootCd
			AND COM.com_cd=@pstrTarifTp
			AND A.payment_st='PAYMENT_ST_0'
			
			--PROSES TRANSAKSI CHILD
			UNION ALL 
			SELECT A.seq_no,simrke.FN_FORMATDATE(A.datetime_trx) AS date_trx,
			--A.data_cd,/*A.data_nm,*/A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') AS data_nm,
			A.data_cd,CASE WHEN @strMedicalTp='MEDICAL_TP_01' THEN A.data_nm ELSE A.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS data_nm,
			A.amount,COM.code_nm AS tarif_tp,A.note,
			A.item_price,A.quantity,'' AS root,ref_seqno
			FROM trx_medical_settlement A 
			LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd 
			LEFT JOIN trx_medical_tindakan MTK ON A.ref_seqno=MTK.medical_tindakan_seqno AND A.tarif_tp='TARIF_TP_04'
			LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
			WHERE A.medical_cd=@strMedicalChildCd
			AND COM.com_cd=@pstrTarifTp
			AND A.payment_st='PAYMENT_ST_0'
			--END PROSES TRANSAKSI CHILD
			
			ORDER BY date_trx DESC,root DESC,tarif_tp
	END	
END
SET NOCOUNT OFF;
DROP FUNCTION IF EXISTS SP_GET_SETTLEMENT_INVOICE(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_SETTLEMENT_INVOICE] 
@pstrMedicalCd Varchar(10)
AS
BEGIN
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalRootRootCd varchar(10)
	DECLARE @strMedicalChildCd varchar(10)
	
	SET @strMedicalRootCd = (SELECT medical_root_cd FROM trx_medical WHERE medical_cd=@pstrMedicalCd)
	
	/*SET @strMedicalRootRootCd = (SELECT A.medical_cd
								FROM trx_medical A 
								WHERE A.medical_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical
													WHERE medical_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical WHERE medical_root_cd=@strMedicalRootCd)
													)
								)*/
	SET @strMedicalRootRootCd = (SELECT A.medical_root_cd
								FROM trx_medical A 
								WHERE A.medical_cd=@strMedicalRootCd)
								
	SET @strMedicalChildCd = (SELECT TOP 1 medical_cd FROM trx_medical WHERE medical_root_cd=@pstrMedicalCd)
	
	SELECT ALIAS.account_cd,ALIAS.account_nm,SUM(ALIAS.amount) AS amount FROM (
	SELECT A.account_cd,ACC.account_nm,
	SUM(A.amount) AS amount 
	FROM trx_medical_settlement A 
	LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
	WHERE A.medical_cd=@pstrMedicalCd
	AND A.payment_st='PAYMENT_ST_0'
	GROUP BY A.account_cd,ACC.account_nm 
	UNION ALL 
	SELECT A.account_cd,ACC.account_nm,
	SUM(A.total_price) AS amount 
	FROM trx_medical_settlement_inv A 
	LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
	WHERE A.medical_cd=@pstrMedicalCd
	AND A.payment_st='PAYMENT_ST_0'
	GROUP BY A.account_cd,ACC.account_nm
	UNION ALL
	--+transaksi root (medical_root_cd)  
	SELECT A.account_cd,ACC.account_nm,
	SUM(A.amount) AS amount 
	FROM trx_medical_settlement A 
	LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
	WHERE A.medical_cd=@strMedicalRootCd
	AND A.payment_st='PAYMENT_ST_0'
	GROUP BY A.account_cd,ACC.account_nm 
	UNION ALL 
	SELECT A.account_cd,ACC.account_nm,
	SUM(A.total_price) AS amount 
	FROM trx_medical_settlement_inv A 
	LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
	WHERE A.medical_cd=@strMedicalRootCd
	AND A.payment_st='PAYMENT_ST_0'
	GROUP BY A.account_cd,ACC.account_nm
	UNION ALL
	--+transaksi root of root
	SELECT A.account_cd,ACC.account_nm,
	SUM(A.amount) AS amount 
	FROM trx_medical_settlement A 
	LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
	WHERE A.medical_cd=@strMedicalRootRootCd
	AND A.payment_st='PAYMENT_ST_0'
	GROUP BY A.account_cd,ACC.account_nm 
	UNION ALL 
	SELECT A.account_cd,ACC.account_nm,
	SUM(A.total_price) AS amount 
	FROM trx_medical_settlement_inv A 
	LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
	WHERE A.medical_cd=@strMedicalRootRootCd
	AND A.payment_st='PAYMENT_ST_0'
	GROUP BY A.account_cd,ACC.account_nm
	
	--PROSES TRANSAKSI CHILD
	UNION ALL
	--+transaksi root of root
	SELECT A.account_cd,ACC.account_nm,
	SUM(A.amount) AS amount 
	FROM trx_medical_settlement A 
	LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
	WHERE A.medical_cd=@strMedicalChildCd
	AND A.payment_st='PAYMENT_ST_0'
	GROUP BY A.account_cd,ACC.account_nm 
	UNION ALL 
	SELECT A.account_cd,ACC.account_nm,
	SUM(A.total_price) AS amount 
	FROM trx_medical_settlement_inv A 
	LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
	WHERE A.medical_cd=@strMedicalChildCd
	AND A.payment_st='PAYMENT_ST_0'
	GROUP BY A.account_cd,ACC.account_nm
	--END PROSES TRANSAKSI CHILD
	--ORDER BY A.account_cd,ACC.account_nm
	) ALIAS
	WHERE ALIAS.amount<>0
	GROUP BY ALIAS.account_cd,ALIAS.account_nm
	ORDER BY ALIAS.account_cd,ALIAS.account_nm
END;

DROP FUNCTION IF EXISTS SP_GET_TERBILANG(numeric) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_TERBILANG] 
@pnumAmount numeric
AS
BEGIN
	SELECT simrke.FN_GET_TERBILANG(@pnumAmount) AS TERBILANG
END;

DROP FUNCTION IF EXISTS SP_GET_TOTALSETTLEMENT(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_GET_TOTALSETTLEMENT] 
@pstrMedicalCd Varchar(10)
AS
BEGIN
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalRootRootCd varchar(10)
	DECLARE @strMedicalChildCd varchar(10)
		
	SET @strMedicalRootCd = (SELECT medical_root_cd FROM trx_medical WHERE medical_cd=@pstrMedicalCd)
	
	SET @strMedicalRootRootCd = (SELECT A.medical_root_cd
								FROM trx_medical A 
								WHERE A.medical_cd=@strMedicalRootCd)
	
	SET @strMedicalChildCd = (SELECT TOP 1 medical_cd FROM trx_medical WHERE medical_root_cd=@pstrMedicalCd)
	
	SELECT	SUM(ALIAS.amount) AS total FROM
	(SELECT A.amount
	FROM trx_medical_settlement A 
	WHERE A.medical_cd=@pstrMedicalCd
	AND A.payment_st='PAYMENT_ST_0'
	UNION ALL 
	SELECT A.total_price
	FROM trx_medical_settlement_inv A 
	WHERE A.medical_cd=@pstrMedicalCd
	AND A.payment_st='PAYMENT_ST_0'
	--+transaksi root (medical_root_cd)    
	UNION ALL 
	SELECT A.amount
	FROM trx_medical_settlement A 
	WHERE A.medical_cd=@strMedicalRootCd
	AND A.payment_st='PAYMENT_ST_0'
	UNION ALL 
	SELECT A.total_price
	FROM trx_medical_settlement_inv A 
	WHERE A.medical_cd=@strMedicalRootCd
	AND A.payment_st='PAYMENT_ST_0'
	--+transaksi root of root   
	UNION ALL 
	SELECT A.amount
	FROM trx_medical_settlement A 
	LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd 
	WHERE A.medical_cd=@strMedicalRootRootCd
	AND A.payment_st='PAYMENT_ST_0'
	UNION ALL 
	SELECT A.total_price
	FROM trx_medical_settlement_inv A 
	LEFT JOIN inv_item_master INV ON A.item_cd=INV.item_cd 
	LEFT JOIN com_code COM ON COM.com_cd='TARIF_TP_05' 
	WHERE A.medical_cd=@strMedicalRootRootCd
	AND A.payment_st='PAYMENT_ST_0'
	
	--PROSES TRANSAKSI CHILD
	--+transaksi child  
	UNION ALL 
	SELECT A.amount
	FROM trx_medical_settlement A 
	LEFT JOIN com_code COM ON A.tarif_tp=COM.com_cd 
	WHERE A.medical_cd=@strMedicalChildCd
	AND A.payment_st='PAYMENT_ST_0'
	UNION ALL 
	SELECT A.total_price
	FROM trx_medical_settlement_inv A 
	WHERE A.medical_cd=@strMedicalChildCd
	AND A.payment_st='PAYMENT_ST_0'
	--END PROSES TRANSAKSI CHILD
	) ALIAS
	WHERE ALIAS.amount<>0
END;

DROP FUNCTION IF EXISTS SP_INV_RPT_GET_ITEM_INVENTORY(varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_INV_RPT_GET_ITEM_INVENTORY]
@pstrPosCd varchar(20)
AS
BEGIN
	IF @pstrPosCd = '' 
	/*--Inventori Master--*/
		SELECT A.item_cd,A.item_nm,B.type_nm,C.unit_nm,
		A.item_price_buy,A.item_price,'' AS pos_cd,'' AS pos_nm,
		CASE WHEN A.type_cd IN ('TP000','TP999','TPALKES','TPINCOME') THEN '' 
			ELSE CASE WHEN A.generic_st='1' THEN 'GENERIK' ELSE 'PATEN' END
		END AS generic_status
		FROM inv_item_master A JOIN inv_item_type B ON A.type_cd=B.type_cd
		JOIN inv_unit C ON A.unit_cd=C.unit_cd
		ORDER BY A.item_nm,B.type_nm
	ELSE
	/*--Inventori Pos--*/
		SELECT A.item_cd,A.item_nm,B.type_nm,C.unit_nm,
		A.item_price_buy,A.item_price,D.pos_cd,POS.pos_nm,
		CASE WHEN A.type_cd IN ('TP000','TP999','TPALKES','TPINCOME') THEN '' 
			ELSE CASE WHEN A.generic_st='1' THEN 'GENERIK' ELSE 'PATEN' END
		END AS generic_status
		FROM inv_item_master A 
		JOIN inv_item_type B ON A.type_cd=B.type_cd 
		JOIN inv_unit C ON A.unit_cd=C.unit_cd 
		JOIN inv_pos_item D ON A.item_cd=D.item_cd 
		JOIN inv_pos_inventory POS ON D.pos_cd=POS.pos_cd
		WHERE D.pos_cd=@pstrPosCd
		ORDER BY A.item_nm,B.type_nm
END;

DROP FUNCTION IF EXISTS SP_INV_RPT_GET_STOK_INVENTORY(varchar,varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_INV_RPT_GET_STOK_INVENTORY]
@pstrPosCd varchar(20),
@pstrPosNm varchar(100)
AS
BEGIN
	IF @pstrPosCd = '' 
	/*--Stok Total--*/
		SELECT A.item_cd,A.item_nm,B.type_nm,C.unit_nm,A.item_price,
		SUM(POS.quantity) AS quantity 
		FROM inv_item_master A 
		JOIN inv_item_type B ON A.type_cd=B.type_cd 
		JOIN inv_unit C ON A.unit_cd=C.unit_cd 
		LEFT JOIN inv_pos_item POS ON A.item_cd=POS.item_cd
		GROUP BY A.item_cd,A.item_nm,B.type_nm,C.unit_nm,A.item_price
		ORDER BY A.item_nm,B.type_nm
	ELSE
	/*--Inventori Pos--*/
		SELECT A.item_cd,A.item_nm,B.type_nm,C.unit_nm,A.item_price,
		SUM(POS.quantity) AS quantity 
		FROM inv_item_master A 
		JOIN inv_item_type B ON A.type_cd=B.type_cd 
		JOIN inv_unit C ON A.unit_cd=C.unit_cd 
		LEFT JOIN inv_pos_item POS ON A.item_cd=POS.item_cd
		WHERE POS.pos_cd=@pstrPosCd
		GROUP BY A.item_cd,A.item_nm,B.type_nm,C.unit_nm,A.item_price
		ORDER BY A.item_nm,B.type_nm
END;

DROP FUNCTION IF EXISTS SP_INV_RPT_GET_STOK_MINIMUM() CASCADE;
CREATE PROCEDURE [simrke].[SP_INV_RPT_GET_STOK_MINIMUM]
AS
BEGIN
	SELECT A.item_cd,A.item_nm,B.type_nm,C.unit_nm,A.item_price,
	A.reorder_point,A.minimum_stock,SUM(POS.quantity) AS quantity 
	FROM inv_item_master A 
	JOIN inv_item_type B ON A.type_cd=B.type_cd 
	JOIN inv_unit C ON A.unit_cd=C.unit_cd 
	LEFT JOIN inv_pos_item POS ON A.item_cd=POS.item_cd
	WHERE A.inventory_st='1'
	GROUP BY A.item_cd,A.item_nm,B.type_nm,C.unit_nm,A.item_price,A.reorder_point,A.minimum_stock
	HAVING SUM(POS.quantity) <= A.minimum_stock
	ORDER BY A.item_nm,B.type_nm
END;

DROP FUNCTION IF EXISTS SP_INV_RPT_GET_STOK_MOVE(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_INV_RPT_GET_STOK_MOVE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrPosCd Varchar(20)
AS
BEGIN
	IF @pstrPosCd <> ''
		SELECT A.item_cd, A.item_nm, B.pos_cd, B.quantity, 
		C.trx_by, C.trx_datetime, C.trx_qty, C.old_stock, C.new_stock,
		C.purpose, COMCD.code_nm AS status,
		D.pos_nm AS name, D.description, E.unit_nm AS unit
		FROM inv_item_master A
		JOIN inv_pos_item B ON A.item_cd=B.item_cd
		JOIN inv_item_move C ON B.pos_cd=C.pos_cd AND B.item_cd=C.item_cd
		LEFT JOIN com_code COMCD On C.move_tp=COMCD.com_cd
		JOIN inv_pos_inventory D ON B.pos_cd=D.pos_cd
		JOIN inv_unit E ON A.unit_cd=E.unit_cd
		WHERE B.pos_cd=@pstrPosCd
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.trx_datetime))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.trx_datetime))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY B.pos_cd,C.trx_datetime,A.item_cd,A.item_nm
	ELSE
		SELECT A.item_cd, A.item_nm, B.pos_cd, B.quantity, 
		C.trx_by, C.trx_datetime, C.trx_qty, C.old_stock, C.new_stock,
		C.purpose, COMCD.code_nm AS status,
		D.pos_nm AS name, D.description, E.unit_nm AS unit
		FROM inv_item_master A
		JOIN inv_pos_item B ON A.item_cd=B.item_cd
		JOIN inv_item_move C ON B.pos_cd=C.pos_cd AND B.item_cd=C.item_cd
		LEFT JOIN com_code COMCD On C.move_tp=COMCD.com_cd
		JOIN inv_pos_inventory D ON B.pos_cd=D.pos_cd
		JOIN inv_unit E ON A.unit_cd=E.unit_cd
		WHERE simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.trx_datetime))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.trx_datetime))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
END;

DROP FUNCTION IF EXISTS SP_INV_RPT_GET_STOKMOVE_REKAP(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_INV_RPT_GET_STOKMOVE_REKAP]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrMoveTp Varchar(20)
AS
BEGIN
	IF @pstrMoveTp <> ''
		SELECT A.item_cd, A.item_nm, B.pos_cd, B.quantity, 
		C.trx_by, C.trx_datetime, C.trx_qty, C.old_stock, C.new_stock,
		POS1.pos_nm + CASE WHEN POS2.pos_nm<>POS1.pos_nm THEN ' - ' + POS2.pos_nm ELSE '' END AS purpose,
		COMCD.code_nm AS status,
		D.pos_nm AS name, D.description, E.unit_nm AS unit
		FROM inv_item_master A
		JOIN inv_pos_item B ON A.item_cd=B.item_cd
		JOIN inv_item_move C ON B.pos_cd=C.pos_cd AND B.item_cd=C.item_cd
		LEFT JOIN inv_pos_inventory POS1 ON C.pos_cd=POS1.pos_cd
		LEFT JOIN inv_pos_inventory POS2 ON C.pos_destination=POS2.pos_cd
		LEFT JOIN com_code COMCD On C.move_tp=COMCD.com_cd
		JOIN inv_pos_inventory D ON B.pos_cd=D.pos_cd
		JOIN inv_unit E ON A.unit_cd=E.unit_cd
		WHERE C.move_tp=@pstrMoveTp
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.trx_datetime))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.trx_datetime))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY B.pos_cd,C.trx_datetime,A.item_cd,A.item_nm
	ELSE
		SELECT A.item_cd, A.item_nm, B.pos_cd, B.quantity, 
		C.trx_by, C.trx_datetime, C.trx_qty, C.old_stock, C.new_stock,
		POS1.pos_nm + CASE WHEN POS2.pos_nm<>POS1.pos_nm THEN ' - ' + POS2.pos_nm ELSE '' END AS purpose,
		COMCD.code_nm AS status,
		D.pos_nm AS name, D.description, E.unit_nm AS unit
		FROM inv_item_master A
		JOIN inv_pos_item B ON A.item_cd=B.item_cd
		JOIN inv_item_move C ON B.pos_cd=C.pos_cd AND B.item_cd=C.item_cd
		LEFT JOIN inv_pos_inventory POS1 ON C.pos_cd=POS1.pos_cd
		LEFT JOIN inv_pos_inventory POS2 ON C.pos_destination=POS2.pos_cd
		LEFT JOIN com_code COMCD On C.move_tp=COMCD.com_cd
		JOIN inv_pos_inventory D ON B.pos_cd=D.pos_cd
		JOIN inv_unit E ON A.unit_cd=E.unit_cd
		WHERE C.move_tp IN ('MOVE_TP_1','MOVE_TP_2','MOVE_TP_3','MOVE_TP_4','MOVE_TP_5')
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.trx_datetime))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.trx_datetime))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
END;

DROP FUNCTION IF EXISTS SP_PO_DELETE(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_DELETE] 
@pstrPoCd Varchar(10)
AS
BEGIN
	UPDATE po_purchase_request SET pr_st='3'
	WHERE pr_cd IN (SELECT DISTINCT pr_cd FROM po_po_detail WHERE po_cd=@pstrPoCd)
	
	DELETE FROM po_po_detail
	WHERE po_cd=@pstrPoCd
	
	DELETE FROM po_purchase_order WHERE po_cd=@pstrPoCd
END;

DROP FUNCTION IF EXISTS SP_PO_EDIT_TRX(Varchar,int,int,Varchar,Datetime,Varchar,Datetime,Varchar,Varchar,Numeric,Varchar,Numeric,Numeric,Numeric,Numeric,Char,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_EDIT_TRX]
@pstrTrxCd Varchar(20),
@pintYear int,
@pintMonth int,
@pstrSupplierCd Varchar(20),
@pdtTrxDate Datetime,
@pstrDeliveryAddress Varchar(1000),
@pdtDeliveryDatetime Datetime,
@pstrTOP Varchar(20),
@pstrCurr Varchar(20),
@pnumRate Numeric,
@pstrVatTp Varchar(20),
@pnumPercentPPN Numeric(5,2),
@pnumTotalPrice Numeric,
@pnumPPN Numeric,
@pnumTotalAmount Numeric,
@pstrStatus Char(1),
@pstrNote Varchar(1000),
@pstrUserId Varchar(20),
@pstrUserNm Varchar(100)
AS
BEGIN
	UPDATE po_purchase_order SET supplier_cd=@pstrSupplierCd,trx_date=@pdtTrxDate,
	delivery_address=@pstrDeliveryAddress,delivery_datetime=@pdtDeliveryDatetime,
	top_cd=@pstrTOP,currency_cd=@pstrCurr,rate=@pnumRate,
	vat_tp=@pstrVatTp,percent_ppn=@pnumPercentPPN,
	total_price=@pnumTotalPrice,ppn=@pnumPPN,total_amount=@pnumTotalAmount,
	po_st=@pstrStatus,note=@pstrNote,
	modi_id=@pstrUserId, modi_datetime=GETDATE()
	WHERE po_cd=@pstrTrxCd
	
	DELETE FROM po_po_detail WHERE po_cd=@pstrTrxCd
END;

DROP FUNCTION IF EXISTS SP_PO_GET_TRX_ALL(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_GET_TRX_ALL] 
@pstrPoNo Varchar(100)
AS
BEGIN
	SELECT A.po_cd AS trx_cd,A.trx_date,A.po_no AS trx_no,
	B.item_cd,C.item_nm,B.unit_cd,B.quantity,B.unit_price AS item_price,
	D.supplier_cd,D.supplier_nm
	FROM po_purchase_order A
	JOIN po_po_detail B ON A.po_cd=B.po_cd
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd
	WHERE A.po_no LIKE '%' + @pstrPoNo + '%'
	ORDER BY A.po_no,A.trx_date,C.item_nm
END;

DROP FUNCTION IF EXISTS SP_PO_GET_TRX_APPROVED(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_GET_TRX_APPROVED] 
@pstrPoNo Varchar(100)
AS
BEGIN
	SELECT A.po_cd AS trx_cd,simrke.FN_FORMATDATE(A.trx_date) AS trx_date,A.po_no AS trx_no,
	B.item_cd,C.item_nm,B.unit_cd,B.quantity,B.unit_price AS item_price,
	D.supplier_cd,D.supplier_nm
	FROM po_purchase_order A
	JOIN po_po_detail B ON A.po_cd=B.po_cd
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd
	WHERE A.po_st IN ('1','2','3')
	AND A.po_no LIKE '%' + @pstrPoNo + '%'
	ORDER BY A.po_no,A.trx_date,C.item_nm
END;

DROP FUNCTION IF EXISTS SP_PO_GET_TRX_LIST(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_GET_TRX_LIST]
@pdtDate Varchar(10),
@pstrSupplier Varchar(100),
@pstrPoNo Varchar(100)
AS
BEGIN
	SELECT A.po_cd AS trx_cd,simrke.FN_FORMATDATE(A.trx_date) AS trx_date,A.po_no AS trx_no,B.supplier_nm,
	A.note,A.po_st,simrke.FN_GET_STATUS(A.po_st) AS po_st_nm
	FROM po_purchase_order A LEFT JOIN po_supplier B ON A.supplier_cd=B.supplier_cd
	WHERE A.po_st IN ('0','1','2','3')
	AND simrke.FN_FORMATDATE(A.trx_date)=@pdtDate
	AND B.supplier_nm LIKE '%' + @pstrSupplier + '%'
	AND A.po_no LIKE '%' + @pstrPoNo + '%'
	ORDER BY A.po_no DESC,A.trx_date DESC
END;

DROP FUNCTION IF EXISTS SP_PO_GET_TRX_LOAD(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_GET_TRX_LOAD]
@pdtDate Varchar(10),
@pstrSupplier Varchar(100),
@pstrPoNo Varchar(100)
AS
BEGIN
	SELECT A.po_cd AS trx_cd,simrke.FN_FORMATDATE(A.trx_date) AS trx_date,A.po_no AS trx_no,
	B.item_cd,C.item_nm,B.quantity,B.unit_price AS item_price,B.unit_cd,
	D.supplier_cd,D.supplier_nm
	FROM po_purchase_order A
	JOIN po_po_detail B ON A.po_cd=B.po_cd
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd
	WHERE A.po_st IN ('1','2','3')
	AND simrke.FN_FORMATDATE(A.trx_date)=@pdtDate
	AND D.supplier_nm LIKE '%' + @pstrSupplier + '%'
	AND A.po_no LIKE '%' + @pstrPoNo + '%'
	ORDER BY A.po_no,A.trx_date,C.item_nm
END;

DROP FUNCTION IF EXISTS SP_PO_GET_UNPROCESS_MONTH(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_GET_UNPROCESS_MONTH] 
@pintYear int,
@pintMonth int
AS
BEGIN
	DECLARE @intResult int
	DECLARE @intTotalUnProcess int
	
	SET @intResult = 0
	
	DECLARE objCursor CURSOR FOR
	SELECT COUNT(po_cd) AS total FROM po_purchase_order
	WHERE trx_year=@pintYear AND trx_month=@pintMonth
	AND po_st IN ('1','2')
	UNION
	SELECT COUNT(ri_cd) AS total FROM po_receive_item
	WHERE trx_year=@pintYear AND trx_month=@pintMonth
	AND ri_st IN ('1','2')
	
	OPEN objCursor
	
	FETCH NEXT FROM objCursor
	INTO @intTotalUnProcess
	
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @intResult = @intResult + @intTotalUnProcess
		
		FETCH NEXT FROM objCursor
		INTO @intTotalUnProcess
	END
	
	CLOSE objCursor
	DEALLOCATE objCursor
	
	SELECT @intResult AS total_unprocess
END;

DROP FUNCTION IF EXISTS SP_PO_PROCESS_TRX(int,int,Varchar,Datetime,Varchar,Datetime,Varchar,Varchar,Numeric,Varchar,Numeric,Numeric,Numeric,Numeric,Char,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_PROCESS_TRX] 
@pintYear int,
@pintMonth int,
@pstrSupplierCd Varchar(20),
@pdtTrxDate Datetime,
@pstrDeliveryAddress Varchar(1000),
@pdtDeliveryDatetime Datetime,
@pstrTOP Varchar(20),
@pstrCurr Varchar(20),
@pnumRate Numeric,
@pstrVatTp Varchar(20),
@pnumPercentPPN Numeric(5,2),
@pnumTotalPrice Numeric,
@pnumPPN Numeric,
@pnumTotalAmount Numeric,
@pstrStatus Char(1),
@pstrNote Varchar(1000),
@pstrUserId Varchar(20),
@pstrUserNm Varchar(100)
AS
BEGIN
	DECLARE @strTrxCd Varchar(20)
	DECLARE @strTrxNo Varchar(10)
	
	SET @strTrxNo = (SELECT simrke.FN_PO_GET_TRX_NO(@pintYear,@pintMonth))
	SET @strTrxCd = @strTrxNo
	
	SELECT @strTrxCd AS trx_cd,@strTrxNo AS trx_no
	
	INSERT INTO po_purchase_order (po_cd,trx_year,trx_month,po_no,
	supplier_cd,trx_date,
	delivery_address,delivery_datetime,top_cd,currency_cd,rate,
	vat_tp,percent_ppn,
	total_price,ppn,total_amount,
	po_st,note,entry_by,entry_date,modi_id,modi_datetime) 
	VALUES (@strTrxCd,@pintYear,@pintMonth,@strTrxNo,
	@pstrSupplierCd,@pdtTrxDate,
	@pstrDeliveryAddress,@pdtDeliveryDatetime,@pstrTOP,@pstrCurr,@pnumRate,
	@pstrVatTp,@pnumPercentPPN,
	@pnumTotalPrice,@pnumPPN,@pnumTotalAmount,
	@pstrStatus,@pstrNote,
	@pstrUserNm,GETDATE(),@pstrUserId,GETDATE())
END;

DROP FUNCTION IF EXISTS SP_PO_RPT_GET_POREKAP(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_RPT_GET_POREKAP]
@pintMonth int,
@pintYear int
AS
BEGIN
	SELECT A.po_cd AS trx_cd,A.trx_date,A.po_no AS trx_no,
	A.total_price,A.ppn,A.total_amount,A.currency_cd,
	D.supplier_nm,D.address,
	B.item_cd,C.item_nm,UNIT.unit_nm,B.quantity,B.unit_price,B.trx_amount
	FROM po_purchase_order A
	JOIN po_po_detail B ON A.po_cd=B.po_cd
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN inv_unit UNIT ON C.unit_cd=UNIT.unit_cd
	LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd
	WHERE A.trx_year=@pintYear
	AND A.trx_month=@pintMonth
	ORDER BY A.po_no,A.trx_date,C.item_nm
END;

DROP FUNCTION IF EXISTS SP_PO_RPT_GET_POREKAP_SUPPLIER(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_RPT_GET_POREKAP_SUPPLIER]
@pintMonth int,
@pintYear int
AS
BEGIN
	SELECT A.po_cd AS trx_cd,A.trx_date,A.po_no AS trx_no,
	A.total_price,A.ppn,A.total_amount,A.currency_cd,
	D.supplier_nm,D.address,
	B.item_cd,C.item_nm,UNIT.unit_nm,B.quantity,B.unit_price,B.trx_amount
	FROM po_purchase_order A
	JOIN po_po_detail B ON A.po_cd=B.po_cd
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN inv_unit UNIT ON C.unit_cd=UNIT.unit_cd
	LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd
	WHERE A.trx_year=@pintYear
	AND A.trx_month=@pintMonth
	ORDER BY D.supplier_nm,A.po_no,A.trx_date,C.item_nm
END;

DROP FUNCTION IF EXISTS SP_PO_RPT_GET_POTRX(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_RPT_GET_POTRX]
@pstrTrxCd Varchar(20)
AS
BEGIN
	SELECT A.po_cd AS trx_cd,A.trx_date,A.po_no AS trx_no,
	A.total_price,A.ppn,A.total_amount,A.currency_cd,
	A.entry_by,A.delivery_address,A.delivery_datetime,D.supplier_nm,D.address,
	B.item_cd,C.item_nm,UNIT.unit_nm,B.quantity,B.unit_price,B.trx_amount
	FROM po_purchase_order A
	JOIN po_po_detail B ON A.po_cd=B.po_cd
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN inv_unit UNIT ON C.unit_cd=UNIT.unit_cd
	LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd
	WHERE A.po_cd=@pstrTrxCd
	ORDER BY C.item_nm
END;

DROP FUNCTION IF EXISTS SP_PO_RPT_GET_RIITEMPERIODE(Varchar,Varchar,varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_RPT_GET_RIITEMPERIODE]
@dtFromDate Varchar(10), 
@dtToDate Varchar(10),
@strItemCode varchar(20)
AS
BEGIN
	IF @strItemCode = ''
		SELECT A.ri_cd AS trx_cd,A.trx_date,A.ri_no AS trx_no,
		A.total_price,A.ppn,A.total_amount,A.currency_cd,
		A.entry_by,D.supplier_nm,D.address,
		B.item_cd,C.item_nm,B.unit_cd,B.quantity,B.unit_price,B.trx_amount
		FROM po_receive_item A LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd,
		PO_RECEIVE_DETAIL B LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
		WHERE A.ri_cd=B.ri_cd
		--AND CONVERT(VARCHAR(10),A.trx_date,101)>=@dtFromDate AND CONVERT(VARCHAR(10),A.trx_date,101)<=@dtToDate
		AND A.trx_date>=CONVERT(DATETIME,@dtFromDate) AND A.trx_date<=CONVERT(DATETIME,@dtToDate)
		ORDER BY C.item_nm,A.trx_date
	ELSE
		SELECT A.ri_cd AS trx_cd,A.trx_date,A.ri_no AS trx_no,
		A.total_price,A.ppn,A.total_amount,A.currency_cd,
		A.entry_by,D.supplier_nm,D.address,
		B.item_cd,C.item_nm,B.unit_cd,B.quantity,B.unit_price,B.trx_amount
		FROM po_receive_item A LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd,
		PO_RECEIVE_DETAIL B LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
		WHERE A.ri_cd=B.ri_cd
		--AND CONVERT(VARCHAR(10),A.trx_date,101)>=@dtFromDate AND CONVERT(VARCHAR(10),A.trx_date,101)<=@dtToDate
		AND A.trx_date>=CONVERT(DATETIME,@dtFromDate) AND A.trx_date<=CONVERT(DATETIME,@dtToDate)
		AND B.item_cd=@strItemCode
		ORDER BY C.item_nm,A.trx_date
END;

DROP FUNCTION IF EXISTS SP_PO_RPT_GET_RIPERIODE(Varchar,Varchar,varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_RPT_GET_RIPERIODE]
@dtFromDate Varchar(10), 
@dtToDate Varchar(10),
@strItemCode varchar(20)
AS
BEGIN
	IF @strItemCode = '' 
		SELECT A.ri_cd AS trx_cd,A.trx_date,A.ri_no AS trx_no,
		A.total_price,A.ppn,A.total_amount,A.currency_cd,
		A.entry_by,D.supplier_nm,D.address,
		B.item_cd,C.item_nm,B.unit_cd,B.quantity,B.unit_price,B.trx_amount
		FROM po_receive_item A LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd,
		PO_RECEIVE_DETAIL B LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
		WHERE A.ri_cd=B.ri_cd
		--AND CONVERT(VARCHAR(10),A.trx_date,101)>=@dtFromDate AND CONVERT(VARCHAR(10),A.trx_date,101)<=@dtToDate
		AND A.trx_date>=CONVERT(DATETIME,@dtFromDate) AND A.trx_date<=CONVERT(DATETIME,@dtToDate)
		ORDER BY A.trx_date,C.item_nm
	ELSE
		SELECT A.ri_cd AS trx_cd,A.trx_date,A.ri_no AS trx_no,
		A.total_price,A.ppn,A.total_amount,A.currency_cd,
		A.entry_by,D.supplier_nm,D.address,
		B.item_cd,C.item_nm,B.unit_cd,B.quantity,B.unit_price,B.trx_amount
		FROM po_receive_item A LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd,
		PO_RECEIVE_DETAIL B LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
		WHERE A.ri_cd=B.ri_cd
		--AND CONVERT(VARCHAR(10),A.trx_date,101)>=@dtFromDate AND CONVERT(VARCHAR(10),A.trx_date,101)<=@dtToDate
		AND A.trx_date>=CONVERT(DATETIME,@dtFromDate) AND A.trx_date<=CONVERT(DATETIME,@dtToDate)
		AND B.item_cd=@strItemCode
		ORDER BY A.trx_date,C.item_nm
END;

DROP FUNCTION IF EXISTS SP_PO_RPT_GET_RIREKAP(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_RPT_GET_RIREKAP]
@pintMonth int,
@pintYear int
AS
BEGIN
	SELECT A.ri_cd AS trx_cd,A.trx_date,A.ri_no AS trx_no,
	A.total_price,A.ppn,A.total_amount,A.currency_cd,
	D.supplier_nm,D.address,
	B.item_cd,C.item_nm,UNIT.unit_nm,B.quantity,B.unit_price,
	B.trx_amount - ISNULL(B.discount_amount,0) AS trx_amount,ISNULL(B.discount_amount,0) AS discount_amount
	FROM po_receive_item A
	JOIN po_receive_detail B ON A.ri_cd=B.ri_cd
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN inv_unit UNIT ON C.unit_cd=UNIT.unit_cd
	LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd
	WHERE A.trx_year=@pintYear
	AND A.trx_month=@pintMonth
	ORDER BY A.ri_no,A.trx_date,C.item_nm
END;

DROP FUNCTION IF EXISTS SP_PO_RPT_GET_RIREKAP_SUPPLIER(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_RPT_GET_RIREKAP_SUPPLIER]
@pintMonth int,
@pintYear int
AS
BEGIN
	SELECT A.ri_cd AS trx_cd,A.trx_date,A.ri_no AS trx_no,
	A.total_price,A.ppn,A.total_amount,A.currency_cd,
	D.supplier_nm,D.address,
	B.item_cd,C.item_nm,UNIT.unit_nm,B.quantity,B.unit_price,
	B.trx_amount - ISNULL(B.discount_amount,0) AS trx_amount,ISNULL(B.discount_amount,0) AS discount_amount
	FROM po_receive_item A
	JOIN po_receive_detail B ON A.ri_cd=B.ri_cd
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN inv_unit UNIT ON C.unit_cd=UNIT.unit_cd
	LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd
	WHERE A.trx_year=@pintYear
	AND A.trx_month=@pintMonth
	ORDER BY D.supplier_nm,A.ri_no,A.trx_date,C.item_nm
END;

DROP FUNCTION IF EXISTS SP_PO_RPT_GET_RISUPPLIERPERIODE(Varchar,Varchar,varchar,varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_RPT_GET_RISUPPLIERPERIODE]
@dtFromDate Varchar(10), 
@dtToDate Varchar(10),
@strItemCode varchar(20),
@strSupplierCode varchar(20)
AS
BEGIN
	IF @strItemCode = ''
	BEGIN
		IF @strSupplierCode = ''
			SELECT A.ri_cd AS trx_cd,A.trx_date,A.ri_no AS trx_no,
			A.total_price,A.ppn,A.total_amount,A.currency_cd,
			A.entry_by,A.supplier_cd,D.supplier_nm,D.address,
			B.item_cd,C.item_nm,B.unit_cd,B.quantity,B.unit_price,B.trx_amount
			FROM po_receive_item A LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd,
			PO_RECEIVE_DETAIL B LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
			WHERE A.ri_cd=B.ri_cd
			--AND CONVERT(VARCHAR(10),A.trx_date,101)>=@dtFromDate AND CONVERT(VARCHAR(10),A.trx_date,101)<=@dtToDate
			AND A.trx_date>=CONVERT(DATETIME,@dtFromDate) AND A.trx_date<=CONVERT(DATETIME,@dtToDate)
			ORDER BY D.supplier_nm,A.trx_date,C.item_nm
		ELSE
			SELECT A.ri_cd AS trx_cd,A.trx_date,A.ri_no AS trx_no,
			A.total_price,A.ppn,A.total_amount,A.currency_cd,
			A.entry_by,A.supplier_cd,D.supplier_nm,D.address,
			B.item_cd,C.item_nm,B.unit_cd,B.quantity,B.unit_price,B.trx_amount
			FROM po_receive_item A LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd,
			PO_RECEIVE_DETAIL B LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
			WHERE A.ri_cd=B.ri_cd
			--AND CONVERT(VARCHAR(10),A.trx_date,101)>=@dtFromDate AND CONVERT(VARCHAR(10),A.trx_date,101)<=@dtToDate
			AND A.trx_date>=CONVERT(DATETIME,@dtFromDate) AND A.trx_date<=CONVERT(DATETIME,@dtToDate)
			AND A.supplier_cd=@strSupplierCode
			ORDER BY D.supplier_nm,A.trx_date,C.item_nm
	END	
	ELSE
	BEGIN
		IF @strSupplierCode = ''
			SELECT A.ri_cd AS trx_cd,A.trx_date,A.ri_no AS trx_no,
			A.total_price,A.ppn,A.total_amount,A.currency_cd,
			A.entry_by,A.supplier_cd,D.supplier_nm,D.address,
			B.item_cd,C.item_nm,B.unit_cd,B.quantity,B.unit_price,B.trx_amount
			FROM po_receive_item A LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd,
			PO_RECEIVE_DETAIL B LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
			WHERE A.ri_cd=B.ri_cd
			--AND CONVERT(VARCHAR(10),A.trx_date,101)>=@dtFromDate AND CONVERT(VARCHAR(10),A.trx_date,101)<=@dtToDate
			AND A.trx_date>=CONVERT(DATETIME,@dtFromDate) AND A.trx_date<=CONVERT(DATETIME,@dtToDate)
			AND B.item_cd=@strItemCode
			ORDER BY D.supplier_nm,A.trx_date,C.item_nm
		ELSE
			SELECT A.ri_cd AS trx_cd,A.trx_date,A.ri_no AS trx_no,
			A.total_price,A.ppn,A.total_amount,A.currency_cd,
			A.entry_by,A.supplier_cd,D.supplier_nm,D.address,
			B.item_cd,C.item_nm,B.unit_cd,B.quantity,B.unit_price,B.trx_amount
			FROM po_receive_item A LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd,
			PO_RECEIVE_DETAIL B LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
			WHERE A.ri_cd=B.ri_cd
			--AND CONVERT(VARCHAR(10),A.trx_date,101)>=@dtFromDate AND CONVERT(VARCHAR(10),A.trx_date,101)<=@dtToDate
			AND A.trx_date>=CONVERT(DATETIME,@dtFromDate) AND A.trx_date<=CONVERT(DATETIME,@dtToDate)
			AND B.item_cd=@strItemCode
			AND A.supplier_cd=@strSupplierCode
			ORDER BY D.supplier_nm,A.trx_date,C.item_nm
	END	
END;

DROP FUNCTION IF EXISTS SP_PO_RPT_GET_RITRX(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PO_RPT_GET_RITRX]
@pstrTrxCd Varchar(20)
AS
BEGIN
	SELECT A.ri_cd AS trx_cd,A.trx_date,A.ri_no AS trx_no,
	A.total_price,A.ppn,A.total_amount,A.currency_cd,
	A.entry_by,A.invoice_no,D.supplier_nm,D.address,
	B.item_cd,C.item_nm,UNIT.unit_nm,B.quantity,B.unit_price,
	B.trx_amount - ISNULL(B.discount_amount,0) AS trx_amount,ISNULL(B.discount_amount,0) AS discount_amount,
	B.faktur_no,B.faktur_date,B.batch_no,B.expire_date
	FROM po_receive_item A
	JOIN po_receive_detail B ON A.ri_cd=B.ri_cd
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN inv_unit UNIT ON C.unit_cd=UNIT.unit_cd
	LEFT JOIN po_supplier D ON A.supplier_cd=D.supplier_cd
	WHERE A.ri_cd=@pstrTrxCd
	ORDER BY C.item_nm
END;

DROP FUNCTION IF EXISTS SP_PROSES_INVOICE(Varchar,Varchar,Datetime,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,numeric,numeric,numeric,numeric,numeric,numeric,numeric,numeric,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PROSES_INVOICE] 
@pstrMedicalCd Varchar(10),
@pstrPasienCd Varchar(20),
@pdtInvoiceDate Datetime,
@pstrPaymentTp Varchar(20),
@pstrInsuranceCd Varchar(20),
@pstrPaymentCd Varchar(20),
@pstrCardNo Varchar(50),
@pstrEntryNm Varchar(100),
@pstrPayNm Varchar(100),
@pstrPaymentSt Varchar(20),
@pnumAmountAsuransi numeric,
@pnumAmountPasien numeric,
@pnumAmountTunai numeric,
@pnumAmountNontunai numeric,
@pnumDiscountPersen numeric(5,2),
@pnumDiscountAmount numeric,
@pnumPayBayar numeric,
@pnumPaySisa numeric,
@pstrNote Varchar(1000),
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalRootRootCd varchar(10)
	DECLARE @strMedicalChildCd varchar(10)
	
	DECLARE @intSettlementNo bigint
	DECLARE @strInvoiceNo varchar(20)
	DECLARE @strAccountCd varchar(20)
	DECLARE @strAccountNm varchar(100)
	DECLARE @numAmount numeric
	
	DECLARE @strMedicalCdSELECT varchar(10)
	DECLARE @intMedicalSettSeqno bigint
	
	SET @strMedicalRootCd = (SELECT medical_root_cd FROM trx_medical WHERE medical_cd=@pstrMedicalCd)
	
	/*SET @strMedicalRootRootCd = (SELECT A.medical_cd
								FROM trx_medical A 
								WHERE A.medical_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical
													WHERE medical_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical WHERE medical_root_cd=@strMedicalRootCd)
													)
								)*/
	SET @strMedicalRootRootCd = (SELECT A.medical_root_cd
								FROM trx_medical A 
								WHERE A.medical_cd=@strMedicalRootCd)
								
	SET @strMedicalChildCd = (SELECT TOP 1 medical_cd FROM trx_medical WHERE medical_root_cd=@pstrMedicalCd)
	
	SET @strInvoiceNo = (SELECT simrke.FN_GET_INVOICENO())
	IF EXISTS (SELECT invoice_no FROM trx_settlement
			   WHERE invoice_no=@strInvoiceNo)
	BEGIN
		SET @strInvoiceNo = (SELECT simrke.FN_GET_INVOICENO())
	END
	
	--Proses Check invoice_no in used at temporary table (trx_temp)
	INSERT INTO trx_temp(data_10) VALUES(@strInvoiceNo)
	--End Proses Check invoice_no
	
	BEGIN TRANSACTION
	
	--Update payment_st
	DECLARE cursorData CURSOR FOR
	SELECT A.seq_no,A.medical_cd
	FROM trx_medical_settlement A 
	WHERE A.medical_cd=@pstrMedicalCd
	AND A.payment_st='PAYMENT_ST_0'
	UNION ALL
	--transaksi root
	SELECT A.seq_no,A.medical_cd
	FROM trx_medical_settlement A 
	WHERE A.medical_cd=@strMedicalRootCd
	AND A.payment_st='PAYMENT_ST_0'
	UNION ALL
	--transaksi root of root
	SELECT A.seq_no,A.medical_cd
	FROM trx_medical_settlement A 
	WHERE A.medical_cd=@strMedicalRootRootCd
	AND A.payment_st='PAYMENT_ST_0'
	--PROSES TRANSAKSI CHILD
	UNION ALL
	--transaksi child
	SELECT A.seq_no,A.medical_cd
	FROM trx_medical_settlement A 
	WHERE A.medical_cd=@strMedicalChildCd
	AND A.payment_st='PAYMENT_ST_0'
	--END PROSES TRANSAKSI CHILD
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @intMedicalSettSeqno,@strMedicalCdSELECT
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		UPDATE trx_medical_settlement SET invoice_no=@strInvoiceNo
		WHERE medical_cd=@strMedicalCdSELECT
		AND seq_no=@intMedicalSettSeqno
		
		FETCH NEXT FROM cursorData
		INTO @intMedicalSettSeqno,@strMedicalCdSELECT
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	DECLARE cursorData CURSOR FOR
	SELECT A.seq_no,A.medical_cd
	FROM trx_medical_settlement_inv A 
	WHERE A.medical_cd=@pstrMedicalCd
	AND A.payment_st='PAYMENT_ST_0'
	UNION ALL
	--transaksi root
	SELECT A.seq_no,A.medical_cd
	FROM trx_medical_settlement_inv A 
	WHERE A.medical_cd=@strMedicalRootCd
	AND A.payment_st='PAYMENT_ST_0'
	UNION ALL
	--transaksi root of root
	SELECT A.seq_no,A.medical_cd
	FROM trx_medical_settlement_inv A 
	WHERE A.medical_cd=@strMedicalRootRootCd
	AND A.payment_st='PAYMENT_ST_0'
	--PROSES TRANSAKSI CHILD
	UNION ALL
	--transaksi child
	SELECT A.seq_no,A.medical_cd
	FROM trx_medical_settlement_inv A 
	WHERE A.medical_cd=@strMedicalChildCd
	AND A.payment_st='PAYMENT_ST_0'
	--END PROSES TRANSAKSI CHILD
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @intMedicalSettSeqno,@strMedicalCdSELECT
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		UPDATE trx_medical_settlement_inv SET invoice_no=@strInvoiceNo
		WHERE medical_cd=@strMedicalCdSELECT
		AND seq_no=@intMedicalSettSeqno
			
		FETCH NEXT FROM cursorData
		INTO @intMedicalSettSeqno,@strMedicalCdSELECT
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	--End Update payment_st
	
	--Insert table trx_settlement
	INSERT INTO trx_settlement (medical_cd,pasien_cd,payment_tp,insurance_cd,invoice_no,invoice_date,
	payment_cd,card_no,amount_asuransi,amount_pasien,amount_tunai,amount_nontunai,discount_percent,discount_amount,
	entry_nm,entry_date,pay_nm,payment_st,info_11,info_12,note,
	modi_id,modi_datetime)
	VALUES (@pstrMedicalCd,@pstrPasienCd,@pstrPaymentTp,@pstrInsuranceCd,@strInvoiceNo,@pdtInvoiceDate,
	@pstrPaymentCd,@pstrCardNo,@pnumAmountAsuransi,@pnumAmountPasien,@pnumAmountTunai,@pnumAmountNontunai,@pnumDiscountPersen,@pnumDiscountAmount,
	@pstrEntryNm,GETDATE(),@pstrPayNm,@pstrPaymentSt,@pnumPayBayar,@pnumPaySisa,@pstrNote,
	@pstrUserId,GETDATE())
	
	SET @intSettlementNo = (SELECT MAX(settlement_no) FROM trx_settlement WHERE medical_cd=@pstrMedicalCd)
	
	DECLARE cursorData CURSOR FOR
	SELECT	ALIAS.account_cd,SUM(ALIAS.amount) AS amount FROM
		(SELECT A.account_cd,ACC.account_nm,
		SUM(A.amount) AS amount 
		FROM trx_medical_settlement A 
		LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
		WHERE A.medical_cd=@pstrMedicalCd
		AND A.payment_st='PAYMENT_ST_0'
		GROUP BY A.account_cd,ACC.account_nm 
		UNION ALL 
		SELECT A.account_cd,ACC.account_nm,
		SUM(A.total_price) AS amount 
		FROM trx_medical_settlement_inv A 
		LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
		WHERE A.medical_cd=@pstrMedicalCd
		AND A.payment_st='PAYMENT_ST_0'
		GROUP BY A.account_cd,ACC.account_nm
		UNION ALL
		--+transaksi root (medical_root_cd)  
		SELECT A.account_cd,ACC.account_nm,
		SUM(A.amount) AS amount 
		FROM trx_medical_settlement A 
		LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
		WHERE A.medical_cd=@strMedicalRootCd
		AND A.payment_st='PAYMENT_ST_0'
		GROUP BY A.account_cd,ACC.account_nm 
		UNION ALL 
		SELECT A.account_cd,ACC.account_nm,
		SUM(A.total_price) AS amount 
		FROM trx_medical_settlement_inv A 
		LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
		WHERE A.medical_cd=@strMedicalRootCd
		AND A.payment_st='PAYMENT_ST_0'
		GROUP BY A.account_cd,ACC.account_nm
		UNION ALL
		--+transaksi root of root  
		SELECT A.account_cd,ACC.account_nm,
		SUM(A.amount) AS amount 
		FROM trx_medical_settlement A 
		LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
		WHERE A.medical_cd=@strMedicalRootRootCd
		AND A.payment_st='PAYMENT_ST_0'
		GROUP BY A.account_cd,ACC.account_nm 
		UNION ALL 
		SELECT A.account_cd,ACC.account_nm,
		SUM(A.total_price) AS amount 
		FROM trx_medical_settlement_inv A 
		LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
		WHERE A.medical_cd=@strMedicalRootRootCd
		AND A.payment_st='PAYMENT_ST_0'
		GROUP BY A.account_cd,ACC.account_nm
		
		--PROSES TRANSAKSI CHILD
		UNION ALL
		--+transaksi root of root  
		SELECT A.account_cd,ACC.account_nm,
		SUM(A.amount) AS amount 
		FROM trx_medical_settlement A 
		LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
		WHERE A.medical_cd=@strMedicalChildCd
		AND A.payment_st='PAYMENT_ST_0'
		GROUP BY A.account_cd,ACC.account_nm 
		UNION ALL 
		SELECT A.account_cd,ACC.account_nm,
		SUM(A.total_price) AS amount 
		FROM trx_medical_settlement_inv A 
		LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
		WHERE A.medical_cd=@strMedicalChildCd
		AND A.payment_st='PAYMENT_ST_0'
		GROUP BY A.account_cd,ACC.account_nm
		--PROSES TRANSAKSI CHILD
		) ALIAS
	WHERE ALIAS.amount<>0
	GROUP BY ALIAS.account_cd,ALIAS.account_nm
	ORDER BY ALIAS.account_cd,ALIAS.account_nm
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strAccountCd,@numAmount
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--Insert table trx_settlement_account
		INSERT INTO trx_settlement_account (settlement_no,account_cd,amount,
		modi_id,modi_datetime)
		VALUES (@intSettlementNo,@strAccountCd,@numAmount,
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strAccountCd,@numAmount
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	--Update trx_medical medical_trx_st
	--UPDATE trx_medical SET medical_trx_st='MEDICAL_TRX_ST_1',datetime_out=@pdtInvoiceDate
	UPDATE trx_medical SET medical_trx_st='MEDICAL_TRX_ST_1',datetime_out=CASE WHEN datetime_out IS NULL THEN @pdtInvoiceDate ELSE datetime_out END
	WHERE medical_cd=@pstrMedicalCd
	
	--IF @strMedicalRootCd <>'' AND @strMedicalRootCd IS NOT NULL
	IF ISNULL(@strMedicalRootCd,'') <>''
	BEGIN
		--UPDATE trx_medical SET medical_trx_st='MEDICAL_TRX_ST_1',datetime_out=@pdtInvoiceDate
		UPDATE trx_medical SET medical_trx_st='MEDICAL_TRX_ST_1',datetime_out=CASE WHEN datetime_out IS NULL THEN @pdtInvoiceDate ELSE datetime_out END
		WHERE medical_cd=@strMedicalRootCd
		
		--root of root
		--UPDATE trx_medical SET medical_trx_st='MEDICAL_TRX_ST_1',datetime_out=@pdtInvoiceDate
		UPDATE trx_medical SET medical_trx_st='MEDICAL_TRX_ST_1',datetime_out=CASE WHEN datetime_out IS NULL THEN @pdtInvoiceDate ELSE datetime_out END
		WHERE medical_cd=@strMedicalRootRootCd
	END
	--IF @strMedicalChildCd <>'' AND @strMedicalChildCd IS NOT NULL
	IF ISNULL(@strMedicalChildCd,'') <>''
	BEGIN
		--UPDATE trx_medical SET medical_trx_st='MEDICAL_TRX_ST_1',datetime_out=@pdtInvoiceDate
		UPDATE trx_medical SET medical_trx_st='MEDICAL_TRX_ST_1',datetime_out=CASE WHEN datetime_out IS NULL THEN @pdtInvoiceDate ELSE datetime_out END
		WHERE medical_cd=@strMedicalChildCd
	END
	
	--Update status pasien --> pasien lama
	UPDATE trx_pasien SET register_st='0'
	WHERE pasien_cd=@pstrPasienCd
	
	--Table trx_bank
	--rawat inap + pasien umum
	/*DECLARE @strMedicalTp varchar(20)
	DECLARE @strPasienNm varchar(100)
	DECLARE @strAddress varchar(1000)
	DECLARE @numPayAmount numeric(15)
	DECLARE @numPayPasien numeric(15)
	
	DECLARE @dtDatetimeIn datetime
	DECLARE @dtDatetimeOut datetime
	DECLARE @strAkomodasi varchar(50)
	
	--SET @strMedicalTp = (SELECT medical_tp FROM trx_medical WHERE medical_cd=@pstrMedicalCd)
	SELECT @strMedicalTp=A.medical_tp,@strAkomodasi=KLS.kelas_nm,
	@dtDatetimeIn=A.datetime_in,@dtDatetimeOut=A.datetime_out
	FROM trx_medical A 
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	WHERE A.medical_cd=@pstrMedicalCd
	
	--IF @strMedicalTp = 'MEDICAL_TP_02' AND ISNULL(@pstrInsuranceCd,'') = ''
	--BEGIN
	--	SELECT @strPasienNm=C.pasien_nm,
	--	@strAddress=ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + 
	--	ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,''),
	--	@numPayAmount=simrke.FN_GET_INVOICE_TOTAL(@intSettlementNo)
	--	FROM trx_settlement A 
	--	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	--	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	--	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	--	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	--	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	--	WHERE A.settlement_no=@intSettlementNo
			
	--	INSERT INTO trx_bank (settlement_no,invoice_no,pasien_nm,address,invoice_date,
	--	amount,bank_cd,payment_tp,payment_st,bank_no,
	--	datetime_in,datetime_out,medical_tp,akomodasi,
	--	modi_id,modi_datetime)
	--	VALUES (@intSettlementNo,@strInvoiceNo,@strPasienNm,@strAddress,@pdtInvoiceDate,
	--	@numPayAmount,'BRI','TELLER','0','',
	--	@dtDatetimeIn,@dtDatetimeOut,'Rawat Inap',@strAkomodasi,
	--	@pstrUserId,GETDATE())
	--END
	SELECT @strPasienNm=C.pasien_nm,
	@strAddress=ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + 
	ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,''),
	@numPayAmount=simrke.FN_GET_INVOICE_TOTAL(@intSettlementNo),@numPayPasien=ISNULL(A.amount_pasien,0)
	FROM trx_settlement A 
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	WHERE A.settlement_no=@intSettlementNo
	
	IF ISNULL(@pstrInsuranceCd,'') <> ''
	BEGIN
		IF @numPayPasien <> 0
			SET @numPayAmount = @numPayPasien
	END
	
	INSERT INTO trx_bank (settlement_no,invoice_no,pasien_nm,address,invoice_date,
	amount,bank_cd,payment_tp,payment_st,bank_no,
	datetime_in,datetime_out,medical_tp,akomodasi,
	modi_id,modi_datetime)
	VALUES (@intSettlementNo,@strInvoiceNo,@strPasienNm,@strAddress,@pdtInvoiceDate,
	@numPayAmount,'BRI','TELLER','0','',
	@dtDatetimeIn,@dtDatetimeOut,'Rawat Inap',@strAkomodasi,
	@pstrUserId,GETDATE())*/
	--End Table trx_bank
	
	--Clear kamar
	UPDATE trx_ruang SET ruang_st='0'
	WHERE ruang_cd=(SELECT ruang_cd FROM trx_medical WHERE medical_cd=@pstrMedicalCd)
	
	DECLARE @strKelasDefault varchar(20)
	SELECT @strKelasDefault=ISNULL(kelas_default,'')
	FROM trx_ruang WHERE ruang_cd=(SELECT ruang_cd FROM trx_medical WHERE medical_cd=@pstrMedicalCd)
	
	IF (@strKelasDefault <> '')
	BEGIN
		IF EXISTS(SELECT kelas_cd FROM trx_kelas WHERE kelas_cd=@strKelasDefault)
		BEGIN
			UPDATE trx_ruang SET kelas_cd=kelas_default
			WHERE ruang_cd=(SELECT ruang_cd FROM trx_medical WHERE medical_cd=@pstrMedicalCd)
		END
	END
	--End Clear kamar
	
	IF @@ERROR<> 0
	BEGIN
		ROLLBACK TRANSACTION
		
		--Proses Check invoice_no in used at temporary table (trx_temp)
		DELETE FROM trx_temp WHERE data_10=@strInvoiceNo
		--End Proses Check invoice_no
		
		RETURN
	END
	ELSE
	BEGIN
		COMMIT TRANSACTION
		
		--Proses Check invoice_no in used at temporary table (trx_temp)
		DELETE FROM trx_temp WHERE data_10=@strInvoiceNo
		--End Proses Check invoice_no
		
		SELECT @intSettlementNo AS settlement_no
	END	
END;

DROP FUNCTION IF EXISTS SP_PROSES_INVOICE_EXT(bigint,Datetime,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PROSES_INVOICE_EXT]
@pintSettlementNo bigint,
@pdtTrxDate Datetime,
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @numAmount numeric
	DECLARE @numAmountPasien numeric
	DECLARE @strExtStatus char(1)
	
	SELECT @numAmountPasien=ISNULL(A.amount_pasien,0),@strExtStatus=ISNULL(A.ext_st,'')
	FROM trx_settlement A
	WHERE A.settlement_no=@pintSettlementNo
	
	SET @numAmount = (SELECT SUM(B.amount)
					  FROM trx_settlement A
					  JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
					  WHERE A.settlement_no=@pintSettlementNo)
	
	IF @strExtStatus <> '0'
	BEGIN
		IF @numAmountPasien <> 0
			UPDATE trx_settlement
			SET ext_amount=@numAmountPasien,ext_date=GETDATE(),
			ext_user=@pstrUserId,ext_st='0'
			WHERE settlement_no=@pintSettlementNo
		ELSE
			UPDATE trx_settlement
			SET ext_amount=@numAmount,ext_date=GETDATE(),
			ext_user=@pstrUserId,ext_st='0'
			WHERE settlement_no=@pintSettlementNo
	END
END;

DROP FUNCTION IF EXISTS SP_PROSES_INVOICE_INCOME(bigint,Datetime,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PROSES_INVOICE_INCOME] 
@pintMedicalResepSeqno bigint,
@pdtInvoiceDate Datetime,
@pstrPaymentTp Varchar(20),
@pstrPaymentCd Varchar(20),
@pstrCardNo Varchar(50),
@pstrEntryNm Varchar(100),
@pstrPayNm Varchar(100),
@pstrPaymentSt Varchar(20),
@pstrNote Varchar(1000),
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @intSettlementNo bigint
	DECLARE @strInvoiceNo varchar(20)
	DECLARE @strAccountCd varchar(20)
	DECLARE @strAccountNm varchar(100)
	DECLARE @numAmount numeric
	
	SET @strInvoiceNo = (SELECT simrke.FN_GET_INVOICENO())
	IF EXISTS (SELECT invoice_no FROM trx_settlement
			   WHERE invoice_no=@strInvoiceNo)
	BEGIN
		SET @strInvoiceNo = (SELECT simrke.FN_GET_INVOICENO())
	END
	
	--Proses Check invoice_no in used at temporary table (trx_temp)
	INSERT INTO trx_temp(data_10) VALUES(@strInvoiceNo)
	--End Proses Check invoice_no
	
	BEGIN TRANSACTION
	
	--Insert table trx_settlement
	INSERT INTO trx_settlement (ref_medical_resep_seqno,payment_tp,invoice_no,invoice_date,
	payment_cd,card_no,
	entry_nm,entry_date,pay_nm,payment_st,note,
	modi_id,modi_datetime)
	VALUES (@pintMedicalResepSeqno,@pstrPaymentTp,@strInvoiceNo,@pdtInvoiceDate,
	@pstrPaymentCd,@pstrCardNo,
	@pstrEntryNm,GETDATE(),@pstrPayNm,@pstrPaymentSt,@pstrNote,
	@pstrUserId,GETDATE())
	
	SET @intSettlementNo = (SELECT MAX(settlement_no) FROM trx_settlement WHERE ref_medical_resep_seqno=@pintMedicalResepSeqno)
	
	DECLARE cursorData CURSOR FOR
	SELECT	ALIAS.account_cd,SUM(ALIAS.amount) AS amount FROM
		(SELECT A.account_cd,ACC.account_nm,
		SUM(A.total_price) AS amount 
		FROM trx_medical_settlement_inv A 
		LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
		WHERE A.ref_medical_resep_seqno=@pintMedicalResepSeqno
		AND A.payment_st='PAYMENT_ST_0'
		GROUP BY A.account_cd,ACC.account_nm) ALIAS
	WHERE ALIAS.amount<>0
	GROUP BY ALIAS.account_cd,ALIAS.account_nm
	ORDER BY ALIAS.account_nm
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strAccountCd,@numAmount
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--Insert table trx_settlement_account
		INSERT INTO trx_settlement_account (settlement_no,account_cd,amount,
		modi_id,modi_datetime)
		VALUES (@intSettlementNo,@strAccountCd,@numAmount,
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strAccountCd,@numAmount
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	IF @@ERROR<> 0
	BEGIN
		ROLLBACK TRANSACTION
		
		--Proses Check invoice_no in used at temporary table (trx_temp)
		DELETE FROM trx_temp WHERE data_10=@strInvoiceNo
		--End Proses Check invoice_no
		
		RETURN
	END
	ELSE
	BEGIN
		COMMIT TRANSACTION
		
		--Proses Check invoice_no in used at temporary table (trx_temp)
		DELETE FROM trx_temp WHERE data_10=@strInvoiceNo
		--End Proses Check invoice_no
		
		SELECT @intSettlementNo AS settlement_no
	END	
END;

DROP FUNCTION IF EXISTS SP_PROSES_INVOICE_ITEM(Varchar,Varchar,Datetime,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,numeric,numeric,numeric,numeric,numeric,numeric,numeric,numeric,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PROSES_INVOICE_ITEM] 
@pstrMedicalCd Varchar(10),
@pstrPasienCd Varchar(20),
@pdtInvoiceDate Datetime,
@pstrPaymentTp Varchar(20),
@pstrInsuranceCd Varchar(20),
@pstrPaymentCd Varchar(20),
@pstrCardNo Varchar(50),
@pstrEntryNm Varchar(100),
@pstrPayNm Varchar(100),
@pstrPaymentSt Varchar(20),
@pnumAmountAsuransi numeric,
@pnumAmountPasien numeric,
@pnumAmountTunai numeric,
@pnumAmountNontunai numeric,
@pnumDiscountPersen numeric(5,2),
@pnumDiscountAmount numeric,
@pnumPayBayar numeric,
@pnumPaySisa numeric,
@pstrNote Varchar(1000),
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @intSettlementNo bigint
	DECLARE @strInvoiceNo varchar(20)
	DECLARE @strAccountCd varchar(20)
	DECLARE @strAccountNm varchar(100)
	DECLARE @numAmount numeric
	
	DECLARE @strTarifTp varchar(20)
	DECLARE @strDataCd varchar(20)
	DECLARE @intRefSeqno bigint
	DECLARE @intMedicalSettSeqno bigint
	
	DECLARE @strMedicalCdSELECT varchar(10)
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalRootRootCd varchar(10)
	DECLARE @strMedicalChildCd varchar(10)
	
	SET @strMedicalRootCd = (SELECT medical_root_cd FROM trx_medical WHERE medical_cd=@pstrMedicalCd)
	SET @strMedicalRootRootCd = (SELECT A.medical_root_cd
								FROM trx_medical A 
								WHERE A.medical_cd=@strMedicalRootCd)
	SET @strMedicalChildCd = (SELECT TOP 1 medical_cd FROM trx_medical WHERE medical_root_cd=@pstrMedicalCd)
	
	SET @strInvoiceNo = (SELECT simrke.FN_GET_INVOICENO())
	IF EXISTS (SELECT invoice_no FROM trx_settlement
			   WHERE invoice_no=@strInvoiceNo)
	BEGIN
		SET @strInvoiceNo = (SELECT simrke.FN_GET_INVOICENO())
	END
	
	--Proses Check invoice_no in used at temporary table (trx_temp)
	INSERT INTO trx_temp(data_10) VALUES(@strInvoiceNo)
	--End Proses Check invoice_no
	
	BEGIN TRANSACTION
	
	--Update payment_st
	DECLARE cursorData CURSOR FOR
	SELECT A.tarif_tp,A.data_cd,A.ref_seqno,A.seq_no,A.medical_cd
	FROM trx_medical_settlement A 
	WHERE A.medical_cd=@pstrMedicalCd
	AND A.payment_st='PAYMENT_ST_0'
	AND A.payitem_st='1'
	UNION ALL
	--transaksi root
	SELECT A.tarif_tp,A.data_cd,A.ref_seqno,A.seq_no,A.medical_cd
	FROM trx_medical_settlement A 
	WHERE A.medical_cd=@strMedicalRootCd
	AND A.payment_st='PAYMENT_ST_0'
	AND A.payitem_st='1'
	UNION ALL
	--transaksi root of root
	SELECT A.tarif_tp,A.data_cd,A.ref_seqno,A.seq_no,A.medical_cd
	FROM trx_medical_settlement A 
	WHERE A.medical_cd=@strMedicalRootRootCd
	AND A.payment_st='PAYMENT_ST_0'
	AND A.payitem_st='1'
	--PROSES TRANSAKSI CHILD
	UNION ALL
	--transaksi child
	SELECT A.tarif_tp,A.data_cd,A.ref_seqno,A.seq_no,A.medical_cd
	FROM trx_medical_settlement A 
	WHERE A.medical_cd=@strMedicalChildCd
	AND A.payment_st='PAYMENT_ST_0'
	AND A.payitem_st='1'
	--END PROSES TRANSAKSI CHILD
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strTarifTp,@strDataCd,@intRefSeqno,@intMedicalSettSeqno,@strMedicalCdSELECT
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		UPDATE trx_medical_settlement SET invoice_no=@strInvoiceNo
		WHERE medical_cd=@strMedicalCdSELECT
		AND seq_no=@intMedicalSettSeqno
			
		IF @strTarifTp = 'TARIF_TP_02'
		--Lab/Radiologi
		BEGIN
			UPDATE trx_medical_unit SET payment_st='PAYMENT_ST_1', 
			modi_id=@pstrUserId, modi_datetime=GETDATE()
			WHERE medical_cd=@strMedicalCdSELECT
			AND medical_unit_seqno=@intRefSeqno
		END
		
		IF @strTarifTp = 'TARIF_TP_04'
		--Tindakan
		BEGIN
			UPDATE trx_medical_tindakan SET payment_st='PAYMENT_ST_1', 
			modi_id=@pstrUserId, modi_datetime=GETDATE()
			WHERE medical_cd=@strMedicalCdSELECT
			AND medical_tindakan_seqno=@intRefSeqno
		END
		
		FETCH NEXT FROM cursorData
		INTO @strTarifTp,@strDataCd,@intRefSeqno,@intMedicalSettSeqno,@strMedicalCdSELECT
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	DECLARE cursorData CURSOR FOR
	SELECT CASE WHEN A.ref_resep_seqno IS NOT NULL THEN 'TARIF_TP_05' ELSE 'TARIF_TP_05B' END AS tarif_tp,
	A.item_cd,CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno,A.seq_no,A.medical_cd
	FROM trx_medical_settlement_inv A 
	WHERE A.medical_cd=@pstrMedicalCd
	AND A.payment_st='PAYMENT_ST_0'
	AND A.payitem_st='1'
	UNION ALL
	--transaksi root
	SELECT CASE WHEN A.ref_resep_seqno IS NOT NULL THEN 'TARIF_TP_05' ELSE 'TARIF_TP_05B' END AS tarif_tp,
	A.item_cd,CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno,A.seq_no,A.medical_cd
	FROM trx_medical_settlement_inv A 
	WHERE A.medical_cd=@strMedicalRootCd
	AND A.payment_st='PAYMENT_ST_0'
	AND A.payitem_st='1'
	UNION ALL
	--transaksi root of root
	SELECT CASE WHEN A.ref_resep_seqno IS NOT NULL THEN 'TARIF_TP_05' ELSE 'TARIF_TP_05B' END AS tarif_tp,
	A.item_cd,CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno,A.seq_no,A.medical_cd
	FROM trx_medical_settlement_inv A 
	WHERE A.medical_cd=@strMedicalRootRootCd
	AND A.payment_st='PAYMENT_ST_0'
	AND A.payitem_st='1'
	--PROSES TRANSAKSI CHILD
	UNION ALL
	--transaksi child
	SELECT CASE WHEN A.ref_resep_seqno IS NOT NULL THEN 'TARIF_TP_05' ELSE 'TARIF_TP_05B' END AS tarif_tp,
	A.item_cd,CASE WHEN A.ref_resep_seqno IS NOT NULL THEN A.ref_resep_seqno ELSE A.ref_medical_alkes_seqno END AS ref_seqno,A.seq_no,A.medical_cd
	FROM trx_medical_settlement_inv A 
	WHERE A.medical_cd=@strMedicalChildCd
	AND A.payment_st='PAYMENT_ST_0'
	AND A.payitem_st='1'
	--END PROSES TRANSAKSI CHILD
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strTarifTp,@strDataCd,@intRefSeqno,@intMedicalSettSeqno,@strMedicalCdSELECT
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		UPDATE trx_medical_settlement_inv SET invoice_no=@strInvoiceNo
		WHERE medical_cd=@strMedicalCdSELECT
		AND seq_no=@intMedicalSettSeqno
			
		IF @strTarifTp = 'TARIF_TP_05'
		--Obat
		BEGIN
			UPDATE trx_resep_data SET payment_st='PAYMENT_ST_1', 
			modi_id=@pstrUserId, modi_datetime=GETDATE()
			WHERE item_cd=@strDataCd
			AND resep_seqno=@intRefSeqno
		END
		
		IF @strTarifTp = 'TARIF_TP_05B'
		--BHP/Alkes
		BEGIN
			UPDATE trx_medical_alkes SET payment_st='PAYMENT_ST_1', 
			modi_id=@pstrUserId, modi_datetime=GETDATE()
			WHERE medical_cd=@strMedicalCdSELECT
			AND medical_alkes_seqno=@intRefSeqno
		END
		
		FETCH NEXT FROM cursorData
		INTO @strTarifTp,@strDataCd,@intRefSeqno,@intMedicalSettSeqno,@strMedicalCdSELECT
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	--End Update payment_st
	
	--Insert table trx_settlement
	INSERT INTO trx_settlement (medical_cd,pasien_cd,payment_tp,insurance_cd,invoice_no,invoice_date,
	payment_cd,card_no,amount_asuransi,amount_pasien,amount_tunai,amount_nontunai,discount_percent,discount_amount,
	entry_nm,entry_date,pay_nm,payment_st,info_11,info_12,note,
	modi_id,modi_datetime)
	VALUES (@pstrMedicalCd,@pstrPasienCd,@pstrPaymentTp,@pstrInsuranceCd,@strInvoiceNo,@pdtInvoiceDate,
	@pstrPaymentCd,@pstrCardNo,@pnumAmountAsuransi,@pnumAmountPasien,@pnumAmountTunai,@pnumAmountNontunai,@pnumDiscountPersen,@pnumDiscountAmount,
	@pstrEntryNm,GETDATE(),@pstrPayNm,@pstrPaymentSt,@pnumPayBayar,@pnumPaySisa,@pstrNote,
	@pstrUserId,GETDATE())
	
	SET @intSettlementNo = (SELECT MAX(settlement_no) FROM trx_settlement WHERE medical_cd=@pstrMedicalCd)
	
	DECLARE cursorData CURSOR FOR
	SELECT	ALIAS.account_cd,SUM(ALIAS.amount) AS amount FROM
		(SELECT A.account_cd,ACC.account_nm,
		SUM(A.amount) AS amount 
		FROM trx_medical_settlement A 
		LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
		--WHERE A.medical_cd=@pstrMedicalCd --OR A.invoice_no=A.invoice_no
		WHERE A.invoice_no=@strInvoiceNo
		AND A.payment_st='PAYMENT_ST_0'
		AND A.payitem_st='1'
		GROUP BY A.account_cd,ACC.account_nm 
		UNION ALL 
		SELECT A.account_cd,ACC.account_nm,
		SUM(A.total_price) AS amount 
		FROM trx_medical_settlement_inv A 
		LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd
		--WHERE A.medical_cd=@pstrMedicalCd --OR A.invoice_no=A.invoice_no
		WHERE A.invoice_no=@strInvoiceNo
		AND A.payment_st='PAYMENT_ST_0'
		AND A.payitem_st='1'
		GROUP BY A.account_cd,ACC.account_nm) ALIAS
	WHERE ALIAS.amount<>0
	GROUP BY ALIAS.account_cd,ALIAS.account_nm
	ORDER BY ALIAS.account_cd,ALIAS.account_nm
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strAccountCd,@numAmount
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--Insert table trx_settlement_account
		INSERT INTO trx_settlement_account (settlement_no,account_cd,amount,
		modi_id,modi_datetime)
		VALUES (@intSettlementNo,@strAccountCd,@numAmount,
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strAccountCd,@numAmount
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	IF @@ERROR<> 0
	BEGIN
		ROLLBACK TRANSACTION
		
		--Proses Check invoice_no in used at temporary table (trx_temp)
		DELETE FROM trx_temp WHERE data_10=@strInvoiceNo
		--End Proses Check invoice_no
		
		RETURN
	END
	ELSE
	BEGIN
		COMMIT TRANSACTION
		
		--Proses Check invoice_no in used at temporary table (trx_temp)
		DELETE FROM trx_temp WHERE data_10=@strInvoiceNo
		--End Proses Check invoice_no
		
		SELECT @intSettlementNo AS settlement_no,@strInvoiceNo AS invoice_no
	END	
END;

DROP FUNCTION IF EXISTS SP_PROSES_INVOICE_SALE(bigint,Datetime,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,numeric,numeric,numeric,numeric,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PROSES_INVOICE_SALE] 
@pintMedicalResepSeqno bigint,
@pdtInvoiceDate Datetime,
@pstrPaymentTp Varchar(20),
@pstrPaymentCd Varchar(20),
@pstrCardNo Varchar(50),
@pstrEntryNm Varchar(100),
@pstrPayNm Varchar(100),
@pstrPaymentSt Varchar(20),
@pnumAmountTunai numeric,
@pnumAmountNontunai numeric,
@pnumDiscountPersen numeric(5,2),
@pnumDiscountAmount numeric,
@pstrNote Varchar(1000),
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @intSettlementNo bigint
	DECLARE @strInvoiceNo varchar(20)
	DECLARE @strAccountCd varchar(20)
	DECLARE @strAccountNm varchar(100)
	DECLARE @numAmount numeric
	
	SET @strInvoiceNo = (SELECT simrke.FN_GET_INVOICENO())
	IF EXISTS (SELECT invoice_no FROM trx_settlement
			   WHERE invoice_no=@strInvoiceNo)
	BEGIN
		SET @strInvoiceNo = (SELECT simrke.FN_GET_INVOICENO())
	END
	
	--Proses Check invoice_no in used at temporary table (trx_temp)
	INSERT INTO trx_temp(data_10) VALUES(@strInvoiceNo)
	--End Proses Check invoice_no
	
	BEGIN TRANSACTION
	
	--Insert table trx_settlement
	INSERT INTO trx_settlement (ref_medical_resep_seqno,payment_tp,invoice_no,invoice_date,
	payment_cd,card_no,amount_tunai,amount_nontunai,discount_percent,discount_amount,
	entry_nm,entry_date,pay_nm,payment_st,note,
	modi_id,modi_datetime)
	VALUES (@pintMedicalResepSeqno,@pstrPaymentTp,@strInvoiceNo,@pdtInvoiceDate,
	@pstrPaymentCd,@pstrCardNo,@pnumAmountTunai,@pnumAmountNontunai,@pnumDiscountPersen,@pnumDiscountAmount,
	@pstrEntryNm,GETDATE(),@pstrPayNm,@pstrPaymentSt,@pstrNote,
	@pstrUserId,GETDATE())
	
	SET @intSettlementNo = (SELECT MAX(settlement_no) FROM trx_settlement WHERE ref_medical_resep_seqno=@pintMedicalResepSeqno)
	
	DECLARE cursorData CURSOR FOR
	SELECT	ALIAS.account_cd,SUM(ALIAS.amount) AS amount FROM
		(SELECT A.account_cd,ACC.account_nm,
		SUM(A.total_price) AS amount 
		FROM trx_medical_settlement_inv A 
		LEFT JOIN com_account ACC ON A.account_cd=ACC.account_cd 
		WHERE A.ref_medical_resep_seqno=@pintMedicalResepSeqno
		AND A.payment_st='PAYMENT_ST_0'
		GROUP BY A.account_cd,ACC.account_nm) ALIAS
	WHERE ALIAS.amount<>0
	GROUP BY ALIAS.account_cd,ALIAS.account_nm
	ORDER BY ALIAS.account_nm
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strAccountCd,@numAmount
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--Insert table trx_settlement_account
		INSERT INTO trx_settlement_account (settlement_no,account_cd,amount,
		modi_id,modi_datetime)
		VALUES (@intSettlementNo,@strAccountCd,@numAmount,
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strAccountCd,@numAmount
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	IF @@ERROR<> 0
	BEGIN
		ROLLBACK TRANSACTION
		
		--Proses Check invoice_no in used at temporary table (trx_temp)
		DELETE FROM trx_temp WHERE data_10=@strInvoiceNo
		--End Proses Check invoice_no
		
		RETURN
	END
	ELSE
	BEGIN
		COMMIT TRANSACTION
		
		--Proses Check invoice_no in used at temporary table (trx_temp)
		DELETE FROM trx_temp WHERE data_10=@strInvoiceNo
		--End Proses Check invoice_no
		
		SELECT @intSettlementNo AS settlement_no
	END	
END;

DROP FUNCTION IF EXISTS SP_PROSES_MEDICAL_AUTO(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PROSES_MEDICAL_AUTO] 
@pstrMedicalCd Varchar(10),
@pstrUserId Varchar(20)
AS
BEGIN
	UPDATE trx_medical_tindakan
	SET modi_datetime=modi_datetime
	WHERE medical_cd=@pstrMedicalCd
	
	/*DECLARE @strMedicalTp varchar(20)
	DECLARE @strPasienCd varchar(20)
	DECLARE @strDrCd varchar(20)
	DECLARE @dtDatetimeTrx datetime
	DECLARE @strMedunitCd varchar(20)
	DECLARE @strKelasCd varchar(20)
	DECLARE @strAsuransiCd varchar(20)
	DECLARE @strTreatmentCd varchar(20)
	DECLARE @strTreatmentNm varchar(1000)
	
	SELECT @strMedicalTp=A.medical_tp,@strPasienCd=A.pasien_cd,@strMedunitCd=A.medunit_cd,
	@strKelasCd=D.kelas_cd,@strAsuransiCd=C.insurance_cd,
	@strDrCd=A.dr_cd,@dtDatetimeTrx=A.datetime_in
	FROM trx_medical A 
	JOIN trx_pasien B ON A.pasien_cd=B.pasien_cd 
	LEFT JOIN trx_pasien_insurance C ON A.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_ruang D ON A.ruang_cd=D.ruang_cd 
	WHERE A.medical_cd=@pstrMedicalCd
	
	SET @strTreatmentCd = ''
	IF @strMedicalTp = 'MEDICAL_TP_01'
	--Rawat Jalan
	BEGIN
		--HARDCODE
		IF @strMedunitCd IN ('POLIUMUM','POLIFIS','POLIGIZI','POLIPSI','POLITK','POLITPW','POLIGIGI')
		--Poli Umum : KARCIS RAWAT JALAN TK. 1
		BEGIN
			SET @strTreatmentCd = 'IRJ01'
		END
		
		IF @strMedunitCd IN ('POLIREHAB','POLISPA','POLISPB','POLISPD','POLISPD2','POLISPJ','POLISPJW','POLISPKK','POLISPM','POLISPOG','POLISPOT','POLISPP','POLISPS','POLISPTHT')
		--Poli Spesialis : KARCIS RAWAT JALAN TK. LANJUTAN
		BEGIN
			SET @strTreatmentCd = 'IRJ02'
		END
		
		IF @strMedunitCd IN ('POLIUGD')
		--Poli UGD : KARCIS RAWAT GAWAT DARURAT
		BEGIN
			SET @strTreatmentCd = 'IRJ03'
		END
		
		--End HARDCODE
		
		IF @strTreatmentCd <> ''
		BEGIN
			SET @strTreatmentNm = (SELECT treatment_nm FROM trx_tindakan WHERE treatment_cd=@strTreatmentCd)
			
			INSERT INTO trx_medical_tindakan (dr_cd,datetime_trx,treatment_cd,medical_cd,
			medical_note,quantity,treatment_st,medunit_cd,modi_id,modi_datetime)
			VALUES (@strDrCd,@dtDatetimeTrx,@strTreatmentCd,@pstrMedicalCd,@strTreatmentNm,1,'0',@strMedunitCd,@pstrUserId,GETDATE())
		END
	END
	
	IF @strMedicalTp = 'MEDICAL_TP_02'
	--Rawat Inap
	BEGIN
		--HARDCODE
		
		--GELANG PASIEN
		SET @strTreatmentCd = 'BHP101'
		BEGIN
			SET @strTreatmentNm = (SELECT treatment_nm FROM trx_tindakan WHERE treatment_cd=@strTreatmentCd)
			
			INSERT INTO trx_medical_tindakan (dr_cd,datetime_trx,treatment_cd,medical_cd,
			medical_note,quantity,treatment_st,modi_id,modi_datetime)
			VALUES (@strDrCd,@dtDatetimeTrx,@strTreatmentCd,@pstrMedicalCd,@strTreatmentNm,1,'0',@pstrUserId,GETDATE())
		END
		
		SET @strTreatmentCd = 'PMI05'
		--PERSONAL HYGIENE
		BEGIN
			SET @strTreatmentNm = (SELECT treatment_nm FROM trx_tindakan WHERE treatment_cd=@strTreatmentCd)
			
			INSERT INTO trx_medical_tindakan (dr_cd,datetime_trx,treatment_cd,medical_cd,
			medical_note,quantity,treatment_st,modi_id,modi_datetime)
			VALUES (@strDrCd,@dtDatetimeTrx,@strTreatmentCd,@pstrMedicalCd,@strTreatmentNm,1,'0',@pstrUserId,GETDATE())
		END
		
		--End HARDCODE
	END
	*/
END;

DROP FUNCTION IF EXISTS SP_PROSES_MEDICAL_DR(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PROSES_MEDICAL_DR] 
@pstrMedicalCd Varchar(10),
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @strMedicalTp varchar(20)
	DECLARE @strDrCd varchar(20)
	DECLARE @strTreatmentDr varchar(20)
	
	SELECT @strMedicalTp=A.medical_tp,@strDrCd=A.dr_cd
	FROM trx_medical A 
	WHERE A.medical_cd=@pstrMedicalCd
	
	IF @strMedicalTp = 'MEDICAL_TP_01'
	--Rawat Jalan
	BEGIN
		IF @strDrCd <> ''
		BEGIN
			--HARDCODE
			UPDATE trx_medical_tindakan
			SET dr_cd=@strDrCd
			WHERE medical_cd=@pstrMedicalCd
			AND dr_cd='TIMDOKTER'
			--OR ISNULL(dr_cd,'')=''
			--End HARDCODE
		END
	END
END;

DROP FUNCTION IF EXISTS SP_PROSES_RUANG(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PROSES_RUANG] 
@pstrMedicalCd Varchar(10),
@pstrNewRuangCd Varchar(20),
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @dtDatetimeIn datetime
	DECLARE @strPrevRuangCd varchar(20)
	DECLARE @strPrevKelasCd varchar(20)
	DECLARE @intSeqNo int
	DECLARE @strHistoryRuangCd varchar(20)
	DECLARE @dtDateHistoryStart datetime
	DECLARE @dtDateHistoryEnd datetime
	
	SELECT @strPrevRuangCd=A.ruang_cd,@strPrevKelasCd=KMR.kelas_cd,@dtDatetimeIn=A.datetime_in
	FROM trx_medical A
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	WHERE A.medical_cd=@pstrMedicalCd
	
	IF @strPrevRuangCd <> @pstrNewRuangCd
	BEGIN
		SELECT TOP 1 @intSeqNo=seq_no,@strHistoryRuangCd=A.ruang_cd,
		@dtDateHistoryStart=A.datetime_start,@dtDateHistoryEnd=A.datetime_end
		FROM trx_medical_ruang A
		WHERE A.medical_cd=@pstrMedicalCd
		ORDER BY datetime_start DESC
	
		UPDATE trx_ruang
		SET ruang_st='0'
		WHERE ruang_cd=@strPrevRuangCd
		
		--Set default kelas
		DECLARE @strKelasDefault varchar(20)
		SELECT @strKelasDefault=ISNULL(kelas_default,'')
		FROM trx_ruang WHERE ruang_cd=@strPrevRuangCd
		
		IF (@strKelasDefault <> '')
		BEGIN
			IF EXISTS(SELECT kelas_cd FROM trx_kelas WHERE kelas_cd=@strKelasDefault)
			BEGIN
				UPDATE trx_ruang SET kelas_cd=kelas_default
				WHERE ruang_cd=@strPrevRuangCd
			END
		END
		--End default kelas
		
		UPDATE trx_ruang
		SET ruang_st='1'
		WHERE ruang_cd=@pstrNewRuangCd
		
		IF simrke.FN_FORMATDATE(@dtDatetimeIn) <> simrke.FN_FORMATDATE(GETDATE())
		BEGIN
			IF @dtDateHistoryStart IS NULL
			BEGIN
				INSERT INTO trx_medical_ruang (medical_cd,ruang_cd,datetime_start,datetime_end,kelas_cd,
				modi_id,modi_datetime)
				VALUES (@pstrMedicalCd,@strPrevRuangCd,@dtDatetimeIn,GETDATE()-1,@strPrevKelasCd,
				@pstrUserId,GETDATE())
			END
			ELSE
			BEGIN
				IF simrke.FN_FORMATDATE(@dtDateHistoryEnd) <> simrke.FN_FORMATDATE(GETDATE()-1)
				BEGIN
					/*UPDATE trx_medical_ruang
					SET ruang_cd=@pstrNewRuangCd
					WHERE seq_no=@intSeqNo*/
					INSERT INTO trx_medical_ruang (medical_cd,ruang_cd,datetime_start,datetime_end,kelas_cd,
					modi_id,modi_datetime)
					VALUES (@pstrMedicalCd,@strPrevRuangCd,@dtDateHistoryEnd+1,GETDATE()-1,@strPrevKelasCd,
					@pstrUserId,GETDATE())
				END
			END
		END
	END
END;

DROP FUNCTION IF EXISTS SP_PROSES_SETTLEMENT(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PROSES_SETTLEMENT] 
@pstrMedicalCd Varchar(10),
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @strMedicalTp varchar(20)
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strPasienCd varchar(20)
	DECLARE @strDrCd varchar(20)
	DECLARE @strMedunitCd varchar(20)
	DECLARE @strKelasCd varchar(20)
	DECLARE @strAsuransiCd varchar(20)
	
	DECLARE @strKelasCdTemp varchar(20)
	DECLARE @strAsuransiCdTemp varchar(20)
	DECLARE @strKelasCdAKTIF varchar(20)
	DECLARE @numAsuransiTarif numeric(5,2)
	
	DECLARE @strTarifTp varchar(20)
	DECLARE @strAccountCd varchar(20)
	DECLARE @dtDatetimeIn datetime
	DECLARE @dtDatetimeTrx datetime
	DECLARE @dtDatetimeOut datetime
	DECLARE @strDataCd varchar(20)
	DECLARE @strDataNm varchar(100)
	DECLARE @numAmountItem numeric
	DECLARE @numAmount numeric
	DECLARE @numJumlah numeric(10,2)
	DECLARE @numTotalPrice numeric
	DECLARE @strNote varchar(1000)
	DECLARE @strTempCd varchar(20)
	DECLARE @strTemp2Cd varchar(20)
	
	DECLARE @intRefSeqno bigint
	DECLARE @intRefResepSeqno bigint
	DECLARE @intRefAlkesSeqno bigint
	
	SELECT @strMedicalTp=A.medical_tp,@strMedicalRootCd=A.medical_root_cd,@strPasienCd=A.pasien_cd,
	@strDrCd=A.dr_cd,@strMedunitCd=A.medunit_cd,@dtDatetimeIn=A.datetime_in,@dtDatetimeOut=A.datetime_out,
	@strKelasCd=D.kelas_cd,@strAsuransiCd=C.insurance_cd
	FROM trx_medical A 
	JOIN trx_pasien B ON A.pasien_cd=B.pasien_cd 
	LEFT JOIN trx_pasien_insurance C ON A.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_ruang D ON A.ruang_cd=D.ruang_cd 
	WHERE A.medical_cd=@pstrMedicalCd
	
	IF @strKelasCd IS NULL SET @strKelasCd = ''
	IF @strAsuransiCd IS NULL SET @strAsuransiCd = ''
	--HARDCODE
	--Semua tarif tidak ada tarif asuransi
	--SET @strAsuransiCd = ''
	--End HARDCODE
	
	--HARDCODE
	--Tarif asuransi di kali 15% dari tarif total
	--IF ISNULL(@strAsuransiCd,'') <> '' SET @numAsuransiTarif = 1.15 ELSE SET @numAsuransiTarif = 1
	SET @numAsuransiTarif = 1
	--End HARDCODE
	
	--HARDCODE
	--Unit/Poli ODC menggunakan tarif kelas 2
	--IF @strMedunitCd='ODC' SET @strKelasCd = 'KL02'
	--End HARDCODE
		
	BEGIN TRANSACTION
	
	EXECUTE SP_CLEAR_SETTLEMENT @pstrMedicalCd
	
	/*--TARIF GENERAL--*/
	SET @strTarifTp = 'TARIF_TP_00'
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmountItem = 0
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
		
	IF EXISTS(SELECT * FROM trx_tarif_general A
				WHERE A.auto_add='1'
				AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				AND (A.medical_tp=@strMedicalTp OR A.medical_tp='' OR A.medical_tp IS NULL)
			)
	BEGIN		
		SET @strKelasCdTemp = @strKelasCd
		SET @strAsuransiCdTemp = @strAsuransiCd
	END	
	ELSE
	BEGIN
		SET @strKelasCdTemp = ''
		--SET @strKelasCdTemp = @strKelasCd
		SET @strAsuransiCdTemp = ''
	END	
	DECLARE cursorData CURSOR FOR
	/*SELECT A.seq_no,A.tarif_nm,A.account_cd,A.tarif
	FROM trx_tarif_general A
	WHERE A.auto_add='1'
	AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))
	AND (A.medical_tp=@strMedicalTp OR A.medical_tp='' OR A.medical_tp IS NULL)
	ORDER BY A.seq_no*/
	SELECT A.seq_no,A.tarif_nm,A.account_cd,A.tarif
	FROM trx_tarif_general A
	WHERE A.auto_add='1'
	AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
	AND (A.medical_tp=@strMedicalTp OR A.medical_tp='' OR A.medical_tp IS NULL)
	ORDER BY A.seq_no
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strDataCd,@strDataNm,@strAccountCd,@numAmount
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--HARDCODE
		IF @strMedunitCd = 'POLIUGD'
		--Biaya Pendaftaran Instalasi Rawat Jalan Poli UGD
		BEGIN
			SET @numAmount = 10000
		END
		--End HARDCODE
		
		SET @numJumlah = 1
		SET @numAmountItem = @numAmount
		
		--HARDCODE
		IF @strDataCd = '90010'
		--Tarif general : PENDAFTARAN RAWAT JALAN
		BEGIN
		--Rawat Jalan --> root : Biaya PENDAFTARAN RAWAT JALAN = 1X
			IF @strMedicalRootCd <>'' AND @strMedicalRootCd IS NOT NULL
				SET @numAmountItem = 0
		END
		--End HARDCODE
		
		IF @strMedicalTp = 'MEDICAL_TP_01'
		BEGIN
		--Rawat Jalan
			SET @dtDatetimeTrx = @dtDatetimeIn
		END
		ELSE
		BEGIN
		--Rawat Inap
			SET @dtDatetimeTrx = GETDATE()
		END
		
		IF @numAmountItem <> 0
		BEGIN
			INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
			data_cd,data_nm,amount,note,manual_st,payment_st,
			quantity,item_price,
			modi_id,modi_datetime)
			VALUES (@pstrMedicalCd,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
			@strDataCd,@strDataNm,@numAmount,@strNote,'0','PAYMENT_ST_0',
			@numJumlah,@numAmountItem,
			@pstrUserId,GETDATE())
		END
		
		FETCH NEXT FROM cursorData
		INTO @strDataCd,@strDataNm,@strAccountCd,@numAmount
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	/*--TARIF PARAMEDIS--*/
	SET @strTarifTp = 'TARIF_TP_01'
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmountItem = 0
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
		
	IF @strMedicalTp = 'MEDICAL_TP_01'
	BEGIN
	--Rawat Jalan
		SELECT @strDataCd=A.dr_cd,@strDataNm=B.dr_nm,@strTemp2Cd=B.spesialis_cd
		FROM trx_medical A
		JOIN trx_dokter B ON A.dr_cd=B.dr_cd 
		WHERE A.medical_cd=@pstrMedicalCd
	
		IF @strDataCd IS NOT NULL AND @strDataCd<>''
		BEGIN
			SET @strAccountCd = ''
			SET @numAmount = 0
	
			IF @strTemp2Cd IS NULL OR @strTemp2Cd=''
			BEGIN
			--Dokter Umum
				IF EXISTS(SELECT * FROM trx_tarif_paramedis A
							WHERE A.paramedis_tp='PARAMEDIS_TP_01'
							AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
						)
				BEGIN
					SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = @strAsuransiCd
				END	
				ELSE
				BEGIN
					SET @strKelasCdTemp = ''
					--SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = ''
				END	
				/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_01'
				AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
				SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_01'
				AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			END	
			ELSE
			BEGIN
			--Dokter Spesialis
				IF EXISTS(SELECT * FROM trx_tarif_paramedis A
							WHERE A.paramedis_tp='PARAMEDIS_TP_02'
							AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
						)
				BEGIN	
					SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = @strAsuransiCd
				END	
				ELSE
				BEGIN
					SET @strKelasCdTemp = ''
					--SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = ''
				END	
				/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_02'
				AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
				SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_02'
				AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			END
			
			SET @numJumlah = 1
			SET @numAmountItem = @numAmount
			
			--Remark : tarif dokter dimasukkan di tindakan
			/*INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
			data_cd,data_nm,amount,note,manual_st,payment_st,
			quantity,item_price,
			modi_id,modi_datetime)
			VALUES (@pstrMedicalCd,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
			@strDataCd,@strDataNm,@numAmount,@strNote,'0','PAYMENT_ST_0',
			@numJumlah,@numAmountItem,
			@pstrUserId,GETDATE())*/
		END
	END
	ELSE
	BEGIN
	--Rawat Inap
		DECLARE cursorData CURSOR FOR
		/*SELECT A.dr_cd,A.datetime_trx,
		CASE WHEN DR.spesialis_cd IS NULL THEN 'PARAMEDIS_TP_01' ELSE 'PARAMEDIS_TP_02' END AS dr_tp,
		CASE WHEN DR.spesialis_cd IS NULL THEN 'Tarif Dokter Umum' ELSE 'Tarif Dokter Spesialis' END AS data_nm,
		B.tarif,B.account_cd
		FROM trx_medical_dokter A
		JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_tarif_paramedis B ON CASE WHEN DR.spesialis_cd IS NULL THEN 'PARAMEDIS_TP_01' ELSE 'PARAMEDIS_TP_02' END=B.paramedis_tp
		WHERE A.medical_cd=@pstrMedicalCd
		ORDER BY A.datetime_trx*/
		SELECT A.dr_cd,A.datetime_trx,DR.spesialis_cd,DR.dr_nm,A.kelas_cd
		FROM trx_medical_dokter A
		JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		WHERE A.medical_cd=@pstrMedicalCd
		ORDER BY A.datetime_trx
		
		OPEN cursorData
		FETCH NEXT FROM cursorData
		INTO @strTempCd,@dtDatetimeTrx,@strTemp2Cd,@strDataNm,@strKelasCdAKTIF
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			SET @strAccountCd = ''
			SET @numAmount = 0
			IF ISNULL(@strKelasCdAKTIF,'') <> '' SET @strKelasCd=@strKelasCdAKTIF
			
			IF @strTemp2Cd IS NULL OR @strTemp2Cd=''
			BEGIN
			--Dokter Umum
				IF EXISTS(SELECT * FROM trx_tarif_paramedis A
							WHERE A.paramedis_tp='PARAMEDIS_TP_01'
							AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
						)
				BEGIN		
					SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = @strAsuransiCd
				END	
				ELSE
				BEGIN
					SET @strKelasCdTemp = ''
					--SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = ''
				END	
				/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_01'
				AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
				SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_01'
				AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			END	
			ELSE
			BEGIN
			--Dokter Spesialis
				IF EXISTS(SELECT * FROM trx_tarif_paramedis A
							WHERE A.paramedis_tp='PARAMEDIS_TP_02'
							AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
						)
				BEGIN		
					SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = @strAsuransiCd
				END	
				ELSE
				BEGIN
					SET @strKelasCdTemp = ''
					--SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = ''
				END	
				/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_02'
				AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
				SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_02'
				AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			END
			SET @strDataCd = @strTempCd
			
			SET @numJumlah = 1
			SET @numAmountItem = @numAmount
			
			--Remark : tarif dokter dimasukkan di tindakan
			/*INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
			data_cd,data_nm,amount,note,manual_st,payment_st,
			quantity,item_price,
			modi_id,modi_datetime)
			VALUES (@pstrMedicalCd,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
			@strDataCd,@strDataNm,@numAmount,@strNote,'0','PAYMENT_ST_0',
			@numJumlah,@numAmountItem,
			@pstrUserId,GETDATE())*/
			
			FETCH NEXT FROM cursorData
			INTO @strTempCd,@dtDatetimeTrx,@strTemp2Cd,@strDataNm,@strKelasCdAKTIF
		END
		CLOSE cursorData
		DEALLOCATE cursorData
	END
		
	/*--TARIF UNIT MEDIS--*/
	SET @strTarifTp = 'TARIF_TP_02'
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmountItem = 0
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
	SET @intRefSeqno = NULL
	
	DECLARE cursorData CURSOR FOR
	SELECT A.medicalunit_cd,A.datetime_trx,B.medicalunit_nm,A.medical_unit_seqno,A.kelas_cd
	FROM trx_medical_unit A
	JOIN trx_unitmedis_item B ON A.medicalunit_cd=B.medicalunit_cd
	WHERE A.medical_cd=@pstrMedicalCd
	AND ISNULL(A.payment_st,'')<>'PAYMENT_ST_1'
	ORDER BY A.datetime_trx
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strTempCd,@dtDatetimeTrx,@strDataNm,@intRefSeqno,@strKelasCdAKTIF
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @strAccountCd = ''
		SET @numAmount = 0
		IF ISNULL(@strKelasCdAKTIF,'') <> '' SET @strKelasCd=@strKelasCdAKTIF
		
		IF EXISTS(SELECT * FROM trx_tarif_unitmedis A
					WHERE A.medicalunit_cd=@strTempCd
					AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				)
		BEGIN		
			SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = @strAsuransiCd
			
			--HARDCODE
			--IF ISNULL(@strAsuransiCd,'') <> '' SET @numAsuransiTarif = 1
			SET @numAsuransiTarif = 1
			--End HARDCODE
		END	
		ELSE
		BEGIN
			SET @strKelasCdTemp = ''
			--SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = ''
		END	
		/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_unitmedis A
		WHERE A.medicalunit_cd=@strTempCd
		AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
		SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_unitmedis A
		WHERE A.medicalunit_cd=@strTempCd
		AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
		
		SET @strDataCd = @strTempCd
		
		SET @numJumlah = 1
		SET @numAmountItem = @numAmount
		
		INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
		data_cd,data_nm,amount,note,manual_st,payment_st,
		quantity,item_price,ref_seqno,
		modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
		@strDataCd,@strDataNm,@numAmount * @numAsuransiTarif,@strNote,'0','PAYMENT_ST_0',
		@numJumlah,@numAmountItem * @numAsuransiTarif,@intRefSeqno,
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strTempCd,@dtDatetimeTrx,@strDataNm,@intRefSeqno,@strKelasCdAKTIF
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	/*--TARIF KELAS--*/
	--HARDCODE
	--IF ISNULL(@strAsuransiCd,'') <> '' SET @numAsuransiTarif = 1.15 ELSE SET @numAsuransiTarif = 1
	SET @numAsuransiTarif = 1
	--End HARDCODE
	
	IF @strMedicalTp = 'MEDICAL_TP_02'
	BEGIN
		DECLARE @intTotalDay int
		SET @strTarifTp = 'TARIF_TP_03'
		SET @strAccountCd = ''
		SET @dtDatetimeTrx = GETDATE()
		SET @strDataCd = ''
		SET @strDataNm = ''
		SET @numAmount = 0
		SET @numJumlah = 0
		SET @numTotalPrice = 0
		SET @strNote = ''
		SET @strTempCd = ''
		SET @strTemp2Cd = ''

		SET @intTotalDay = 1
		
		DECLARE @strTarifTp2 varchar(20)
		DECLARE @strKelasNm varchar(100)
		SET @strTarifTp2 = 'TARIF_TP_01'
		
		/*SELECT @strDataCd=B.kelas_cd,@strDataNm=C.kelas_nm,
		@intTotalDay=CASE WHEN DATEDIFF(DAY,A.datetime_in,GETDATE())+1 <=0 THEN 1 ELSE DATEDIFF(DAY,A.datetime_in,GETDATE())+1 END
		FROM trx_medical A
		JOIN trx_ruang B ON A.ruang_cd=B.ruang_cd
		JOIN trx_kelas C ON B.kelas_cd=C.kelas_cd
		WHERE A.medical_cd=@pstrMedicalCd*/
		IF NOT @dtDatetimeOut IS NULL
			SELECT @strDataCd=B.kelas_cd,@strDataNm=C.kelas_nm,
			@intTotalDay=CASE WHEN DATEDIFF(DAY,A.datetime_in,@dtDatetimeOut)+1 <=0 THEN 1 ELSE DATEDIFF(DAY,A.datetime_in,@dtDatetimeOut)+1 END
			FROM trx_medical A
			JOIN trx_ruang B ON A.ruang_cd=B.ruang_cd
			JOIN trx_kelas C ON B.kelas_cd=C.kelas_cd
			WHERE A.medical_cd=@pstrMedicalCd
		ELSE
			SELECT @strDataCd=B.kelas_cd,@strDataNm=C.kelas_nm,
			@intTotalDay=CASE WHEN DATEDIFF(DAY,A.datetime_in,GETDATE())+1 <=0 THEN 1 ELSE DATEDIFF(DAY,A.datetime_in,GETDATE())+1 END
			FROM trx_medical A
			JOIN trx_ruang B ON A.ruang_cd=B.ruang_cd
			JOIN trx_kelas C ON B.kelas_cd=C.kelas_cd
			WHERE A.medical_cd=@pstrMedicalCd
		
		SET @strKelasNm = @strDataNm
		--IF @intTotalDay = 0 SET @intTotalDay = 1
		
		--cek from history Kamar
		IF EXISTS(SELECT TOP 1 A.datetime_end
				FROM trx_medical_ruang A
				WHERE A.medical_cd=@pstrMedicalCd
				ORDER BY A.datetime_start DESC)
		BEGIN
			SET @intTotalDay = (SELECT TOP 1 CASE WHEN DATEDIFF(DAY,A.datetime_end,GETDATE())<=0 THEN 1
												ELSE DATEDIFF(DAY,A.datetime_end,GETDATE()) END
								FROM trx_medical_ruang A
								WHERE A.medical_cd=@pstrMedicalCd
								ORDER BY A.datetime_start DESC)
		END
		IF @intTotalDay = 0 SET @intTotalDay = 1
		--end cek history Kamar
		
		--HARDCODE
		IF EXISTS(SELECT * FROM trx_tarif_kelas A
					WHERE ISNULL(A.kelas_cd,'')=@strDataCd
					AND ISNULL(A.insurance_cd,'')=@strAsuransiCd
				)
		BEGIN		
			--IF ISNULL(@strAsuransiCd,'') <> '' SET @numAsuransiTarif = 1
			SET @numAsuransiTarif = 1
		END
		--End HARDCODE
		
		SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_kelas A
		WHERE ISNULL(A.kelas_cd,'')=@strDataCd
		AND (ISNULL(A.insurance_cd,'')=@strAsuransiCd OR ISNULL(A.insurance_cd,'')='')
		
		SET @numTotalPrice = @numAmount * @intTotalDay
		SET @strDataNm = 'Akomodasi ' + @strDataNm + ' @Total ' + CONVERT(VARCHAR(2),@intTotalDay) + ' hari'
		
		SET @numJumlah = @intTotalDay
		SET @numAmountItem = @numAmount
		
		INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
		data_cd,data_nm,amount,note,manual_st,payment_st,
		quantity,item_price,
		modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
		@strDataCd,@strDataNm,@numTotalPrice * @numAsuransiTarif,@strNote,'0','PAYMENT_ST_0',
		@numJumlah,@numAmountItem * @numAsuransiTarif,
		@pstrUserId,GETDATE())
		
		--HARDCODE
		--Perawatan
		--Tarif perawat
		/*IF EXISTS(SELECT * FROM trx_tarif_paramedis A
					WHERE A.paramedis_tp='PARAMEDIS_TP_04'
					AND (ISNULL(A.kelas_cd,'')=@strDataCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				)
		BEGIN		
			SET @strKelasCdTemp = @strDataCd
			SET @strAsuransiCdTemp = @strAsuransiCd
		END	
		ELSE
		BEGIN
			SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = ''
		END
		SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_paramedis A
		WHERE A.paramedis_tp='PARAMEDIS_TP_04'
		AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
		
		SET @numTotalPrice = @numAmount * @intTotalDay
		SET @strDataNm = 'Perawat ' + @strKelasNm + ' @Total ' + CONVERT(VARCHAR(2),@intTotalDay) + ' hari'
		
		SET @numJumlah = @intTotalDay
		SET @numAmountItem = @numAmount
		
		INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
		data_cd,data_nm,amount,note,manual_st,payment_st,
		quantity,item_price,
		modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@strTarifTp2,@strAccountCd,@dtDatetimeTrx,
		'PARAMEDIS_TP_04',@strDataNm,@numTotalPrice,@strNote,'0','PAYMENT_ST_0',
		@numJumlah,@numAmountItem,
		@pstrUserId,GETDATE())*/
		--End perawatan
		--End HARDCODE
		
		--Looping Kamar
		DECLARE cursorData CURSOR FOR
		SELECT B.kelas_cd,
		CASE WHEN DATEDIFF(DAY,A.datetime_start,A.datetime_end)<=0 THEN 1 ELSE DATEDIFF(DAY,A.datetime_start,A.datetime_end)+1 END,C.kelas_nm
		FROM trx_medical_ruang A
		JOIN trx_ruang B ON A.ruang_cd=B.ruang_cd
		JOIN trx_kelas C ON B.kelas_cd=C.kelas_cd
		WHERE A.medical_cd=@pstrMedicalCd
		ORDER BY A.datetime_start
		
		OPEN cursorData
		FETCH NEXT FROM cursorData
		INTO @strDataCd,@intTotalDay,@strDataNm
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			SET @strKelasNm = @strDataNm
			
			SET @strAccountCd = ''
			SET @numAmount = 0
			
			IF @intTotalDay = 0 SET @intTotalDay = 1
			
			SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
			FROM trx_tarif_kelas A
			WHERE ISNULL(A.kelas_cd,'')=@strDataCd
			AND (ISNULL(A.insurance_cd,'')=@strAsuransiCd OR ISNULL(A.insurance_cd,'')='')
			
			SET @numTotalPrice = @numAmount * @intTotalDay
			SET @strDataNm = 'Akomodasi ' + @strDataNm + ' @Total ' + CONVERT(VARCHAR(2),@intTotalDay) + ' hari'
			
			SET @numJumlah = @intTotalDay
			SET @numAmountItem = @numAmount
			
			--Kamar
			INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
			data_cd,data_nm,amount,note,manual_st,payment_st,
			quantity,item_price,
			modi_id,modi_datetime)
			VALUES (@pstrMedicalCd,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
			@strDataCd,@strDataNm,@numTotalPrice * @numAsuransiTarif,@strNote,'0','PAYMENT_ST_0',
			@numJumlah,@numAmountItem * @numAsuransiTarif,
			@pstrUserId,GETDATE())
			
			--HARDCODE
			--Perawatan
			--Tarif perawat
			/*IF EXISTS(SELECT * FROM trx_tarif_paramedis A
						WHERE A.paramedis_tp='PARAMEDIS_TP_04'
						AND (ISNULL(A.kelas_cd,'')=@strDataCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
					)
			BEGIN		
				SET @strKelasCdTemp = @strDataCd
				SET @strAsuransiCdTemp = @strAsuransiCd
			END	
			ELSE
			BEGIN
				SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = ''
			END
			SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
			FROM trx_tarif_paramedis A
			WHERE A.paramedis_tp='PARAMEDIS_TP_04'
			AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			
			SET @numTotalPrice = @numAmount * @intTotalDay
			SET @strDataNm = 'Perawat ' + @strKelasNm + ' @Total ' + CONVERT(VARCHAR(2),@intTotalDay) + ' hari'
			
			SET @numJumlah = @intTotalDay
			SET @numAmountItem = @numAmount
			
			INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
			data_cd,data_nm,amount,note,manual_st,payment_st,
			quantity,item_price,
			modi_id,modi_datetime)
			VALUES (@pstrMedicalCd,@strTarifTp2,@strAccountCd,@dtDatetimeTrx,
			'PARAMEDIS_TP_04',@strDataNm,@numTotalPrice,@strNote,'0','PAYMENT_ST_0',
			@numJumlah,@numAmountItem,
			@pstrUserId,GETDATE())*/
			--End perawatan
			--End HARDCODE
			
			FETCH NEXT FROM cursorData
			INTO @strDataCd,@intTotalDay,@strDataNm
		END
		CLOSE cursorData
		DEALLOCATE cursorData
		--End Looping Kamar
	END
		
	/*--TARIF TINDAKAN MEDIS--*/
	--HARDCODE
	--IF ISNULL(@strAsuransiCd,'') <> '' SET @numAsuransiTarif = 1.15 ELSE SET @numAsuransiTarif = 1
	SET @numAsuransiTarif = 1
	--End HARDCODE
	
	SET @strTarifTp = 'TARIF_TP_04'
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmountItem = 0
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
	SET @intRefSeqno = NULL
	
	DECLARE cursorData CURSOR FOR
	SELECT A.treatment_cd,A.datetime_trx,
	CASE WHEN A.medical_note = '' THEN B.treatment_nm ELSE A.medical_note END AS treatment_nm,A.quantity,A.medical_tindakan_seqno,A.kelas_cd
	FROM trx_medical_tindakan A
	JOIN trx_tindakan B ON A.treatment_cd=B.treatment_cd
	WHERE A.medical_cd=@pstrMedicalCd
	AND ISNULL(A.payment_st,'')<>'PAYMENT_ST_1'
	ORDER BY A.datetime_trx
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strTempCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefSeqno,@strKelasCdAKTIF
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @strAccountCd = ''
		SET @numAmount = 0
		IF ISNULL(@strKelasCdAKTIF,'') <> '' SET @strKelasCd=@strKelasCdAKTIF
		
		IF EXISTS(SELECT * FROM trx_tarif_tindakan A
					WHERE A.treatment_cd=@strTempCd
					AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				)
		BEGIN		
			SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = @strAsuransiCd
			
			--HARDCODE
			--IF ISNULL(@strAsuransiCd,'') <> '' SET @numAsuransiTarif = 1
			SET @numAsuransiTarif = 1
			--End HARDCODE
		END	
		ELSE
		BEGIN
			SET @strKelasCdTemp = ''
			--SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = ''
		END	
		/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_tindakan A
		WHERE A.treatment_cd=@strTempCd
		AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
		SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_tindakan A
		WHERE A.treatment_cd=@strTempCd
		AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
		
		SET @strDataCd = @strTempCd
		
		IF @numJumlah IS NULL SET @numJumlah = 1
		SET @numTotalPrice = @numAmount * @numJumlah
		
		SET @numAmountItem = @numAmount
		
		INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
		data_cd,data_nm,amount,note,manual_st,payment_st,
		quantity,item_price,ref_seqno,
		modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
		@strDataCd,@strDataNm,@numTotalPrice * @numAsuransiTarif,@strNote,'0','PAYMENT_ST_0',
		@numJumlah,@numAmountItem * @numAsuransiTarif,@intRefSeqno,
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strTempCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefSeqno,@strKelasCdAKTIF
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	/*--TARIF TINDAKAN MEDIS <--> VISIT DOKTER--*/
	--visit dokter masuk ke dalam tindakan
	--HARDCODE
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmountItem = 0
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
	SET @intRefSeqno = NULL
	
	DECLARE cursorData CURSOR FOR
	SELECT CASE WHEN ISNULL(B.spesialis_cd,'')<>'' THEN 'TRJM002' ELSE 'TRJM001' END AS data_cd,
	B.spesialis_cd,A.datetime_trx,
	CASE WHEN ISNULL(B.spesialis_cd,'')<>'' THEN 'Visit Dokter Spesialis' ELSE 'Visit Dokter Umum' END AS treatment_nm,
	1 AS quantity,A.medical_dr_seqno,A.kelas_cd
	FROM trx_medical_dokter A
	JOIN trx_dokter B ON A.dr_cd=B.dr_cd
	WHERE A.medical_cd=@pstrMedicalCd
	AND A.trx_dr_tp='TRXDR_TP_1'
	ORDER BY A.datetime_trx
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strTempCd,@strTemp2Cd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefSeqno,@strKelasCdAKTIF
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @strAccountCd = ''
		SET @numAmount = 0
		IF ISNULL(@strKelasCdAKTIF,'') <> '' SET @strKelasCd=@strKelasCdAKTIF
		
		/*IF ISNULL(@strTemp2Cd,'')=''
		BEGIN
		--Dokter Umum
		END
		ELSE
		BEGIN
		--Dokter Spesialis
		END*/
		
		IF EXISTS(SELECT * FROM trx_tarif_tindakan A
					WHERE A.treatment_cd=@strTempCd
					AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				)
		BEGIN		
			SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = @strAsuransiCd
		END	
		ELSE
		BEGIN
			SET @strKelasCdTemp = ''
			--SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = ''
		END	
		SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_tindakan A
		WHERE A.treatment_cd=@strTempCd
		AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
		
		SET @strDataCd = @strTempCd
		
		IF @numJumlah IS NULL SET @numJumlah = 1
		SET @numTotalPrice = @numAmount * @numJumlah
		
		SET @numAmountItem = @numAmount
		
		INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
		data_cd,data_nm,amount,note,manual_st,payment_st,
		quantity,item_price,ref_seqno,
		modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
		@strDataCd,@strDataNm,@numTotalPrice * @numAsuransiTarif,@strNote,'0','PAYMENT_ST_0',
		@numJumlah,@numAmountItem * @numAsuransiTarif,@intRefSeqno,
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strTempCd,@strTemp2Cd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefSeqno,@strKelasCdAKTIF
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	--End HARDCODE
		
	/*--TARIF INVENTORI--*/
	--HARDCODE
	--IF ISNULL(@strAsuransiCd,'') <> '' SET @numAsuransiTarif = 1.15 ELSE SET @numAsuransiTarif = 1
	SET @numAsuransiTarif = 1
	--End HARDCODE
	
	DECLARE @numItemPriceMaster Numeric(15)
	--Obat
	--HARDCODE obat/BHP --> harga obat/BHP per kelas sama
	--DECLARE @strKelasCd2 varchar(20)
	--SET @strKelasCd2 = ''
	--End HARDCODE obat
	
	DECLARE @intResepSeqno bigint
	DECLARE @strResepTp varchar(20)
	DECLARE @intTotalObat int
	
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
	SET @intTotalObat = 0
	
	SET @intRefResepSeqno = NULL
	SET @intRefAlkesSeqno = NULL
	
	DECLARE cursorData CURSOR FOR
	SELECT B.item_cd,A.resep_date,CASE WHEN B.resep_tp='RESEP_TP_2' THEN B.data_nm ELSE C.item_nm END AS item_nm,
	B.quantity,A.medical_resep_seqno,A.resep_no,B.resep_seqno,B.resep_tp,A.kelas_cd
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	WHERE A.medical_cd=@pstrMedicalCd
	AND ISNULL(B.payment_st,'')<>'PAYMENT_ST_1'
	ORDER BY A.medical_resep_seqno,B.resep_seqno
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strDataCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefResepSeqno,@strNote,@intResepSeqno,@strResepTp,@strKelasCdAKTIF
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--SET @intTotalObat = @intTotalObat + 1
		
		SET @strAccountCd = ''
		SET @numAmount = 0
		SET @numTotalPrice = 0
		IF ISNULL(@strKelasCdAKTIF,'') <> '' SET @strKelasCd=@strKelasCdAKTIF
		
		IF @strResepTp='RESEP_TP_1'
		--Resep per item
		BEGIN
			SET @intTotalObat = @intTotalObat + 1
			
			IF EXISTS(SELECT * FROM trx_tarif_inventori A
					WHERE A.item_cd=@strDataCd
					AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd) --AND (A.kelas_cd=@strKelasCd2 AND A.insurance_cd=@strAsuransiCd)
					--AND (A.kelas_cd=@strKelasCd2 AND A.insurance_cd=@strAsuransiCd) --AND (A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd)
				)
			BEGIN		
				SET @strKelasCdTemp = @strKelasCd --SET @strKelasCdTemp = @strKelasCd2
				--SET @strKelasCdTemp = @strKelasCd2 --SET @strKelasCdTemp = @strKelasCd
				
				SET @strAsuransiCdTemp = @strAsuransiCd
				
				--HARDCODE
				--IF ISNULL(@strAsuransiCd,'') <> '' SET @numAsuransiTarif = 1
				SET @numAsuransiTarif = 1
				--End HARDCODE
			END	
			ELSE
			BEGIN
				SET @strKelasCdTemp = ''
				--SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = ''
			END	
			/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
			FROM trx_tarif_inventori A
			WHERE A.item_cd=@strDataCd
			AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
			SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
			FROM trx_tarif_inventori A
			WHERE A.item_cd=@strDataCd
			AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			
			SET @numTotalPrice = @numAmount * @numJumlah
		END
		ELSE
		--Resep racik
		BEGIN
			SET @intTotalObat = @intTotalObat + 1
			/*IF @numJumlah<=30
				SET @intTotalObat = @intTotalObat + 1
			ELSE IF @numJumlah>30 AND @numJumlah<=60
				SET @intTotalObat = @intTotalObat + 2
			ELSE
				SET @intTotalObat = @intTotalObat + 3*/
				
			SET @strAccountCd = 'AC301' /*--set account obat racik to account obat--*/
			SET @numTotalPrice = (SELECT simrke.FN_GET_ITEM_PRICE_RACIK(@pstrMedicalCd,@intResepSeqno))
		END
		
		SET @numItemPriceMaster = (SELECT ISNULL(item_price_buy,0) FROM inv_item_master WHERE item_cd=@strDataCd)
		
		INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
		ref_medical_resep_seqno,ref_medical_alkes_seqno,ref_resep_seqno,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@strAccountCd,@dtDatetimeTrx,
		@intRefResepSeqno,@intRefAlkesSeqno,@intResepSeqno,
		@strDataCd,@numJumlah,@numAmount * @numAsuransiTarif,@numItemPriceMaster,@numTotalPrice * @numAsuransiTarif,@strNote,'0','PAYMENT_ST_0',
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strDataCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefResepSeqno,@strNote,@intResepSeqno,@strResepTp,@strKelasCdAKTIF
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	--HARDCODE
	--Obat : Jasa Racik Per Item Total
	IF @intTotalObat <> 0
	BEGIN
		DECLARE @numBiayaRacik numeric
		DECLARE @intTotal int
		
		SET @numBiayaRacik = 0
		SET @intTotal = 0
		
		SELECT @intTotal=ISNULL(COUNT(B.resep_seqno),0)
		FROM trx_medical_resep A
		JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
		WHERE A.medical_cd=@pstrMedicalCd AND B.resep_tp='RESEP_TP_2' --Resep racik
		AND ISNULL(payment_st,'')<>'PAYMENT_ST_1'
		
		SELECT @numBiayaRacik=ISNULL(tarif,0)
		FROM trx_tarif_inventori
		WHERE item_cd='NONINV900'
		
		SET @strAccountCd = 'AC301' --set jasa to account obat
		
		IF @intTotal <> 0
		BEGIN
			INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
			item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
			ref_medical_resep_seqno,ref_resep_seqno,
			modi_id,modi_datetime)
			VALUES (@pstrMedicalCd,@strAccountCd,@dtDatetimeTrx,
			'NONINV900',@intTotal,@numBiayaRacik,@numBiayaRacik,@numBiayaRacik * @intTotal,'','0','PAYMENT_ST_0',
			0,0,
			@pstrUserId,GETDATE())
		END
	END
	--Obat : Jasa
	/*
	IF @intTotalObat <> 0
	BEGIN
		DECLARE @intTariftpNo bigint
		DECLARE @numJS numeric
		DECLARE @numJM numeric
		DECLARE @numJP numeric
		DECLARE @numJSTemp numeric
		DECLARE @numJMTemp numeric
		DECLARE @numJPTemp numeric
		DECLARE @intTotal int
		DECLARE @intTotalRacik int
		
		SET @numJS = 0
		SET @numJM = 0
		SET @numJP = 0
		SET @intTotal = 0
		
		DECLARE cursorData CURSOR FOR
		SELECT B.item_cd,B.quantity,B.resep_seqno,B.resep_tp
		FROM trx_medical_resep A
		JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
		LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
		WHERE A.medical_cd=@pstrMedicalCd
		ORDER BY B.resep_seqno
			
		OPEN cursorData
		FETCH NEXT FROM cursorData
		INTO @strDataCd,@numJumlah,@intResepSeqno,@strResepTp
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			IF @strResepTp='RESEP_TP_1'
			--Resep per item
			BEGIN
				SET @intTotal = @intTotal + 1
				
				--Biaya JASA FARMASI
				SET @intTariftpNo = 5000000
				--Jasa Sarana Farmasi
				SELECT @numJSTemp=ISNULL(tarif_item,0)
				FROM trx_tariftp_item
				WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
				--Jasa Medis Farmasi
				SELECT @numJMTemp=ISNULL(tarif_item,0)
				FROM trx_tariftp_item
				WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
				--Jasa Pelaksana Farmasi
				SELECT @numJPTemp=ISNULL(tarif_item,0)
				FROM trx_tariftp_item
				WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
				
				SET @numJS = @numJS + @numJSTemp
				SET @numJM = @numJM + @numJMTemp
				SET @numJP = @numJP + @numJPTemp
				--End Biaya JASA FARMASI
			END
			ELSE
			--Resep racik
			BEGIN
				--Biaya JASA FARMASI
				SET @intTotalRacik = 0
				IF @numJumlah<=30
					SET @intTotalRacik = 1
				ELSE IF @numJumlah>30 AND @numJumlah<=60
					SET @intTotalRacik = 2
				ELSE
					SET @intTotalRacik = 3
					
				SET @intTotal = @intTotal + @intTotalRacik
					
				SET @intTariftpNo = 5000020
				--Jasa Sarana Farmasi
				SELECT @numJSTemp=ISNULL(tarif_item,0) * @intTotalRacik
				FROM trx_tariftp_item
				WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
				--Jasa Medis Farmasi
				SELECT @numJMTemp=ISNULL(tarif_item,0) * @intTotalRacik
				FROM trx_tariftp_item
				WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
				--Jasa Pelaksana Farmasi
				SELECT @numJPTemp=ISNULL(tarif_item,0) * @intTotalRacik
				FROM trx_tariftp_item
				WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
				
				SET @numJS = @numJS + @numJSTemp
				SET @numJM = @numJM + @numJMTemp
				SET @numJP = @numJP + @numJPTemp
				--End Biaya JASA FARMASI
			END
			
			FETCH NEXT FROM cursorData
			INTO @strDataCd,@numJumlah,@intResepSeqno,@strResepTp
		END
		CLOSE cursorData
		DEALLOCATE cursorData
		
		SET @strAccountCd = 'AC301' --set jasa to account obat
		
		INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		ref_medical_resep_seqno,ref_resep_seqno,
		modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@strAccountCd,@dtDatetimeTrx,
		'NONINV101',1,@numJS,@numJS,@numJS,'','0','PAYMENT_ST_0',
		@intRefResepSeqno,@intResepSeqno,
		@pstrUserId,GETDATE())
		
		INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		ref_medical_resep_seqno,ref_resep_seqno,
		modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@strAccountCd,@dtDatetimeTrx,
		'NONINV102',1,@numJM,@numJM,@numJM,'','0','PAYMENT_ST_0',
		@intRefResepSeqno,@intResepSeqno,
		@pstrUserId,GETDATE())
		
		INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		ref_medical_resep_seqno,ref_resep_seqno,
		modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@strAccountCd,@dtDatetimeTrx,
		'NONINV103',1,@numJP,@numJP,@numJP,'','0','PAYMENT_ST_0',
		@intRefResepSeqno,@intResepSeqno,
		@pstrUserId,GETDATE())
	END
	*/
	--JASA FARMASI semua item sama
	/*IF @intTotalObat <> 0
	BEGIN
		DECLARE @numJS numeric
		DECLARE @numJM numeric
		DECLARE @numJP numeric
		SELECT @numJS=ISNULL(tarif_item,0) --* @intTotal
		FROM trx_tariftp_item
		WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
		--Jasa Medis Farmasi
		SELECT @numJM=ISNULL(tarif_item,0) --* @intTotal
		FROM trx_tariftp_item
		WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
		--Jasa Pelaksana Farmasi
		SELECT @numJP=ISNULL(tarif_item,0) --* @intTotal
		FROM trx_tariftp_item
		WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
		
		SET @strAccountCd = 'AC301' --set jasa to account obat
		
		INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		ref_medical_resep_seqno,ref_resep_seqno,
		modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@strAccountCd,@dtDatetimeTrx,
		'NONINV101',@intTotalObat,@numJS,@numJS,@numJS * @intTotalObat,'','0','PAYMENT_ST_0',
		@intRefResepSeqno,@intResepSeqno,
		@pstrUserId,GETDATE())
		
		INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		ref_medical_resep_seqno,ref_resep_seqno,
		modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@strAccountCd,@dtDatetimeTrx,
		'NONINV102',@intTotalObat,@numJM,@numJM,@numJM * @intTotalObat,'','0','PAYMENT_ST_0',
		@intRefResepSeqno,@intResepSeqno,
		@pstrUserId,GETDATE())
		
		INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		ref_medical_resep_seqno,ref_resep_seqno,
		modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@strAccountCd,@dtDatetimeTrx,
		'NONINV103',@intTotalObat,@numJP,@numJP,@numJP * @intTotalObat,'','0','PAYMENT_ST_0',
		@intRefResepSeqno,@intResepSeqno,
		@pstrUserId,GETDATE())
	END*/
	--End Obat : Jasa
	
	--BHP
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
	
	SET @intRefResepSeqno = NULL
	SET @intRefAlkesSeqno = NULL
	
	DECLARE cursorData CURSOR FOR
	SELECT A.item_cd,A.datetime_trx,B.item_nm,A.quantity,A.medical_alkes_seqno,A.kelas_cd
	FROM trx_medical_alkes A
	JOIN inv_item_master B ON A.item_cd=B.item_cd
	WHERE A.medical_cd=@pstrMedicalCd
	AND ISNULL(A.payment_st,'')<>'PAYMENT_ST_1'
	ORDER BY A.medical_alkes_seqno
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strDataCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefAlkesSeqno,@strKelasCdAKTIF
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @strAccountCd = ''
		SET @numAmount = 0
		SET @numTotalPrice = 0
		IF ISNULL(@strKelasCdAKTIF,'') <> '' SET @strKelasCd=@strKelasCdAKTIF
		
		IF EXISTS(SELECT * FROM trx_tarif_inventori A
				WHERE A.item_cd=@strDataCd
				AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				--AND (A.kelas_cd=@strKelasCd2 AND A.insurance_cd=@strAsuransiCd)
			)
		BEGIN		
			SET @strKelasCdTemp = @strKelasCd
			--SET @strKelasCdTemp = @strKelasCd2
			
			SET @strAsuransiCdTemp = @strAsuransiCd
		END	
		ELSE
		BEGIN
			SET @strKelasCdTemp = ''
			--SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = ''
		END	
		
		SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_inventori A
		WHERE A.item_cd=@strDataCd
		AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
		
		--HARDCODE
		--solusi sementara hardcode account BHP
		SET @strAccountCd = 'AC302'
		--End HARDCODE
		
		SET @numTotalPrice = @numAmount * @numJumlah
		
		SET @numItemPriceMaster = (SELECT ISNULL(item_price_buy,0) FROM inv_item_master WHERE item_cd=@strDataCd)
		
		INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
		ref_medical_resep_seqno,ref_medical_alkes_seqno,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@strAccountCd,@dtDatetimeTrx,
		@intRefResepSeqno,@intRefAlkesSeqno,
		@strDataCd,@numJumlah,@numAmount,@numItemPriceMaster,@numTotalPrice,@strNote,'0','PAYMENT_ST_0',
		@pstrUserId,GETDATE())

		FETCH NEXT FROM cursorData
		INTO @strDataCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefAlkesSeqno,@strKelasCdAKTIF
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	DELETE FROM trx_settlement_share WHERE medical_cd=@pstrMedicalCd
	
	IF @@ERROR<> 0
	BEGIN
		ROLLBACK TRANSACTION
		--RAISERROR('Error proses',16,1)
		RETURN
	END
	ELSE
		COMMIT TRANSACTION
	
	IF @strMedicalRootCd <>'' AND @strMedicalRootCd IS NOT NULL
	BEGIN
		EXECUTE SP_PROSES_SETTLEMENT @strMedicalRootCd,@pstrUserId
		
		--root of root
		DECLARE @strMedicalRootRootCd varchar(10)
		
		/*SET @strMedicalRootRootCd = (SELECT A.medical_cd
								FROM trx_medical A 
								WHERE A.medical_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical
													WHERE medical_cd=(SELECT TOP 1 medical_root_cd FROM trx_medical WHERE medical_root_cd=@strMedicalRootCd)
													)
								)*/
		SET @strMedicalRootRootCd = (SELECT A.medical_root_cd
								FROM trx_medical A 
								WHERE A.medical_cd=@strMedicalRootCd)
		
		IF @strMedicalRootRootCd <>'' AND @strMedicalRootRootCd IS NOT NULL
			EXECUTE SP_PROSES_SETTLEMENT @strMedicalRootRootCd,@pstrUserId
	END
	
	--proses child
	/*DECLARE @strMedicalChildCd varchar(10)
	DECLARE @strMedicalChildCd1 varchar(10)
	DECLARE @strMedicalChildCd2 varchar(10)
	DECLARE @strMedicalChildCd3 varchar(10)
	DECLARE @intCounter int
	SET @intCounter = 0
	DECLARE cursorData CURSOR FOR
	SELECT A.medical_cd
	FROM trx_medical A 
	WHERE A.medical_root_cd=@pstrMedicalCd
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strMedicalChildCd
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @intCounter = @intCounter + 1
		--HARDCODE
		IF @intCounter = 1 SET @strMedicalChildCd1 = @strMedicalChildCd
		IF @intCounter = 2 SET @strMedicalChildCd2 = @strMedicalChildCd
		IF @intCounter = 3 SET @strMedicalChildCd3 = @strMedicalChildCd
		--End HARDCODE
		
		FETCH NEXT FROM cursorData
		INTO @strMedicalChildCd
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	IF @strMedicalChildCd1 <>'' EXECUTE SP_PROSES_SETTLEMENT @strMedicalChildCd1,@pstrUserId
	IF @strMedicalChildCd2 <>'' EXECUTE SP_PROSES_SETTLEMENT @strMedicalChildCd2,@pstrUserId
	IF @strMedicalChildCd3 <>'' EXECUTE SP_PROSES_SETTLEMENT @strMedicalChildCd3,@pstrUserId*/
	/*SELECT TOP 1 @strMedicalChildCd=medical_cd
	FROM trx_medical
	WHERE medical_root_cd=@pstrMedicalCd
	
	IF @strMedicalChildCd <>'' EXECUTE SP_PROSES_SETTLEMENT @strMedicalChildCd,@pstrUserId*/	
	--end proses child
	
	--EXECUTE SP_SETTLEMENT_EXCEPTION @pstrMedicalCd,@pstrUserId
	--EXECUTE SP_SETTLEMENT_SHARE @pstrMedicalCd,@pstrUserId
END;

DROP FUNCTION IF EXISTS SP_PROSES_SETTLEMENT_2(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PROSES_SETTLEMENT_2] 
@pstrMedicalCd Varchar(10),
@pstrMedicalRootCd Varchar(10),
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @strMedicalTp varchar(20)
	DECLARE @strMedicalCdAKTIF varchar(10)
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strPasienCd varchar(20)
	DECLARE @strDrCd varchar(20)
	DECLARE @strMedunitCd varchar(20)
	DECLARE @strKelasCd varchar(20)
	DECLARE @strAsuransiCd varchar(20)
	
	DECLARE @strKelasCdTemp varchar(20)
	DECLARE @strAsuransiCdTemp varchar(20)
	DECLARE @strKelasCdAKTIF varchar(20)
	DECLARE @numAsuransiTarif numeric(5,2)
	
	DECLARE @strTarifTp varchar(20)
	DECLARE @strAccountCd varchar(20)
	DECLARE @dtDatetimeIn datetime
	DECLARE @dtDatetimeTrx datetime
	DECLARE @dtDatetimeOut datetime
	DECLARE @strDataCd varchar(20)
	DECLARE @strDataNm varchar(100)
	DECLARE @numAmountItem numeric
	DECLARE @numAmount numeric
	DECLARE @numJumlah numeric(5,2)
	DECLARE @numTotalPrice numeric
	DECLARE @strNote varchar(1000)
	DECLARE @strTempCd varchar(20)
	DECLARE @strTemp2Cd varchar(20)
	
	DECLARE @intRefSeqno bigint
	DECLARE @intRefResepSeqno bigint
	DECLARE @intRefAlkesSeqno bigint
	
	--SET @strMedicalCdAKTIF = @pstrMedicalRootCd
	SET @strMedicalCdAKTIF = @pstrMedicalCd
	
	SELECT @strMedicalTp=A.medical_tp,@strMedicalRootCd=A.medical_root_cd,@strPasienCd=A.pasien_cd,
	@strDrCd=A.dr_cd,@strMedunitCd=A.medunit_cd,@dtDatetimeIn=A.datetime_in,@dtDatetimeOut=A.datetime_out,
	@strKelasCd=D.kelas_cd,@strAsuransiCd=C.insurance_cd
	FROM trx_medical A 
	JOIN trx_pasien B ON A.pasien_cd=B.pasien_cd 
	LEFT JOIN trx_pasien_insurance C ON A.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_ruang D ON A.ruang_cd=D.ruang_cd 
	WHERE A.medical_cd=@pstrMedicalCd
	
	IF @strKelasCd IS NULL SET @strKelasCd = ''
	IF @strAsuransiCd IS NULL SET @strAsuransiCd = ''
	--HARDCODE
	--Semua tarif tidak ada tarif asuransi
	--SET @strAsuransiCd = ''
	--End HARDCODE
	--HARDCODE
	--Tarif asuransi di kali 15% dari tarif total
	--IF ISNULL(@strAsuransiCd,'') <> '' SET @numAsuransiTarif = 1.15 ELSE SET @numAsuransiTarif = 1
	SET @numAsuransiTarif = 1
	--End HARDCODE
	
	--HARDCODE
	--Unit/Poli ODC menggunakan tarif kelas 2
	IF @strMedunitCd='ODC' SET @strKelasCd = 'KL02'
	--End HARDCODE
	
	BEGIN TRANSACTION
	
	EXECUTE SP_CLEAR_SETTLEMENT_2 @pstrMedicalCd
	
	/*--TARIF GENERAL--*/
	SET @strTarifTp = 'TARIF_TP_00'
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmountItem = 0
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
		
	IF EXISTS(SELECT * FROM trx_tarif_general A
				WHERE A.auto_add='1'
				AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				AND (A.medical_tp=@strMedicalTp OR A.medical_tp='' OR A.medical_tp IS NULL)
			)
	BEGIN		
		SET @strKelasCdTemp = @strKelasCd
		SET @strAsuransiCdTemp = @strAsuransiCd
	END	
	ELSE
	BEGIN
		SET @strKelasCdTemp = ''
		--SET @strKelasCdTemp = @strKelasCd
		SET @strAsuransiCdTemp = ''
	END	
	DECLARE cursorData CURSOR FOR
	/*SELECT A.seq_no,A.tarif_nm,A.account_cd,A.tarif
	FROM trx_tarif_general A
	WHERE A.auto_add='1'
	AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))
	AND (A.medical_tp=@strMedicalTp OR A.medical_tp='' OR A.medical_tp IS NULL)
	ORDER BY A.seq_no*/
	SELECT A.seq_no,A.tarif_nm,A.account_cd,A.tarif
	FROM trx_tarif_general A
	WHERE A.auto_add='1'
	AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
	AND (A.medical_tp=@strMedicalTp OR A.medical_tp='' OR A.medical_tp IS NULL)
	ORDER BY A.seq_no
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strDataCd,@strDataNm,@strAccountCd,@numAmount
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--HARDCODE
		IF @strMedunitCd = 'POLIUGD'
		--Biaya Pendaftaran Instalasi Rawat Jalan Poli UGD
		BEGIN
			SET @numAmount = 10000
		END
		--End HARDCODE
		
		SET @numJumlah = 1
		SET @numAmountItem = @numAmount
		
		--HARDCODE
		IF @strDataCd = '90010'
		--Tarif general : PENDAFTARAN RAWAT JALAN
		BEGIN
		--Rawat Jalan --> root : Biaya PENDAFTARAN RAWAT JALAN = 1X
			IF @strMedicalRootCd <>'' AND @strMedicalRootCd IS NOT NULL
				SET @numAmountItem = 0
		END
		--End HARDCODE
		
		IF @strMedicalTp = 'MEDICAL_TP_01'
		BEGIN
		--Rawat Jalan
			SET @dtDatetimeTrx = @dtDatetimeIn
		END
		ELSE
		BEGIN
		--Rawat Inap
			SET @dtDatetimeTrx = GETDATE()
		END
		
		IF @numAmountItem <> 0
		BEGIN
			INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
			data_cd,data_nm,amount,note,manual_st,payment_st,
			quantity,item_price,
			modi_id,modi_datetime)
			VALUES (@strMedicalCdAKTIF,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
			@strDataCd,@strDataNm,@numAmount,@strNote,'0','PAYMENT_ST_0',
			@numJumlah,@numAmountItem,
			@pstrUserId,GETDATE())
		END
		
		FETCH NEXT FROM cursorData
		INTO @strDataCd,@strDataNm,@strAccountCd,@numAmount
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	/*--TARIF PARAMEDIS--*/
	SET @strTarifTp = 'TARIF_TP_01'
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmountItem = 0
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
		
	IF @strMedicalTp = 'MEDICAL_TP_01'
	BEGIN
	--Rawat Jalan
		SELECT @strDataCd=A.dr_cd,@strDataNm=B.dr_nm,@strTemp2Cd=B.spesialis_cd
		FROM trx_medical A
		JOIN trx_dokter B ON A.dr_cd=B.dr_cd 
		WHERE A.medical_cd=@pstrMedicalCd
	
		IF @strDataCd IS NOT NULL AND @strDataCd<>''
		BEGIN
			SET @strAccountCd = ''
			SET @numAmount = 0
	
			IF @strTemp2Cd IS NULL OR @strTemp2Cd=''
			BEGIN
			--Dokter Umum
				IF EXISTS(SELECT * FROM trx_tarif_paramedis A
							WHERE A.paramedis_tp='PARAMEDIS_TP_01'
							AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
						)
				BEGIN
					SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = @strAsuransiCd
				END	
				ELSE
				BEGIN
					SET @strKelasCdTemp = ''
					--SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = ''
				END	
				/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_01'
				AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
				SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_01'
				AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			END	
			ELSE
			BEGIN
			--Dokter Spesialis
				IF EXISTS(SELECT * FROM trx_tarif_paramedis A
							WHERE A.paramedis_tp='PARAMEDIS_TP_02'
							AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
						)
				BEGIN	
					SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = @strAsuransiCd
				END	
				ELSE
				BEGIN
					SET @strKelasCdTemp = ''
					--SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = ''
				END	
				/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_02'
				AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
				SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_02'
				AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			END
			
			SET @numJumlah = 1
			SET @numAmountItem = @numAmount
			
			--Remark : tarif dokter dimasukkan di tindakan
			/*INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
			data_cd,data_nm,amount,note,manual_st,payment_st,
			quantity,item_price,
			modi_id,modi_datetime)
			VALUES (@strMedicalCdAKTIF,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
			@strDataCd,@strDataNm,@numAmount,@strNote,'0','PAYMENT_ST_0',
			@numJumlah,@numAmountItem,
			@pstrUserId,GETDATE())*/
		END
	END
	ELSE
	BEGIN
	--Rawat Inap
		DECLARE cursorData CURSOR FOR
		/*SELECT A.dr_cd,A.datetime_trx,
		CASE WHEN DR.spesialis_cd IS NULL THEN 'PARAMEDIS_TP_01' ELSE 'PARAMEDIS_TP_02' END AS dr_tp,
		CASE WHEN DR.spesialis_cd IS NULL THEN 'Tarif Dokter Umum' ELSE 'Tarif Dokter Spesialis' END AS data_nm,
		B.tarif,B.account_cd
		FROM trx_medical_dokter A
		JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_tarif_paramedis B ON CASE WHEN DR.spesialis_cd IS NULL THEN 'PARAMEDIS_TP_01' ELSE 'PARAMEDIS_TP_02' END=B.paramedis_tp
		WHERE A.medical_cd=@pstrMedicalCd
		ORDER BY A.datetime_trx*/
		SELECT A.dr_cd,A.datetime_trx,DR.spesialis_cd,DR.dr_nm,A.kelas_cd
		FROM trx_medical_dokter A
		JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		WHERE A.medical_cd=@pstrMedicalCd
		ORDER BY A.datetime_trx
		
		OPEN cursorData
		FETCH NEXT FROM cursorData
		INTO @strTempCd,@dtDatetimeTrx,@strTemp2Cd,@strDataNm,@strKelasCdAKTIF
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			SET @strAccountCd = ''
			SET @numAmount = 0
			IF ISNULL(@strKelasCdAKTIF,'') <> '' SET @strKelasCd=@strKelasCdAKTIF
			
			IF @strTemp2Cd IS NULL OR @strTemp2Cd=''
			BEGIN
			--Dokter Umum
				IF EXISTS(SELECT * FROM trx_tarif_paramedis A
							WHERE A.paramedis_tp='PARAMEDIS_TP_01'
							AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
						)
				BEGIN		
					SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = @strAsuransiCd
				END	
				ELSE
				BEGIN
					SET @strKelasCdTemp = ''
					--SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = ''
				END	
				/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_01'
				AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
				SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_01'
				AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			END	
			ELSE
			BEGIN
			--Dokter Spesialis
				IF EXISTS(SELECT * FROM trx_tarif_paramedis A
							WHERE A.paramedis_tp='PARAMEDIS_TP_02'
							AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
						)
				BEGIN		
					SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = @strAsuransiCd
				END	
				ELSE
				BEGIN
					SET @strKelasCdTemp = ''
					--SET @strKelasCdTemp = @strKelasCd
					SET @strAsuransiCdTemp = ''
				END	
				/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_02'
				AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
				SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
				FROM trx_tarif_paramedis A
				WHERE A.paramedis_tp='PARAMEDIS_TP_02'
				AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			END
			SET @strDataCd = @strTempCd
			
			SET @numJumlah = 1
			SET @numAmountItem = @numAmount
			
			--Remark : tarif dokter dimasukkan di tindakan
			/*INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
			data_cd,data_nm,amount,note,manual_st,payment_st,
			quantity,item_price,
			modi_id,modi_datetime)
			VALUES (@strMedicalCdAKTIF,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
			@strDataCd,@strDataNm,@numAmount,@strNote,'0','PAYMENT_ST_0',
			@numJumlah,@numAmountItem,
			@pstrUserId,GETDATE())*/
			
			FETCH NEXT FROM cursorData
			INTO @strTempCd,@dtDatetimeTrx,@strTemp2Cd,@strDataNm,@strKelasCdAKTIF
		END
		CLOSE cursorData
		DEALLOCATE cursorData
	END
		
	/*--TARIF UNIT MEDIS--*/
	SET @strTarifTp = 'TARIF_TP_02'
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmountItem = 0
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
	SET @intRefSeqno = NULL
	
	DECLARE cursorData CURSOR FOR
	SELECT A.medicalunit_cd,A.datetime_trx,B.medicalunit_nm,A.medical_unit_seqno,A.kelas_cd
	FROM trx_medical_unit A
	JOIN trx_unitmedis_item B ON A.medicalunit_cd=B.medicalunit_cd
	WHERE A.medical_cd=@pstrMedicalCd
	AND ISNULL(A.payment_st,'')<>'PAYMENT_ST_1'
	ORDER BY A.datetime_trx
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strTempCd,@dtDatetimeTrx,@strDataNm,@intRefSeqno,@strKelasCdAKTIF
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @strAccountCd = ''
		SET @numAmount = 0
		IF ISNULL(@strKelasCdAKTIF,'') <> '' SET @strKelasCd=@strKelasCdAKTIF
		
		IF EXISTS(SELECT * FROM trx_tarif_unitmedis A
					WHERE A.medicalunit_cd=@strTempCd
					AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				)
		BEGIN		
			SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = @strAsuransiCd
		END	
		ELSE
		BEGIN
			SET @strKelasCdTemp = ''
			--SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = ''
		END	
		/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_unitmedis A
		WHERE A.medicalunit_cd=@strTempCd
		AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
		SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_unitmedis A
		WHERE A.medicalunit_cd=@strTempCd
		AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
		
		SET @strDataCd = @strTempCd
		
		SET @numJumlah = 1
		SET @numAmountItem = @numAmount
		
		INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
		data_cd,data_nm,amount,note,manual_st,payment_st,
		quantity,item_price,ref_seqno,
		modi_id,modi_datetime)
		VALUES (@strMedicalCdAKTIF,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
		@strDataCd,@strDataNm,@numAmount,@strNote,'0','PAYMENT_ST_0',
		@numJumlah,@numAmountItem,@intRefSeqno,
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strTempCd,@dtDatetimeTrx,@strDataNm,@intRefSeqno,@strKelasCdAKTIF
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	/*--TARIF KELAS--*/
	IF @strMedicalTp = 'MEDICAL_TP_02'
	BEGIN
		DECLARE @intTotalDay int
		SET @strTarifTp = 'TARIF_TP_03'
		SET @strAccountCd = ''
		SET @dtDatetimeTrx = GETDATE()
		SET @strDataCd = ''
		SET @strDataNm = ''
		SET @numAmount = 0
		SET @numJumlah = 0
		SET @numTotalPrice = 0
		SET @strNote = ''
		SET @strTempCd = ''
		SET @strTemp2Cd = ''

		SET @intTotalDay = 1
		
		DECLARE @strTarifTp2 varchar(20)
		DECLARE @strKelasNm varchar(100)
		SET @strTarifTp2 = 'TARIF_TP_01'
		
		/*SELECT @strDataCd=B.kelas_cd,@strDataNm=C.kelas_nm,
		@intTotalDay=CASE WHEN DATEDIFF(DAY,A.datetime_in,GETDATE())+1 <=0 THEN 1 ELSE DATEDIFF(DAY,A.datetime_in,GETDATE())+1 END
		FROM trx_medical A
		JOIN trx_ruang B ON A.ruang_cd=B.ruang_cd
		JOIN trx_kelas C ON B.kelas_cd=C.kelas_cd
		WHERE A.medical_cd=@pstrMedicalCd*/
		IF NOT @dtDatetimeOut IS NULL
			SELECT @strDataCd=B.kelas_cd,@strDataNm=C.kelas_nm,
			@intTotalDay=CASE WHEN DATEDIFF(DAY,A.datetime_in,@dtDatetimeOut)+1 <=0 THEN 1 ELSE DATEDIFF(DAY,A.datetime_in,@dtDatetimeOut)+1 END
			FROM trx_medical A
			JOIN trx_ruang B ON A.ruang_cd=B.ruang_cd
			JOIN trx_kelas C ON B.kelas_cd=C.kelas_cd
			WHERE A.medical_cd=@pstrMedicalCd
		ELSE
			SELECT @strDataCd=B.kelas_cd,@strDataNm=C.kelas_nm,
			@intTotalDay=CASE WHEN DATEDIFF(DAY,A.datetime_in,GETDATE())+1 <=0 THEN 1 ELSE DATEDIFF(DAY,A.datetime_in,GETDATE())+1 END
			FROM trx_medical A
			JOIN trx_ruang B ON A.ruang_cd=B.ruang_cd
			JOIN trx_kelas C ON B.kelas_cd=C.kelas_cd
			WHERE A.medical_cd=@pstrMedicalCd
		
		SET @strKelasNm = @strDataNm
		--IF @intTotalDay = 0 SET @intTotalDay = 1
		
		--cek from history Kamar
		IF EXISTS(SELECT TOP 1 A.datetime_end
				FROM trx_medical_ruang A
				WHERE A.medical_cd=@pstrMedicalCd
				ORDER BY A.datetime_start DESC)
		BEGIN
			SET @intTotalDay = (SELECT TOP 1 CASE WHEN DATEDIFF(DAY,A.datetime_end,GETDATE())<=0 THEN 1
												ELSE DATEDIFF(DAY,A.datetime_end,GETDATE()) END
								FROM trx_medical_ruang A
								WHERE A.medical_cd=@pstrMedicalCd
								ORDER BY A.datetime_start DESC)
		END
		IF @intTotalDay = 0 SET @intTotalDay = 1
		--end cek history Kamar
		
		SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_kelas A
		WHERE A.kelas_cd=@strDataCd
		AND (ISNULL(A.insurance_cd,'')=@strAsuransiCd OR ISNULL(A.insurance_cd,'')='')
		
		SET @numTotalPrice = @numAmount * @intTotalDay
		SET @strDataNm = 'Akomodasi ' + @strDataNm + ' @Total ' + CONVERT(VARCHAR(2),@intTotalDay) + ' hari'
		
		SET @numJumlah = @intTotalDay
		SET @numAmountItem = @numAmount
		
		INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
		data_cd,data_nm,amount,note,manual_st,payment_st,
		quantity,item_price,
		modi_id,modi_datetime)
		VALUES (@strMedicalCdAKTIF,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
		@strDataCd,@strDataNm,@numTotalPrice,@strNote,'0','PAYMENT_ST_0',
		@numJumlah,@numAmountItem,
		@pstrUserId,GETDATE())
		
		--HARDCODE
		--Perawatan
		--Tarif perawat
		/*IF EXISTS(SELECT * FROM trx_tarif_paramedis A
					WHERE A.paramedis_tp='PARAMEDIS_TP_04'
					AND (A.kelas_cd=@strDataCd AND A.insurance_cd=@strAsuransiCd)
				)
		BEGIN		
			SET @strKelasCdTemp = @strDataCd
			SET @strAsuransiCdTemp = @strAsuransiCd
		END	
		ELSE
		BEGIN
			SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = ''
		END
		SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_paramedis A
		WHERE A.paramedis_tp='PARAMEDIS_TP_04'
		AND (A.kelas_cd=@strKelasCdTemp AND A.insurance_cd=@strAsuransiCdTemp)
		
		SET @numTotalPrice = @numAmount * @intTotalDay
		SET @strDataNm = 'Perawat ' + @strKelasNm + ' @Total ' + CONVERT(VARCHAR(2),@intTotalDay) + ' hari'
		
		SET @numJumlah = @intTotalDay
		SET @numAmountItem = @numAmount
		
		INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
		data_cd,data_nm,amount,note,manual_st,payment_st,
		quantity,item_price,
		modi_id,modi_datetime)
		VALUES (@strMedicalCdAKTIF,@strTarifTp2,@strAccountCd,@dtDatetimeTrx,
		'PARAMEDIS_TP_04',@strDataNm,@numTotalPrice,@strNote,'0','PAYMENT_ST_0',
		@numJumlah,@numAmountItem,
		@pstrUserId,GETDATE())*/
		--End perawatan
		--End HARDCODE
		
		--Looping Kamar
		DECLARE cursorData CURSOR FOR
		SELECT B.kelas_cd,
		CASE WHEN DATEDIFF(DAY,A.datetime_start,A.datetime_end)<=0 THEN 1 ELSE DATEDIFF(DAY,A.datetime_start,A.datetime_end) END,C.kelas_nm
		FROM trx_medical_ruang A
		JOIN trx_ruang B ON A.ruang_cd=B.ruang_cd
		JOIN trx_kelas C ON B.kelas_cd=C.kelas_cd
		WHERE A.medical_cd=@pstrMedicalCd
		ORDER BY A.datetime_start
		
		OPEN cursorData
		FETCH NEXT FROM cursorData
		INTO @strDataCd,@intTotalDay,@strDataNm
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			SET @strKelasNm = @strDataNm
			
			SET @strAccountCd = ''
			SET @numAmount = 0
			
			IF @intTotalDay = 0 SET @intTotalDay = 1
				
			SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
			FROM trx_tarif_kelas A
			WHERE A.kelas_cd=@strDataCd
			AND (ISNULL(A.insurance_cd,'')=@strAsuransiCd OR ISNULL(A.insurance_cd,'')='')
			
			SET @numTotalPrice = @numAmount * @intTotalDay
			SET @strDataNm = 'Akomodasi ' + @strDataNm + ' @Total ' + CONVERT(VARCHAR(2),@intTotalDay) + ' hari'
			
			SET @numJumlah = @intTotalDay
			SET @numAmountItem = @numAmount
			
			--Kamar
			INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
			data_cd,data_nm,amount,note,manual_st,payment_st,
			quantity,item_price,
			modi_id,modi_datetime)
			VALUES (@strMedicalCdAKTIF,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
			@strDataCd,@strDataNm,@numTotalPrice,@strNote,'0','PAYMENT_ST_0',
			@numJumlah,@numAmountItem,
			@pstrUserId,GETDATE())
			
			--HARDCODE
			--Perawatan
			--Tarif perawat
			/*IF EXISTS(SELECT * FROM trx_tarif_paramedis A
						WHERE A.paramedis_tp='PARAMEDIS_TP_04'
						AND (A.kelas_cd=@strDataCd AND A.insurance_cd=@strAsuransiCd)
					)
			BEGIN		
				SET @strKelasCdTemp = @strDataCd
				SET @strAsuransiCdTemp = @strAsuransiCd
			END	
			ELSE
			BEGIN
				SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = ''
			END
			SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
			FROM trx_tarif_paramedis A
			WHERE A.paramedis_tp='PARAMEDIS_TP_04'
			AND (A.kelas_cd=@strKelasCdTemp AND A.insurance_cd=@strAsuransiCdTemp)
			
			SET @numTotalPrice = @numAmount * @intTotalDay
			SET @strDataNm = 'Perawat ' + @strKelasNm + ' @Total ' + CONVERT(VARCHAR(2),@intTotalDay) + ' hari'
			
			SET @numJumlah = @intTotalDay
			SET @numAmountItem = @numAmount
			
			INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
			data_cd,data_nm,amount,note,manual_st,payment_st,
			quantity,item_price,
			modi_id,modi_datetime)
			VALUES (@strMedicalCdAKTIF,@strTarifTp2,@strAccountCd,@dtDatetimeTrx,
			'PARAMEDIS_TP_04',@strDataNm,@numTotalPrice,@strNote,'0','PAYMENT_ST_0',
			@numJumlah,@numAmountItem,
			@pstrUserId,GETDATE())*/
			--End perawatan
			--End HARDCODE
			
			FETCH NEXT FROM cursorData
			INTO @strDataCd,@intTotalDay,@strDataNm
		END
		CLOSE cursorData
		DEALLOCATE cursorData
		--End Looping Kamar
	END
		
	/*--TARIF TINDAKAN MEDIS--*/
	SET @strTarifTp = 'TARIF_TP_04'
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmountItem = 0
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
	SET @intRefSeqno = NULL
	
	DECLARE cursorData CURSOR FOR
	SELECT A.treatment_cd,A.datetime_trx,
	CASE WHEN A.medical_note = '' THEN B.treatment_nm ELSE A.medical_note END AS treatment_nm,A.quantity,A.medical_tindakan_seqno,A.kelas_cd
	FROM trx_medical_tindakan A
	JOIN trx_tindakan B ON A.treatment_cd=B.treatment_cd
	WHERE A.medical_cd=@pstrMedicalCd
	AND ISNULL(A.payment_st,'')<>'PAYMENT_ST_1'
	ORDER BY A.datetime_trx
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strTempCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefSeqno,@strKelasCdAKTIF
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @strAccountCd = ''
		SET @numAmount = 0
		IF ISNULL(@strKelasCdAKTIF,'') <> '' SET @strKelasCd=@strKelasCdAKTIF
		
		IF EXISTS(SELECT * FROM trx_tarif_tindakan A
					WHERE A.treatment_cd=@strTempCd
					AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				)
		BEGIN		
			SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = @strAsuransiCd
		END	
		ELSE
		BEGIN
			SET @strKelasCdTemp = ''
			--SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = ''
		END	
		/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_tindakan A
		WHERE A.treatment_cd=@strTempCd
		AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
		SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_tindakan A
		WHERE A.treatment_cd=@strTempCd
		AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
		
		SET @strDataCd = @strTempCd
		
		IF @numJumlah IS NULL SET @numJumlah = 1
		SET @numTotalPrice = @numAmount * @numJumlah
		
		SET @numAmountItem = @numAmount
		
		INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
		data_cd,data_nm,amount,note,manual_st,payment_st,
		quantity,item_price,ref_seqno,
		modi_id,modi_datetime)
		VALUES (@strMedicalCdAKTIF,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
		@strDataCd,@strDataNm,@numTotalPrice,@strNote,'0','PAYMENT_ST_0',
		@numJumlah,@numAmountItem,@intRefSeqno,
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strTempCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefSeqno,@strKelasCdAKTIF
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	/*--TARIF TINDAKAN MEDIS <--> VISIT DOKTER--*/
	--visit dokter masuk ke dalam tindakan
	--HARDCODE
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmountItem = 0
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
	SET @intRefSeqno = NULL
	
	DECLARE cursorData CURSOR FOR
	SELECT CASE WHEN ISNULL(B.spesialis_cd,'')<>'' THEN 'TRJM002' ELSE 'TRJM001' END AS data_cd,
	B.spesialis_cd,A.datetime_trx,
	CASE WHEN ISNULL(B.spesialis_cd,'')<>'' THEN 'Visit Dokter Spesialis' ELSE 'Visit Dokter Umum' END AS treatment_nm,
	1 AS quantity,A.medical_dr_seqno,A.kelas_cd
	FROM trx_medical_dokter A
	JOIN trx_dokter B ON A.dr_cd=B.dr_cd
	WHERE A.medical_cd=@pstrMedicalCd
	AND A.trx_dr_tp='TRXDR_TP_1'
	ORDER BY A.datetime_trx
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strTempCd,@strTemp2Cd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefSeqno,@strKelasCdAKTIF
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @strAccountCd = ''
		SET @numAmount = 0
		IF ISNULL(@strKelasCdAKTIF,'') <> '' SET @strKelasCd=@strKelasCdAKTIF
		
		/*IF ISNULL(@strTemp2Cd,'')=''
		BEGIN
		--Dokter Umum
		END
		ELSE
		BEGIN
		--Dokter Spesialis
		END*/
		
		IF EXISTS(SELECT * FROM trx_tarif_tindakan A
					WHERE A.treatment_cd=@strTempCd
					AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				)
		BEGIN		
			SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = @strAsuransiCd
		END	
		ELSE
		BEGIN
			SET @strKelasCdTemp = ''
			--SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = ''
		END	
		SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_tindakan A
		WHERE A.treatment_cd=@strTempCd
		AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
		
		SET @strDataCd = @strTempCd
		
		IF @numJumlah IS NULL SET @numJumlah = 1
		SET @numTotalPrice = @numAmount * @numJumlah
		
		SET @numAmountItem = @numAmount
		
		INSERT INTO trx_medical_settlement (medical_cd,tarif_tp,account_cd,datetime_trx,
		data_cd,data_nm,amount,note,manual_st,payment_st,
		quantity,item_price,ref_seqno,
		modi_id,modi_datetime)
		VALUES (@strMedicalCdAKTIF,@strTarifTp,@strAccountCd,@dtDatetimeTrx,
		@strDataCd,@strDataNm,@numTotalPrice,@strNote,'0','PAYMENT_ST_0',
		@numJumlah,@numAmountItem,@intRefSeqno,
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strTempCd,@strTemp2Cd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefSeqno,@strKelasCdAKTIF
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	--End HARDCODE
		
	/*--TARIF INVENTORI--*/
	DECLARE @numItemPriceMaster Numeric(15)
	--Obat
	--HARDCODE obat/BHP --> harga obat/BHP per kelas sama
	--DECLARE @strKelasCd2 varchar(20)
	--SET @strKelasCd2 = ''
	--End HARDCODE obat
	
	DECLARE @intResepSeqno bigint
	DECLARE @strResepTp varchar(20)
	DECLARE @intTotalObat int
	
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
	SET @intTotalObat = 0
	
	SET @intRefResepSeqno = NULL
	SET @intRefAlkesSeqno = NULL
	
	DECLARE cursorData CURSOR FOR
	SELECT B.item_cd,A.resep_date,CASE WHEN B.resep_tp='RESEP_TP_2' THEN B.data_nm ELSE C.item_nm END AS item_nm,
	B.quantity,A.medical_resep_seqno,A.resep_no,B.resep_seqno,B.resep_tp,A.kelas_cd
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	WHERE A.medical_cd=@pstrMedicalCd
	AND ISNULL(B.payment_st,'')<>'PAYMENT_ST_1'
	ORDER BY A.medical_resep_seqno,B.resep_seqno
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strDataCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefResepSeqno,@strNote,@intResepSeqno,@strResepTp,@strKelasCdAKTIF
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--SET @intTotalObat = @intTotalObat + 1
		
		SET @strAccountCd = ''
		SET @numAmount = 0
		SET @numTotalPrice = 0
		IF ISNULL(@strKelasCdAKTIF,'') <> '' SET @strKelasCd=@strKelasCdAKTIF
		
		IF @strResepTp='RESEP_TP_1'
		--Resep per item
		BEGIN
			SET @intTotalObat = @intTotalObat + 1
			
			IF EXISTS(SELECT * FROM trx_tarif_inventori A
					WHERE A.item_cd=@strDataCd
					AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd) --AND (A.kelas_cd=@strKelasCd2 AND A.insurance_cd=@strAsuransiCd)
					--AND (A.kelas_cd=@strKelasCd2 AND A.insurance_cd=@strAsuransiCd) --AND (A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd)
				)
			BEGIN		
				SET @strKelasCdTemp = @strKelasCd --SET @strKelasCdTemp = @strKelasCd2
				--SET @strKelasCdTemp = @strKelasCd2 --SET @strKelasCdTemp = @strKelasCd
				
				SET @strAsuransiCdTemp = @strAsuransiCd
			END	
			ELSE
			BEGIN
				SET @strKelasCdTemp = ''
				--SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = ''
			END	
			/*SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
			FROM trx_tarif_inventori A
			WHERE A.item_cd=@strDataCd
			AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))*/
			SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
			FROM trx_tarif_inventori A
			WHERE A.item_cd=@strDataCd
			AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			
			SET @numTotalPrice = @numAmount * @numJumlah
		END
		ELSE
		--Resep racik
		BEGIN
			SET @intTotalObat = @intTotalObat + 1
			/*IF @numJumlah<=30
				SET @intTotalObat = @intTotalObat + 1
			ELSE IF @numJumlah>30 AND @numJumlah<=60
				SET @intTotalObat = @intTotalObat + 2
			ELSE
				SET @intTotalObat = @intTotalObat + 3*/
				
			SET @strAccountCd = 'AC301' /*--set account obat racik to account obat--*/
			SET @numTotalPrice = (SELECT simrke.FN_GET_ITEM_PRICE_RACIK(@pstrMedicalCd,@intResepSeqno))
		END
		
		SET @numItemPriceMaster = (SELECT ISNULL(item_price_buy,0) FROM inv_item_master WHERE item_cd=@strDataCd)
		
		INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
		ref_medical_resep_seqno,ref_medical_alkes_seqno,ref_resep_seqno,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		modi_id,modi_datetime)
		VALUES (@strMedicalCdAKTIF,@strAccountCd,@dtDatetimeTrx,
		@intRefResepSeqno,@intRefAlkesSeqno,@intResepSeqno,
		@strDataCd,@numJumlah,@numAmount,@numItemPriceMaster,@numTotalPrice,@strNote,'0','PAYMENT_ST_0',
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strDataCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefResepSeqno,@strNote,@intResepSeqno,@strResepTp,@strKelasCdAKTIF
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	--HARDCODE
	--Obat : Jasa Racik Per Item Total
	IF @intTotalObat <> 0
	BEGIN
		DECLARE @numBiayaRacik numeric
		DECLARE @intTotal int
		
		SET @numBiayaRacik = 0
		SET @intTotal = 0
		
		SELECT @intTotal=ISNULL(COUNT(B.resep_seqno),0)
		FROM trx_medical_resep A
		JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
		WHERE A.medical_cd=@pstrMedicalCd AND B.resep_tp='RESEP_TP_2' --Resep racik
		AND ISNULL(payment_st,'')<>'PAYMENT_ST_1'
		
		SELECT @numBiayaRacik=ISNULL(tarif,0)
		FROM trx_tarif_inventori
		WHERE item_cd='NONINV900'
		
		SET @strAccountCd = 'AC301' --set jasa to account obat
		
		IF @intTotal <> 0
		BEGIN
			INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
			item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
			ref_medical_resep_seqno,ref_resep_seqno,
			modi_id,modi_datetime)
			VALUES (@pstrMedicalCd,@strAccountCd,@dtDatetimeTrx,
			'NONINV900',@intTotal,@numBiayaRacik,@numBiayaRacik,@numBiayaRacik * @intTotal,'','0','PAYMENT_ST_0',
			0,0,
			@pstrUserId,GETDATE())
		END
	END
	--Obat : Jasa
	/*
	IF @intTotalObat <> 0
	BEGIN
		DECLARE @intTariftpNo bigint
		DECLARE @numJS numeric
		DECLARE @numJM numeric
		DECLARE @numJP numeric
		DECLARE @numJSTemp numeric
		DECLARE @numJMTemp numeric
		DECLARE @numJPTemp numeric
		DECLARE @intTotal int
		DECLARE @intTotalRacik int
		
		SET @numJS = 0
		SET @numJM = 0
		SET @numJP = 0
		SET @intTotal = 0
		
		DECLARE cursorData CURSOR FOR
		SELECT B.item_cd,B.quantity,B.resep_seqno,B.resep_tp
		FROM trx_medical_resep A
		JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
		LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
		WHERE A.medical_cd=@strMedicalCdAKTIF
		ORDER BY B.resep_seqno
			
		OPEN cursorData
		FETCH NEXT FROM cursorData
		INTO @strDataCd,@numJumlah,@intResepSeqno,@strResepTp
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			IF @strResepTp='RESEP_TP_1'
			--Resep per item
			BEGIN
				SET @intTotal = @intTotal + 1
				
				--Biaya JASA FARMASI
				SET @intTariftpNo = 5000000
				--Jasa Sarana Farmasi
				SELECT @numJSTemp=ISNULL(tarif_item,0)
				FROM trx_tariftp_item
				WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
				--Jasa Medis Farmasi
				SELECT @numJMTemp=ISNULL(tarif_item,0)
				FROM trx_tariftp_item
				WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
				--Jasa Pelaksana Farmasi
				SELECT @numJPTemp=ISNULL(tarif_item,0)
				FROM trx_tariftp_item
				WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
				
				SET @numJS = @numJS + @numJSTemp
				SET @numJM = @numJM + @numJMTemp
				SET @numJP = @numJP + @numJPTemp
				--End Biaya JASA FARMASI
			END
			ELSE
			--Resep racik
			BEGIN
				--Biaya JASA FARMASI
				SET @intTotalRacik = 0
				IF @numJumlah<=30
					SET @intTotalRacik = 1
				ELSE IF @numJumlah>30 AND @numJumlah<=60
					SET @intTotalRacik = 2
				ELSE
					SET @intTotalRacik = 3
					
				SET @intTotal = @intTotal + @intTotalRacik
					
				SET @intTariftpNo = 5000020
				--Jasa Sarana Farmasi
				SELECT @numJSTemp=ISNULL(tarif_item,0) * @intTotalRacik
				FROM trx_tariftp_item
				WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
				--Jasa Medis Farmasi
				SELECT @numJMTemp=ISNULL(tarif_item,0) * @intTotalRacik
				FROM trx_tariftp_item
				WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
				--Jasa Pelaksana Farmasi
				SELECT @numJPTemp=ISNULL(tarif_item,0) * @intTotalRacik
				FROM trx_tariftp_item
				WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
				
				SET @numJS = @numJS + @numJSTemp
				SET @numJM = @numJM + @numJMTemp
				SET @numJP = @numJP + @numJPTemp
				--End Biaya JASA FARMASI
			END
			
			FETCH NEXT FROM cursorData
			INTO @strDataCd,@numJumlah,@intResepSeqno,@strResepTp
		END
		CLOSE cursorData
		DEALLOCATE cursorData
		
		SET @strAccountCd = 'AC301' --set jasa to account obat
		
		INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		ref_medical_resep_seqno,ref_resep_seqno,
		modi_id,modi_datetime)
		VALUES (@strMedicalCdAKTIF,@strAccountCd,@dtDatetimeTrx,
		'NONINV101',1,@numJS,@numJS,@numJS,'','0','PAYMENT_ST_0',
		@intRefResepSeqno,@intResepSeqno,
		@pstrUserId,GETDATE())
		
		INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		ref_medical_resep_seqno,ref_resep_seqno,
		modi_id,modi_datetime)
		VALUES (@strMedicalCdAKTIF,@strAccountCd,@dtDatetimeTrx,
		'NONINV102',1,@numJM,@numJM,@numJM,'','0','PAYMENT_ST_0',
		@intRefResepSeqno,@intResepSeqno,
		@pstrUserId,GETDATE())
		
		INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		ref_medical_resep_seqno,ref_resep_seqno,
		modi_id,modi_datetime)
		VALUES (@strMedicalCdAKTIF,@strAccountCd,@dtDatetimeTrx,
		'NONINV103',1,@numJP,@numJP,@numJP,'','0','PAYMENT_ST_0',
		@intRefResepSeqno,@intResepSeqno,
		@pstrUserId,GETDATE())
	END
	*/
	--End Obat : Jasa
	
	--BHP
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	SET @strTempCd = ''
	SET @strTemp2Cd = ''
	
	SET @intRefResepSeqno = NULL
	SET @intRefAlkesSeqno = NULL
	
	DECLARE cursorData CURSOR FOR
	SELECT A.item_cd,A.datetime_trx,B.item_nm,A.quantity,A.medical_alkes_seqno,A.kelas_cd
	FROM trx_medical_alkes A
	JOIN inv_item_master B ON A.item_cd=B.item_cd
	WHERE A.medical_cd=@pstrMedicalCd
	AND ISNULL(A.payment_st,'')<>'PAYMENT_ST_1'
	ORDER BY A.medical_alkes_seqno
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strDataCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefAlkesSeqno,@strKelasCdAKTIF
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @strAccountCd = ''
		SET @numAmount = 0
		SET @numTotalPrice = 0
		IF ISNULL(@strKelasCdAKTIF,'') <> '' SET @strKelasCd=@strKelasCdAKTIF
		
		IF EXISTS(SELECT * FROM trx_tarif_inventori A
				WHERE A.item_cd=@strDataCd
				AND (A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd)
				--AND (A.kelas_cd=@strKelasCd2 AND A.insurance_cd=@strAsuransiCd)
			)
		BEGIN		
			SET @strKelasCdTemp = @strKelasCd
			--SET @strKelasCdTemp = @strKelasCd2
			
			SET @strAsuransiCdTemp = @strAsuransiCd
		END	
		ELSE
		BEGIN
			SET @strKelasCdTemp = ''
			--SET @strKelasCdTemp = @strKelasCd
			SET @strAsuransiCdTemp = ''
		END	
		
		SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
		FROM trx_tarif_inventori A
		WHERE A.item_cd=@strDataCd
		AND (A.kelas_cd=@strKelasCdTemp AND A.insurance_cd=@strAsuransiCdTemp)
		
		--HARDCODE
		--solusi sementara hardcode account BHP
		SET @strAccountCd = 'AC302'
		--End HARDCODE
		
		SET @numTotalPrice = @numAmount * @numJumlah
		
		SET @numItemPriceMaster = (SELECT ISNULL(item_price_buy,0) FROM inv_item_master WHERE item_cd=@strDataCd)
		
		INSERT INTO trx_medical_settlement_inv (medical_cd,account_cd,datetime_trx,
		ref_medical_resep_seqno,ref_medical_alkes_seqno,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		modi_id,modi_datetime)
		VALUES (@strMedicalCdAKTIF,@strAccountCd,@dtDatetimeTrx,
		@intRefResepSeqno,@intRefAlkesSeqno,
		@strDataCd,@numJumlah,@numAmount,@numItemPriceMaster,@numTotalPrice,@strNote,'0','PAYMENT_ST_0',
		@pstrUserId,GETDATE())

		FETCH NEXT FROM cursorData
		INTO @strDataCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefAlkesSeqno,@strKelasCdAKTIF
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	DELETE FROM trx_settlement_share WHERE medical_cd=@pstrMedicalCd
	
	IF @@ERROR<> 0
	BEGIN
		ROLLBACK TRANSACTION
		--RAISERROR('Error proses',16,1)
		RETURN
	END
	ELSE
		COMMIT TRANSACTION
END;

DROP FUNCTION IF EXISTS SP_PROSES_SETTLEMENT_INCOME(bigint,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PROSES_SETTLEMENT_INCOME] 
@pintMedicalResepSeqno bigint,
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @strPasienNm varchar(100)
	DECLARE @strInfo01 varchar(1000)
	
	DECLARE @strTarifTp varchar(20)
	DECLARE @strAccountCd varchar(20)
	DECLARE @dtDatetimeTrx datetime
	DECLARE @strDataCd varchar(20)
	DECLARE @strDataNm varchar(100)
	DECLARE @numJumlah numeric(5,2)
	DECLARE @numPrice numeric
	DECLARE @numTotalPrice numeric
	DECLARE @strNote varchar(1000)
	
	DECLARE @intRefResepSeqno bigint
	DECLARE @intRefAlkesSeqno bigint
	
	SELECT @strPasienNm=A.pasien_nm,@strInfo01=A.info_01
	FROM trx_medical_resep A 
	WHERE A.medical_resep_seqno=@pintMedicalResepSeqno
	
	BEGIN TRANSACTION
	
	EXECUTE SP_CLEAR_SETTLEMENT_INCOME @pintMedicalResepSeqno
	
	DECLARE @intResepSeqno bigint
	
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numJumlah = 0
	SET @numPrice = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	
	SET @intRefResepSeqno = NULL
	SET @intRefAlkesSeqno = NULL
	
	DECLARE cursorData CURSOR FOR
	SELECT B.item_cd,A.resep_date,C.item_nm,
	B.quantity,B.price,A.medical_resep_seqno,A.info_01,B.resep_seqno
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	WHERE A.medical_resep_seqno=@pintMedicalResepSeqno
	ORDER BY A.medical_resep_seqno,B.resep_seqno
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strDataCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@numPrice,@intRefResepSeqno,@strNote,@intResepSeqno
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @strAccountCd = ''
		SET @numTotalPrice = 0
		
		SET @numTotalPrice = @numPrice * @numJumlah
		
		--HARDCODE
		SET @strAccountCd = 'AC801' /*--set account to account pendapatan lain--*/
		--End HARDCODE
		
		INSERT INTO trx_medical_settlement_inv (account_cd,datetime_trx,
		ref_medical_resep_seqno,ref_medical_alkes_seqno,ref_resep_seqno,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		modi_id,modi_datetime)
		VALUES (@strAccountCd,@dtDatetimeTrx,
		@intRefResepSeqno,@intRefAlkesSeqno,@intResepSeqno,
		@strDataCd,@numJumlah,@numPrice,@numPrice,@numTotalPrice,@strNote,'0','PAYMENT_ST_0',
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strDataCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@numPrice,@intRefResepSeqno,@strNote,@intResepSeqno
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	IF @@ERROR<> 0
	BEGIN
		ROLLBACK TRANSACTION
		--RAISERROR('Error proses',16,1)
		RETURN
	END
	ELSE
		COMMIT TRANSACTION
END;

DROP FUNCTION IF EXISTS SP_PROSES_SETTLEMENT_SALE(bigint,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_PROSES_SETTLEMENT_SALE] 
@pintMedicalResepSeqno bigint,
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @strPasienNm varchar(100)
	DECLARE @strDrNm varchar(210)
	DECLARE @strAddress varchar(1000)
	
	DECLARE @strKelasCd varchar(20)
	DECLARE @strAsuransiCd varchar(20)
	DECLARE @strKelasCdTemp varchar(20)
	DECLARE @strAsuransiCdTemp varchar(20)
	
	DECLARE @strTarifTp varchar(20)
	DECLARE @strAccountCd varchar(20)
	DECLARE @dtDatetimeTrx datetime
	DECLARE @strDataCd varchar(20)
	DECLARE @strDataNm varchar(100)
	DECLARE @numAmount numeric
	DECLARE @numJumlah numeric(5,2)
	DECLARE @numTotalPrice numeric
	DECLARE @strNote varchar(1000)
	
	DECLARE @intRefResepSeqno bigint
	DECLARE @intRefAlkesSeqno bigint
	
	SELECT @strPasienNm=A.pasien_nm,@strDrNm=A.dr_nm,@strAddress=A.info_01
	FROM trx_medical_resep A 
	WHERE A.medical_resep_seqno=@pintMedicalResepSeqno
	
	SET @strKelasCd = ''
	SET @strAsuransiCd = ''
	
	BEGIN TRANSACTION
	
	EXECUTE SP_CLEAR_SETTLEMENT_SALE @pintMedicalResepSeqno
	
	/*--TARIF INVENTORI--*/
	DECLARE @numItemPriceMaster Numeric(15)
	--Obat
	DECLARE @intResepSeqno bigint
	DECLARE @strResepTp varchar(20)
	
	SET @strAccountCd = ''
	SET @dtDatetimeTrx = GETDATE()
	SET @strDataCd = ''
	SET @strDataNm = ''
	SET @numAmount = 0
	SET @numJumlah = 0
	SET @numTotalPrice = 0
	SET @strNote = ''
	
	SET @intRefResepSeqno = NULL
	SET @intRefAlkesSeqno = NULL
	
	DECLARE cursorData CURSOR FOR
	SELECT B.item_cd,A.resep_date,CASE WHEN B.resep_tp='RESEP_TP_2' THEN B.data_nm ELSE C.item_nm END AS item_nm,
	B.quantity,A.medical_resep_seqno,A.resep_no,B.resep_seqno,B.resep_tp
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	WHERE A.medical_resep_seqno=@pintMedicalResepSeqno
	ORDER BY A.medical_resep_seqno,B.resep_seqno
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strDataCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefResepSeqno,@strNote,@intResepSeqno,@strResepTp
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @strAccountCd = ''
		SET @numAmount = 0
		SET @numTotalPrice = 0
		
		IF @strResepTp='RESEP_TP_1'
		--Resep per item
		BEGIN
			IF EXISTS(SELECT * FROM trx_tarif_inventori A
					WHERE A.item_cd=@strDataCd
					AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				)
			BEGIN		
				SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = @strAsuransiCd
			END	
			ELSE
			BEGIN
				SET @strKelasCdTemp = ''
				SET @strAsuransiCdTemp = ''
			END	
			SELECT @strAccountCd=A.account_cd,@numAmount=A.tarif
			FROM trx_tarif_inventori A
			WHERE A.item_cd=@strDataCd
			AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			
			SET @numTotalPrice = @numAmount * @numJumlah
			
			--HARDCODE
			SET @strAccountCd = 'AC310' /*--set account obat to account penjualan bebas--*/
			--End HARDCODE
		END
		ELSE
		--Resep racik
		BEGIN
			--HARDCODE
			SET @strAccountCd = 'AC310' /*--set account obat racik to account penjualan bebas--*/
			--End HARDCODE
			SET @numTotalPrice = (SELECT simrke.FN_GET_ITEM_PRICE_RACIK('',@intResepSeqno))
		END
		
		SET @numItemPriceMaster = (SELECT ISNULL(item_price_buy,0) FROM inv_item_master WHERE item_cd=@strDataCd)
		
		INSERT INTO trx_medical_settlement_inv (account_cd,datetime_trx,
		ref_medical_resep_seqno,ref_medical_alkes_seqno,ref_resep_seqno,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		modi_id,modi_datetime)
		VALUES (@strAccountCd,@dtDatetimeTrx,
		@intRefResepSeqno,@intRefAlkesSeqno,@intResepSeqno,
		@strDataCd,@numJumlah,@numAmount,@numItemPriceMaster,@numTotalPrice,@strNote,'0','PAYMENT_ST_0',
		@pstrUserId,GETDATE())
		
		FETCH NEXT FROM cursorData
		INTO @strDataCd,@dtDatetimeTrx,@strDataNm,@numJumlah,@intRefResepSeqno,@strNote,@intResepSeqno,@strResepTp
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	
	--HARDCODE
	--Obat : Jasa Racik Per Item Total
	DECLARE @numBiayaRacik numeric
	DECLARE @intTotal int
	
	SET @numBiayaRacik = 0
	SET @intTotal = 0
	
	SELECT @intTotal=ISNULL(COUNT(B.resep_seqno),0)
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	WHERE A.medical_resep_seqno=@pintMedicalResepSeqno AND B.resep_tp='RESEP_TP_2' --Resep racik
	
	SELECT @numBiayaRacik=ISNULL(tarif,0)
	FROM trx_tarif_inventori
	WHERE item_cd='NONINV900'
	
	SET @strAccountCd = 'AC301' --set jasa to account obat
	
	IF @intTotal <> 0
	BEGIN
		INSERT INTO trx_medical_settlement_inv (account_cd,datetime_trx,
		ref_medical_resep_seqno,ref_medical_alkes_seqno,ref_resep_seqno,
		item_cd,quantity,item_price,item_price_master,total_price,note,manual_st,payment_st,
		modi_id,modi_datetime)
		VALUES (@strAccountCd,@dtDatetimeTrx,
		@intRefResepSeqno,@intRefAlkesSeqno,@intResepSeqno,
		'NONINV900',@intTotal,@numBiayaRacik,@numBiayaRacik,@numBiayaRacik * @intTotal,@strNote,'0','PAYMENT_ST_0',
		@pstrUserId,GETDATE())
	END
	--End Obat : Jasa Racik Per Item Total
	
	IF @@ERROR<> 0
	BEGIN
		ROLLBACK TRANSACTION
		--RAISERROR('Error proses',16,1)
		RETURN
	END
	ELSE
		COMMIT TRANSACTION
END;

DROP FUNCTION IF EXISTS SP_RI_DELETE(Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RI_DELETE] 
@pstrRiCd Varchar(10)
AS
BEGIN
	DELETE FROM po_receive_detail
	WHERE ri_cd=@pstrRiCd
	
	DELETE FROM po_receive_item WHERE ri_cd=@pstrRiCd
END;

DROP FUNCTION IF EXISTS SP_RI_EDIT_TRX(Varchar,int,int,Varchar,Varchar,Datetime,Varchar,Numeric,Varchar,Numeric,Numeric,Numeric,Numeric,Numeric,Char,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RI_EDIT_TRX]
@pstrTrxCd Varchar(20), 
@pintYear int,
@pintMonth int,
@pstrSupplierCd Varchar(20),
@pstrSupplierNo Varchar(20),
@pdtTrxDate Datetime,
@pstrCurr Varchar(20),
@pnumRate Numeric,
@pstrVatTp Varchar(20),
@pnumPercentPPN Numeric(5,2),
@pnumTotalPrice Numeric,
@pnumPPN Numeric,
@pnumTotalAmount Numeric,
@pnumTotalDiscount Numeric,
@pstrStatus Char(1),
@pstrNote Varchar(1000),
@pstrUserId Varchar(20),
@pstrUserNm Varchar(100)
AS
BEGIN
	UPDATE po_receive_item SET supplier_cd=@pstrSupplierCd,trx_date=@pdtTrxDate,
	invoice_no=@pstrSupplierNo,currency_cd=@pstrCurr,rate=@pnumRate,
	vat_tp=@pstrVatTp,percent_ppn=@pnumPercentPPN,
	total_price=@pnumTotalPrice,ppn=@pnumPPN,total_amount=@pnumTotalAmount,total_discount=@pnumTotalDiscount,
	ri_st=@pstrStatus,note=@pstrNote,
	modi_id=@pstrUserId, modi_datetime=GETDATE()
	WHERE ri_cd=@pstrTrxCd
	
	DELETE FROM po_receive_detail WHERE ri_cd=@pstrTrxCd
END;

DROP FUNCTION IF EXISTS SP_RI_GET_TRX_LIST(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RI_GET_TRX_LIST]
@pdtDate Varchar(10),
@pstrSupplier Varchar(100),
@pstrRiNo Varchar(100)
AS
BEGIN
	SELECT A.ri_cd AS trx_cd,simrke.FN_FORMATDATE(A.trx_date) AS trx_date,A.ri_no AS trx_no,B.supplier_nm,
	A.note,A.ri_st,simrke.FN_GET_STATUS(A.ri_st) AS po_st_nm
	FROM po_receive_item A LEFT JOIN po_supplier B ON A.supplier_cd=B.supplier_cd
	WHERE A.ri_st IN ('0','1','2','3')
	AND simrke.FN_FORMATDATE(A.trx_date)=@pdtDate
	AND B.supplier_nm LIKE '%' + @pstrSupplier + '%'
	AND A.ri_no LIKE '%' + @pstrRiNo + '%'
	ORDER BY A.ri_no DESC,A.trx_date DESC
END;

DROP FUNCTION IF EXISTS SP_RI_PROCESS_TRX(int,int,Varchar,Varchar,Datetime,Varchar,Numeric,Varchar,Numeric,Numeric,Numeric,Numeric,Numeric,Char,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RI_PROCESS_TRX] 
@pintYear int,
@pintMonth int,
@pstrSupplierCd Varchar(20),
@pstrSupplierNo Varchar(20),
@pdtTrxDate Datetime,
@pstrCurr Varchar(20),
@pnumRate Numeric,
@pstrVatTp Varchar(20),
@pnumPercentPPN Numeric(5,2),
@pnumTotalPrice Numeric,
@pnumPPN Numeric,
@pnumTotalAmount Numeric,
@pnumTotalDiscount Numeric,
@pstrStatus Char(1),
@pstrNote Varchar(1000),
@pstrUserId Varchar(20),
@pstrUserNm Varchar(100)
AS
BEGIN
	DECLARE @strTrxCd Varchar(20)
	DECLARE @strTrxNo Varchar(10)
	
	SET @strTrxNo = (SELECT simrke.FN_RI_GET_TRX_NO(@pintYear,@pintMonth))
	SET @strTrxCd = @strTrxNo
	
	SELECT @strTrxCd AS trx_cd,@strTrxNo AS trx_no
	
	INSERT INTO po_receive_item (ri_cd,trx_year,trx_month,ri_no,
	supplier_cd,invoice_no,trx_date,
	currency_cd,rate,
	vat_tp,percent_ppn,
	total_price,ppn,total_amount,total_discount,
	ri_st,note,entry_by,entry_date,modi_id,modi_datetime) 
	VALUES (@strTrxCd,@pintYear,@pintMonth,@strTrxNo,
	@pstrSupplierCd,@pstrSupplierNo,@pdtTrxDate,
	@pstrCurr,@pnumRate,
	@pstrVatTp,@pnumPercentPPN,
	@pnumTotalPrice,@pnumPPN,@pnumTotalAmount,@pnumTotalDiscount,
	@pstrStatus,@pstrNote,
	@pstrUserNm,GETDATE(),@pstrUserId,GETDATE())
END;

DROP FUNCTION IF EXISTS SP_RI_PROSES_STOK(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RI_PROSES_STOK] 
@pstrRiCd Varchar(10),
@pstrUserId Varchar(20),
@pstrUserNm Varchar(100)
AS
BEGIN
	DECLARE @strItemCd varchar(20)
	DECLARE @numQuantity numeric
	DECLARE @numUnitPrice numeric
	
	DECLARE @strTarifTp Varchar(20)
	DECLARE @numPersenTarif Numeric(5,2)
	
	DECLARE @strInsuranceCd Varchar(20)
	DECLARE @numPersenTarifInsurance Numeric(5,2)
	
	DECLARE @strPosCd varchar(20)
	DECLARE @numOldStok Numeric(10,2)
	
	SET @strPosCd = 'WHMASTER'
	
	DECLARE cursorData CURSOR FOR
	SELECT A.item_cd,A.quantity,A.unit_price
	FROM po_receive_detail A
	WHERE ri_cd=@pstrRiCd
	ORDER BY A.item_cd
	
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strItemCd,@numQuantity,@numUnitPrice
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		/*--Update stok --*/
		IF EXISTS(SELECT * FROM inv_pos_item
			WHERE pos_cd=@strPosCd
			AND item_cd=@strItemCd)
				UPDATE inv_pos_item
				SET quantity=quantity + @numQuantity,
				modi_id=@pstrUserId,modi_datetime=GETDATE()
				WHERE pos_cd=@strPosCd
				AND item_cd=@strItemCd
		ELSE
				INSERT INTO inv_pos_item (pos_cd,item_cd,quantity,modi_id,modi_datetime)
				VALUES (@strPosCd,@strItemCd,@numQuantity,
				@pstrUserId,GETDATE())
		/*--End Update stok --*/
		
		/*--Log stok --*/
		SET @numOldStok = (SELECT ISNULL(quantity,0) FROM inv_pos_item 
							WHERE pos_cd=@strPosCd
							AND item_cd=@strItemCd)
				 
		INSERT INTO inv_item_move (move_tp,pos_cd,pos_destination,item_cd,
		trx_qty,old_stock,new_stock,note,
		trx_by,trx_datetime,modi_id,modi_datetime)
		VALUES ('MOVE_TP_1',@strPosCd,@strPosCd,@strItemCd,
		@numQuantity,@numOldStok,@numQuantity+@numOldStok,'Penerimaan Barang',
		@pstrUserNm,GETDATE(),@pstrUserId,GETDATE())
		/*--End Log stok --*/
		
		/*--Update harga--*/
		--Using inv_item_tariftp
		/*SET @strTarifTp = (SELECT tariftp_cd FROM inv_item_master WHERE item_cd=@strItemCd)*/
		/*SET @numPersenTarif = (SELECT TOP 1 ISNULL(persen_tarif,0) FROM inv_item_tariftp
							   WHERE tariftp_cd=@strTarifTp
							   AND ISNULL(insurance_cd,'')='')*/
		
		--Using com_code					   
		/*SET @numPersenTarif = (SELECT ISNULL(code_value,0) FROM com_code 
							   WHERE com_cd='APPCD_INVMARGIN')*/
		/*IF @numPersenTarif <> 0
		BEGIN		
		END*/	
		
		--Update harga table inv_item_master
		/*UPDATE inv_item_master
		SET item_price_buy=@numUnitPrice,item_price=@numUnitPrice + (@numUnitPrice * (@numPersenTarif/100)),
		modi_id=@pstrUserId,modi_datetime=GETDATE()
		WHERE item_cd=@strItemCd*/
		
		--Update harga table trx_tarif_inventori
		/*UPDATE trx_tarif_inventori
		SET tarif=@numUnitPrice + (@numUnitPrice * (@numPersenTarif/100)),
		modi_id=@pstrUserId,modi_datetime=GETDATE()
		WHERE item_cd=@strItemCd
		AND ISNULL(insurance_cd,'')=''*/
		
		--looping tariftp_cd --> tariftp asuransi
		/*
			DECLARE cursorData2 CURSOR FOR
			SELECT insurance_cd,ISNULL(persen_tarif,0)
			FROM inv_item_tariftp
			WHERE tariftp_cd=@strTarifTp
			AND insurance_cd<>''
			
			OPEN cursorData2
			FETCH NEXT FROM cursorData2
			INTO @strInsuranceCd,@numPersenTarifInsurance
			WHILE (@@FETCH_STATUS = 0)
			BEGIN
				IF EXISTS(SELECT TOP 1 * FROM trx_tarif_inventori
					WHERE item_cd=@strItemCd
					AND insurance_cd=@strInsuranceCd)
						UPDATE trx_tarif_inventori
						SET tarif=@numUnitPrice + (@numUnitPrice * (@numPersenTarifInsurance/100)),
						modi_id=@pstrUserId,modi_datetime=GETDATE()
						WHERE item_cd=@strItemCd
						AND insurance_cd=@strInsuranceCd
				ELSE
						INSERT INTO trx_tarif_inventori (item_cd,insurance_cd,tarif,modi_id,modi_datetime)
						VALUES (@strItemCd,@strInsuranceCd,@numUnitPrice + (@numUnitPrice * (@numPersenTarifInsurance/100)),
						@pstrUserId,GETDATE())
				
				FETCH NEXT FROM cursorData2
				INTO @strInsuranceCd,@numPersenTarifInsurance
			END
			CLOSE cursorData2
			DEALLOCATE cursorData2
		*/	
		--End looping
		/*--End Update harga--*/
		
		FETCH NEXT FROM cursorData
		INTO @strItemCd,@numQuantity,@numUnitPrice
	END
	CLOSE cursorData
	DEALLOCATE cursorData
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_BOR(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_BOR]
@pintMonth int,
@pintYear int
AS
BEGIN
	DECLARE @dtDateStart Datetime
	DECLARE @dtDateEnd Datetime
	
	SET @dtDateStart = CONVERT(DATETIME,simrke.FN_FORMAT_STRING(@pintMonth,'0',2) + '/01/' + simrke.FN_FORMAT_STRING(@pintYear,'0',4))
	SET @dtDateEnd = DATEADD(S,-1,DATEADD(MM, DATEDIFF(M,0,@dtDateStart)+1,0))

	SELECT A.kelas_cd,A.kelas_nm,COUNT(B.ruang_cd) AS total_kamar,
	ISNULL(simrke.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd,@dtDateStart,@dtDateEnd),0) AS total_pasien,
	ISNULL(simrke.FN_RPT_TOTAL_HARIRAWAT(A.kelas_cd,@dtDateStart,@dtDateEnd),0) AS total_harirawat,
	DATEPART(dd,@dtDateEnd) AS day_month
	FROM trx_kelas A 
	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd AND ISNULL(B.ruang_tp,'')<>'1'
	GROUP BY A.kelas_cd,A.kelas_nm
	ORDER BY A.kelas_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_BOR_PERIODE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_BOR_PERIODE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	--Format Parameter date : dd/mm/yyyy
	DECLARE @dtDateStart Datetime
	DECLARE @dtDateEnd Datetime
	
	SET @dtDateStart = CONVERT(DATETIME,SUBSTRING(@pdtDateStart,4,2) + '/' + SUBSTRING(@pdtDateStart,1,2) + '/' + SUBSTRING(@pdtDateStart,7,4))
	SET @dtDateEnd = DATEADD(S,-1,DATEADD(MM, DATEDIFF(M,0,@dtDateStart)+1,0))
	
	SELECT A.kelas_cd,A.kelas_nm,COUNT(B.ruang_cd) AS total_kamar,
	ISNULL(simrke.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd,@dtDateStart,@dtDateEnd),0) AS total_pasien,
	ISNULL(simrke.FN_RPT_TOTAL_HARIRAWAT(A.kelas_cd,@dtDateStart,@dtDateEnd),0) AS total_harirawat,
	DATEPART(dd,@dtDateEnd) AS day_month
	FROM trx_kelas A 
	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd AND ISNULL(B.ruang_tp,'')<>'1'
	GROUP BY A.kelas_cd,A.kelas_nm
	ORDER BY A.kelas_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_DATA_PASIEN(varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_DATA_PASIEN]
@pstrPasienCd varchar(20)
AS
BEGIN
	SELECT A.pasien_cd,A.no_rm,A.pasien_nm,B.code_nm AS tipe_pasien,
	A.address,postcode,REGION1.region_nm AS propinsi,REGION2.region_nm AS kabupaten,REGION3.region_nm AS kecamatan,REGION3.region_nm AS kelurahan,
	A.birth_place,A.birth_date,simrke.FN_RPT_GET_AGE_LONG(A.birth_date) AS age,A.phone,A.mobile,A.register_date,A.weight,A.height,A.email,
	COMCD1.code_nm AS blood,COMCD2.code_value AS gender,COMCD3.code_nm AS marital,
	COMCD4.code_nm AS religion,COMCD5.code_nm AS occupation,COMCD6.code_nm AS education,COMCD7.code_nm AS race,C.nation_nm,
	FAMILYTP1.family_nm AS ayah, FAMILYTP2.family_nm AS ibu,
	FAMILYTP.family_nm AS penanggung, FAMILYTP.address AS penanggung_address, FAMILYTP.phone AS penanggung_phone,
	FAMILYTP.birth_date AS penanggung_birthdate,simrke.FN_RPT_GET_AGE_LONG(FAMILYTP.birth_date) AS penanggung_age, 
	COMCD8.code_nm AS penanggung_hubungan,INS.insurance_nm,PASINS.insurance_no
	FROM trx_pasien A
	LEFT JOIN com_code B On A.pasien_tp=B.com_cd
	LEFT JOIN com_nation C On A.nation_cd=C.nation_cd
	LEFT JOIN com_region REGION1 On A.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On A.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On A.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On A.region_kel=REGION4.region_cd
	LEFT JOIN com_code COMCD1 On A.blood_tp=COMCD1.com_cd
	LEFT JOIN com_code COMCD2 On A.gender_tp=COMCD2.com_cd
	LEFT JOIN com_code COMCD3 On A.marital_tp=COMCD3.com_cd
	LEFT JOIN com_code COMCD4 On A.religion_cd=COMCD4.com_cd
	LEFT JOIN com_code COMCD5 On A.occupation_cd=COMCD5.com_cd
	LEFT JOIN com_code COMCD6 On A.education_cd=COMCD6.com_cd
	LEFT JOIN com_code COMCD7 On A.race_cd=COMCD7.com_cd
	LEFT JOIN trx_pasien_family FAMILYTP1 On A.pasien_cd=FAMILYTP1.pasien_cd AND FAMILYTP1.family_tp='FAMILY_TP_01'
	LEFT JOIN trx_pasien_family FAMILYTP2 On A.pasien_cd=FAMILYTP2.pasien_cd AND FAMILYTP2.family_tp='FAMILY_TP_02'
	LEFT JOIN trx_pasien_family FAMILYTP On A.pasien_cd=FAMILYTP.pasien_cd AND (FAMILYTP.family_tp<>'FAMILY_TP_01' AND FAMILYTP.family_tp<>'FAMILY_TP_02')
	LEFT JOIN com_code COMCD8 On FAMILYTP.family_tp=COMCD8.com_cd
	LEFT JOIN trx_pasien_insurance PASINS ON A.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	WHERE A.pasien_cd = @pstrPasienCd
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_DATA_RADIOLOGI(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_DATA_RADIOLOGI]
@intTrxSeqNo bigint
AS
BEGIN
	SELECT MU.*,UMEDIS.medicalunit_nm, PAS.no_rm, PAS.pasien_nm,DATEDIFF(YEAR,PAS.birth_date,MU.datetime_trx) AS usia,
	REF.referensi_nm,DR.dr_nm,DR2.dr_nm AS dr2_nm
	FROM trx_medical_unit MU
	JOIN trx_medical MED ON MED.medical_cd=MU.medical_cd
	JOIN trx_unitmedis_item UMEDIS ON MU.medicalunit_cd=UMEDIS.medicalunit_cd
	LEFT JOIN trx_referensi REF ON MED.referensi_cd=REF.referensi_cd
	LEFT JOIN trx_dokter DR ON MU.dr_cd=DR.dr_cd 
	LEFT JOIN trx_dokter DR2 ON MU.dr2_cd=DR2.dr_cd
	JOIN trx_pasien PAS ON MED.pasien_cd=PAS.pasien_cd
	WHERE MU.medical_unit_seqno=@intTrxSeqNo
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_DATA_RADIOLOGI_ALL(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_DATA_RADIOLOGI_ALL]
@intTrxSeqNo bigint
AS
BEGIN
	DECLARE @strMedicalCd varchar(10)
	
	SET @strMedicalCd = (SELECT TOP 1 A.medical_cd
						FROM trx_medical_unit A 
						WHERE A.medical_unit_seqno=@intTrxSeqNo)
	
	SELECT MU.*,UMITEM.medicalunit_nm, PAS.no_rm, PAS.pasien_nm,DATEDIFF(YEAR,PAS.birth_date,MU.datetime_trx) AS usia,
	REF.referensi_nm,DR.dr_nm,DR2.dr_nm AS dr2_nm
	FROM trx_medical_unit MU
	JOIN trx_medical MED ON MED.medical_cd=MU.medical_cd
	JOIN trx_unitmedis_item UMITEM ON MU.medicalunit_cd=UMITEM.medicalunit_cd
	JOIN trx_unit_medis UMEDIS ON UMITEM.medunit_cd=UMEDIS.medunit_cd AND UMEDIS.medicalunit_tp='MEDICALUNIT_TP_3'
	LEFT JOIN trx_referensi REF ON MED.referensi_cd=REF.referensi_cd
	LEFT JOIN trx_dokter DR ON MU.dr_cd=DR.dr_cd 
	LEFT JOIN trx_dokter DR2 ON MU.dr2_cd=DR2.dr_cd
	JOIN trx_pasien PAS ON MED.pasien_cd=PAS.pasien_cd
	WHERE MU.medical_cd=@strMedicalCd
	ORDER BY MU.datetime_trx
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_DEMOGRAFI_PASIEN(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_DEMOGRAFI_PASIEN]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT REGION3.region_nm AS kecamatan,COUNT(*) AS total
	FROM trx_medical A 
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd AND PAS.region_prop='33' AND PAS.region_kab='3307' AND ISNULL(PAS.region_kec,'')<>''
	JOIN com_region REGION1 On PAS.region_prop=REGION1.region_cd
	JOIN com_region REGION2 On PAS.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On PAS.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On PAS.region_kel=REGION4.region_cd
	WHERE A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	GROUP BY REGION3.region_nm
	UNION
	SELECT 'LAIN-LAIN' AS kecamatan,COUNT(*) AS total
	FROM trx_medical A 
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd AND ((PAS.region_prop<>'33' AND PAS.region_kab<>'3307') OR ISNULL(PAS.region_kec,'')='') 
	WHERE A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	ORDER BY REGION3.region_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_DEMOGRAFI_PASIEN_DETAIL(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_DEMOGRAFI_PASIEN_DETAIL]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT REGION1.region_nm AS propinsi,REGION2.region_nm AS kabupaten,REGION3.region_nm AS kecamatan,
	CASE WHEN ISNULL(REGION4.region_nm,'')='' THEN '-----' ELSE REGION4.region_nm END AS kelurahan,
	COUNT(*) AS total
	FROM trx_medical A 
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	JOIN com_region REGION1 On PAS.region_prop=REGION1.region_cd AND REGION1.region_cd='33' --Jawa Tengah
	JOIN com_region REGION2 On PAS.region_kab=REGION2.region_cd AND REGION2.region_cd='3307' --Wonosobo
	LEFT JOIN com_region REGION3 On PAS.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On PAS.region_kel=REGION4.region_cd
	WHERE A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	GROUP BY REGION1.region_nm,REGION2.region_nm,REGION3.region_nm,REGION4.region_nm
	UNION
	SELECT 'LAIN-LAIN' AS propinsi,'LAIN-LAIN' AS kabupaten,'LAIN-LAIN' AS kecamatan,'LAIN-LAIN' AS kelurahan,
	COUNT(*) AS total
	FROM trx_medical A 
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd AND PAS.region_prop<>'33' AND PAS.region_kab<>'3307'
	WHERE A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	ORDER BY propinsi,kabupaten,kecamatan,kelurahan
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_DOKTER() CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_DOKTER]
AS
BEGIN
	SELECT A.dr_cd, A.dr_nm, A.spesialis_cd, B.spesialis_nm
	FROM trx_dokter A
	LEFT JOIN trx_spesialis B ON A.spesialis_cd=B.spesialis_cd
	ORDER BY B.spesialis_nm, A.dr_nm
END;

-- sikat
DROP FUNCTION IF EXISTS SP_RPT_GET_FARMASIPENDAPATAN_BYDATE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_FARMASIPENDAPATAN_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	/*SELECT CASE WHEN ISNULL(MED.medical_tp,'')='' THEN 'ZPENJUALAN' ELSE MED.medical_tp END AS medical_tp,
	CASE WHEN ISNULL(COM.code_nm,'')='' THEN 'Penjualan' ELSE COM.code_nm END AS medical_tp_nm,
	simrke.FN_FORMATDATE(RESEP.resep_date) AS trx_datetime,
	CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
	OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
	OBAT.quantity,U.unit_nm,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(MED.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(MED.medical_cd,OBAT.resep_seqno) END AS harga,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(MED.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(MED.medical_cd,OBAT.resep_seqno) END AS total
	FROM trx_medical_resep RESEP
	JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
	LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
	LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
	LEFT JOIN trx_medical MED ON RESEP.medical_cd=MED.medical_cd
	LEFT JOIN com_code COM ON MED.medical_tp=COM.com_cd
	LEFT JOIN trx_pasien PAS ON RESEP.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	ORDER BY medical_tp,INS.insurance_nm,RESEP.resep_date*/
	
	SELECT medical_tp,medical_tp_nm,insurance_nm,SUM(total) AS total
	FROM
	(SELECT CASE WHEN ISNULL(MED.medical_tp,'')='' THEN 'ZPENJUALAN' ELSE MED.medical_tp END AS medical_tp,
	CASE WHEN ISNULL(COM.code_nm,'')='' THEN 'Penjualan' ELSE COM.code_nm END AS medical_tp_nm,
	simrke.FN_FORMATDATE(RESEP.resep_date) AS trx_datetime,
	CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
	OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
	OBAT.quantity,U.unit_nm,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(MED.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(MED.medical_cd,OBAT.resep_seqno) END AS harga,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(MED.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(MED.medical_cd,OBAT.resep_seqno) END AS total
	FROM trx_medical_resep RESEP
	JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
	LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
	LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
	LEFT JOIN trx_medical MED ON RESEP.medical_cd=MED.medical_cd
	LEFT JOIN com_code COM ON MED.medical_tp=COM.com_cd
	LEFT JOIN trx_pasien PAS ON RESEP.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	) 
	AS DATA
	GROUP BY medical_tp,medical_tp_nm,insurance_nm
	ORDER BY medical_tp,insurance_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_GDR(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_GDR]
@pintMonth int,
@pintYear int
AS
BEGIN
	DECLARE @dtDateStart Datetime
	DECLARE @dtDateEnd Datetime
	
	SET @dtDateStart = CONVERT(DATETIME,simrke.FN_FORMAT_STRING(@pintMonth,'0',2) + '/01/' + simrke.FN_FORMAT_STRING(@pintYear,'0',4))
	SET @dtDateEnd = DATEADD(S,-1,DATEADD(MM, DATEDIFF(M,0,@dtDateStart)+1,0))
	
	SELECT A.kelas_cd,A.kelas_nm,COUNT(B.ruang_cd) AS total_kamar,
	ISNULL(simrke.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd,@dtDateStart,@dtDateEnd),0) AS total_pasien,
	ISNULL(simrke.FN_RPT_TOTAL_DEATH(A.kelas_cd,@dtDateStart,@dtDateEnd),0) AS total_meninggal,
	DATEPART(dd,@dtDateEnd) AS day_month
	FROM trx_kelas A 
	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd AND ISNULL(B.ruang_tp,'')<>'1'
	GROUP BY A.kelas_cd,A.kelas_nm
	ORDER BY A.kelas_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_GDR_PERIODE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_GDR_PERIODE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	--Format Parameter date : dd/mm/yyyy
	DECLARE @dtDateStart Datetime
	DECLARE @dtDateEnd Datetime
	
	SET @dtDateStart = CONVERT(DATETIME,SUBSTRING(@pdtDateStart,4,2) + '/' + SUBSTRING(@pdtDateStart,1,2) + '/' + SUBSTRING(@pdtDateStart,7,4))
	SET @dtDateEnd = DATEADD(S,-1,DATEADD(MM, DATEDIFF(M,0,@dtDateStart)+1,0))
	
	SELECT A.kelas_cd,A.kelas_nm,COUNT(B.ruang_cd) AS total_kamar,
	ISNULL(simrke.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd,@dtDateStart,@dtDateEnd),0) AS total_pasien,
	ISNULL(simrke.FN_RPT_TOTAL_DEATH(A.kelas_cd,@dtDateStart,@dtDateEnd),0) AS total_meninggal,
	DATEPART(dd,@dtDateEnd) AS day_month
	FROM trx_kelas A 
	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd AND ISNULL(B.ruang_tp,'')<>'1'
	GROUP BY A.kelas_cd,A.kelas_nm
	ORDER BY A.kelas_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_HISTORY_STOCK(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_HISTORY_STOCK]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrItemCode Varchar(20)
AS
BEGIN
	DECLARE @pstrPosCd Varchar(20)
	
	IF @pstrItemCode = ''
		SELECT A.item_cd, A.item_nm, UNIT.unit_nm AS unit, B.trx_datetime,
		B.trx_qty, B.old_stock, B.new_stock,
		CASE WHEN B.move_tp='MOVE_TP_1' THEN B.trx_qty ELSE 0 END AS trx_in,
		CASE WHEN B.move_tp='MOVE_TP_2' THEN B.trx_qty ELSE 0 END AS trx_out,
		B.purpose, COMCD.code_nm AS status,B.note,
		CASE WHEN B.trx_by<>'' THEN B.trx_by ELSE B.modi_id END AS trx_by
		FROM inv_item_master A
		JOIN inv_item_move B ON A.item_cd=B.item_cd
		LEFT JOIN com_code COMCD On B.move_tp=COMCD.com_cd
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.trx_datetime))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.trx_datetime))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	ELSE
		SELECT A.item_cd, A.item_nm, UNIT.unit_nm AS unit, B.trx_datetime,
		B.trx_qty, B.old_stock, B.new_stock,
		CASE WHEN B.move_tp='MOVE_TP_1' THEN B.trx_qty ELSE 0 END AS trx_in,
		CASE WHEN B.move_tp='MOVE_TP_2' THEN B.trx_qty ELSE 0 END AS trx_out,
		B.purpose, COMCD.code_nm AS status,B.note,
		CASE WHEN B.trx_by<>'' THEN B.trx_by ELSE B.modi_id END AS trx_by
		FROM inv_item_master A
		JOIN inv_item_move B ON A.item_cd=B.item_cd
		LEFT JOIN com_code COMCD On B.move_tp=COMCD.com_cd
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE A.item_cd=@pstrItemCode
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.trx_datetime))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.trx_datetime))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_KAMAR() CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_KAMAR]
AS
BEGIN
	SELECT B.kelas_nm, A.ruang_cd, A.ruang_nm, C.bangsal_nm
	FROM trx_ruang A
	JOIN trx_kelas B ON A.kelas_cd=B.kelas_cd
	LEFT JOIN trx_bangsal C ON A.bangsal_cd=C.bangsal_cd
	ORDER BY B.kelas_nm, A.ruang_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_KEUANGANOBAT_BYDATE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_KEUANGANOBAT_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
	INV.type_cd,INVTP.type_nm,OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
	OBAT.quantity,U.unit_nm,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS item_price_master,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total_price_master
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
	JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
	LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
	JOIN inv_item_type INVTP ON INV.type_cd=INVTP.type_cd
	LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
	WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND RESEP.proses_st='1'
	ORDER BY INV.type_cd,A.medical_tp,INS.insurance_nm,RESEP.resep_date
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_KEUANGANOBAT_BYDOKTER(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_KEUANGANOBAT_BYDOKTER]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrDrCd Varchar(20)
AS
BEGIN
	IF @pstrDrCd = '' 
	/*--Semua--*/
		/*SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		INV.type_cd,INVTP.type_nm,OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
		OBAT.quantity,U.unit_nm,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS item_price_master,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total_price_master
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
		JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
		LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
		JOIN inv_item_type INVTP ON INV.type_cd=INVTP.type_cd
		LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
		WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND RESEP.proses_st='1'
		ORDER BY DR.dr_nm,A.medical_tp,INS.insurance_nm,RESEP.resep_date*/
		SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
		CASE WHEN ISNULL(RESEP.dr_cd,'')<>'' THEN RESEPDR.dr_nm ELSE DR.dr_nm END AS dr_nm,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		INV.type_cd,INVTP.type_nm,OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
		OBAT.quantity,U.unit_nm,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS item_price_master,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total_price_master
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
		LEFT JOIN trx_dokter RESEPDR ON RESEP.dr_cd=RESEPDR.dr_cd
		JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
		LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
		JOIN inv_item_type INVTP ON INV.type_cd=INVTP.type_cd
		LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
		WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND RESEP.proses_st='1'
		ORDER BY dr_nm,A.medical_tp,INS.insurance_nm,RESEP.resep_date
	ELSE
	/*--Per dokter--*/
		/*SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		INV.type_cd,INVTP.type_nm,OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
		OBAT.quantity,U.unit_nm,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS item_price_master,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total_price_master
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
		JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
		LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
		JOIN inv_item_type INVTP ON INV.type_cd=INVTP.type_cd
		LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
		WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND DR.dr_cd=@pstrDrCd
		AND RESEP.proses_st='1'
		ORDER BY DR.dr_nm,A.medical_tp,INS.insurance_nm,RESEP.resep_date*/
		SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
		CASE WHEN ISNULL(RESEP.dr_cd,'')<>'' THEN RESEPDR.dr_nm ELSE DR.dr_nm END AS dr_nm,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		INV.type_cd,INVTP.type_nm,OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
		OBAT.quantity,U.unit_nm,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS item_price_master,
		CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_RPT_GET_ITEM_PRICE_MASTER(OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total_price_master
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
		LEFT JOIN trx_dokter RESEPDR ON RESEP.dr_cd=RESEPDR.dr_cd
		JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
		LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
		JOIN inv_item_type INVTP ON INV.type_cd=INVTP.type_cd
		LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
		WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND RESEPDR.dr_cd=@pstrDrCd
		AND RESEP.proses_st='1'
		ORDER BY dr_nm,A.medical_tp,INS.insurance_nm,RESEP.resep_date
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_KLAIMTRX_BYDATE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_KLAIMTRX_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,PAS.address,INS.insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,MR.medical_data
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_medical_record MR ON A.medical_cd=MR.medical_cd
	JOIN trx_settlement SETT ON A.medical_cd=SETT.medical_cd
	WHERE SETT.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND SETT.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND SETT.payment_st='PAYMENT_ST_1'
	ORDER BY A.medical_tp,INS.insurance_nm,SETT.invoice_date
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_LABORATORIUM() CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_LABORATORIUM]
AS
BEGIN
	SELECT C.medicalunit_nm AS root_nm, A.medicalunit_cd, A.medicalunit_nm
	FROM trx_unitmedis_item A
	JOIN trx_unit_medis B On A.medunit_cd=B.medunit_cd AND B.medicalunit_tp='MEDICALUNIT_TP_2'
	LEFT JOIN trx_unitmedis_item C ON A.medicalunit_root=C.medicalunit_cd AND C.medicalunit_nm IS NOT NULL
	ORDER BY CASE WHEN C.medicalunit_nm IS NULL THEN A.medicalunit_cd ELSE A.medicalunit_root END,
	A.medicalunit_root,A.medicalunit_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_MEDICAL_SEP(varchar,varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_MEDICAL_SEP]
@pstrMedicalCd varchar(10),
@pstrSepNo varchar(50)
AS
BEGIN
	SELECT A.insurance_no_trx,A.datetime_trx,A.reff_datetime,A.reff_insurance_notrx,
	A.referensi_nm,A.reff_icd_cd,A.reff_icd_nm,A.member_tp,A.print_no,A.note,
	PAS.no_rm,PAS.pasien_nm,MED.datetime_in,PAS.birth_date,
	simrke.FN_DECODE(PAS.gender_tp,'GENDER_TP_01','L','GENDER_TP_02','P') AS gender_nm,
	MED.medical_tp,simrke.FN_DECODE(MED.medical_tp,'MEDICAL_TP_01','Rawat Jalan','MEDICAL_TP_02','Rawat Inap') AS medical_tp_nm,
	MED.medunit_cd,C.medunit_nm,MED.ruang_cd,
	CASE WHEN ISNULL(INS.standar_kelas_cd,'')<>'' THEN INS.standar_kelas_cd ELSE KMR.kelas_cd END AS kelas_cd,
	CASE WHEN ISNULL(INS.standar_kelas_cd,'')<>'' THEN INSKLS.kelas_nm ELSE KLS.kelas_nm END AS kelas_nm,
	ISNULL(PAS.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + 
	ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	INS.insurance_no
	FROM trx_medical_insurance A
	JOIN trx_medical MED ON A.medical_cd=MED.medical_cd
	JOIN trx_pasien PAS ON MED.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance INS ON PAS.pasien_cd=INS.pasien_cd AND INS.default_st='1'
	LEFT JOIN trx_kelas INSKLS ON INS.standar_kelas_cd=INSKLS.kelas_cd
	LEFT JOIN com_region REGION1 On PAS.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On PAS.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On PAS.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On PAS.region_kel=REGION4.region_cd
	LEFT JOIN trx_unit_medis C ON MED.medunit_cd=C.medunit_cd 
	LEFT JOIN trx_ruang KMR ON MED.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	WHERE A.insurance_no_trx= @pstrSepNo
	AND MED.medical_cd= @pstrMedicalCd
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_NDR(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_NDR]
@pintMonth int,
@pintYear int
AS
BEGIN
	DECLARE @dtDateStart Datetime
	DECLARE @dtDateEnd Datetime
	
	SET @dtDateStart = CONVERT(DATETIME,simrke.FN_FORMAT_STRING(@pintMonth,'0',2) + '/01/' + simrke.FN_FORMAT_STRING(@pintYear,'0',4))
	SET @dtDateEnd = DATEADD(S,-1,DATEADD(MM, DATEDIFF(M,0,@dtDateStart)+1,0))
	
	SELECT A.kelas_cd,A.kelas_nm,COUNT(B.ruang_cd) AS total_kamar,
	ISNULL(simrke.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd,@dtDateStart,@dtDateEnd),0) AS total_pasien,
	ISNULL(simrke.FN_RPT_TOTAL_DEATH_OUTTP11(A.kelas_cd,@dtDateStart,@dtDateEnd),0) AS total_meninggal,
	DATEPART(dd,@dtDateEnd) AS day_month
	FROM trx_kelas A 
	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd AND ISNULL(B.ruang_tp,'')<>'1'
	GROUP BY A.kelas_cd,A.kelas_nm
	ORDER BY A.kelas_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_NDR_PERIODE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_NDR_PERIODE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	--Format Parameter date : dd/mm/yyyy
	DECLARE @dtDateStart Datetime
	DECLARE @dtDateEnd Datetime
	
	SET @dtDateStart = CONVERT(DATETIME,SUBSTRING(@pdtDateStart,4,2) + '/' + SUBSTRING(@pdtDateStart,1,2) + '/' + SUBSTRING(@pdtDateStart,7,4))
	SET @dtDateEnd = DATEADD(S,-1,DATEADD(MM, DATEDIFF(M,0,@dtDateStart)+1,0))
	
	SELECT A.kelas_cd,A.kelas_nm,COUNT(B.ruang_cd) AS total_kamar,
	ISNULL(simrke.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd,@dtDateStart,@dtDateEnd),0) AS total_pasien,
	ISNULL(simrke.FN_RPT_TOTAL_DEATH_OUTTP11(A.kelas_cd,@dtDateStart,@dtDateEnd),0) AS total_meninggal,
	DATEPART(dd,@dtDateEnd) AS day_month
	FROM trx_kelas A 
	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd AND ISNULL(B.ruang_tp,'')<>'1'
	GROUP BY A.kelas_cd,A.kelas_nm
	ORDER BY A.kelas_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_OBATKRONIS_BYDATE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_OBATKRONIS_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
	OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
	OBAT.num_01 AS qty_real,OBAT.quantity AS qty_kwitansi,OBAT.num_02 AS qty_tagihan,U.unit_nm,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.num_02 * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total
	--MR.medical_data
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	--LEFT JOIN trx_medical_record MR ON A.medical_cd=MR.medical_cd
	JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
	JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
	LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
	LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
	WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND ISNULL(RESEP.case_tp,'')='1'
	--AND RESEP.proses_st='1'
	ORDER BY A.medical_tp,INS.insurance_nm,RESEP.resep_date
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_PARAMEDIS() CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_PARAMEDIS]
AS
BEGIN
	SELECT A.paramedis_cd, A.paramedis_nm, A.paramedis_tp, COMCD.code_nm AS tipe_nm
	FROM trx_paramedis A
	LEFT JOIN com_code COMCD On A.paramedis_tp=COMCD.com_cd
	ORDER BY COMCD.code_nm, A.paramedis_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_PENYAKIT() CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_PENYAKIT]
AS
BEGIN
	SELECT A.icd_cd, A.icd_nm, A.icd_root, B.icd_cd AS root_nm
	FROM trx_icd A
	LEFT JOIN trx_icd B On A.icd_root=B.icd_cd
	ORDER BY A.icd_root, A.icd_cd
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_POLIKLINIK() CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_POLIKLINIK]
AS
BEGIN
	SELECT A.medunit_cd, A.medunit_nm
	FROM trx_unit_medis A
	WHERE A.medicalunit_tp='MEDICALUNIT_TP_1'
	ORDER BY A.medunit_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RADIOLOGI() CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RADIOLOGI]
AS
BEGIN
	SELECT C.medicalunit_nm AS root_nm, A.medicalunit_cd, A.medicalunit_nm
	FROM trx_unitmedis_item A
	JOIN trx_unit_medis B On A.medunit_cd=B.medunit_cd AND B.medicalunit_tp='MEDICALUNIT_TP_3'
	LEFT JOIN trx_unitmedis_item C ON A.medicalunit_root=C.medicalunit_cd AND C.medicalunit_nm IS NOT NULL
	ORDER BY CASE WHEN C.medicalunit_nm IS NULL THEN A.medicalunit_cd ELSE A.medicalunit_root END,
	A.medicalunit_root,A.medicalunit_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RESEPCOPY(bigint,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RESEPCOPY] 
@pintMedicalResepNo bigint,
@pstrUserNm Varchar(100)
AS
BEGIN
	SELECT B.item_cd,A.resep_date,
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN B.data_nm ELSE C.item_nm END AS item_nm,
	B.quantity,A.medical_resep_seqno,A.resep_no,B.info_01 AS catatan,DR.dr_nm,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.pasien_nm ELSE A.pasien_nm END AS pasien_nm,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.no_rm ELSE '' END AS no_rm,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.address ELSE A.info_01 END AS address
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN trx_pasien D ON A.pasien_cd=D.pasien_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	WHERE A.medical_resep_seqno=@pintMedicalResepNo
	ORDER BY B.resep_seqno
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RESEPRESI(bigint,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RESEPRESI] 
@pintMedicalResepNo bigint,
@pstrUserNm Varchar(100)
AS
BEGIN
	DECLARE @numTotalPrice numeric
	DECLARE @strKelasCd varchar(20)
	DECLARE @strAsuransiCd varchar(20)
	DECLARE @strItemCd varchar(20)
	DECLARE @numJumlah numeric
	DECLARE @numAmount numeric
	DECLARE @intTotal int
	
	DECLARE @strTrxMedicalCd Varchar(10)
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalCd Varchar(10)
	DECLARE @intResepSeqno bigint
	DECLARE @strResepTp varchar(20)
	
	DECLARE @strKelasCdTemp varchar(20)
	DECLARE @strAsuransiCdTemp varchar(20)
	
	DECLARE @intTariftpNo bigint
	DECLARE @numJS numeric
	DECLARE @numJM numeric
	DECLARE @numJP numeric
	DECLARE @numJSTemp numeric
	DECLARE @numJMTemp numeric
	DECLARE @numJPTemp numeric
	DECLARE @intTotalRacik int
	
	SELECT @strTrxMedicalCd=D.medical_cd,@strMedicalRootCd=D.medical_root_cd,
	@strAsuransiCd=C.insurance_cd,@strKelasCd=E.kelas_cd
	FROM trx_medical_resep A
	LEFT JOIN trx_pasien B ON A.pasien_cd=B.pasien_cd
	LEFT JOIN trx_pasien_insurance C ON B.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_ruang E ON D.ruang_cd=E.ruang_cd 
	WHERE A.medical_resep_seqno=@pintMedicalResepNo
	
	IF @strKelasCd IS NULL SET @strKelasCd = ''
	IF @strAsuransiCd IS NULL SET @strAsuransiCd = ''
	
	/*DECLARE @intMedicalResepNoRoot bigint
	DECLARE @intMedicalResepNoChild bigint
	DECLARE @strTrxMedicalCdRoot Varchar(10)
	DECLARE @strTrxMedicalCdChild Varchar(10)
	SELECT TOP 1 @intMedicalResepNoRoot=A.medical_resep_seqno 
	FROM trx_medical_resep A 
	JOIN trx_medical B ON A.medical_cd=B.medical_cd 
	WHERE B.medical_cd IN (SELECT medical_root_cd 
						 FROM trx_medical 
						 WHERE medical_cd=@strTrxMedicalCd)
	SELECT TOP 1 @intMedicalResepNoChild=A.medical_resep_seqno 
	FROM trx_medical_resep A 
	JOIN trx_medical B ON A.medical_cd=B.medical_cd 
	WHERE B.medical_cd IN (SELECT medical_cd 
						 FROM trx_medical 
						 WHERE medical_root_cd=@strTrxMedicalCd)*/
	
	--get total price
	SET @intTotal = 0
	SET @numTotalPrice = 0
	SET @numJS = 0
	SET @numJM = 0
	SET @numJP = 0
	
	IF ISNULL(@strTrxMedicalCd,'') <> ''
	--Transaksi Resep Pasien
		DECLARE cursorData CURSOR FOR
		SELECT B.item_cd,B.quantity,B.resep_seqno,B.resep_tp,A.medical_cd
		FROM trx_medical_resep A
		JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
		LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
		--WHERE A.medical_resep_seqno=@pintMedicalResepNo
		WHERE A.medical_cd=@strTrxMedicalCd
		ORDER BY B.resep_seqno
	ELSE
	--Transaksi Penjualan Bebas
		DECLARE cursorData CURSOR FOR
		SELECT B.item_cd,B.quantity,B.resep_seqno,B.resep_tp,A.medical_cd
		FROM trx_medical_resep A
		JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
		LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
		WHERE A.medical_resep_seqno=@pintMedicalResepNo
		ORDER BY B.resep_seqno
			
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strItemCd,@numJumlah,@intResepSeqno,@strResepTp,@strMedicalCd
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--SET @intTotal = @intTotal + 1
		
		SET @numAmount = 0
		
		/*
		SELECT @numAmount=A.tarif
		FROM trx_tarif_inventori A
		WHERE A.item_cd=@strItemCd
		AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd='' AND A.insurance_cd='') 
				OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))
				
		SET @numTotalPrice = @numTotalPrice + (@numAmount * @numJumlah)
		*/
		
		IF @strResepTp='RESEP_TP_1'
		--Resep per item
		BEGIN
			SET @intTotal = @intTotal + 1
			
			IF EXISTS(SELECT * FROM trx_tarif_inventori A
					WHERE A.item_cd=@strItemCd
					--AND (A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd)
					AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				)
			BEGIN		
				SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = @strAsuransiCd
			END	
			ELSE
			BEGIN
				--SET @strKelasCdTemp = ''
				SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = ''
			END	
			
			SELECT @numAmount=A.tarif
			FROM trx_tarif_inventori A
			WHERE A.item_cd=@strItemCd
			--AND (A.kelas_cd=@strKelasCdTemp AND A.insurance_cd=@strAsuransiCdTemp)
			AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			
			SET @numTotalPrice = @numTotalPrice + (@numAmount * @numJumlah)
			
			--HARDCODE
			--Biaya JASA FARMASI
			/*
			SET @intTariftpNo = 5000000
			--Jasa Sarana Farmasi
			SELECT @numJSTemp=ISNULL(tarif_item,0)
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
			--Jasa Medis Farmasi
			SELECT @numJMTemp=ISNULL(tarif_item,0)
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
			--Jasa Pelaksana Farmasi
			SELECT @numJPTemp=ISNULL(tarif_item,0)
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
			
			SET @numJS = @numJS + @numJSTemp
			SET @numJM = @numJM + @numJMTemp
			SET @numJP = @numJP + @numJPTemp
			*/
			--End Biaya JASA FARMASI
		END
		ELSE
		--Resep racik
		BEGIN
			/*IF @numJumlah<=30
				SET @intTotal = @intTotal + 1
			ELSE IF @numJumlah>30 AND @numJumlah<=60
				SET @intTotal = @intTotal + 2
			ELSE
				SET @intTotal = @intTotal + 3*/
			
			SET @numTotalPrice = @numTotalPrice + (SELECT simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,@intResepSeqno))
			
			--HARDCODE
			--Biaya JASA FARMASI
			/*
			SET @intTotalRacik = 0
			IF @numJumlah<=30
				SET @intTotalRacik = 1
			ELSE IF @numJumlah>30 AND @numJumlah<=60
				SET @intTotalRacik = 2
			ELSE
				SET @intTotalRacik = 3
				
			SET @intTariftpNo = 5000020
			--Jasa Sarana Farmasi
			SELECT @numJSTemp=ISNULL(tarif_item,0) * @intTotalRacik
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
			--Jasa Medis Farmasi
			SELECT @numJMTemp=ISNULL(tarif_item,0) * @intTotalRacik
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
			--Jasa Pelaksana Farmasi
			SELECT @numJPTemp=ISNULL(tarif_item,0) * @intTotalRacik
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
			
			SET @numJS = @numJS + @numJSTemp
			SET @numJM = @numJM + @numJMTemp
			SET @numJP = @numJP + @numJPTemp
			*/
			--End Biaya JASA FARMASI
		END
		
		FETCH NEXT FROM cursorData
		INTO @strItemCd,@numJumlah,@intResepSeqno,@strResepTp,@strMedicalCd
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	--end total price
	
	/*--Jasa Sarana Farmasi
	SELECT @numJS=ISNULL(tarif_item,0) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
	--Jasa Medis Farmasi
	SELECT @numJM=ISNULL(tarif_item,0) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
	--Jasa Pelaksana Farmasi
	SELECT @numJP=ISNULL(tarif_item,0) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004*/
	
	SET @numTotalPrice = @numTotalPrice + @numJS + @numJM + @numJP
	
	IF ISNULL(@strTrxMedicalCd,'') <> ''
	--Transaksi Resep Pasien
		SELECT DISTINCT B.resep_seqno,B.item_cd,A.resep_date,
		CASE WHEN B.resep_tp='RESEP_TP_2' THEN B.data_nm ELSE C.item_nm END AS item_nm,
		B.quantity,simrke.FN_RPT_GET_ITEM_RETUR(A.medical_resep_seqno,B.item_cd) AS retur,
		A.medical_resep_seqno,A.resep_no,
		/*TARIF.tarif AS item_price,*/
		CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE TARIF.tarif END AS item_price,
		/*B.quantity * TARIF.tarif AS item_total_price,*/
		CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE B.quantity * TARIF.tarif END AS item_total_price,
		@numTotalPrice AS total_price,
		ISNULL(simrke.FN_GET_TERBILANG(@numTotalPrice),'') AS total_terbilang,
		CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.pasien_nm ELSE A.pasien_nm END AS pasien_nm,
		CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.no_rm ELSE '' END AS no_rm,
		CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.address ELSE A.info_01 END AS address,
		CASE WHEN ISNULL(A.dr_cd,'')<>'' THEN DR.dr_nm ELSE A.dr_nm END AS dr_nm,
		@numJS AS JS,@numJM AS JM,@numJP AS JP, @intTotal AS total_item
		FROM trx_medical_resep A
		JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
		LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
		LEFT JOIN trx_pasien D ON A.pasien_cd=D.pasien_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		--LEFT JOIN trx_tarif_inventori TARIF ON B.item_cd=TARIF.item_cd AND ((TARIF.kelas_cd=@strKelasCd AND TARIF.insurance_cd=@strAsuransiCd) OR (TARIF.kelas_cd='' AND TARIF.insurance_cd='') OR (TARIF.kelas_cd IS NULL AND TARIF.insurance_cd IS NULL))
		LEFT JOIN trx_tarif_inventori TARIF ON B.item_cd=TARIF.item_cd AND ((TARIF.kelas_cd=@strKelasCd AND TARIF.insurance_cd=@strAsuransiCd) OR (ISNULL(TARIF.kelas_cd,'')=@strKelasCdTemp AND ISNULL(TARIF.insurance_cd,'')=@strAsuransiCdTemp) OR (TARIF.kelas_cd IS NULL AND TARIF.insurance_cd IS NULL))
		--WHERE A.medical_resep_seqno=@pintMedicalResepNo
		WHERE A.medical_cd=@strTrxMedicalCd
		ORDER BY B.resep_seqno
	ELSE
	--Transaksi Penjualan Bebas
		SELECT DISTINCT B.resep_seqno,B.item_cd,A.resep_date,
		CASE WHEN B.resep_tp='RESEP_TP_2' THEN B.data_nm ELSE C.item_nm END AS item_nm,
		B.quantity,simrke.FN_RPT_GET_ITEM_RETUR(A.medical_resep_seqno,B.item_cd) AS retur,
		A.medical_resep_seqno,A.resep_no,
		/*TARIF.tarif AS item_price,*/
		CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE TARIF.tarif END AS item_price,
		/*B.quantity * TARIF.tarif AS item_total_price,*/
		CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE B.quantity * TARIF.tarif END AS item_total_price,
		@numTotalPrice AS total_price,
		ISNULL(simrke.FN_GET_TERBILANG(@numTotalPrice),'') AS total_terbilang,
		CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.pasien_nm ELSE A.pasien_nm END AS pasien_nm,
		CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.no_rm ELSE '' END AS no_rm,
		CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.address ELSE A.info_01 END AS address,
		CASE WHEN ISNULL(A.dr_cd,'')<>'' THEN DR.dr_nm ELSE A.dr_nm END AS dr_nm,
		@numJS AS JS,@numJM AS JM,@numJP AS JP, @intTotal AS total_item
		FROM trx_medical_resep A
		JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
		LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
		LEFT JOIN trx_pasien D ON A.pasien_cd=D.pasien_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		--LEFT JOIN trx_tarif_inventori TARIF ON B.item_cd=TARIF.item_cd AND ((TARIF.kelas_cd=@strKelasCd AND TARIF.insurance_cd=@strAsuransiCd) OR (TARIF.kelas_cd='' AND TARIF.insurance_cd='') OR (TARIF.kelas_cd IS NULL AND TARIF.insurance_cd IS NULL))
		LEFT JOIN trx_tarif_inventori TARIF ON B.item_cd=TARIF.item_cd AND ((TARIF.kelas_cd=@strKelasCd AND TARIF.insurance_cd=@strAsuransiCd) OR (ISNULL(TARIF.kelas_cd,'')=@strKelasCdTemp AND ISNULL(TARIF.insurance_cd,'')=@strAsuransiCdTemp) OR (TARIF.kelas_cd IS NULL AND TARIF.insurance_cd IS NULL))
		WHERE A.medical_resep_seqno=@pintMedicalResepNo
		ORDER BY B.resep_seqno
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RESEPRESI_ALL(bigint,bigint,bigint,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RESEPRESI_ALL] 
@pintMedicalResepNo bigint,
@pintMedicalResepNoRoot bigint,
@pintMedicalResepNoChild bigint,
@pstrUserNm Varchar(100)
AS
BEGIN
	DECLARE @numTotalPrice numeric
	DECLARE @strKelasCd varchar(20)
	DECLARE @strAsuransiCd varchar(20)
	DECLARE @strItemCd varchar(20)
	DECLARE @numJumlah numeric
	DECLARE @numAmount numeric
	DECLARE @intTotal int
	
	DECLARE @strTrxMedicalCd Varchar(10)
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalCd Varchar(10)
	DECLARE @intResepSeqno bigint
	DECLARE @strResepTp varchar(20)
	
	DECLARE @strKelasCdTemp varchar(20)
	DECLARE @strAsuransiCdTemp varchar(20)
		
	DECLARE @intTariftpNo bigint
	DECLARE @numJS numeric
	DECLARE @numJM numeric
	DECLARE @numJP numeric
	DECLARE @numJSTemp numeric
	DECLARE @numJMTemp numeric
	DECLARE @numJPTemp numeric
	DECLARE @intTotalRacik int
	
	SELECT @strTrxMedicalCd=D.medical_cd,@strMedicalRootCd=D.medical_root_cd,
	@strAsuransiCd=C.insurance_cd,@strKelasCd=E.kelas_cd
	FROM trx_medical_resep A
	LEFT JOIN trx_pasien B ON A.pasien_cd=B.pasien_cd
	LEFT JOIN trx_pasien_insurance C ON B.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_ruang E ON D.ruang_cd=E.ruang_cd 
	WHERE A.medical_resep_seqno=@pintMedicalResepNo
	
	IF @strKelasCd IS NULL SET @strKelasCd = ''
	IF @strAsuransiCd IS NULL SET @strAsuransiCd = ''
	
	--get total price
	SET @intTotal = 0
	SET @numTotalPrice = 0
	SET @numJS = 0
	SET @numJM = 0
	SET @numJP = 0
	DECLARE cursorData CURSOR FOR
	SELECT B.item_cd,B.quantity,B.resep_seqno,B.resep_tp,A.medical_cd
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	--WHERE A.medical_resep_seqno=@pintMedicalResepNo
	WHERE A.medical_cd=@strTrxMedicalCd
	ORDER BY B.resep_seqno
		
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strItemCd,@numJumlah,@intResepSeqno,@strResepTp,@strMedicalCd
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--SET @intTotal = @intTotal + 1
		
		SET @numAmount = 0
		
		/*
		SELECT @numAmount=A.tarif
		FROM trx_tarif_inventori A
		WHERE A.item_cd=@strItemCd
		AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd='' AND A.insurance_cd='') 
				OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))
				
		SET @numTotalPrice = @numTotalPrice + (@numAmount * @numJumlah)
		*/
		
		IF @strResepTp='RESEP_TP_1'
		--Resep per item
		BEGIN
			SET @intTotal = @intTotal + 1
			
			IF EXISTS(SELECT * FROM trx_tarif_inventori A
					WHERE A.item_cd=@strItemCd
					--AND (A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd)
					AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				)
			BEGIN		
				SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = @strAsuransiCd
			END	
			ELSE
			BEGIN
				--SET @strKelasCdTemp = ''
				SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = ''
			END	
			
			SELECT @numAmount=A.tarif
			FROM trx_tarif_inventori A
			WHERE A.item_cd=@strItemCd
			AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			
			SET @numTotalPrice = @numTotalPrice + (@numAmount * @numJumlah)
			
			--HARDCODE
			--Biaya JASA FARMASI
			/*
			SET @intTariftpNo = 5000000
			--Jasa Sarana Farmasi
			SELECT @numJSTemp=ISNULL(tarif_item,0)
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
			--Jasa Medis Farmasi
			SELECT @numJMTemp=ISNULL(tarif_item,0)
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
			--Jasa Pelaksana Farmasi
			SELECT @numJPTemp=ISNULL(tarif_item,0)
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
			
			SET @numJS = @numJS + @numJSTemp
			SET @numJM = @numJM + @numJMTemp
			SET @numJP = @numJP + @numJPTemp
			*/
			--End Biaya JASA FARMASI
		END
		ELSE
		--Resep racik
		BEGIN
			/*IF @numJumlah<=30
				SET @intTotal = @intTotal + 1
			ELSE IF @numJumlah>30 AND @numJumlah<=60
				SET @intTotal = @intTotal + 2
			ELSE
				SET @intTotal = @intTotal + 3*/
				
			SET @numTotalPrice = @numTotalPrice + (SELECT simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,@intResepSeqno))
			
			--HARDCODE
			--Biaya JASA FARMASI
			/*
			SET @intTotalRacik = 0
			IF @numJumlah<=30
				SET @intTotalRacik = 1
			ELSE IF @numJumlah>30 AND @numJumlah<=60
				SET @intTotalRacik = 2
			ELSE
				SET @intTotalRacik = 3
				
			SET @intTariftpNo = 5000020
			--Jasa Sarana Farmasi
			SELECT @numJSTemp=ISNULL(tarif_item,0) * @intTotalRacik
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
			--Jasa Medis Farmasi
			SELECT @numJMTemp=ISNULL(tarif_item,0) * @intTotalRacik
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
			--Jasa Pelaksana Farmasi
			SELECT @numJPTemp=ISNULL(tarif_item,0) * @intTotalRacik
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
			
			SET @numJS = @numJS + @numJSTemp
			SET @numJM = @numJM + @numJMTemp
			SET @numJP = @numJP + @numJPTemp
			*/
			--End Biaya JASA FARMASI
		END
		
		FETCH NEXT FROM cursorData
		INTO @strItemCd,@numJumlah,@intResepSeqno,@strResepTp,@strMedicalCd
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	--end total price
	
	/*--Jasa Sarana Farmasi
	SELECT @numJS=ISNULL(tarif_item,0) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
	--Jasa Medis Farmasi
	SELECT @numJM=ISNULL(tarif_item,0) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
	--Jasa Pelaksana Farmasi
	SELECT @numJP=ISNULL(tarif_item,0) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004*/
	
	SET @numTotalPrice = @numTotalPrice + @numJS + @numJM + @numJP
	
	SELECT DISTINCT B.resep_seqno,B.item_cd,A.resep_date,
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN B.data_nm ELSE C.item_nm END AS item_nm,
	B.quantity,simrke.FN_RPT_GET_ITEM_RETUR(A.medical_resep_seqno,B.item_cd) AS retur,
	A.medical_resep_seqno,A.resep_no,
	/*TARIF.tarif AS item_price,*/
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE TARIF.tarif END AS item_price,
	/*B.quantity * TARIF.tarif AS item_total_price,*/
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE B.quantity * TARIF.tarif END AS item_total_price,
	@numTotalPrice AS total_price,
	ISNULL(simrke.FN_GET_TERBILANG(@numTotalPrice),'') AS total_terbilang,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.pasien_nm ELSE A.pasien_nm END AS pasien_nm,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.no_rm ELSE '' END AS no_rm,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.address ELSE A.info_01 END AS address,DR.dr_nm,
	@numJS AS JS,@numJM AS JM,@numJP AS JP, @intTotal AS total_item
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN trx_pasien D ON A.pasien_cd=D.pasien_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_tarif_inventori TARIF ON B.item_cd=TARIF.item_cd AND ((TARIF.kelas_cd=@strKelasCd AND TARIF.insurance_cd=@strAsuransiCd) OR (ISNULL(TARIF.kelas_cd,'')=@strKelasCdTemp AND ISNULL(TARIF.insurance_cd,'')=@strAsuransiCdTemp) OR (TARIF.kelas_cd IS NULL AND TARIF.insurance_cd IS NULL))
	--WHERE A.medical_resep_seqno=@pintMedicalResepNo
	WHERE A.medical_cd=@strTrxMedicalCd
	ORDER BY B.resep_seqno
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RESEPRESI_CHILD(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RESEPRESI_CHILD] 
@pintMedicalResepNoChild bigint
AS
BEGIN
	DECLARE @numTotalPrice numeric
	DECLARE @strKelasCd varchar(20)
	DECLARE @strAsuransiCd varchar(20)
	DECLARE @strItemCd varchar(20)
	DECLARE @numJumlah numeric
	DECLARE @numAmount numeric
	DECLARE @intTotal int
	
	DECLARE @strTrxMedicalCd Varchar(10)
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalCd Varchar(10)
	DECLARE @intResepSeqno bigint
	DECLARE @strResepTp varchar(20)
	
	DECLARE @strKelasCdTemp varchar(20)
	DECLARE @strAsuransiCdTemp varchar(20)
	
	DECLARE @numJS numeric
	DECLARE @numJM numeric
	DECLARE @numJP numeric
	
	SELECT @strTrxMedicalCd=D.medical_cd,@strMedicalRootCd=D.medical_root_cd,
	@strAsuransiCd=C.insurance_cd,@strKelasCd=E.kelas_cd
	FROM trx_medical_resep A
	LEFT JOIN trx_pasien B ON A.pasien_cd=B.pasien_cd
	LEFT JOIN trx_pasien_insurance C ON B.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_ruang E ON D.ruang_cd=E.ruang_cd 
	WHERE A.medical_resep_seqno=@pintMedicalResepNoChild
	
	IF @strKelasCd IS NULL SET @strKelasCd = ''
	IF @strAsuransiCd IS NULL SET @strAsuransiCd = ''
	
	--get total price
	SET @intTotal = 0
	SET @numTotalPrice = 0
	DECLARE cursorData CURSOR FOR
	SELECT B.item_cd,B.quantity,B.resep_seqno,B.resep_tp,A.medical_cd
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	--WHERE A.medical_resep_seqno=@pintMedicalResepNoChild
	WHERE A.medical_cd=@strTrxMedicalCd
	ORDER BY B.resep_seqno
		
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strItemCd,@numJumlah,@intResepSeqno,@strResepTp,@strMedicalCd
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--SET @intTotal = @intTotal + 1
		
		SET @numAmount = 0
		
		/*
		SELECT @numAmount=A.tarif
		FROM trx_tarif_inventori A
		WHERE A.item_cd=@strItemCd
		AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd='' AND A.insurance_cd='') 
				OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))
				
		SET @numTotalPrice = @numTotalPrice + (@numAmount * @numJumlah)
		*/
		
		IF @strResepTp='RESEP_TP_1'
		--Resep per item
		BEGIN
			SET @intTotal = @intTotal + 1
			
			IF EXISTS(SELECT * FROM trx_tarif_inventori A
					WHERE A.item_cd=@strItemCd
					AND (A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd)
				)
			BEGIN		
				SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = @strAsuransiCd
			END	
			ELSE
			BEGIN
				--SET @strKelasCdTemp = ''
				SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = ''
			END	
			
			SELECT @numAmount=A.tarif
			FROM trx_tarif_inventori A
			WHERE A.item_cd=@strItemCd
			AND (A.kelas_cd=@strKelasCdTemp AND A.insurance_cd=@strAsuransiCdTemp)
			
			SET @numTotalPrice = @numTotalPrice + (@numAmount * @numJumlah)
		END
		ELSE
		--Resep racik
		BEGIN
			/*IF @numJumlah<=30
				SET @intTotal = @intTotal + 1
			ELSE IF @numJumlah>30 AND @numJumlah<=60
				SET @intTotal = @intTotal + 2
			ELSE
				SET @intTotal = @intTotal + 3*/
				
			SET @numTotalPrice = @numTotalPrice + (SELECT simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,@intResepSeqno))
		END
		
		FETCH NEXT FROM cursorData
		INTO @strItemCd,@numJumlah,@intResepSeqno,@strResepTp,@strMedicalCd
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	--end total price
	
	/*
	--Jasa Sarana Farmasi
	SELECT @numJS=simrke.FN_CHECK_NULL(tarif_item) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
	--Jasa Medis Farmasi
	SELECT @numJM=simrke.FN_CHECK_NULL(tarif_item) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
	--Jasa Pelaksana Farmasi
	SELECT @numJP=simrke.FN_CHECK_NULL(tarif_item) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
	*/
	
	SET @numTotalPrice = @numTotalPrice + @numJS + @numJM + @numJP
	
	SELECT DISTINCT B.resep_seqno,B.item_cd,A.resep_date,
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN B.data_nm ELSE C.item_nm END AS item_nm,
	B.quantity,simrke.FN_RPT_GET_ITEM_RETUR(A.medical_resep_seqno,B.item_cd) AS retur,
	A.medical_resep_seqno,A.resep_no,
	/*TARIF.tarif AS item_price,*/
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE TARIF.tarif END AS item_price,
	/*B.quantity * TARIF.tarif AS item_total_price,*/
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE B.quantity * TARIF.tarif END AS item_total_price,
	@numTotalPrice AS total_price,
	simrke.FN_CHECK_NULL_STRING(simrke.FN_GET_TERBILANG(@numTotalPrice)) AS total_terbilang,
	CASE WHEN simrke.FN_CHECK_NULL_STRING(A.pasien_cd)<>'' THEN D.pasien_nm ELSE A.pasien_nm END AS pasien_nm,
	CASE WHEN simrke.FN_CHECK_NULL_STRING(A.pasien_cd)<>'' THEN D.no_rm ELSE '' END AS no_rm,
	CASE WHEN simrke.FN_CHECK_NULL_STRING(A.pasien_cd)<>'' THEN D.address ELSE A.info_01 END AS address,DR.dr_nm,
	@numJS AS JS,@numJM AS JM,@numJP AS JP, @intTotal AS total_item
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN trx_pasien D ON A.pasien_cd=D.pasien_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_tarif_inventori TARIF ON B.item_cd=TARIF.item_cd AND ((TARIF.kelas_cd=@strKelasCd AND TARIF.insurance_cd=@strAsuransiCd) OR (TARIF.kelas_cd='' AND TARIF.insurance_cd='') OR (TARIF.kelas_cd IS NULL AND TARIF.insurance_cd IS NULL))
	--WHERE A.medical_resep_seqno=@pintMedicalResepNoChild
	WHERE A.medical_cd=@strTrxMedicalCd
	ORDER BY B.resep_seqno
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RESEPRESI_DATE(bigint,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RESEPRESI_DATE]
@pintMedicalResepNo bigint,
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrUserNm Varchar(100)
AS
BEGIN
	DECLARE @numTotalPrice numeric
	DECLARE @strKelasCd varchar(20)
	DECLARE @strAsuransiCd varchar(20)
	DECLARE @strItemCd varchar(20)
	DECLARE @numJumlah numeric
	DECLARE @numAmount numeric
	DECLARE @intTotal int
	
	DECLARE @strTrxMedicalCd Varchar(10)
	DECLARE @strMedicalCd Varchar(10)
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalRootRootCd varchar(10)
	DECLARE @strMedicalChildCd varchar(10)
	DECLARE @strDrNm Varchar(100)
	
	DECLARE @intResepSeqno bigint
	DECLARE @strResepTp varchar(20)
	
	DECLARE @strKelasCdTemp varchar(20)
	DECLARE @strAsuransiCdTemp varchar(20)
	
	DECLARE @intTariftpNo bigint
	DECLARE @numJS numeric
	DECLARE @numJM numeric
	DECLARE @numJP numeric
	DECLARE @numJSTemp numeric
	DECLARE @numJMTemp numeric
	DECLARE @numJPTemp numeric
	DECLARE @intTotalRacik int
	
	SELECT @strTrxMedicalCd=D.medical_cd,@strMedicalRootCd=D.medical_root_cd,
	@strAsuransiCd=C.insurance_cd,@strKelasCd=E.kelas_cd,@strDrNm=DR.dr_nm
	FROM trx_medical_resep A
	LEFT JOIN trx_pasien B ON A.pasien_cd=B.pasien_cd
	LEFT JOIN trx_pasien_insurance C ON B.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_ruang E ON D.ruang_cd=E.ruang_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.medical_resep_seqno=@pintMedicalResepNo
	
	/*SET @strMedicalRootRootCd = (SELECT A.medical_root_cd
								FROM trx_medical A 
								WHERE A.medical_cd=@strMedicalRootCd)
	
	SET @strMedicalChildCd = (SELECT TOP 1 medical_cd FROM trx_medical WHERE medical_root_cd=@strMedicalCd)*/
	
	IF @strKelasCd IS NULL SET @strKelasCd = ''
	IF @strAsuransiCd IS NULL SET @strAsuransiCd = ''
	
	--get total price
	SET @intTotal = 0
	SET @numTotalPrice = 0
	SET @numJS = 0
	SET @numJM = 0
	SET @numJP = 0
	DECLARE cursorData CURSOR FOR
	SELECT B.item_cd,B.quantity,B.resep_seqno,B.resep_tp,A.medical_cd
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	WHERE A.medical_cd=@strTrxMedicalCd
	AND A.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	UNION ALL
	SELECT B.item_cd,B.quantity,B.resep_seqno,B.resep_tp,A.medical_cd
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	WHERE A.medical_cd=@strMedicalRootCd
	AND A.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	ORDER BY resep_seqno
		
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strItemCd,@numJumlah,@intResepSeqno,@strResepTp,@strMedicalCd
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--SET @intTotal = @intTotal + 1
		
		SET @numAmount = 0
		
		/*
		SELECT @numAmount=A.tarif
		FROM trx_tarif_inventori A
		WHERE A.item_cd=@strItemCd
		AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd='' AND A.insurance_cd='') 
				OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))
				
		SET @numTotalPrice = @numTotalPrice + (@numAmount * @numJumlah)
		*/
		
		IF @strResepTp='RESEP_TP_1'
		--Resep per item
		BEGIN
			SET @intTotal = @intTotal + 1
			
			IF EXISTS(SELECT * FROM trx_tarif_inventori A
					WHERE A.item_cd=@strItemCd
					--AND (A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd)
					AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				)
			BEGIN		
				SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = @strAsuransiCd
			END	
			ELSE
			BEGIN
				--SET @strKelasCdTemp = ''
				SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = ''
			END	
			
			SELECT @numAmount=A.tarif
			FROM trx_tarif_inventori A
			WHERE A.item_cd=@strItemCd
			--AND (A.kelas_cd=@strKelasCdTemp AND A.insurance_cd=@strAsuransiCdTemp)
			AND (ISNULL(A.kelas_cd,'')=@strKelasCdTemp AND ISNULL(A.insurance_cd,'')=@strAsuransiCdTemp)
			
			SET @numTotalPrice = @numTotalPrice + (@numAmount * @numJumlah)
			
			--HARDCODE
			--Biaya JASA FARMASI
			/*
			SET @intTariftpNo = 5000000
			--Jasa Sarana Farmasi
			SELECT @numJSTemp=ISNULL(tarif_item,0)
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
			--Jasa Medis Farmasi
			SELECT @numJMTemp=ISNULL(tarif_item,0)
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
			--Jasa Pelaksana Farmasi
			SELECT @numJPTemp=ISNULL(tarif_item,0)
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
			
			SET @numJS = @numJS + @numJSTemp
			SET @numJM = @numJM + @numJMTemp
			SET @numJP = @numJP + @numJPTemp
			*/
			--End Biaya JASA FARMASI
		END
		ELSE
		--Resep racik
		BEGIN
			/*IF @numJumlah<=30
				SET @intTotal = @intTotal + 1
			ELSE IF @numJumlah>30 AND @numJumlah<=60
				SET @intTotal = @intTotal + 2
			ELSE
				SET @intTotal = @intTotal + 3*/
			
			SET @numTotalPrice = @numTotalPrice + (SELECT simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,@intResepSeqno))
			
			--HARDCODE
			--Biaya JASA FARMASI
			/*
			SET @intTotalRacik = 0
			IF @numJumlah<=30
				SET @intTotalRacik = 1
			ELSE IF @numJumlah>30 AND @numJumlah<=60
				SET @intTotalRacik = 2
			ELSE
				SET @intTotalRacik = 3
				
			SET @intTariftpNo = 5000020
			--Jasa Sarana Farmasi
			SELECT @numJSTemp=ISNULL(tarif_item,0) * @intTotalRacik
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
			--Jasa Medis Farmasi
			SELECT @numJMTemp=ISNULL(tarif_item,0) * @intTotalRacik
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
			--Jasa Pelaksana Farmasi
			SELECT @numJPTemp=ISNULL(tarif_item,0) * @intTotalRacik
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
			
			SET @numJS = @numJS + @numJSTemp
			SET @numJM = @numJM + @numJMTemp
			SET @numJP = @numJP + @numJPTemp
			*/
			--End Biaya JASA FARMASI
		END
		
		FETCH NEXT FROM cursorData
		INTO @strItemCd,@numJumlah,@intResepSeqno,@strResepTp,@strMedicalCd
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	--end total price
	
	/*--Jasa Sarana Farmasi
	SELECT @numJS=ISNULL(tarif_item,0) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
	--Jasa Medis Farmasi
	SELECT @numJM=ISNULL(tarif_item,0) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
	--Jasa Pelaksana Farmasi
	SELECT @numJP=ISNULL(tarif_item,0) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004*/
	
	SET @numTotalPrice = @numTotalPrice + @numJS + @numJM + @numJP
	
	SELECT DISTINCT B.resep_seqno,B.item_cd,A.resep_date,
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN B.data_nm ELSE C.item_nm END AS item_nm,
	B.quantity,simrke.FN_RPT_GET_ITEM_RETUR(A.medical_resep_seqno,B.item_cd) AS retur,
	A.medical_resep_seqno,A.resep_no,
	/*TARIF.tarif AS item_price,*/
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE TARIF.tarif END AS item_price,
	/*B.quantity * TARIF.tarif AS item_total_price,*/
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE B.quantity * TARIF.tarif END AS item_total_price,
	@numTotalPrice AS total_price,
	ISNULL(simrke.FN_GET_TERBILANG(@numTotalPrice),'') AS total_terbilang,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.pasien_nm ELSE A.pasien_nm END AS pasien_nm,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.no_rm ELSE '' END AS no_rm,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.address ELSE A.info_01 END AS address,@strDrNm AS dr_nm,
	@numJS AS JS,@numJM AS JM,@numJP AS JP, @intTotal AS total_item
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN trx_pasien D ON A.pasien_cd=D.pasien_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	--LEFT JOIN trx_tarif_inventori TARIF ON B.item_cd=TARIF.item_cd AND ((TARIF.kelas_cd=@strKelasCd AND TARIF.insurance_cd=@strAsuransiCd) OR (TARIF.kelas_cd='' AND TARIF.insurance_cd='') OR (TARIF.kelas_cd IS NULL AND TARIF.insurance_cd IS NULL))
	LEFT JOIN trx_tarif_inventori TARIF ON B.item_cd=TARIF.item_cd AND ((TARIF.kelas_cd=@strKelasCd AND TARIF.insurance_cd=@strAsuransiCd) OR (ISNULL(TARIF.kelas_cd,'')=@strKelasCdTemp AND ISNULL(TARIF.insurance_cd,'')=@strAsuransiCdTemp) OR (TARIF.kelas_cd IS NULL AND TARIF.insurance_cd IS NULL))
	--WHERE A.medical_resep_seqno=@pintMedicalResepNo
	WHERE A.medical_cd=@strTrxMedicalCd
	AND A.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	UNION
	SELECT DISTINCT B.resep_seqno,B.item_cd,A.resep_date,
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN B.data_nm ELSE C.item_nm END AS item_nm,
	B.quantity,simrke.FN_RPT_GET_ITEM_RETUR(A.medical_resep_seqno,B.item_cd) AS retur,
	A.medical_resep_seqno,A.resep_no,
	/*TARIF.tarif AS item_price,*/
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE TARIF.tarif END AS item_price,
	/*B.quantity * TARIF.tarif AS item_total_price,*/
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE B.quantity * TARIF.tarif END AS item_total_price,
	@numTotalPrice AS total_price,
	ISNULL(simrke.FN_GET_TERBILANG(@numTotalPrice),'') AS total_terbilang,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.pasien_nm ELSE A.pasien_nm END AS pasien_nm,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.no_rm ELSE '' END AS no_rm,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.address ELSE A.info_01 END AS address,@strDrNm AS dr_nm,
	@numJS AS JS,@numJM AS JM,@numJP AS JP, @intTotal AS total_item
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN trx_pasien D ON A.pasien_cd=D.pasien_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	--LEFT JOIN trx_tarif_inventori TARIF ON B.item_cd=TARIF.item_cd AND ((TARIF.kelas_cd=@strKelasCd AND TARIF.insurance_cd=@strAsuransiCd) OR (TARIF.kelas_cd='' AND TARIF.insurance_cd='') OR (TARIF.kelas_cd IS NULL AND TARIF.insurance_cd IS NULL))
	LEFT JOIN trx_tarif_inventori TARIF ON B.item_cd=TARIF.item_cd AND ((TARIF.kelas_cd=@strKelasCd AND TARIF.insurance_cd=@strAsuransiCd) OR (ISNULL(TARIF.kelas_cd,'')=@strKelasCdTemp AND ISNULL(TARIF.insurance_cd,'')=@strAsuransiCdTemp) OR (TARIF.kelas_cd IS NULL AND TARIF.insurance_cd IS NULL))
	WHERE A.medical_cd=@strMedicalRootCd
	AND A.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	ORDER BY resep_seqno
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RESEPRESI_ROOT(bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RESEPRESI_ROOT] 
@pintMedicalResepNoRoot bigint
AS
BEGIN
	DECLARE @numTotalPrice numeric
	DECLARE @strKelasCd varchar(20)
	DECLARE @strAsuransiCd varchar(20)
	DECLARE @strItemCd varchar(20)
	DECLARE @numJumlah numeric
	DECLARE @numAmount numeric
	DECLARE @intTotal int
	
	DECLARE @strTrxMedicalCd Varchar(10)
	DECLARE @strMedicalRootCd varchar(10)
	DECLARE @strMedicalCd Varchar(10)
	DECLARE @intResepSeqno bigint
	DECLARE @strResepTp varchar(20)
	
	DECLARE @strKelasCdTemp varchar(20)
	DECLARE @strAsuransiCdTemp varchar(20)
	
	DECLARE @intTariftpNo bigint
	DECLARE @numJS numeric
	DECLARE @numJM numeric
	DECLARE @numJP numeric
	DECLARE @numJSTemp numeric
	DECLARE @numJMTemp numeric
	DECLARE @numJPTemp numeric
	DECLARE @intTotalRacik int
	
	SELECT @strTrxMedicalCd=D.medical_cd,@strMedicalRootCd=D.medical_root_cd,
	@strAsuransiCd=C.insurance_cd,@strKelasCd=E.kelas_cd
	FROM trx_medical_resep A
	LEFT JOIN trx_pasien B ON A.pasien_cd=B.pasien_cd
	LEFT JOIN trx_pasien_insurance C ON B.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_ruang E ON D.ruang_cd=E.ruang_cd 
	WHERE A.medical_resep_seqno=@pintMedicalResepNoRoot
	
	IF @strKelasCd IS NULL SET @strKelasCd = ''
	IF @strAsuransiCd IS NULL SET @strAsuransiCd = ''
	
	--get total price
	SET @intTotal = 0
	SET @numTotalPrice = 0
	SET @numJS = 0
	SET @numJM = 0
	SET @numJP = 0
	DECLARE cursorData CURSOR FOR
	SELECT B.item_cd,B.quantity,B.resep_seqno,B.resep_tp,A.medical_cd
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	--WHERE A.medical_resep_seqno=@pintMedicalResepNoRoot
	WHERE A.medical_cd=@strTrxMedicalCd
	ORDER BY B.resep_seqno
		
	OPEN cursorData
	FETCH NEXT FROM cursorData
	INTO @strItemCd,@numJumlah,@intResepSeqno,@strResepTp,@strMedicalCd
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--SET @intTotal = @intTotal + 1
		
		SET @numAmount = 0
		
		/*
		SELECT @numAmount=A.tarif
		FROM trx_tarif_inventori A
		WHERE A.item_cd=@strItemCd
		AND ((A.kelas_cd=@strKelasCd AND A.insurance_cd=@strAsuransiCd) OR (A.kelas_cd='' AND A.insurance_cd='') 
				OR (A.kelas_cd IS NULL AND A.insurance_cd IS NULL))
				
		SET @numTotalPrice = @numTotalPrice + (@numAmount * @numJumlah)
		*/
		
		IF @strResepTp='RESEP_TP_1'
		--Resep per item
		BEGIN
			SET @intTotal = @intTotal + 1
			
			IF EXISTS(SELECT * FROM trx_tarif_inventori A
					WHERE A.item_cd=@strItemCd
					AND (ISNULL(A.kelas_cd,'')=@strKelasCd AND ISNULL(A.insurance_cd,'')=@strAsuransiCd)
				)
			BEGIN		
				SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = @strAsuransiCd
			END	
			ELSE
			BEGIN
				--SET @strKelasCdTemp = ''
				SET @strKelasCdTemp = @strKelasCd
				SET @strAsuransiCdTemp = ''
			END	
			
			SELECT @numAmount=A.tarif
			FROM trx_tarif_inventori A
			WHERE A.item_cd=@strItemCd
			AND (A.kelas_cd=@strKelasCdTemp AND A.insurance_cd=@strAsuransiCdTemp)
			
			SET @numTotalPrice = @numTotalPrice + (@numAmount * @numJumlah)
			
			/*--Biaya JASA FARMASI
			SET @intTariftpNo = 5000000
			--Jasa Sarana Farmasi
			SELECT @numJSTemp=simrke.FN_CHECK_NULL(tarif_item)
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
			--Jasa Medis Farmasi
			SELECT @numJMTemp=simrke.FN_CHECK_NULL(tarif_item)
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
			--Jasa Pelaksana Farmasi
			SELECT @numJPTemp=simrke.FN_CHECK_NULL(tarif_item)
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
			
			SET @numJS = @numJS + @numJSTemp
			SET @numJM = @numJM + @numJMTemp
			SET @numJP = @numJP + @numJPTemp
			--End Biaya JASA FARMASI*/
		END
		ELSE
		--Resep racik
		BEGIN
			/*IF @numJumlah<=30
				SET @intTotal = @intTotal + 1
			ELSE IF @numJumlah>30 AND @numJumlah<=60
				SET @intTotal = @intTotal + 2
			ELSE
				SET @intTotal = @intTotal + 3*/
				
			SET @numTotalPrice = @numTotalPrice + (SELECT simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,@intResepSeqno))
			
			/*
			--Biaya JASA FARMASI
			SET @intTotalRacik = 0
			IF @numJumlah<=30
				SET @intTotalRacik = 1
			ELSE IF @numJumlah>30 AND @numJumlah<=60
				SET @intTotalRacik = 2
			ELSE
				SET @intTotalRacik = 3
				
			SET @intTariftpNo = 5000020
			--Jasa Sarana Farmasi
			SELECT @numJSTemp=simrke.FN_CHECK_NULL(tarif_item) * @intTotalRacik
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
			--Jasa Medis Farmasi
			SELECT @numJMTemp=simrke.FN_CHECK_NULL(tarif_item) * @intTotalRacik
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
			--Jasa Pelaksana Farmasi
			SELECT @numJPTemp=simrke.FN_CHECK_NULL(tarif_item) * @intTotalRacik
			FROM trx_tariftp_item
			WHERE tariftp_no=@intTariftpNo AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004
			
			SET @numJS = @numJS + @numJSTemp
			SET @numJM = @numJM + @numJMTemp
			SET @numJP = @numJP + @numJPTemp
			--End Biaya JASA FARMASI
			*/
		END
		
		FETCH NEXT FROM cursorData
		INTO @strItemCd,@numJumlah,@intResepSeqno,@strResepTp,@strMedicalCd
	END
	CLOSE cursorData
	DEALLOCATE cursorData
	--end total price
	
	/*--Jasa Sarana Farmasi
	SELECT @numJS=simrke.FN_CHECK_NULL(tarif_item) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_00' AND trx_tarif_seqno=90001
	--Jasa Medis Farmasi
	SELECT @numJM=simrke.FN_CHECK_NULL(tarif_item) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10010
	--Jasa Pelaksana Farmasi
	SELECT @numJP=simrke.FN_CHECK_NULL(tarif_item) * @intTotal
	FROM trx_tariftp_item
	WHERE tariftp_no=5000000 AND tarif_tp='TARIF_TP_01' AND trx_tarif_seqno=10004*/
	
	SET @numTotalPrice = @numTotalPrice + @numJS + @numJM + @numJP
	
	SELECT DISTINCT B.resep_seqno,B.item_cd,A.resep_date,
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN B.data_nm ELSE C.item_nm END AS item_nm,
	B.quantity,simrke.FN_RPT_GET_ITEM_RETUR(A.medical_resep_seqno,B.item_cd) AS retur,
	A.medical_resep_seqno,A.resep_no,
	/*TARIF.tarif AS item_price,*/
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE TARIF.tarif END AS item_price,
	/*B.quantity * TARIF.tarif AS item_total_price,*/
	CASE WHEN B.resep_tp='RESEP_TP_2' THEN simrke.FN_GET_ITEM_PRICE_RACIK(@strMedicalCd,B.resep_seqno) ELSE B.quantity * TARIF.tarif END AS item_total_price,
	@numTotalPrice AS total_price,
	simrke.FN_CHECK_NULL_STRING(simrke.FN_GET_TERBILANG(@numTotalPrice)) AS total_terbilang,
	CASE WHEN simrke.FN_CHECK_NULL_STRING(A.pasien_cd)<>'' THEN D.pasien_nm ELSE A.pasien_nm END AS pasien_nm,
	CASE WHEN simrke.FN_CHECK_NULL_STRING(A.pasien_cd)<>'' THEN D.no_rm ELSE '' END AS no_rm,
	CASE WHEN simrke.FN_CHECK_NULL_STRING(A.pasien_cd)<>'' THEN D.address ELSE A.info_01 END AS address,DR.dr_nm,
	@numJS AS JS,@numJM AS JM,@numJP AS JP, @intTotal AS total_item
	FROM trx_medical_resep A
	JOIN trx_resep_data B ON A.medical_resep_seqno=B.medical_resep_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN trx_pasien D ON A.pasien_cd=D.pasien_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_tarif_inventori TARIF ON B.item_cd=TARIF.item_cd AND ((TARIF.kelas_cd=@strKelasCd AND TARIF.insurance_cd=@strAsuransiCd) OR (TARIF.kelas_cd='' AND TARIF.insurance_cd='') OR (TARIF.kelas_cd IS NULL AND TARIF.insurance_cd IS NULL))
	--WHERE A.medical_resep_seqno=@pintMedicalResepNoRoot
	WHERE A.medical_cd=@strTrxMedicalCd
	ORDER BY B.resep_seqno
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RETUROBAT_BYDATE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RETUROBAT_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT DISTINCT RETUR.retur_date,PAS.pasien_nm,PAS.no_rm,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
	simrke.FN_FORMATDATE(RETUR.retur_date) AS date_trx,
	INV.item_nm,A.quantity,U.unit_nm,
	TARIF.tarif AS harga,A.quantity * TARIF.tarif AS total
	FROM trx_retur_data A  
	JOIN trx_resep_retur RETUR ON A.retur_seqno=RETUR.retur_seqno 
	JOIN inv_item_master INV ON A.item_cd=INV.item_cd
	LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
	JOIN trx_pasien PAS ON RETUR.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	JOIN trx_medical MEDICAL ON RETUR.medical_cd=MEDICAL.medical_cd
	LEFT JOIN trx_unit_medis RJ ON MEDICAL.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON MEDICAL.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_tarif_inventori TARIF ON A.item_cd=TARIF.item_cd AND ((TARIF.kelas_cd=KMR.kelas_cd AND TARIF.insurance_cd=PASINS.insurance_cd) OR (TARIF.kelas_cd='' AND TARIF.insurance_cd='') OR (TARIF.kelas_cd IS NULL AND TARIF.insurance_cd IS NULL))
	WHERE RETUR.retur_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RETUR.retur_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	ORDER BY RETUR.retur_date DESC,INV.item_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RETURRESI(bigint,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RETURRESI] 
@pintReturNo bigint,
@pstrUserNm Varchar(100)
AS
BEGIN
	DECLARE @numTotalPrice numeric
	DECLARE @strKelasCd varchar(20)
	DECLARE @strAsuransiCd varchar(20)
	
	DECLARE @strMedicalCd Varchar(10)
	
	SELECT @strAsuransiCd=C.insurance_cd,@strKelasCd=E.kelas_cd
	FROM trx_resep_retur A
	LEFT JOIN trx_pasien B ON A.pasien_cd=B.pasien_cd
	LEFT JOIN trx_pasien_insurance C ON B.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN trx_ruang E ON D.ruang_cd=E.ruang_cd 
	WHERE A.retur_seqno=@pintReturNo
	
	IF @strKelasCd IS NULL SET @strKelasCd = ''
	IF @strAsuransiCd IS NULL SET @strAsuransiCd = ''
	
	SELECT DISTINCT B.retur_seqno,B.item_cd,A.retur_date,
	C.item_nm,B.quantity,
	TARIF.tarif AS item_price,B.quantity * TARIF.tarif AS item_total_price,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.pasien_nm ELSE A.pasien_nm END AS pasien_nm,
	CASE WHEN ISNULL(A.pasien_cd,'')<>'' THEN D.no_rm ELSE '' END AS no_rm
	FROM trx_resep_retur A
	JOIN trx_retur_data B ON A.retur_seqno=B.retur_seqno
	LEFT JOIN inv_item_master C ON B.item_cd=C.item_cd
	LEFT JOIN trx_pasien D ON A.pasien_cd=D.pasien_cd
	LEFT JOIN trx_tarif_inventori TARIF ON B.item_cd=TARIF.item_cd AND ((TARIF.kelas_cd=@strKelasCd AND TARIF.insurance_cd=@strAsuransiCd) OR (TARIF.kelas_cd='' AND TARIF.insurance_cd='') OR (TARIF.kelas_cd IS NULL AND TARIF.insurance_cd IS NULL))
	WHERE A.retur_seqno=@pintReturNo
	ORDER BY B.item_cd
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RMOUTTP_BYDATE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RMOUTTP_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	simrke.FN_FORMATDATE(A.datetime_out) AS datetime_out,
	PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
	CASE WHEN ISNULL(A.out_tp,'')='' THEN 'OUT_TP_01' ELSE A.out_tp END AS out_tp,
	CASE WHEN ISNULL(A.out_tp,'')='' THEN 'Sembuh' ELSE OUTTP.code_nm END AS outtp_nm
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	LEFT JOIN com_code OUTTP ON A.out_tp=OUTTP.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	WHERE A.medical_trx_st='MEDICAL_TRX_ST_1'
	AND A.medical_tp='MEDICAL_TP_02'
	AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	ORDER BY A.medical_tp,A.out_tp,A.datetime_out
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RMTRX_BYDATE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RMTRX_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	--WHERE simrke.FN_FORMATDATE(A.datetime_in)>=@pdtDateStart AND simrke.FN_FORMATDATE(A.datetime_in)<=@pdtDateEnd
	WHERE A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	ORDER BY A.medical_tp,A.datetime_in
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RMTRX_BYDATE_BYPARAM(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RMTRX_BYDATE_BYPARAM]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrParam Varchar(10)
AS
BEGIN
	IF UPPER(@pstrParam) = '' 
	/*--Semua--*/
		SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY A.medical_tp,A.datetime_in
	ELSE IF UPPER(@pstrParam) = 'RJ' 
	/*--Rawat Jalan--*/
		SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND RJ.medunit_nm IS NOT NULL
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY RJ.medunit_nm,A.datetime_in
	ELSE IF UPPER(@pstrParam) = 'RI' 
	/*--Rawat Inap--*/
		SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND KLS.kelas_nm IS NOT NULL
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY KLS.kelas_nm,A.datetime_in
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RMTRX_BYDATE_ORDERASURANSI(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RMTRX_BYDATE_ORDERASURANSI]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	WHERE INS.insurance_nm IS NOT NULL
	AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	ORDER BY INS.insurance_nm,A.medical_tp,A.datetime_in
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RMTRX_BYDATE_ORDERDR(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RMTRX_BYDATE_ORDERDR]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrDrCd Varchar(20)
AS
BEGIN
	IF @pstrDrCd = '' 
	/*--Semua--*/
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		--simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,MTK.datetime_trx,TK.treatment_nm
		FROM trx_medical A 
		LEFT JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		LEFT JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND DR.dr_nm IS NOT NULL
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,MTK.datetime_trx,TK.treatment_nm
		FROM trx_medical A
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND DR.dr_nm IS NOT NULL
		AND MTK.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MTK.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY DR.dr_nm,A.medical_tp,MTK.datetime_trx,A.datetime_in
	ELSE
	/*--Per dokter--*/
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,MTK.datetime_trx,TK.treatment_nm
		FROM trx_medical A 
		LEFT JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		LEFT JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND DR.dr_nm IS NOT NULL
		AND DR.dr_cd=@pstrDrCd
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,MTK.datetime_trx,TK.treatment_nm
		FROM trx_medical A
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND DR.dr_nm IS NOT NULL
		AND DR.dr_cd=@pstrDrCd
		AND MTK.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MTK.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY DR.dr_nm,A.medical_tp,MTK.datetime_trx,A.datetime_in
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RMTRX_REGISTER_BYDATE(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RMTRX_REGISTER_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrParam Varchar(10)
AS
BEGIN
	IF UPPER(@pstrParam) = '' 
	/*--Semua--*/
		SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,'' AS medunit_send
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND ISNULL(RJ.medunit_nm,'') <> ''
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION
		SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(MUNIT.datetime_trx) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,UM.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,RJ.medunit_nm AS medunit_send
		FROM trx_medical_unit MUNIT
		JOIN trx_unitmedis_item UMITEM ON MUNIT.medicalunit_cd=UMITEM.medicalunit_cd
		JOIN trx_unit_medis UM ON UMITEM.medunit_cd=UM.medunit_cd
		JOIN trx_medical A ON MUNIT.medical_cd=A.medical_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON MUNIT.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE MUNIT.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MUNIT.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY medunit_nm,trx_datetime
	ELSE
	/*--By Unit--*/
		SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,'' AS medunit_send
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND ISNULL(RJ.medunit_nm,'') <> ''
		AND RJ.medunit_cd=@pstrParam
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION
		SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(MUNIT.datetime_trx) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,UM.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,RJ.medunit_nm AS medunit_send
		FROM trx_medical_unit MUNIT
		JOIN trx_unitmedis_item UMITEM ON MUNIT.medicalunit_cd=UMITEM.medicalunit_cd
		JOIN trx_unit_medis UM ON UMITEM.medunit_cd=UM.medunit_cd
		JOIN trx_medical A ON MUNIT.medical_cd=A.medical_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON MUNIT.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE UM.medunit_cd=@pstrParam
		AND MUNIT.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MUNIT.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY medunit_nm,trx_datetime
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RMTRX_SENSUS_BYDATE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RMTRX_SENSUS_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,A.datetime_in,
	PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,simrke.FN_RPT_GET_AGE(PAS.birth_date) AS age,GENDER.code_value AS gender_cd,
	CASE WHEN simrke.FN_FORMATDATE(PAS.register_date)=simrke.FN_FORMATDATE(A.datetime_in) THEN 'Baru' ELSE 'Lama' END AS register_st,
	DR.dr_nm,RJ.medunit_nm AS unit_nm,CONVERT(Varchar(5000),RM.medical_data) AS diagnosa
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_medical_record RM ON A.medical_cd=RM.medical_cd
	LEFT JOIN com_code GENDER ON PAS.gender_tp=GENDER.com_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	WHERE A.medical_tp='MEDICAL_TP_01'
	AND RJ.medunit_nm IS NOT NULL
	AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	UNION
	SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,A.datetime_in,
	PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,simrke.FN_RPT_GET_AGE(PAS.birth_date) AS age,GENDER.code_value AS gender_cd,
	CASE WHEN simrke.FN_FORMATDATE(PAS.register_date)=simrke.FN_FORMATDATE(A.datetime_in) THEN 'Baru' ELSE 'Lama' END AS register_st,
	DR.dr_nm,KLS.kelas_nm AS unit_nm,CONVERT(Varchar(5000),RM.medical_data) AS diagnosa
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_medical_record RM ON A.medical_cd=RM.medical_cd
	LEFT JOIN com_code GENDER ON PAS.gender_tp=GENDER.com_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	WHERE A.medical_tp='MEDICAL_TP_02'
	AND KLS.kelas_nm IS NOT NULL
	AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	ORDER BY A.medical_tp,unit_nm,A.datetime_in
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RMTRX_SENSUS_RINAP_BYDATE_BYUNITCD(Varchar,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RMTRX_SENSUS_RINAP_BYDATE_BYUNITCD]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrBangsalCd Varchar(10),
@pstrBangsalNm Varchar(100)
AS
BEGIN
	IF @pstrBangsalCd = '' 
	/*--Semua--*/
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,
		simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,A.datetime_in,
		simrke.FN_FORMATDATE(A.datetime_out) AS datetime_out,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,simrke.FN_RPT_GET_AGE(PAS.birth_date) AS age,GENDER.code_value AS gender_cd,
		CASE WHEN simrke.FN_FORMATDATE(PAS.register_date)=simrke.FN_FORMATDATE(A.datetime_in) THEN 'Baru' ELSE 'Lama' END AS register_st,
		DR.dr_nm, KLS.kelas_cd AS unit_cd, KLS.kelas_nm AS unit_nm,
		KMR.bangsal_cd,SAL.bangsal_nm,
		CONVERT(Varchar(5000),RM.medical_data) AS diagnosa,
		CASE WHEN ISNULL(A.out_tp,'')='' THEN 'Sembuh' ELSE OUTTP.code_nm END AS outtp_nm
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		LEFT JOIN com_code OUTTP ON A.out_tp=OUTTP.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_medical_record RM ON A.medical_cd=RM.medical_cd
		LEFT JOIN com_code GENDER ON PAS.gender_tp=GENDER.com_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal SAL ON KMR.bangsal_cd=SAL.bangsal_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND A.medical_trx_st='MEDICAL_TRX_ST_1'
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY A.medical_tp,KMR.bangsal_cd,A.datetime_in
	ELSE
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,
		simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,A.datetime_in,
		simrke.FN_FORMATDATE(A.datetime_out) AS datetime_out,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,simrke.FN_RPT_GET_AGE(PAS.birth_date) AS age,GENDER.code_value AS gender_cd,
		CASE WHEN simrke.FN_FORMATDATE(PAS.register_date)=simrke.FN_FORMATDATE(A.datetime_in) THEN 'Baru' ELSE 'Lama' END AS register_st,
		DR.dr_nm, KLS.kelas_cd AS unit_cd, KLS.kelas_nm AS unit_nm,
		KMR.bangsal_cd,SAL.bangsal_nm,
		CONVERT(Varchar(5000),RM.medical_data) AS diagnosa,
		CASE WHEN ISNULL(A.out_tp,'')='' THEN 'Sembuh' ELSE OUTTP.code_nm END AS outtp_nm
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		LEFT JOIN com_code OUTTP ON A.out_tp=OUTTP.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_medical_record RM ON A.medical_cd=RM.medical_cd
		LEFT JOIN com_code GENDER ON PAS.gender_tp=GENDER.com_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal SAL ON KMR.bangsal_cd=SAL.bangsal_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND A.medical_trx_st='MEDICAL_TRX_ST_1'
		AND KMR.bangsal_cd=@pstrBangsalCd
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY A.medical_tp,KMR.bangsal_cd,A.datetime_in
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RMTRX_SENSUS_RJALAN_BYDATE_BYUNITCD(Varchar,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RMTRX_SENSUS_RJALAN_BYDATE_BYUNITCD]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrUnitCd Varchar(10),
@pstrUnitNm Varchar(100)
AS
BEGIN
	IF @pstrUnitCd = '' 
	/*--Semua--*/
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,simrke.FN_RPT_GET_AGE(PAS.birth_date) AS age,GENDER.code_value AS gender_cd,
		CASE WHEN simrke.FN_FORMATDATE(PAS.register_date)=simrke.FN_FORMATDATE(A.datetime_in) THEN 'Baru' ELSE 'Lama' END AS register_st,
		DR.dr_nm, RJ.medunit_cd as unit_cd, RJ.medunit_nm AS unit_nm,
		CONVERT(Varchar(5000),RM.medical_data) AS diagnosa
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_medical_record RM ON A.medical_cd=RM.medical_cd
		LEFT JOIN com_code GENDER ON PAS.gender_tp=GENDER.com_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY A.medical_tp,RJ.medunit_cd,A.datetime_in
	ELSE
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,simrke.FN_RPT_GET_AGE(PAS.birth_date) AS age,GENDER.code_value AS gender_cd,
		CASE WHEN simrke.FN_FORMATDATE(PAS.register_date)=simrke.FN_FORMATDATE(A.datetime_in) THEN 'Baru' ELSE 'Lama' END AS register_st,
		DR.dr_nm, RJ.medunit_cd as unit_cd, RJ.medunit_nm AS unit_nm,
		CONVERT(Varchar(5000),RM.medical_data) AS diagnosa
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_medical_record RM ON A.medical_cd=RM.medical_cd
		LEFT JOIN com_code GENDER ON PAS.gender_tp=GENDER.com_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND RJ.medunit_cd=@pstrUnitCd
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY A.medical_tp,RJ.medunit_cd,A.datetime_in
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_RUJUKAN_BYDATE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_RUJUKAN_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,pas.address,
	REFF.referensi_nm,REFF.address AS reff_address,
	INS.insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm
	FROM trx_medical A
	JOIN trx_referensi REFF ON A.referensi_cd=REFF.referensi_cd
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	JOIN trx_settlement SETT ON A.medical_cd=SETT.medical_cd
	WHERE A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND SETT.payment_st='PAYMENT_ST_1'
	ORDER BY A.medical_tp,A.datetime_in
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_STATISTIK_PENYAKIT(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_STATISTIK_PENYAKIT]
@pintMonth int,
@pintYear int
AS
BEGIN
	/*IF EXISTS(SELECT TOP 1 * FROM trx_icd_group)
		SELECT TOP 10 GRUP.group_nm,COUNT(A.icd_cd) AS total
		FROM trx_medical_record A 
		JOIN trx_icdgroup_item ICD ON A.icd_cd=ICD.icd_cd
		JOIN trx_icd_group GRUP ON ICD.group_cd=GRUP.group_cd
		WHERE (MONTH(A.datetime_record)=@pintMonth AND YEAR(A.datetime_record)=@pintYear)
		GROUP BY GRUP.group_nm
		ORDER BY total DESC
	ELSE*/
		SELECT TOP 10 ICD.icd_cd,ICD.icd_nm,COUNT(A.icd_cd) AS total
		FROM trx_medical_record A 
		JOIN trx_icd ICD ON A.icd_cd=ICD.icd_cd
		WHERE (MONTH(A.datetime_record)=@pintMonth AND YEAR(A.datetime_record)=@pintYear)
		GROUP BY ICD.icd_cd,ICD.icd_nm
		ORDER BY total DESC
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TINDAKAN() CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TINDAKAN]
AS
BEGIN
	SELECT A.treatment_cd, A.treatment_nm, A.treatment_root, B.treatment_cd AS root_nm
	FROM trx_tindakan A
	LEFT JOIN trx_tindakan B On A.treatment_root=B.treatment_cd
	ORDER BY A.treatment_root, A.treatment_cd
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TOPRINAP(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TOPRINAP]
@pintMonth int,
@pintYear int
AS
BEGIN
	SELECT KLS.kelas_nm,COUNT(A.medical_cd) AS total
	FROM trx_medical A 
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd AND ISNULL(KMR.ruang_tp,'')<>'1'
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	WHERE A.medical_tp='MEDICAL_TP_02'
	AND (MONTH(A.datetime_in)=@pintMonth AND YEAR(A.datetime_in)=@pintYear)
	GROUP BY KLS.kelas_nm
	ORDER BY KLS.kelas_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TOPRJALAN(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TOPRJALAN]
@pintMonth int,
@pintYear int
AS
BEGIN
	SELECT RJ.medunit_nm,COUNT(A.medical_cd) AS total
	FROM trx_medical A 
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	WHERE A.medical_tp='MEDICAL_TP_01'
	AND (MONTH(A.datetime_in)=@pintMonth AND YEAR(A.datetime_in)=@pintYear)
	GROUP BY RJ.medunit_nm
	ORDER BY RJ.medunit_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TOPRJALAN_DETAIL(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TOPRJALAN_DETAIL]
@pintMonth int,
@pintYear int
AS
BEGIN

SELECT medunit_cd,medunit_nm,
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('01/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '1',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('02/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '2',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('03/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '3',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('04/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '4',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('05/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '5',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('06/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '6',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('07/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '7',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('08/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '8',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('09/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '9',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('10/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '10',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('11/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '11',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('12/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '12',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('13/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '13',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('14/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '14',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('15/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '15',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('16/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '16',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('17/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '17',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('18/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '18',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('19/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '19',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('20/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '20',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('21/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '21',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('22/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '22',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('23/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '23',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('24/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '24',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('25/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '25',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('26/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '26',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('27/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '27',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('28/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '28',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('29/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '29',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('30/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '30',
	simrke.FN_RPT_TOTAL_PASIEN_BYUNITMEDIS_DATE(medunit_cd,('31/'+simrke.FN_FORMAT_STRING(convert(varchar(2),@pintMonth),'0',2)+  '/' + CONVERT(VARCHAR(4),@pintYear))) AS '31'
	FROM trx_unit_medis
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TOTAL_TAGIHAN(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TOTAL_TAGIHAN]
@pintMonth int,
@pintYear int
AS
BEGIN
	DECLARE @numCharge numeric
	
	SET @numCharge = 1000
	
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_out) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
	CASE WHEN A.medical_tp='MEDICAL_TP_01' THEN 1 ELSE (DATEDIFF(DAY,A.datetime_in,CASE WHEN A.datetime_out IS NULL THEN GETDATE() ELSE A.datetime_out END)+1) END AS jumlah,
	CASE WHEN A.medical_tp='MEDICAL_TP_01' THEN 1*@numCharge ELSE (DATEDIFF(DAY,A.datetime_in,CASE WHEN A.datetime_out IS NULL THEN GETDATE() ELSE A.datetime_out END)+1)*@numCharge END AS charge
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	WHERE MONTH(A.datetime_out)=@pintMonth AND YEAR(A.datetime_out)=@pintYear
	ORDER BY A.medical_tp,A.datetime_out
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRX_BYDATE(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRX_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrUnitCd Varchar(20)
AS
BEGIN
	IF @pstrUnitCd = '' 
	/*--Semua--*/
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,
		C.pasien_nm,C.no_rm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
		--ACC.account_nm,B.amount,simrke.FN_GET_INVOICE_TOTAL(A.settlement_no) AS total_invoice
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		--WHERE simrke.FN_FORMATDATE(A.invoice_date)>=@pdtDateStart AND simrke.FN_FORMATDATE(A.invoice_date)<=@pdtDateEnd
		WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		UNION
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,'' insurance_nm,A.pay_nm,A.entry_nm,
		CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
		CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,
		'' medunit_nm,'' ruang_nm,'' kelas_nm,
		CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
		'PENDAPATAN' medical_tp,'Pendapatan Lain' AS medical_tp_nm,
		A.invoice_date AS datetime_in,A.invoice_date AS datetime_out,
		ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
		LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		WHERE A.medical_cd IS NULL
		AND A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		ORDER BY medical_tp,A.invoice_date
	ELSE
	/*--Per unit--*/
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,
		C.pasien_nm,C.no_rm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
		--ACC.account_nm,B.amount,simrke.FN_GET_INVOICE_TOTAL(A.settlement_no) AS total_invoice
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		--WHERE simrke.FN_FORMATDATE(A.invoice_date)>=@pdtDateStart AND simrke.FN_FORMATDATE(A.invoice_date)<=@pdtDateEnd
		WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		AND (D.medunit_cd=@pstrUnitCd OR BNG.bangsal_cd=@pstrUnitCd)
		UNION
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,'' insurance_nm,A.pay_nm,A.entry_nm,
		CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
		CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,
		'' medunit_nm,'' ruang_nm,'' kelas_nm,
		CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
		'PENDAPATAN' medical_tp,'Pendapatan Lain' AS medical_tp_nm,
		A.invoice_date AS datetime_in,A.invoice_date AS datetime_out,
		ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
		LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		WHERE A.medical_cd IS NULL
		AND A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		ORDER BY medical_tp,A.invoice_date
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRX_BYDATE_BYMEDICALTP(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRX_BYDATE_BYMEDICALTP]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrMedicalTp Varchar(20)
AS
BEGIN
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,
	COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,
	C.pasien_nm,C.no_rm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
	D.datetime_in,D.datetime_out,
	ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE D.medical_tp=@pstrMedicalTp
	AND A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND A.payment_st='PAYMENT_ST_1'
	ORDER BY D.medical_tp,A.invoice_date
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRX_BYDATE_DETAIL(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRX_BYDATE_DETAIL]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrUnitCd Varchar(20)
AS
BEGIN
	IF @pstrUnitCd = '' 
	/*--Semua--*/
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
		INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
		ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
		A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
		MEDSTL.data_cd AS detail_cd,CASE WHEN D.medical_tp='MEDICAL_TP_01' THEN MEDSTL.data_nm ELSE MEDSTL.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS detail_nm,
		MEDSTL.quantity AS quantity,MEDSTL.amount AS detail_amount,MEDSTL.item_price AS item_price,
		MEDSTL.data_nm AS urutan,MTKDR.dr_nm AS mtk_dr_nm,
		CASE WHEN ISNULL(MTKUNITRJ.medunit_cd,'')<>'' THEN MTKUNITRJ.medunit_nm ELSE MTKUNITRI.bangsal_nm END AS mtk_medunit_nm
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		JOIN trx_medical_settlement MEDSTL ON (A.invoice_no=MEDSTL.invoice_no AND B.account_cd=MEDSTL.account_cd)
		LEFT JOIN trx_medical_tindakan MTK ON MEDSTL.ref_seqno=MTK.medical_tindakan_seqno AND MEDSTL.tarif_tp='TARIF_TP_04'
		LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
		LEFT JOIN trx_unit_medis MTKUNITRJ ON MTK.medunit_cd=MTKUNITRJ.medunit_cd
		LEFT JOIN trx_bangsal MTKUNITRI ON MTK.medunit_cd=MTKUNITRI.bangsal_cd
		LEFT JOIN trx_medical_unit MUNIT ON MEDSTL.ref_seqno=MUNIT.medical_unit_seqno AND MEDSTL.tarif_tp='TARIF_TP_02'
		LEFT JOIN trx_dokter MUNITDR ON MUNIT.dr_cd=MUNITDR.dr_cd
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
		LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
		LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
		LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		UNION ALL
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
		INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
		ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
		A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
		INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
		INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount,INVSTL.item_price AS item_price,
		CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' 
		ELSE CASE WHEN INVSTL.item_cd IN ('NONINV101','NONINV102','NONINV103') THEN 'ZZ' + INV.item_nm ELSE INV.item_nm END
		END 
		AS urutan,
		'' AS mtk_dr_nm,'' AS mtk_medunit_nm
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
		LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
		LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
		LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
		LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		UNION ALL
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
		'' insurance_nm,A.pay_nm,A.entry_nm,
		CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
		CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,'' AS address,
		'' medunit_nm,'' ruang_nm,'' kelas_nm,
		CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
		'PENDAPATAN' AS medical_tp,'Pendapatan Lain' AS medical_tp_nm,
		A.invoice_date AS datetime_in,A.invoice_date AS datetime_out,
		B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
		A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
		INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
		INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount,INVSTL.item_price AS item_price,
		CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' 
		ELSE CASE WHEN INVSTL.item_cd IN ('NONINV101','NONINV102','NONINV103') THEN 'ZZ' + INV.item_nm ELSE INV.item_nm END
		END 
		AS urutan,
		'' AS mtk_dr_nm,'' AS mtk_medunit_nm
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
		LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
		LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
		WHERE A.medical_cd IS NULL
		AND A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		ORDER BY D.medical_tp,A.invoice_no,B.account_cd,urutan
	ELSE
	/*--Per unit--*/
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
		INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
		ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
		A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
		MEDSTL.data_cd AS detail_cd,CASE WHEN D.medical_tp='MEDICAL_TP_01' THEN MEDSTL.data_nm ELSE MEDSTL.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS detail_nm,
		MEDSTL.quantity AS quantity,MEDSTL.amount AS detail_amount,MEDSTL.item_price AS item_price,
		MEDSTL.data_nm AS urutan,MTKDR.dr_nm AS mtk_dr_nm,
		CASE WHEN ISNULL(MTKUNITRJ.medunit_cd,'')<>'' THEN MTKUNITRJ.medunit_nm ELSE MTKUNITRI.bangsal_nm END AS mtk_medunit_nm
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		JOIN trx_medical_settlement MEDSTL ON (A.invoice_no=MEDSTL.invoice_no AND B.account_cd=MEDSTL.account_cd)
		LEFT JOIN trx_medical_tindakan MTK ON MEDSTL.ref_seqno=MTK.medical_tindakan_seqno AND MEDSTL.tarif_tp='TARIF_TP_04'
		LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
		LEFT JOIN trx_unit_medis MTKUNITRJ ON MTK.medunit_cd=MTKUNITRJ.medunit_cd
		LEFT JOIN trx_bangsal MTKUNITRI ON MTK.medunit_cd=MTKUNITRI.bangsal_cd
		LEFT JOIN trx_medical_unit MUNIT ON MEDSTL.ref_seqno=MUNIT.medical_unit_seqno AND MEDSTL.tarif_tp='TARIF_TP_02'
		LEFT JOIN trx_dokter MUNITDR ON MUNIT.dr_cd=MUNITDR.dr_cd
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
		LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
		LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
		LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND (D.medunit_cd=@pstrUnitCd OR BNG.bangsal_cd=@pstrUnitCd)
		AND A.payment_st='PAYMENT_ST_1'
		UNION ALL
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
		INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
		ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
		A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
		INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
		INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount,INVSTL.item_price AS item_price,
		CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' 
		ELSE CASE WHEN INVSTL.item_cd IN ('NONINV101','NONINV102','NONINV103') THEN 'ZZ' + INV.item_nm ELSE INV.item_nm END
		END 
		AS urutan,
		'' AS mtk_dr_nm,'' AS mtk_medunit_nm
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
		LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
		LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
		LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
		LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND (D.medunit_cd=@pstrUnitCd OR BNG.bangsal_cd=@pstrUnitCd)
		AND A.payment_st='PAYMENT_ST_1'
		ORDER BY D.medical_tp,A.invoice_no,B.account_cd,urutan
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRX_BYDATE_DETAIL_GROUP(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRX_BYDATE_DETAIL_GROUP]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrUnitCd Varchar(20)
AS
BEGIN
	IF @pstrUnitCd = '' 
	/*--Semua--*/
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
		INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
		ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
		A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
		MEDSTL.data_cd AS detail_cd,CASE WHEN D.medical_tp='MEDICAL_TP_01' THEN MEDSTL.data_nm ELSE MEDSTL.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS detail_nm,
		MEDSTL.quantity AS quantity,MEDSTL.amount AS detail_amount,MEDSTL.item_price AS item_price,
		MEDSTL.data_nm AS urutan,MTKDR.dr_nm AS mtk_dr_nm,
		CASE WHEN ISNULL(MTKUNITRJ.medunit_cd,'')<>'' THEN MTKUNITRJ.medunit_nm ELSE MTKUNITRI.bangsal_nm END AS mtk_medunit_nm
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		JOIN trx_medical_settlement MEDSTL ON (A.invoice_no=MEDSTL.invoice_no AND B.account_cd=MEDSTL.account_cd)
		LEFT JOIN trx_medical_tindakan MTK ON MEDSTL.ref_seqno=MTK.medical_tindakan_seqno AND MEDSTL.tarif_tp='TARIF_TP_04'
		LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
		LEFT JOIN trx_unit_medis MTKUNITRJ ON MTK.medunit_cd=MTKUNITRJ.medunit_cd
		LEFT JOIN trx_bangsal MTKUNITRI ON MTK.medunit_cd=MTKUNITRI.bangsal_cd
		LEFT JOIN trx_medical_unit MUNIT ON MEDSTL.ref_seqno=MUNIT.medical_unit_seqno AND MEDSTL.tarif_tp='TARIF_TP_02'
		LEFT JOIN trx_dokter MUNITDR ON MUNIT.dr_cd=MUNITDR.dr_cd
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
		LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
		LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
		LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		UNION ALL
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
		INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
		ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
		A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
		INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
		INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount,INVSTL.item_price AS item_price,
		CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' 
		ELSE CASE WHEN INVSTL.item_cd IN ('NONINV101','NONINV102','NONINV103') THEN 'ZZ' + INV.item_nm ELSE INV.item_nm END
		END 
		AS urutan,
		'' AS mtk_dr_nm,'' AS mtk_medunit_nm
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
		LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
		LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
		LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
		LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		UNION ALL
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
		'' insurance_nm,A.pay_nm,A.entry_nm,
		CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
		CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,'' AS address,
		'' medunit_nm,'' ruang_nm,'' kelas_nm,
		CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
		'PENDAPATAN' AS medical_tp,'Pendapatan Lain' AS medical_tp_nm,
		A.invoice_date AS datetime_in,A.invoice_date AS datetime_out,
		B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
		A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
		INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
		INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount,INVSTL.item_price AS item_price,
		CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' 
		ELSE CASE WHEN INVSTL.item_cd IN ('NONINV101','NONINV102','NONINV103') THEN 'ZZ' + INV.item_nm ELSE INV.item_nm END
		END 
		AS urutan,
		'' AS mtk_dr_nm,'' AS mtk_medunit_nm
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
		LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
		LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
		WHERE A.medical_cd IS NULL
		AND A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		ORDER BY D.medical_tp,A.invoice_no,B.account_cd,urutan
	ELSE
	BEGIN
	/*--Per group--*/
		IF @pstrUnitCd = 'AC301' 
		/*--OBAT--*/
			SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
			COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
			INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
			ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
			RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
			D.datetime_in,D.datetime_out,
			B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
			A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
			INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
			INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount,INVSTL.item_price AS item_price,
			CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' 
			ELSE CASE WHEN INVSTL.item_cd IN ('NONINV101','NONINV102','NONINV103') THEN 'ZZ' + INV.item_nm ELSE INV.item_nm END
			END 
			AS urutan,
			'' AS mtk_dr_nm,'' AS mtk_medunit_nm
			FROM trx_settlement A 
			JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
			JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
			LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
			LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
			LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
			LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
			LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
			JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
			LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
			LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
			LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
			LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
			JOIN trx_medical D ON A.medical_cd=D.medical_cd
			LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
			LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
			LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
			LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
			LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
			WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
			AND A.payment_st='PAYMENT_ST_1'
			AND ACC.account_cd=@pstrUnitCd
			UNION ALL
			SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
			COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
			'' insurance_nm,A.pay_nm,A.entry_nm,
			CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
			CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,'' AS address,
			'' medunit_nm,'' ruang_nm,'' kelas_nm,
			CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
			'PENDAPATAN' AS medical_tp,'Pendapatan Lain' AS medical_tp_nm,
			A.invoice_date AS datetime_in,A.invoice_date AS datetime_out,
			B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
			A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
			INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
			INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount,INVSTL.item_price AS item_price,
			CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' 
			ELSE CASE WHEN INVSTL.item_cd IN ('NONINV101','NONINV102','NONINV103') THEN 'ZZ' + INV.item_nm ELSE INV.item_nm END
			END 
			AS urutan,
			'' AS mtk_dr_nm,'' AS mtk_medunit_nm
			FROM trx_settlement A 
			JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
			JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
			LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
			LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
			JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
			LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
			LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
			LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
			LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
			WHERE A.medical_cd IS NULL
			AND A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
			AND A.payment_st='PAYMENT_ST_1'
			ORDER BY D.medical_tp,A.invoice_no,B.account_cd,urutan	
		ELSE
			SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
			COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
			INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
			ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
			RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
			D.datetime_in,D.datetime_out,
			B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
			A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
			MEDSTL.data_cd AS detail_cd,CASE WHEN D.medical_tp='MEDICAL_TP_01' THEN MEDSTL.data_nm ELSE MEDSTL.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS detail_nm,
			MEDSTL.quantity AS quantity,MEDSTL.amount AS detail_amount,MEDSTL.item_price AS item_price,
			MEDSTL.data_nm AS urutan,MTKDR.dr_nm AS mtk_dr_nm,
			CASE WHEN ISNULL(MTKUNITRJ.medunit_cd,'')<>'' THEN MTKUNITRJ.medunit_nm ELSE MTKUNITRI.bangsal_nm END AS mtk_medunit_nm
			FROM trx_settlement A 
			JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
			JOIN trx_medical_settlement MEDSTL ON (A.invoice_no=MEDSTL.invoice_no AND B.account_cd=MEDSTL.account_cd)
			LEFT JOIN trx_medical_tindakan MTK ON MEDSTL.ref_seqno=MTK.medical_tindakan_seqno AND MEDSTL.tarif_tp='TARIF_TP_04'
			LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
			LEFT JOIN trx_unit_medis MTKUNITRJ ON MTK.medunit_cd=MTKUNITRJ.medunit_cd
			LEFT JOIN trx_bangsal MTKUNITRI ON MTK.medunit_cd=MTKUNITRI.bangsal_cd
			LEFT JOIN trx_medical_unit MUNIT ON MEDSTL.ref_seqno=MUNIT.medical_unit_seqno AND MEDSTL.tarif_tp='TARIF_TP_02'
			LEFT JOIN trx_dokter MUNITDR ON MUNIT.dr_cd=MUNITDR.dr_cd
			LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
			LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
			LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
			LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
			JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
			LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
			LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
			LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
			LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
			JOIN trx_medical D ON A.medical_cd=D.medical_cd
			LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
			LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
			LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
			LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
			LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
			WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
			AND A.payment_st='PAYMENT_ST_1'
			AND ACC.account_cd=@pstrUnitCd
			ORDER BY D.medical_tp,A.invoice_no,B.account_cd,urutan	
	END
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRX_BYDATE_DETAIL_MEDICALTP(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRX_BYDATE_DETAIL_MEDICALTP]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrMedicalTp Varchar(20)
AS
BEGIN
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
	COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
	INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
	D.datetime_in,D.datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
	MEDSTL.data_cd AS detail_cd,CASE WHEN D.medical_tp='MEDICAL_TP_01' THEN MEDSTL.data_nm ELSE MEDSTL.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS detail_nm,
	MEDSTL.quantity AS quantity,MEDSTL.amount AS detail_amount,MEDSTL.item_price AS item_price,
	MEDSTL.data_nm AS urutan,MTKDR.dr_nm AS mtk_dr_nm,
	CASE WHEN ISNULL(MTKUNITRJ.medunit_cd,'')<>'' THEN MTKUNITRJ.medunit_nm ELSE MTKUNITRI.bangsal_nm END AS mtk_medunit_nm
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	JOIN trx_medical_settlement MEDSTL ON (A.invoice_no=MEDSTL.invoice_no AND B.account_cd=MEDSTL.account_cd)
	LEFT JOIN trx_medical_tindakan MTK ON MEDSTL.ref_seqno=MTK.medical_tindakan_seqno AND MEDSTL.tarif_tp='TARIF_TP_04'
	LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
	LEFT JOIN trx_unit_medis MTKUNITRJ ON MTK.medunit_cd=MTKUNITRJ.medunit_cd
	LEFT JOIN trx_bangsal MTKUNITRI ON MTK.medunit_cd=MTKUNITRI.bangsal_cd
	LEFT JOIN trx_medical_unit MUNIT ON MEDSTL.ref_seqno=MUNIT.medical_unit_seqno AND MEDSTL.tarif_tp='TARIF_TP_02'
	LEFT JOIN trx_dokter MUNITDR ON MUNIT.dr_cd=MUNITDR.dr_cd
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND D.medical_tp=@pstrMedicalTp
	AND A.payment_st='PAYMENT_ST_1'
	UNION ALL
	SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
	COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
	INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
	ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
	RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
	D.datetime_in,D.datetime_out,
	B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
	A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
	INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
	INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount,INVSTL.item_price AS item_price,
	CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' 
	ELSE CASE WHEN INVSTL.item_cd IN ('NONINV101','NONINV102','NONINV103') THEN 'ZZ' + INV.item_nm ELSE INV.item_nm END
	END 
	AS urutan,
	'' AS mtk_dr_nm,'' AS mtk_medunit_nm
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
	LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
	LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
	LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
	LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
	LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
	LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
	JOIN trx_medical D ON A.medical_cd=D.medical_cd
	LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
	LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
	WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND D.medical_tp=@pstrMedicalTp
	AND A.payment_st='PAYMENT_ST_1'
	ORDER BY D.medical_tp,A.invoice_no,B.account_cd,urutan
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRX_BYDATE_DETAIL_PARAM(Varchar,Varchar,Varchar,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRX_BYDATE_DETAIL_PARAM]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrMedicalTp Varchar(20),
@pstrUnitCd Varchar(20),
@pstrDrCd Varchar(20),
@pstrGroupItem Varchar(20)
AS
BEGIN
	IF @pstrGroupItem = 'AC301' 
	/*--OBAT--*/
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
		INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
		ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
		A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
		INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
		INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount,INVSTL.item_price AS item_price,
		CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' 
		ELSE CASE WHEN INVSTL.item_cd IN ('NONINV101','NONINV102','NONINV103') THEN 'ZZ' + INV.item_nm ELSE INV.item_nm END
		END 
		AS urutan,
		'' AS mtk_dr_nm,'' AS mtk_medunit_nm
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
		LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
		LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
		LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
		LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND D.medical_tp LIKE @pstrMedicalTp
		AND (D.medunit_cd LIKE @pstrUnitCd OR BNG.bangsal_cd LIKE @pstrUnitCd)
		AND D.dr_cd LIKE @pstrDrCd
		AND ACC.account_cd=@pstrGroupItem
		AND A.payment_st='PAYMENT_ST_1'
		UNION ALL
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
		'' insurance_nm,A.pay_nm,A.entry_nm,
		CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
		CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,'' AS address,
		'' medunit_nm,'' ruang_nm,'' kelas_nm,
		CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
		'PENDAPATAN' AS medical_tp,'Pendapatan Lain' AS medical_tp_nm,
		A.invoice_date AS datetime_in,A.invoice_date AS datetime_out,
		B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
		A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
		INVSTL.item_cd AS detail_cd,CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' ELSE INV.item_nm END AS detail_nm,
		INVSTL.quantity AS quantity,INVSTL.total_price AS detail_amount,INVSTL.item_price AS item_price,
		CASE WHEN INVSTL.item_cd='' THEN 'OBAT RACIK' 
		ELSE CASE WHEN INVSTL.item_cd IN ('NONINV101','NONINV102','NONINV103') THEN 'ZZ' + INV.item_nm ELSE INV.item_nm END
		END 
		AS urutan,
		'' AS mtk_dr_nm,'' AS mtk_medunit_nm
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		JOIN trx_medical_settlement_inv INVSTL ON (A.invoice_no=INVSTL.invoice_no AND B.account_cd=INVSTL.account_cd)
		LEFT JOIN inv_item_master INV ON INVSTL.item_cd=INV.item_cd
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
		LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
		WHERE A.medical_cd IS NULL
		AND A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		ORDER BY D.medical_tp,A.invoice_no,B.account_cd,urutan	
	ELSE
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,CASE WHEN ISNULL(PAY.payment_nm,'')='' THEN 'KARTU' ELSE PAY.payment_nm END AS payment_nm,
		INS.insurance_nm,A.pay_nm,A.entry_nm,C.pasien_nm,C.no_rm,
		ISNULL(C.address,'') + ' ' + ISNULL(REGION4.region_nm,'') + ' ' + ISNULL(REGION3.region_nm,'') + ' ' + ISNULL(REGION2.region_nm,'') AS address,
		RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		B.account_cd,ACC.account_nm,B.amount,A.amount_asuransi,A.amount_pasien,
		A.amount_tunai,A.amount_nontunai,A.discount_percent,A.discount_amount,
		MEDSTL.data_cd AS detail_cd,CASE WHEN D.medical_tp='MEDICAL_TP_01' THEN MEDSTL.data_nm ELSE MEDSTL.data_nm + ' - ' + ISNULL(MTKDR.dr_nm,'') END AS detail_nm,
		MEDSTL.quantity AS quantity,MEDSTL.amount AS detail_amount,MEDSTL.item_price AS item_price,
		MEDSTL.data_nm AS urutan,MTKDR.dr_nm AS mtk_dr_nm,
		CASE WHEN ISNULL(MTKUNITRJ.medunit_cd,'')<>'' THEN MTKUNITRJ.medunit_nm ELSE MTKUNITRI.bangsal_nm END AS mtk_medunit_nm
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		JOIN trx_medical_settlement MEDSTL ON (A.invoice_no=MEDSTL.invoice_no AND B.account_cd=MEDSTL.account_cd)
		LEFT JOIN trx_medical_tindakan MTK ON MEDSTL.ref_seqno=MTK.medical_tindakan_seqno AND MEDSTL.tarif_tp='TARIF_TP_04'
		LEFT JOIN trx_dokter MTKDR ON MTK.dr_cd=MTKDR.dr_cd
		LEFT JOIN trx_unit_medis MTKUNITRJ ON MTK.medunit_cd=MTKUNITRJ.medunit_cd
		LEFT JOIN trx_bangsal MTKUNITRI ON MTK.medunit_cd=MTKUNITRI.bangsal_cd
		LEFT JOIN trx_medical_unit MUNIT ON MEDSTL.ref_seqno=MUNIT.medical_unit_seqno AND MEDSTL.tarif_tp='TARIF_TP_02'
		LEFT JOIN trx_dokter MUNITDR ON MUNIT.dr_cd=MUNITDR.dr_cd
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN com_payment_method PAY ON A.payment_cd=PAY.payment_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN com_region REGION1 On C.region_prop=REGION1.region_cd
		LEFT JOIN com_region REGION2 On C.region_kab=REGION2.region_cd
		LEFT JOIN com_region REGION3 On C.region_kec=REGION3.region_cd
		LEFT JOIN com_region REGION4 On C.region_kel=REGION4.region_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		AND D.medical_tp LIKE @pstrMedicalTp
		AND (D.medunit_cd LIKE @pstrUnitCd OR BNG.bangsal_cd LIKE @pstrUnitCd)
		AND D.dr_cd LIKE @pstrDrCd
		AND ACC.account_cd LIKE @pstrGroupItem
		ORDER BY D.medical_tp,A.invoice_no,B.account_cd,urutan	
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRX_BYDATE_SHIFT(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRX_BYDATE_SHIFT]
@pdtDateStart Varchar(10),
@pstrTime Varchar(12)
AS
BEGIN
	DECLARE @dtTimeStart Datetime
	DECLARE @dtTimeEnd Datetime
	DECLARE @dtTime00 Datetime
	DECLARE @dtTime24 Datetime
	
	SET @dtTimeStart = LEFT(@pstrTime,5)
	SET @dtTimeEnd = RIGHT(@pstrTime,5)
	SET @dtTime00 = CONVERT(Datetime,'00:00')
	SET @dtTime24 = CONVERT(Datetime,'23:59')
	
	IF @dtTimeStart<@dtTimeEnd
	BEGIN
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,
		C.pasien_nm,C.no_rm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		ACC.account_nm,B.amount,A.entry_date,A.amount_tunai,A.amount_nontunai
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE simrke.FN_FORMATDATE(A.entry_date)=@pdtDateStart
		AND (CONVERT(DATETIME,CONVERT(VARCHAR(5),A.entry_date,108))>=@dtTimeStart AND CONVERT(DATETIME,CONVERT(VARCHAR(5),A.entry_date,108))<=@dtTimeEnd)
		AND A.payment_st='PAYMENT_ST_1'
		UNION
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,'' insurance_nm,A.pay_nm,A.entry_nm,
		CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
		CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,
		'' medunit_nm,'' ruang_nm,'' kelas_nm,
		CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
		'PENDAPATAN' medical_tp,'Pendapatan Lain' AS medical_tp_nm,
		A.invoice_date AS datetime_in,A.invoice_date AS datetime_out,
		ACC.account_nm,B.amount,A.entry_date,A.amount_tunai,A.amount_nontunai
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
		LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		WHERE A.medical_cd IS NULL
		AND simrke.FN_FORMATDATE(A.entry_date)=@pdtDateStart
		AND (CONVERT(DATETIME,CONVERT(VARCHAR(5),A.entry_date,108))>=@dtTimeStart AND CONVERT(DATETIME,CONVERT(VARCHAR(5),A.entry_date,108))<=@dtTimeEnd)
		AND A.payment_st='PAYMENT_ST_1'
		ORDER BY medical_tp,A.entry_date
	END
	ELSE
	BEGIN
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,
		C.pasien_nm,C.no_rm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		ACC.account_nm,B.amount,A.entry_date,A.amount_tunai,A.amount_nontunai
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE (
			(simrke.FN_FORMATDATE(A.entry_date)=@pdtDateStart AND (CONVERT(DATETIME,CONVERT(VARCHAR(5),A.entry_date,108))>=@dtTimeStart AND CONVERT(DATETIME,CONVERT(VARCHAR(5),A.entry_date,108))<=@dtTime24))
			OR
			(simrke.FN_FORMATDATE(A.entry_date)=simrke.FN_FORMATDATE(simrke.FN_STRINGTODATE(@pdtDateStart)+1) AND (CONVERT(DATETIME,CONVERT(VARCHAR(5),A.entry_date,108))>=@dtTime00 AND CONVERT(DATETIME,CONVERT(VARCHAR(5),A.entry_date,108))<=@dtTimeEnd))
		)
		AND A.payment_st='PAYMENT_ST_1'
		UNION
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,'' insurance_nm,A.pay_nm,A.entry_nm,
		CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
		CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,
		'' medunit_nm,'' ruang_nm,'' kelas_nm,
		CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
		'PENDAPATAN' medical_tp,'Pendapatan Lain' AS medical_tp_nm,
		A.invoice_date AS datetime_in,A.invoice_date AS datetime_out,
		ACC.account_nm,B.amount,A.entry_date,A.amount_tunai,A.amount_nontunai
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
		LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		WHERE A.medical_cd IS NULL
		AND (
			(simrke.FN_FORMATDATE(A.entry_date)=@pdtDateStart AND (CONVERT(DATETIME,CONVERT(VARCHAR(5),A.entry_date,108))>=@dtTimeStart AND CONVERT(DATETIME,CONVERT(VARCHAR(5),A.entry_date,108))<=@dtTime24))
			OR
			(simrke.FN_FORMATDATE(A.entry_date)=simrke.FN_FORMATDATE(simrke.FN_STRINGTODATE(@pdtDateStart)+1) AND (CONVERT(DATETIME,CONVERT(VARCHAR(5),A.entry_date,108))>=@dtTime00 AND CONVERT(DATETIME,CONVERT(VARCHAR(5),A.entry_date,108))<=@dtTimeEnd))
		)
		AND A.payment_st='PAYMENT_ST_1'
		ORDER BY medical_tp,A.entry_date
	END
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRX_BYMONTH(int,int,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRX_BYMONTH]
@pintMonth int,
@pintYear int,
@pstrUnitCd Varchar(20)
AS
BEGIN
	IF @pstrUnitCd = '' 
	/*--Semua--*/
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,
		C.pasien_nm,C.no_rm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
		--ACC.account_nm,B.amount,simrke.FN_GET_INVOICE_TOTAL(A.settlement_no) AS total_invoice
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE (MONTH(A.invoice_date)=@pintMonth AND YEAR(A.invoice_date)=@pintYear)
		AND A.payment_st='PAYMENT_ST_1'
		UNION
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,'' insurance_nm,A.pay_nm,A.entry_nm,
		CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
		CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,
		'' medunit_nm,'' ruang_nm,'' kelas_nm,
		CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
		'PENDAPATAN' medical_tp,'Pendapatan Lain' AS medical_tp_nm,
		A.invoice_date AS datetime_in,A.invoice_date AS datetime_out,
		ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
		LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		WHERE A.medical_cd IS NULL
		AND (MONTH(A.invoice_date)=@pintMonth AND YEAR(A.invoice_date)=@pintYear)
		AND A.payment_st='PAYMENT_ST_1'
		ORDER BY medical_tp,A.invoice_date
	ELSE
	/*--Per unit--*/
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,
		C.pasien_nm,C.no_rm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
		--ACC.account_nm,B.amount,simrke.FN_GET_INVOICE_TOTAL(A.settlement_no) AS total_invoice
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		WHERE (MONTH(A.invoice_date)=@pintMonth AND YEAR(A.invoice_date)=@pintYear)
		AND A.payment_st='PAYMENT_ST_1'
		AND (D.medunit_cd=@pstrUnitCd OR BNG.bangsal_cd=@pstrUnitCd)
		UNION
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,'' insurance_nm,A.pay_nm,A.entry_nm,
		CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
		CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,
		'' medunit_nm,'' ruang_nm,'' kelas_nm,
		CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
		'PENDAPATAN' medical_tp,'Pendapatan Lain' AS medical_tp_nm,
		A.invoice_date AS datetime_in,A.invoice_date AS datetime_out,
		ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
		LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		WHERE A.medical_cd IS NULL
		AND (MONTH(A.invoice_date)=@pintMonth AND YEAR(A.invoice_date)=@pintYear)
		AND A.payment_st='PAYMENT_ST_1'
		ORDER BY medical_tp,A.invoice_date
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRX_BYUNIT(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRX_BYUNIT]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,RJ.medunit_nm
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	WHERE A.medical_tp='MEDICAL_TP_01'
	AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	ORDER BY RJ.medunit_nm,A.datetime_in
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRX_RINAP_AKTIF() CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRX_RINAP_AKTIF]
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	WHERE A.medical_tp='MEDICAL_TP_02'
	AND A.medical_trx_st='MEDICAL_TRX_ST_0'
	ORDER BY KLS.kelas_nm,A.datetime_in
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRXACCOUNT_BYDATE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRXACCOUNT_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.invoice_no,INS.insurance_nm,C.pasien_nm,C.no_rm,
	ACC.account_nm,B.amount
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	--WHERE simrke.FN_FORMATDATE(A.invoice_date)>=@pdtDateStart AND simrke.FN_FORMATDATE(A.invoice_date)<=@pdtDateEnd
	WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND A.payment_st='PAYMENT_ST_1'
	UNION
	SELECT A.invoice_no,'' insurance_nm,'PENDAPATAN LAIN' pasien_nm,'' no_rm,
	ACC.account_nm,B.amount
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	JOIN com_account ACC ON B.account_cd=ACC.account_cd
	WHERE A.medical_cd IS NULL
	AND A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND A.payment_st='PAYMENT_ST_1'
	ORDER BY A.invoice_no
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRXGROUP_BYDATE(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRXGROUP_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrUnitCd Varchar(20)
AS
BEGIN
	IF @pstrUnitCd = '' 
	/*--Semua--*/
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,
		C.pasien_nm,C.no_rm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
		D.datetime_in,D.datetime_out,
		ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
		--ACC.account_nm,B.amount,simrke.FN_GET_INVOICE_TOTAL(A.settlement_no) AS total_invoice
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
		JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		JOIN trx_medical D ON A.medical_cd=D.medical_cd
		LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
		LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
		--WHERE simrke.FN_FORMATDATE(A.invoice_date)>=@pdtDateStart AND simrke.FN_FORMATDATE(A.invoice_date)<=@pdtDateEnd
		WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		UNION
		SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
		COM.code_nm AS payment_tp_nm,'' insurance_nm,A.pay_nm,A.entry_nm,
		CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
		CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,
		'' medunit_nm,'' ruang_nm,'' kelas_nm,
		CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
		'PENDAPATAN' medical_tp,'Pendapatan Lain' AS medical_tp_nm,
		A.invoice_date AS datetime_in,A.invoice_date AS datetime_out,
		ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
		FROM trx_settlement A 
		JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
		LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
		JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
		LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
		LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
		LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
		WHERE A.medical_cd IS NULL
		AND A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.payment_st='PAYMENT_ST_1'
		ORDER BY medical_tp,A.invoice_date
	ELSE
	BEGIN
	/*--Per group--*/
		IF @pstrUnitCd = 'AC301' 
		/*--OBAT--*/
			SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
			COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,
			C.pasien_nm,C.no_rm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
			D.datetime_in,D.datetime_out,
			ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
			--ACC.account_nm,B.amount,simrke.FN_GET_INVOICE_TOTAL(A.settlement_no) AS total_invoice
			FROM trx_settlement A 
			JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
			LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
			LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
			LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
			JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
			JOIN trx_medical D ON A.medical_cd=D.medical_cd
			LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
			LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
			LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
			LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
			LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
			LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
			--WHERE simrke.FN_FORMATDATE(A.invoice_date)>=@pdtDateStart AND simrke.FN_FORMATDATE(A.invoice_date)<=@pdtDateEnd
			WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
			AND A.payment_st='PAYMENT_ST_1'
			AND ACC.account_cd=@pstrUnitCd
			UNION
			SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
			COM.code_nm AS payment_tp_nm,'' insurance_nm,A.pay_nm,A.entry_nm,
			CASE WHEN A.pasien_cd IS NULL THEN RS.pasien_nm ELSE C.pasien_nm END AS pasien_nm,
			CASE WHEN A.pasien_cd IS NULL THEN '' ELSE C.no_rm END AS no_rm,
			'' medunit_nm,'' ruang_nm,'' kelas_nm,
			CASE WHEN RS.dr_cd='' THEN RS.dr_nm ELSE DR.dr_nm END AS dr_nm,
			'PENDAPATAN' medical_tp,'Pendapatan Lain' AS medical_tp_nm,
			A.invoice_date AS datetime_in,A.invoice_date AS datetime_out,
			ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
			FROM trx_settlement A 
			JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
			LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
			JOIN trx_medical_resep RS ON A.ref_medical_resep_seqno=RS.medical_resep_seqno
			LEFT JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
			LEFT JOIN trx_dokter DR ON RS.dr_cd=DR.dr_cd
			LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
			WHERE A.medical_cd IS NULL
			AND A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
			AND A.payment_st='PAYMENT_ST_1'
			ORDER BY medical_tp,A.invoice_date
		ELSE
			SELECT A.invoice_no,simrke.FN_FORMATDATE(A.invoice_date) AS invoice_date,A.invoice_date AS invoice_datetime,
			COM.code_nm AS payment_tp_nm,INS.insurance_nm,A.pay_nm,A.entry_nm,
			C.pasien_nm,C.no_rm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,DR.dr_nm,D.medical_tp,COM2.code_nm AS medical_tp_nm,
			D.datetime_in,D.datetime_out,
			ACC.account_nm,B.amount,A.amount_tunai,A.amount_nontunai
			--ACC.account_nm,B.amount,simrke.FN_GET_INVOICE_TOTAL(A.settlement_no) AS total_invoice
			FROM trx_settlement A 
			JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
			LEFT JOIN com_account ACC ON B.account_cd=ACC.account_cd
			LEFT JOIN com_code COM ON A.payment_tp=COM.com_cd
			LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
			JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
			JOIN trx_medical D ON A.medical_cd=D.medical_cd
			LEFT JOIN com_code COM2 ON D.medical_tp=COM2.com_cd
			LEFT JOIN trx_unit_medis RJ ON D.medunit_cd=RJ.medunit_cd
			LEFT JOIN trx_ruang KMR ON D.ruang_cd=KMR.ruang_cd
			LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
			LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
			LEFT JOIN trx_dokter DR ON D.dr_cd=DR.dr_cd
			--WHERE simrke.FN_FORMATDATE(A.invoice_date)>=@pdtDateStart AND simrke.FN_FORMATDATE(A.invoice_date)<=@pdtDateEnd
			WHERE A.invoice_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.invoice_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
			AND A.payment_st='PAYMENT_ST_1'
			AND ACC.account_cd=@pstrUnitCd
	END
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRXITEM_BYDATE(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRXITEM_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrItemCode Varchar(20)
AS
BEGIN
	DECLARE @pstrPosCd Varchar(20)
	
	IF @pstrItemCode = ''
		SELECT A.item_cd, A.item_nm, UNIT.unit_nm AS unit,B.quantity
		FROM inv_item_master A
		JOIN trx_resep_data B ON A.item_cd=B.item_cd
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE B.resep_tp='RESEP_TP_1'
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION ALL
		SELECT A.item_cd, A.item_nm, UNIT.unit_nm AS unit,B.quantity
		FROM inv_item_master A
		JOIN trx_medical_alkes B ON A.item_cd=B.item_cd
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_trx))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_trx))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION ALL
		SELECT A.item_cd, A.item_nm, UNIT.unit_nm AS unit,RACIK.quantity
		FROM inv_item_master A
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		JOIN trx_resep_racik RACIK ON A.item_cd=RACIK.item_cd
		JOIN trx_resep_data B ON RACIK.resep_seqno=B.resep_seqno
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		WHERE B.resep_tp='RESEP_TP_2'
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY A.item_cd,A.item_nm
	ELSE
		SELECT A.item_cd, A.item_nm, UNIT.unit_nm AS unit,B.quantity
		FROM inv_item_master A
		JOIN trx_resep_data B ON A.item_cd=B.item_cd
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE B.resep_tp='RESEP_TP_1'
		AND A.item_cd=@pstrItemCode
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION ALL
		SELECT A.item_cd, A.item_nm, UNIT.unit_nm AS unit,B.quantity
		FROM inv_item_master A
		JOIN trx_medical_alkes B ON A.item_cd=B.item_cd
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE A.item_cd=@pstrItemCode
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_trx))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_trx))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION ALL
		SELECT A.item_cd, A.item_nm, UNIT.unit_nm AS unit,RACIK.quantity
		FROM inv_item_master A
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		JOIN trx_resep_racik RACIK ON A.item_cd=RACIK.item_cd
		JOIN trx_resep_data B ON RACIK.resep_seqno=B.resep_seqno
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		WHERE B.resep_tp='RESEP_TP_2'
		AND A.item_cd=@pstrItemCode
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY A.item_cd,A.item_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRXITEMTYPE_BYDATE(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRXITEMTYPE_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrTypeCd Varchar(20)
AS
BEGIN
	IF @pstrTypeCd = ''
		SELECT A.type_cd,TP.type_nm,A.item_cd, A.item_nm, UNIT.unit_nm AS unit,B.quantity
		FROM inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_resep_data B ON A.item_cd=B.item_cd
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE B.resep_tp='RESEP_TP_1'
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
		UNION ALL
		SELECT A.type_cd,TP.type_nm,A.item_cd, A.item_nm, UNIT.unit_nm AS unit,B.quantity
		FROM inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_medical_alkes B ON A.item_cd=B.item_cd
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_trx))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_trx))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
		UNION ALL
		SELECT A.type_cd,TP.type_nm,A.item_cd, A.item_nm, UNIT.unit_nm AS unit,RACIK.quantity
		FROM inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_resep_data B ON A.item_cd=B.item_cd
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		JOIN trx_resep_racik RACIK ON B.resep_seqno=RACIK.resep_seqno
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE B.resep_tp='RESEP_TP_2'
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
		ORDER BY A.type_cd,TP.type_nm,A.item_cd,A.item_nm
	ELSE
		SELECT A.type_cd,TP.type_nm,A.item_cd, A.item_nm, UNIT.unit_nm AS unit,B.quantity
		FROM inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_resep_data B ON A.item_cd=B.item_cd
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE B.resep_tp='RESEP_TP_1'
		AND A.type_cd=@pstrTypeCd
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
		UNION ALL
		SELECT A.type_cd,TP.type_nm,A.item_cd, A.item_nm, UNIT.unit_nm AS unit,B.quantity
		FROM inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_medical_alkes B ON A.item_cd=B.item_cd
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE A.type_cd=@pstrTypeCd
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_trx))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_trx))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
		UNION ALL
		SELECT A.type_cd,TP.type_nm,A.item_cd, A.item_nm, UNIT.unit_nm AS unit,RACIK.quantity
		FROM inv_item_master A
		JOIN inv_item_type TP ON A.type_cd=TP.type_cd
		JOIN trx_resep_data B ON A.item_cd=B.item_cd
		JOIN trx_medical_resep C ON B.medical_resep_seqno=C.medical_resep_seqno
		JOIN trx_resep_racik RACIK ON B.resep_seqno=RACIK.resep_seqno
		JOIN inv_unit UNIT ON A.unit_cd=UNIT.unit_cd
		WHERE B.resep_tp='RESEP_TP_2'
		AND A.type_cd=@pstrTypeCd
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))>=simrke.FN_STRINGTODATE(@pdtDateStart)
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(C.resep_date))<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		AND A.type_cd IN (SELECT code_value FROM com_code WHERE code_group='RPT_ITEMTP')
		ORDER BY A.type_cd,TP.type_nm,A.item_cd,A.item_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRXJM_BYDATE_ORDERDR(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRXJM_BYDATE_ORDERDR]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrDrCd Varchar(20)
AS
BEGIN
	IF @pstrDrCd = '' 
	/*--Semua--*/
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		--simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN MTK.datetime_trx ELSE A.datetime_in END AS datetime_trx,TK.treatment_nm,
		simrke.FN_RPT_GET_TRXJM_DOKTER(MTK.dr_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_dr
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MTK.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN MTK.datetime_trx ELSE A.datetime_in END AS datetime_trx,TK.treatment_nm,
		simrke.FN_RPT_GET_TRXJM_DOKTER(MTK.dr_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_dr
		FROM trx_medical A
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MTK.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND MTK.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MTK.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		--Non tindakan
		--pemeriksaan rawat jalan
		/*UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		A.datetime_in AS datetime_trx,'' AS treatment_nm,
		simrke.FN_RPT_GET_TRXJM_DOKTER(A.dr_cd,'',A.medical_cd) AS jm_dr
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)*/
		--mutasi rawat inap
		--HARDCODE
		--15000
		--End HARDCODE
		/*
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		A.datetime_in AS datetime_trx,'MUTASI PASIEN' AS treatment_nm,
		15000 AS jm_dr
		FROM trx_medical A
		JOIN trx_medical MROOT ON A.medical_cd=MROOT.medical_root_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		*/
		--End Non tindakan
		--Visit dokter
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MDR.datetime_trx IS NOT NULL THEN MDR.datetime_trx ELSE A.datetime_in END AS datetime_trx,
		CASE WHEN ISNULL(DR.spesialis_cd,'')<>'' THEN 'Visit Dokter Spesialis' ELSE 'Visit Dokter Umum' END AS treatment_nm,
		simrke.FN_RPT_GET_TRXJM_DOKTER_VISIT(MDR.dr_cd,'TRXDR_TP_1',A.medical_cd) AS jm_dr
		FROM trx_medical A
		JOIN trx_medical_dokter MDR ON A.medical_cd=MDR.medical_cd AND trx_dr_tp='TRXDR_TP_1' AND MDR.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MDR.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MDR.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		--End Visit dokter
		--Laboratorium/Radiologi (unit medis)
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MUNIT.datetime_trx IS NOT NULL THEN MUNIT.datetime_trx ELSE A.datetime_in END AS datetime_trx,MUITEM.medicalunit_nm AS treatment_nm,
		simrke.FN_RPT_GET_TRXJM_DOKTER_UNITMEDIS(MUNIT.dr2_cd,MUNIT.medicalunit_cd,A.medical_cd) AS jm_dr
		FROM trx_medical A
		JOIN trx_medical_unit MUNIT ON A.medical_cd=MUNIT.medical_cd AND MUNIT.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MUNIT.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		JOIN trx_unitmedis_item MUITEM ON MUNIT.medicalunit_cd=MUITEM.medicalunit_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MUNIT.dr2_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		--End Laboratorium/Radiologi (unit medis)
		ORDER BY DR.dr_nm,A.medical_tp,datetime_trx,A.datetime_in
	ELSE
	/*--Per dokter--*/
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN MTK.datetime_trx ELSE A.datetime_in END AS datetime_trx,TK.treatment_nm,
		simrke.FN_RPT_GET_TRXJM_DOKTER(MTK.dr_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_dr
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MTK.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL AND DR.dr_cd=@pstrDrCd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN MTK.datetime_trx ELSE A.datetime_in END AS datetime_trx,TK.treatment_nm,
		simrke.FN_RPT_GET_TRXJM_DOKTER(MTK.dr_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_dr
		FROM trx_medical A
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MTK.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL AND DR.dr_cd=@pstrDrCd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND MTK.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MTK.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		--Non tindakan
		--pemeriksaan rawat jalan
		/*UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		A.datetime_in AS datetime_trx,'' AS treatment_nm,
		simrke.FN_RPT_GET_TRXJM_DOKTER(A.dr_cd,'',A.medical_cd) AS jm_dr
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL AND DR.dr_cd=@pstrDrCd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)*/
		--mutasi rawat inap
		--HARDCODE
		--15000
		--End HARDCODE
		/*
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		A.datetime_in AS datetime_trx,'MUTASI PASIEN' AS treatment_nm,
		15000 AS jm_dr
		FROM trx_medical A
		JOIN trx_medical MROOT ON A.medical_cd=MROOT.medical_root_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL AND DR.dr_cd=@pstrDrCd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		*/
		--End Non tindakan
		--Visit dokter
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MDR.datetime_trx IS NOT NULL THEN MDR.datetime_trx ELSE A.datetime_in END AS datetime_trx,
		CASE WHEN ISNULL(DR.spesialis_cd,'')<>'' THEN 'Visit Dokter Spesialis' ELSE 'Visit Dokter Umum' END AS treatment_nm,
		simrke.FN_RPT_GET_TRXJM_DOKTER_VISIT(MDR.dr_cd,'TRXDR_TP_1',A.medical_cd) AS jm_dr
		FROM trx_medical A
		JOIN trx_medical_dokter MDR ON A.medical_cd=MDR.medical_cd AND trx_dr_tp='TRXDR_TP_1' AND MDR.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MDR.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MDR.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL AND DR.dr_cd=@pstrDrCd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		--End Visit dokter
		--Laboratorium/Radiologi (unit medis)
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MUNIT.datetime_trx IS NOT NULL THEN MUNIT.datetime_trx ELSE A.datetime_in END AS datetime_trx,MUITEM.medicalunit_nm AS treatment_nm,
		simrke.FN_RPT_GET_TRXJM_DOKTER_UNITMEDIS(MUNIT.dr2_cd,MUNIT.medicalunit_cd,A.medical_cd) AS jm_dr
		FROM trx_medical A
		JOIN trx_medical_unit MUNIT ON A.medical_cd=MUNIT.medical_cd AND MUNIT.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MUNIT.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		JOIN trx_unitmedis_item MUITEM ON MUNIT.medicalunit_cd=MUITEM.medicalunit_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MUNIT.dr2_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL AND DR.dr_cd=@pstrDrCd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		--End Laboratorium/Radiologi (unit medis)
		ORDER BY DR.dr_nm,A.medical_tp,datetime_trx,A.datetime_in
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRXJM_BYDATE_ORDERPARAMEDIS(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRXJM_BYDATE_ORDERPARAMEDIS]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrParamedisCd Varchar(20)
AS
BEGIN
	IF @pstrParamedisCd = '' 
	/*--Semua--*/
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,DR.dr_nm,
		MP.paramedis_cd,PRM.paramedis_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN MTK.datetime_trx ELSE A.datetime_in END AS datetime_trx,TK.treatment_nm,
		simrke.FN_RPT_GET_TRXJM_PARAMEDIS(MP.paramedis_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_paramedis
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_medical_paramedis MP ON MTK.medical_tindakan_seqno=MP.ref_proses_seqno
		JOIN trx_paramedis PRM ON MP.paramedis_cd=PRM.paramedis_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,DR.dr_nm,
		MP.paramedis_cd,PRM.paramedis_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN MTK.datetime_trx ELSE A.datetime_in END AS datetime_trx,TK.treatment_nm,
		simrke.FN_RPT_GET_TRXJM_PARAMEDIS(MP.paramedis_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_paramedis
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_medical_paramedis MP ON MTK.medical_tindakan_seqno=MP.ref_proses_seqno
		JOIN trx_paramedis PRM ON MP.paramedis_cd=PRM.paramedis_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND MTK.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MTK.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY PRM.paramedis_nm,A.medical_tp,datetime_trx,A.datetime_in
	ELSE
	/*--Per paramedis--*/
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,DR.dr_nm,
		MP.paramedis_cd,PRM.paramedis_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN MTK.datetime_trx ELSE A.datetime_in END AS datetime_trx,TK.treatment_nm,
		simrke.FN_RPT_GET_TRXJM_PARAMEDIS(MP.paramedis_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_paramedis
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_medical_paramedis MP ON MTK.medical_tindakan_seqno=MP.ref_proses_seqno
		JOIN trx_paramedis PRM ON MP.paramedis_cd=PRM.paramedis_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND MP.paramedis_cd=@pstrParamedisCd
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,DR.dr_nm,
		MP.paramedis_cd,PRM.paramedis_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN MTK.datetime_trx ELSE A.datetime_in END AS datetime_trx,TK.treatment_nm,
		simrke.FN_RPT_GET_TRXJM_PARAMEDIS(MP.paramedis_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_paramedis
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_medical_paramedis MP ON MTK.medical_tindakan_seqno=MP.ref_proses_seqno
		JOIN trx_paramedis PRM ON MP.paramedis_cd=PRM.paramedis_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND MP.paramedis_cd=@pstrParamedisCd
		AND MTK.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MTK.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		ORDER BY PRM.paramedis_nm,A.medical_tp,datetime_trx,A.datetime_in
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRXJM_BYDATE_ORDERUNIT(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRXJM_BYDATE_ORDERUNIT]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrUnitCd Varchar(20)
AS
BEGIN
	IF @pstrUnitCd = '' 
	/*--Semua--*/
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		CASE WHEN ISNULL(MTK.medunit_cd,'') <> '' THEN RUNIT.medunit_nm ELSE RJ.medunit_nm END AS unit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN MTK.datetime_trx ELSE A.datetime_in END AS datetime_trx,TK.treatment_nm,
		simrke.FN_RPT_GET_TRXJM_UNIT(MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_unit
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_unit_medis RUNIT ON MTK.medunit_cd=RUNIT.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		CASE WHEN ISNULL(MTK.medunit_cd,'') <> '' THEN RUNIT.medunit_nm ELSE BNG.bangsal_nm END AS unit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN MTK.datetime_trx ELSE A.datetime_in END AS datetime_trx,TK.treatment_nm,
		simrke.FN_RPT_GET_TRXJM_UNIT(MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_unit
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_unit_medis RUNIT ON MTK.medunit_cd=RUNIT.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND MTK.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MTK.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		--Laboratorium/Radiologi
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		UNIT.medunit_nm AS unit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MUNIT.datetime_trx IS NOT NULL THEN MUNIT.datetime_trx ELSE A.datetime_in END AS datetime_trx,MUITEM.medicalunit_nm AS treatment_nm,
		simrke.FN_RPT_GET_TRXJM_UNIT_UNITMEDIS(MUNIT.medicalunit_cd,A.medical_cd) AS jm_unit
		FROM trx_medical A
		JOIN trx_medical_unit MUNIT ON A.medical_cd=MUNIT.medical_cd
		JOIN trx_unitmedis_item MUITEM ON MUNIT.medicalunit_cd=MUITEM.medicalunit_cd
		JOIN trx_unit_medis UNIT ON MUITEM.medunit_cd=UNIT.medunit_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		AND MUNIT.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MUNIT.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		--End Laboratorium/Radiologi (unit medis)
		ORDER BY unit_nm,A.medical_tp,datetime_trx,A.datetime_in
	ELSE
	/*--Per unit--*/
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		CASE WHEN ISNULL(MTK.medunit_cd,'') <> '' THEN RUNIT.medunit_nm ELSE RJ.medunit_nm END AS unit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN MTK.datetime_trx ELSE A.datetime_in END AS datetime_trx,TK.treatment_nm,
		simrke.FN_RPT_GET_TRXJM_UNIT(MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_unit
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_unit_medis RUNIT ON MTK.medunit_cd=RUNIT.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND RJ.medunit_cd=@pstrUnitCd
		AND A.datetime_in>=simrke.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		CASE WHEN ISNULL(MTK.medunit_cd,'') <> '' THEN RUNIT.medunit_nm ELSE BNG.bangsal_nm END AS unit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN MTK.datetime_trx ELSE A.datetime_in END AS datetime_trx,TK.treatment_nm,
		simrke.FN_RPT_GET_TRXJM_UNIT(MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_unit
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_unit_medis RUNIT ON MTK.medunit_cd=RUNIT.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND BNG.bangsal_cd=@pstrUnitCd
		AND MTK.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MTK.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		--Laboratorium/Radiologi
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		PAS.pasien_nm,PAS.no_rm,INS.insurance_nm,
		UNIT.medunit_nm AS unit_nm,KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MUNIT.datetime_trx IS NOT NULL THEN MUNIT.datetime_trx ELSE A.datetime_in END AS datetime_trx,MUITEM.medicalunit_nm AS treatment_nm,
		simrke.FN_RPT_GET_TRXJM_UNIT_UNITMEDIS(MUNIT.medicalunit_cd,A.medical_cd) AS jm_unit
		FROM trx_medical A
		JOIN trx_medical_unit MUNIT ON A.medical_cd=MUNIT.medical_cd
		JOIN trx_unitmedis_item MUITEM ON MUNIT.medicalunit_cd=MUITEM.medicalunit_cd
		JOIN trx_unit_medis UNIT ON MUITEM.medunit_cd=UNIT.medunit_cd AND UNIT.medunit_cd=@pstrUnitCd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		AND MUNIT.datetime_trx>=simrke.FN_STRINGTODATE(@pdtDateStart) AND MUNIT.datetime_trx<=simrke.FN_STRINGTODATE(@pdtDateEnd)
		--End Laboratorium/Radiologi (unit medis)
		ORDER BY unit_nm,A.medical_tp,datetime_trx,A.datetime_in
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRXOBAT_BYDATE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRXOBAT_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
	OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
	OBAT.quantity,U.unit_nm,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
	CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total
	--MR.medical_data
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	--LEFT JOIN trx_medical_record MR ON A.medical_cd=MR.medical_cd
	JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
	JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
	LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
	LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
	WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND RESEP.proses_st='1'
	ORDER BY A.medical_tp,INS.insurance_nm,RESEP.resep_date
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_TRXOBATGENERIK_BYDATE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_TRXOBATGENERIK_BYDATE]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT A.medical_tp,COM.code_nm AS medical_tp_nm,simrke.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
	PAS.pasien_nm,PAS.no_rm,PAS.address,CASE WHEN INS.insurance_nm IS NULL THEN 'UMUM' ELSE INS.insurance_nm END AS insurance_nm,
	DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
	OBAT.item_cd,CASE WHEN OBAT.resep_tp='RESEP_TP_2' THEN OBAT.data_nm ELSE INV.item_nm END AS item_nm,
	OBAT.quantity,U.unit_nm,
	--CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS harga,
	--CASE WHEN OBAT.resep_tp='RESEP_TP_1' THEN OBAT.quantity * simrke.FN_GET_ITEM_PRICE(A.medical_cd,OBAT.item_cd) ELSE simrke.FN_GET_ITEM_PRICE_RACIK(A.medical_cd,OBAT.resep_seqno) END AS total,
	CASE WHEN ISNULL(INV.generic_st,'')='1' THEN 1 ELSE 0 END AS generik,
	CASE WHEN ISNULL(INV.generic_st,'')<>'1' THEN 1 ELSE 0 END AS paten,
	RESEP.medical_resep_seqno
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	JOIN trx_medical_resep RESEP ON A.medical_cd=RESEP.medical_cd
	JOIN trx_resep_data OBAT ON RESEP.medical_resep_seqno=OBAT.medical_resep_seqno
	LEFT JOIN inv_item_master INV ON OBAT.item_cd=INV.item_cd
	LEFT JOIN inv_unit U ON INV.unit_cd=U.unit_cd
	WHERE RESEP.resep_date>=simrke.FN_STRINGTODATE(@pdtDateStart) AND RESEP.resep_date<=simrke.FN_STRINGTODATE(@pdtDateEnd)
	AND RESEP.proses_st='1'
	ORDER BY A.medical_tp,INS.insurance_nm,RESEP.medical_resep_seqno
END;

DROP FUNCTION IF EXISTS SP_RPT_GET_USER() CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_GET_USER]
AS
BEGIN
	SELECT A.user_id, C.role_nm, A.user_nm
	FROM com_user A LEFT JOIN com_role_user B ON A.user_id=B.user_id
	JOIN com_role C ON B.role_cd=C.role_cd
	ORDER BY B.role_cd, A.user_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_MOBIRDITAS_RAWAT_INAP(Varchar,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_MOBIRDITAS_RAWAT_INAP] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrIcdCdStart Varchar(50),
@pstrIcdCdEnd Varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ICD.icd_cd,ICD.icd_nm,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 0 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 6 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age <= 6)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_0To6hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 0 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 6 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age <= 6)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_0To6hari,	
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 7 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 7 AND Pas2.age <= 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_7To28hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 7 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 7 AND Pas2.age <= 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_7To28hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 28 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_28hariTo1th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 28 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_28hariTo1th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 365 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age <= (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_1To4th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 365 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age <= (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_1To4th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (5*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age <= (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_5To14th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (5*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age <= (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_5To14th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (15*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age <= (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_15To24th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (15*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age <= (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_15To24th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (25*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age <= (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_25To44th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (25*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age <= (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_25To44th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (45*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age <= (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_45To64th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (45*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age <= (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_45To64th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365) )
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_lebih65th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365) )
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_lebih65th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
					AND PAS2.birth_date IS NULL AND Pas2.age IS NULL) as kosong,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND Med2.out_tp IS NOT NULL
			AND Pas2.gender_tp='GENDER_TP_01'
			AND ICD2.icd_cd = ICD.icd_cd
			AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_laki,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2 
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND Med2.out_tp IS NOT NULL
			AND Pas2.gender_tp='GENDER_TP_02'
			AND ICD2.icd_cd = ICD.icd_cd
			AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_perempuan,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND (Med2.out_tp <> 'OUT_TP_04' AND Med2.out_tp IS NOT NULL)
			AND ICD2.icd_cd = ICD.icd_cd
			AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_hidup,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND (Med2.out_tp = 'OUT_TP_04' AND Med2.out_tp IS NOT NULL)
			AND ICD2.icd_cd = ICD.icd_cd
			AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_mati,	
COUNT(MR.medical_record_seqno) as total
FROM trx_medical_record MR
LEFT JOIN trx_medical Med ON Med.medical_cd = MR.medical_cd  
LEFT JOIN trx_pasien Pas ON Pas.pasien_cd = MR.pasien_cd
--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW ON VW.icd_cd = MR.icd_cd
LEFT JOIN trx_icd ICD ON ICD.icd_cd = MR.icd_cd
WHERE med.medical_tp='MEDICAL_TP_02'
AND Med.out_tp IS NOT NULL
AND ICD.icd_cd IS NOT NULL
AND (ICD.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
GROUP BY ICD.icd_cd, ICD.icd_nm
ORDER BY ICD.icd_cd
END;

DROP FUNCTION IF EXISTS SP_RPT_MOBIRDITAS_RAWAT_JALAN(Varchar,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_MOBIRDITAS_RAWAT_JALAN] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10),
@pstrIcdCdStart Varchar(50),
@pstrIcdCdEnd Varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ICD.icd_cd,ICD.icd_nm,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 0 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 6 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age <= 6)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_0To6hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 0 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 6 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age <= 6)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_0To6hari,	
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 7 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 7 AND Pas2.age <= 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_7To28hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 7 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 7 AND Pas2.age <= 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_7To28hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 28 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_28hariTo1th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 28 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_28hariTo1th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 365 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age <= (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_1To4th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 365 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age <= (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_1To4th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (5*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age <= (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_5To14th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (5*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age <= (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_5To14th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (15*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age <= (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_15To24th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (15*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age <= (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_15To24th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (25*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age <= (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_25To44th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (25*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age <= (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_25To44th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (45*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age <= (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_45To64th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (45*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age <= (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_45To64th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365) )
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_lebih65th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365) )
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_lebih65th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND ICD2.icd_cd = ICD.icd_cd
					AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
					AND PAS2.birth_date IS NULL AND Pas2.age IS NULL) as kosong,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND Med2.out_tp IS NOT NULL
			AND Pas2.gender_tp='GENDER_TP_01'
			AND ICD2.icd_cd = ICD.icd_cd
			AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_laki,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2 
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND Med2.out_tp IS NOT NULL
			AND Pas2.gender_tp='GENDER_TP_02'
			AND ICD2.icd_cd = ICD.icd_cd
			AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_perempuan,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND (Med2.out_tp <> 'OUT_TP_04' AND Med2.out_tp IS NOT NULL)
			AND ICD2.icd_cd = ICD.icd_cd
			AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_hidup,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND (Med2.out_tp = 'OUT_TP_04' AND Med2.out_tp IS NOT NULL)
			AND ICD2.icd_cd = ICD.icd_cd
			AND (ICD2.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_mati,	
COUNT(MR.medical_record_seqno) as total
FROM trx_medical_record MR
LEFT JOIN trx_medical Med ON Med.medical_cd = MR.medical_cd  
LEFT JOIN trx_pasien Pas ON Pas.pasien_cd = MR.pasien_cd
--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW ON VW.icd_cd = MR.icd_cd
LEFT JOIN trx_icd ICD ON ICD.icd_cd = MR.icd_cd
WHERE med.medical_tp='MEDICAL_TP_01'
AND Med.out_tp IS NOT NULL
AND ICD.icd_cd IS NOT NULL
AND (ICD.icd_cd BETWEEN @pstrIcdCdStart AND @pstrIcdCdEnd)
AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
GROUP BY ICD.icd_cd, ICD.icd_nm
ORDER BY ICD.icd_cd
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_1_2_INDIKATOR_PELAYANAN_RUMAH_SAKIT(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_1_2_INDIKATOR_PELAYANAN_RUMAH_SAKIT] 
@pintMonth int,
@pintYear int
AS
BEGIN
	DECLARE @dtDateStart Datetime
	DECLARE @dtDateEnd Datetime
	
	SET @dtDateStart = CONVERT(DATETIME,simrke.FN_FORMAT_STRING(@pintMonth,'0',2) + '/01/' + simrke.FN_FORMAT_STRING(@pintYear,'0',4))
	SET @dtDateEnd = DATEADD(S,-1,DATEADD(MM, DATEDIFF(M,0,@dtDateStart)+1,0))
	
	SELECT 
	A.kelas_cd,A.kelas_nm,
	COUNT(B.ruang_cd) AS total_kamar,
	simrke.FN_CHECK_NULL(simrke.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd,@dtDateStart,@dtDateEnd)) AS total_pasien,
	simrke.FN_CHECK_NULL(simrke.FN_RPT_TOTAL_HARIRAWAT(A.kelas_cd,@dtDateStart,@dtDateEnd)) AS total_harirawat,
	simrke.FN_CHECK_NULL(simrke.FN_RPT_TOTAL_DEATH_OUTTP11(A.kelas_cd,@dtDateStart,@dtDateEnd)) AS total_meninggal,
	DATEPART(dd,@dtDateEnd) AS day_month
	FROM trx_kelas A 
	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd 
	GROUP BY A.kelas_cd,A.kelas_nm
	ORDER BY A.kelas_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_1_3_FASILITAS_TEMPAT_TIDUR_RAWAT_INAP(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_1_3_FASILITAS_TEMPAT_TIDUR_RAWAT_INAP] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT
		trx_ruang.bangsal_cd,
		trx_bangsal.bangsal_nm,
		COUNT(1)AS jumlah_tt,
		SUM(CASE WHEN trx_ruang.kelas_cd = 'KLVVIP' THEN 1 ELSE 0 END)AS vvip,
		SUM(CASE WHEN trx_ruang.kelas_cd = 'KLVIP' THEN 1 ELSE 0 END)AS vip,
		SUM(CASE WHEN trx_ruang.kelas_cd = 'KL01' THEN 1 ELSE 0 END)AS kelasI,
		SUM(CASE WHEN trx_ruang.kelas_cd = 'KL02' THEN 1 ELSE 0 END)AS kelasII,
		SUM(CASE WHEN trx_ruang.kelas_cd = 'KL03' THEN 1 ELSE 0 END)AS kelasIII,
		SUM(CASE WHEN trx_ruang.kelas_cd NOT IN ('KLVVIP','KLVIP','KL01','KL02','KL03') THEN 1 ELSE 0 END) AS kelasLAIN
	FROM
		trx_ruang
	JOIN trx_bangsal ON trx_ruang.bangsal_cd = trx_bangsal.bangsal_cd
	WHERE simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(trx_ruang.modi_datetime)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(trx_ruang.modi_datetime)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
	GROUP BY
		trx_ruang.bangsal_cd,
		trx_bangsal.bangsal_nm
	
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_2_KETENAGAAN() CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_2_KETENAGAAN] 
AS
BEGIN
	SELECT A.paramedis_cd, A.paramedis_nm, A.paramedis_tp, COMCD.code_nm AS tipe_nm
	FROM trx_paramedis A
	LEFT JOIN com_code COMCD On A.paramedis_tp=COMCD.com_cd
	ORDER BY COMCD.code_nm, A.paramedis_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_1_KEGIATAN_PELAYANAN_RAWAT_INAP(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_1_KEGIATAN_PELAYANAN_RAWAT_INAP] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select 
	um.medunit_cd, um.medunit_nm,
		(select count(medical_cd) from trx_medical where 
		YEAR(datetime_in) = YEAR(simrke.FN_STRINGTODATE(@pdtDateStart))-1
		AND MONTH(datetime_in) = 12
		AND DAY(datetime_in) = 31
		 and medunit_cd=um.medunit_cd) as pasien_awal,
		(select count(medical_cd) from trx_medical where  
		simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) >= simrke.FN_STRINGTODATE(@pdtDateStart) 
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
		 and medunit_cd=um.medunit_cd) as pasien_masuk,
		(select count(medical_cd) from trx_medical where 
		simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
		 and medunit_cd=um.medunit_cd and out_tp<>'OUT_TP_04') as pasien_keluar_hidup,
		(select count(medical_cd) from trx_medical where 
		simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
		 and medunit_cd=um.medunit_cd and out_tp='OUT_TP_04' and DATEDIFF(HOUR, datetime_in, datetime_out) <= 48) as keluar_mati_krg_28_jam,
		(select count(medical_cd) from trx_medical where 
		simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) and medunit_cd=um.medunit_cd and out_tp='OUT_TP_04' and DATEDIFF(HOUR, datetime_in, datetime_out) > 48) as keluar_mati_lbh_28_jam,
		(select SUM(DATEDIFF(HOUR, datetime_in, datetime_out)) from trx_medical where medunit_cd=um.medunit_cd and out_tp<>'') as lama_dirawat,
		(select count(medical_cd) from trx_medical where YEAR(datetime_in)=YEAR(simrke.FN_STRINGTODATE(@pdtDateStart)) 
		and MONTH(datetime_in)=12 and DAY(datetime_in)=31 and medunit_cd=um.medunit_cd) as pasien_akhir_satu_thn,
		(select SUM(DATEDIFF(HOUR, datetime_in, datetime_out)) from trx_medical where medunit_cd=um.medunit_cd) as jml_hari_perawatan,
		(select count(m.medical_cd) from trx_medical m
									join trx_ruang r on m.ruang_cd=r.ruang_cd
									join trx_kelas k on k.kelas_cd=r.kelas_cd where 
									simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
									AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
									and medunit_cd=um.medunit_cd and k.kelas_cd='KLVVIP') as vvip,
		(select count(m.medical_cd) from trx_medical m
									join trx_ruang r on m.ruang_cd=r.ruang_cd
									join trx_kelas k on k.kelas_cd=r.kelas_cd where 
									simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
									AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
									and medunit_cd=um.medunit_cd and k.kelas_cd='KLVIP') as vip,
		(select count(m.medical_cd) from trx_medical m
									join trx_ruang r on m.ruang_cd=r.ruang_cd
									join trx_kelas k on k.kelas_cd=r.kelas_cd where 
									simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
									AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
									and medunit_cd=um.medunit_cd and k.kelas_cd='KL01') as kelas_1,
		(select count(m.medical_cd) from trx_medical m
									join trx_ruang r on m.ruang_cd=r.ruang_cd
									join trx_kelas k on k.kelas_cd=r.kelas_cd where 
									simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
									AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
									 and medunit_cd=um.medunit_cd and k.kelas_cd='KL02') as kelas_2,
		(select count(m.medical_cd) from trx_medical m
									join trx_ruang r on m.ruang_cd=r.ruang_cd
									join trx_kelas k on k.kelas_cd=r.kelas_cd where 
									simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
									AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
									 and medunit_cd=um.medunit_cd and k.kelas_cd='KL03') as kelas_3,
		(select count(m.medical_cd) from trx_medical m
									join trx_ruang r on m.ruang_cd=r.ruang_cd
									join trx_kelas k on k.kelas_cd=r.kelas_cd where 
									simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
									AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
									 and medunit_cd=um.medunit_cd and k.kelas_cd='KLKHUSUS') as kelas_khusus
	from 
	trx_unit_medis um
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_10_KEGIATAN_PELAYANAN_KHUSUS(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_10_KEGIATAN_PELAYANAN_KHUSUS] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT  RTRIM(LTRIM(A.medical_note)) as Jenis_Kegiatan,COUNT(A.medical_note) AS Jumlah
	FROM simrke.trx_medical_tindakan A JOIN simrke.trx_medical B ON A.medical_cd = B.medical_cd
	WHERE  simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
	OR A.medical_note LIKE '%elektro%'
	OR A.medical_note LIKE '%kardiographi%'
	OR A.medical_note LIKE '%myographi%' 
	OR A.medical_note LIKE '%echo%'
	OR A.medical_note LIKE '%cardiographi%'
	OR A.medical_note LIKE '%endoskopi%'
	OR A.medical_note LIKE '%hemodialisa%'
	OR A.medical_note LIKE '%densometri%'
	OR A.medical_note LIKE '%tulang%'
	OR A.medical_note LIKE '%densometri tulang%'
	OR A.medical_note LIKE '%pungsi%'
	OR A.medical_note LIKE '%spirometri%'
	OR A.medical_note LIKE '%kulit%'
	OR A.medical_note LIKE '%alergi%'
	OR A.medical_note LIKE '%histamin%'
	OR A.medical_note LIKE '%topometri%'
	OR A.medical_note LIKE '%akupuntur%'
	OR A.medical_note LIKE '%hiperbarik%'
	OR A.medical_note LIKE '%herbal%'
	OR A.medical_note LIKE '%jamu%'
	GROUP BY A.medical_note
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_11_KEGIATAN_KESEHATAN_JIWA(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_11_KEGIATAN_KESEHATAN_JIWA] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  RTRIM(LTRIM(A.medical_note)) as Jenis_Kegiatan,COUNT(A.medical_note) AS Jumlah
		FROM simrke.trx_medical_tindakan A JOIN simrke.trx_medical B ON A.medical_cd = B.medical_cd
		WHERE B.medunit_cd = 'POLISPJW' 
		AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
		GROUP BY A.medical_note
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_13_PENGADAAN_OBAT_PENULISAN_DAN_PELAYANAN_RESEP(int,int) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_13_PENGADAAN_OBAT_PENULISAN_DAN_PELAYANAN_RESEP] 
	-- Add the parameters for the stored procedure here
@pintMonth int,
@pintYear int
AS
BEGIN
	SELECT r.resep_tp, com.code_nm, iim.generic_st,
		((select COUNT(RD.item_cd) from simrke.inv_item_master IIM2
		left join simrke.trx_resep_data RD on RD.item_cd=IIM2.item_cd
		where RD.resep_tp=r.resep_tp and IIM2.generic_st=iim.generic_st
		and MONTH(RD.modi_datetime)=@pintMonth and YEAR(RD.modi_datetime)=@pintYear ) + 
		(select SUM(MA.quantity) from simrke.trx_medical_alkes MA
		left join simrke.trx_resep_data RD on MA.item_cd=RD.item_cd
		left join simrke.inv_item_master IIM2 on RD.item_cd=IIM2.item_cd
		where RD.resep_tp=r.resep_tp and IIM2.generic_st=iim.generic_st
		and MONTH(RD.modi_datetime)=@pintMonth and YEAR(RD.modi_datetime)=@pintYear )) as Jumlah_Total,
		
		(select COUNT(RD.item_cd) from simrke.inv_item_master IIM2
		left join simrke.trx_resep_data RD on RD.item_cd=IIM2.item_cd
		where RD.resep_tp=r.resep_tp and IIM2.generic_st=iim.generic_st
		and MONTH(RD.modi_datetime)=@pintMonth and YEAR(RD.modi_datetime)=@pintYear) as Jumlah_Di_RS,
		
		(select SUM(MA.quantity) from simrke.trx_medical_alkes MA
		left join simrke.trx_resep_data RD on MA.item_cd=RD.item_cd
		left join simrke.inv_item_master IIM2 on RD.item_cd=IIM2.item_cd
		where RD.resep_tp=r.resep_tp and IIM2.generic_st=iim.generic_st
		and MONTH(RD.modi_datetime)=@pintMonth and YEAR(RD.modi_datetime)=@pintYear) as Jumlah_Dipakai
		
	FROM [simrke].[trx_resep_data] r
	JOIN simrke.com_code com ON r.resep_tp=com.com_cd
	LEFT JOIN simrke.inv_item_master iim ON r.item_cd=iim.item_cd
	WHERE MONTH(r.modi_datetime)=@pintMonth AND YEAR(r.modi_datetime)=@pintYear
	GROUP BY r.resep_tp, com.code_nm, iim.generic_st;

	SELECT TOP 1000 r.resep_tp, com.code_nm,iim.generic_st, 
	COUNT(r.item_cd) as Jumlah_Di_RS,
		(SELECT COUNT(Y.item_cd) FROM simrke.trx_resep_data X 
		JOIN simrke.trx_medical_alkes Y ON X.item_cd=Y.item_cd 
		JOIN simrke.trx_medical Z ON Z.medical_cd=Y.medical_cd
		JOIN simrke.inv_item_master X2 ON X.item_cd=X2.item_cd
		WHERE Z.medical_tp='MEDICAL_TP_01' AND X.resep_tp=r.resep_tp AND X2.generic_st=iim.generic_st
		AND MONTH(Z.datetime_in)=@pintMonth AND YEAR(Z.datetime_in)=@pintYear ) as Jml_Di_RawatJalan,
		
		(SELECT COUNT(Y.item_cd) FROM simrke.trx_resep_data X 
		JOIN simrke.trx_medical_alkes Y ON X.item_cd=Y.item_cd 
		JOIN simrke.trx_medical Z ON Z.medical_cd=Y.medical_cd
		JOIN simrke.inv_item_master X2 ON X.item_cd=X2.item_cd
		WHERE Z.medical_tp='MEDICAL_TP_02' AND X.resep_tp=r.resep_tp AND X2.generic_st=iim.generic_st
		AND MONTH(Z.datetime_in)=@pintMonth AND YEAR(Z.datetime_in)=@pintYear ) as Jml_Di_RawatInap,
		
		(SELECT COUNT(Y.item_cd) FROM simrke.trx_resep_data X 
		JOIN simrke.trx_medical_alkes Y ON X.item_cd=Y.item_cd 
		JOIN simrke.trx_medical Z ON Z.medical_cd=Y.medical_cd
		JOIN simrke.inv_item_master X2 ON X.item_cd=X2.item_cd
		WHERE Z.medunit_cd='POLIUGD' AND X.resep_tp=r.resep_tp AND X2.generic_st=iim.generic_st
		AND MONTH(Z.datetime_in)=@pintMonth AND YEAR(Z.datetime_in)=@pintYear ) as Jml_Di_UGD
	
	FROM [simrke].[trx_resep_data] r
	JOIN simrke.com_code com ON r.resep_tp=com.com_cd
	LEFT JOIN simrke.inv_item_master iim ON r.item_cd=iim.item_cd
	WHERE MONTH(r.modi_datetime)=@pintMonth AND YEAR(r.modi_datetime)=@pintYear 
	GROUP BY r.resep_tp,com.code_nm,iim.generic_st
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_14_KEGIATAN_RUJUKAN(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_14_KEGIATAN_RUJUKAN] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT U.medunit_cd, U.medunit_nm,
		(SELECT COUNT(X.medical_cd) 
			FROM simrke.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND X.reff_tp = 'REFF_TP_05' 
			AND X.out_tp='OUT_TP_07' 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
			) as Diterima_Dari_Puskesmas,
		(SELECT COUNT(X.medical_cd) 
			FROM simrke.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND (X.reff_tp = 'REFF_TP_01' 
			OR X.reff_tp = 'REFF_TP_02' 
			OR X.reff_tp = 'REFF_TP_07') 
			AND X.out_tp='OUT_TP_06' 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
			) as Diterima_Dari_FasKes_Lain,
		(SELECT COUNT(X.medical_cd) 
			FROM simrke.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND X.reff_tp = 'REFF_TP_07' 
			AND X.out_tp='OUT_TP_07' 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
			) as Diterima_Dari_RS_Lain,
		(SELECT COUNT(X.medical_cd) 
			FROM simrke.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND X.reff_tp = 'REFF_TP_05' 
			AND X.out_tp='OUT_TP_06' 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
			) as Dikembalikan_Ke_Puskesmas,
		(SELECT COUNT(X.medical_cd) 
			FROM simrke.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND (X.reff_tp = 'REFF_TP_01' 
			OR X.reff_tp = 'REFF_TP_02' 
			OR X.reff_tp = 'REFF_TP_06' 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
			AND X.out_tp='OUT_TP_06')) as Dikembalikan_Ke_FasKes_Lain,
		(SELECT COUNT(X.medical_cd) 
			FROM simrke.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND X.reff_tp = 'REFF_TP_07' 
			AND X.out_tp='OUT_TP_06' 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
			) as Dikembalikan_Ke_RS_Lain,
		(SELECT COUNT(X.medical_cd) 
			FROM simrke.trx_medical X 
			WHERE X.medunit_cd=U.medunit_cd 
			AND X.out_tp ='OUT_TP_02' 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart)
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(X.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
			) as Dirujuk
	FROM simrke.trx_unit_medis U  
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_15_CARA_BAYAR(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_15_CARA_BAYAR] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT COM.code_nm,
		(SELECT COUNT(med.pasien_cd) 
			FROM simrke.trx_medical med 
			JOIN simrke.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_02' 
			AND med.out_tp IS NOT NULL 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
			) as RI_PasienKeluar,
		(SELECT COUNT(med.pasien_cd) 
			FROM simrke.trx_medical med 
			JOIN simrke.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_02' 
			AND med.out_tp IS NULL 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
			) as RI_LamDirawat,
		(SELECT COUNT(med.pasien_cd) 
			FROM simrke.trx_medical med 
			JOIN simrke.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_02' 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
			) as RawatInap,
		(SELECT COUNT(med.pasien_cd) 
			FROM simrke.trx_medical med 
			JOIN simrke.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_01' 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
			) as RawatJalan,
		(SELECT COUNT(med.pasien_cd) 
			FROM simrke.trx_medical med 
			JOIN simrke.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			JOIN simrke.trx_unit_medis um ON med.medunit_cd=um.medunit_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_01' 
			AND um.medicalunit_tp='MEDICALUNIT_TP_2' 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
			) as RJ_Laboratorium,
		(SELECT COUNT(med.pasien_cd) 
			FROM simrke.trx_medical med 
			JOIN simrke.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			JOIN simrke.trx_unit_medis um ON med.medunit_cd=um.medunit_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_01' 
			AND um.medicalunit_tp='MEDICALUNIT_TP_3' 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
			) as RJ_Radiologi,
		(SELECT COUNT(med.pasien_cd) 
			FROM simrke.trx_medical med 
			JOIN simrke.trx_settlement sett ON med.pasien_cd=sett.pasien_cd
			JOIN simrke.trx_unit_medis um ON med.medunit_cd=um.medunit_cd
			WHERE sett.payment_tp=COM.com_cd 
			AND med.medical_tp='MEDICAL_TP_01' 
			AND um.medicalunit_tp='MEDICALUNIT_TP_1' 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) 
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MED.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
			) as RJ_Lain 
	FROM simrke.com_code COM
	WHERE COM.code_group='PAYMENT_TP'
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_2_KUNJUNGAN_RAWAT_DARURAT(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_2_KUNJUNGAN_RAWAT_DARURAT] 
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT B.medunit_cd, B.medunit_nm,
		(SELECT COUNT(medical_cd) 
			FROM simrke.trx_medical Y 
			WHERE simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.reff_tp IS NOT NULL 
			AND Y.referensi_cd IS NOT NULL) as Total_Pasien_Rujukan,
		(SELECT COUNT(medical_cd) 
			FROM simrke.trx_medical Y 
			WHERE simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.reff_tp IS NULL 
			AND Y.referensi_cd IS NULL) as Total_Pasien_Non_Rujukan,	
		(SELECT COUNT(medical_cd) 
			FROM simrke.trx_medical Y 
			WHERE simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.out_tp ='OUT_TP_01') as Dirawat,
		(SELECT COUNT(medical_cd) 
			FROM simrke.trx_medical Y 
			WHERE simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.out_tp ='OUT_TP_02') as Dirujuk,
		(SELECT COUNT(medical_cd) 
			FROM simrke.trx_medical Y 
			WHERE simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.out_tp ='OUT_TP_03') as Pulang,
		(SELECT COUNT(medical_cd) 
			FROM simrke.trx_medical Y 
			WHERE simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.out_tp ='OUT_TP_04') as Mati_Di_IGD,
		(SELECT COUNT(medical_cd) 
			FROM simrke.trx_medical Y 
			WHERE simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(Y.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
			AND Y.medunit_cd=B.medunit_cd 
			AND Y.out_tp ='OUT_TP_05') as DOA
	FROM simrke.trx_unit_medis B 
	WHERE B.medunit_cd = 'POLISPB'
	OR B.medunit_cd = 'POLISPOG'
	OR B.medunit_cd = 'POLISPA'
	OR B.medunit_cd = 'POLISPJW'
	OR B.medunit_cd = 'POLIUGD'
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_3_KEGIATAN_KESEHATAN_GIGI_MULUT(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_3_KEGIATAN_KESEHATAN_GIGI_MULUT] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		RTRIM(LTRIM(C.treatment_nm)) AS Jenis_Kegiatan,
		COUNT(1)AS Jumlah
	FROM
		simrke.trx_medical_tindakan A
	JOIN simrke.trx_medical B ON A.medical_cd = B.medical_cd
	JOIN trx_tindakan C ON A.treatment_cd = C.treatment_cd
	WHERE
		B.medunit_cd = 'POLIGIGI'
	AND simrke.FN_STRINGTODATE(
		simrke.FN_FORMATDATE(B.datetime_in)
	)>= simrke.FN_STRINGTODATE(@pdtDateStart)
	AND simrke.FN_STRINGTODATE(
		simrke.FN_FORMATDATE(B.datetime_in)
	)<= simrke.FN_STRINGTODATE(@pdtDateEnd)
	GROUP BY
		C.treatment_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_4_KEGIATAN_KEBIDANAN(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_4_KEGIATAN_KEBIDANAN] 
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT  A.medical_note,
	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.reff_tp = 'REFF_TP_07') as R_Med_Rumah_Sakit,
	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.reff_tp = 'REFF_TP_03') as R_Med_Bidan,
	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.reff_tp = 'REFF_TP_05') as R_Med_Puskesmas,
	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND (Y.reff_tp = 'REFF_TP_01' OR Y.reff_tp = 'REFF_TP_02' OR Y.reff_tp = 'REFF_TP_06') ) as R_Faskes_Lain,

	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.out_tp ='OUT_TP_01'
	AND (Y.reff_tp = 'REFF_TP_01' OR Y.reff_tp = 'REFF_TP_02' OR Y.reff_tp = 'REFF_TP_03' OR Y.reff_tp = 'REFF_TP_05' OR Y.reff_tp = 'REFF_TP_06' OR Y.reff_tp = 'REFF_TP_07') ) as R_Med_Jumlah_Hidup,
	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.out_tp ='OUT_TP_04'
	AND (Y.reff_tp = 'REFF_TP_01' OR Y.reff_tp = 'REFF_TP_02' OR Y.reff_tp = 'REFF_TP_03' OR Y.reff_tp = 'REFF_TP_05' OR Y.reff_tp = 'REFF_TP_06' OR Y.reff_tp = 'REFF_TP_07') ) as R_Med_Jumlah_Mati,
	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note 
	AND (Y.reff_tp = 'REFF_TP_01' OR Y.reff_tp = 'REFF_TP_02' OR Y.reff_tp = 'REFF_TP_03' OR Y.reff_tp = 'REFF_TP_05' OR Y.reff_tp = 'REFF_TP_06' OR Y.reff_tp = 'REFF_TP_07') ) as R_Med_Jumlah_Total,

	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.out_tp ='OUT_TP_01' AND (Y.reff_tp = 'REFF_TP_00' OR Y.reff_tp = 'REFF_TP_04') ) as R_NonMed_Jumlah_Hidup,
	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.out_tp ='OUT_TP_04' AND (Y.reff_tp = 'REFF_TP_00' OR Y.reff_tp = 'REFF_TP_04') ) as R_NonMed_Jumlah_Mati,
	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND (Y.reff_tp = 'REFF_TP_00' OR Y.reff_tp = 'REFF_TP_04') ) as R_NonMed_Jumlah_Total,

	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.out_tp ='OUT_TP_01' AND Y.reff_tp IS NULL AND Y.referensi_cd IS NULL) as Non_R_Jumlah_Hidup,
	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.out_tp ='OUT_TP_04' AND Y.reff_tp IS NULL AND Y.referensi_cd IS NULL) as Non_R_Jumlah_Mati,
	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.reff_tp IS NULL AND Y.referensi_cd IS NULL) as Non_R_Jumlah_Total,

	(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.out_tp ='OUT_TP_02') as Dirujuk
	FROM simrke.trx_medical_tindakan  A JOIN simrke .trx_medical B ON A.medical_cd =B.medical_cd 
	WHERE B.medunit_cd = 'POLISPOG' 
	AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
	GROUP BY A.medical_note
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_5_KEGIATAN_PERINATOLOGI(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_5_KEGIATAN_PERINATOLOGI] 
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT  A.medical_note,
		(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.reff_tp = 'REFF_TP_07') as R_Med_Rumah_Sakit,
		(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.reff_tp = 'REFF_TP_03') as R_Med_Bidan,
		(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.reff_tp = 'REFF_TP_05') as R_Med_Puskesmas,
		(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND (Y.reff_tp = 'REFF_TP_01' OR Y.reff_tp = 'REFF_TP_02' OR Y.reff_tp = 'REFF_TP_06') ) as R_Faskes_Lain,

		(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.out_tp ='OUT_TP_04'
		AND (Y.reff_tp = 'REFF_TP_01' OR Y.reff_tp = 'REFF_TP_02' OR Y.reff_tp = 'REFF_TP_03' OR Y.reff_tp = 'REFF_TP_05' OR Y.reff_tp = 'REFF_TP_06' OR Y.reff_tp = 'REFF_TP_07') ) as R_Med_Jumlah_Mati,
		(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note 
		AND (Y.reff_tp = 'REFF_TP_01' OR Y.reff_tp = 'REFF_TP_02' OR Y.reff_tp = 'REFF_TP_03' OR Y.reff_tp = 'REFF_TP_05' OR Y.reff_tp = 'REFF_TP_06' OR Y.reff_tp = 'REFF_TP_07') ) as R_Med_Jumlah_Total,

		(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.out_tp ='OUT_TP_04' AND (Y.reff_tp = 'REFF_TP_00' OR Y.reff_tp = 'REFF_TP_04') ) as R_NonMed_Jumlah_Mati,
		(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND (Y.reff_tp = 'REFF_TP_00' OR Y.reff_tp = 'REFF_TP_04') ) as R_NonMed_Jumlah_Total,

		(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.out_tp ='OUT_TP_04' AND Y.reff_tp IS NULL AND Y.referensi_cd IS NULL) as Non_R_Jumlah_Mati,
		(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.reff_tp IS NULL AND Y.referensi_cd IS NULL) as Non_R_Jumlah_Total,

		(SELECT COUNT(medical_note) FROM simrke.trx_medical_tindakan X JOIN simrke.trx_medical Y ON X.medical_cd =Y.medical_cd  WHERE medical_note = A.medical_note AND Y.out_tp ='OUT_TP_02') as Dirujuk
	FROM simrke.trx_medical_tindakan  A JOIN simrke.trx_medical B ON A.medical_cd =B.medical_cd 
	WHERE B.medunit_cd = 'POLITK' 
	AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
	GROUP BY A.medical_note
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_6_KEGIATAN_PEMBEDAHAN(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_6_KEGIATAN_PEMBEDAHAN] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		RTRIM(LTRIM(C.treatment_nm)) AS Jenis_Kegiatan,
		COUNT(1)AS Jumlah
	FROM
		simrke.trx_medical_tindakan A
	JOIN simrke.trx_medical B ON A.medical_cd = B.medical_cd
	JOIN trx_tindakan C ON A.treatment_cd = C.treatment_cd
	WHERE
		B.medunit_cd = 'POLISPB'
	AND simrke.FN_STRINGTODATE(
		simrke.FN_FORMATDATE(B.datetime_in)
	)>= simrke.FN_STRINGTODATE(@pdtDateStart)
	AND simrke.FN_STRINGTODATE(
		simrke.FN_FORMATDATE(B.datetime_in)
	)<= simrke.FN_STRINGTODATE(@pdtDateEnd)
	GROUP BY
		C.treatment_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_7_KEGIATAN_RADIOLOGI(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_7_KEGIATAN_RADIOLOGI] 
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT
		medicalunit_nm AS Jenis_Kegiatan,
		COUNT(1)AS Jumlah
	FROM
		trx_medical_unit mast
	JOIN trx_unitmedis_item a ON a.medicalunit_cd = mast.medicalunit_cd
	AND a.medunit_cd = 'RADIO00'
	AND simrke.FN_STRINGTODATE(
		simrke.FN_FORMATDATE(mast.datetime_trx)
	)>= simrke.FN_STRINGTODATE(@pdtDateStart)
	AND simrke.FN_STRINGTODATE(
		simrke.FN_FORMATDATE(mast.datetime_trx)
	)<= simrke.FN_STRINGTODATE(@pdtDateEnd)
	GROUP BY
		medicalunit_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_8_PEMERIKSAAN_LABORATORIUM(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_8_PEMERIKSAAN_LABORATORIUM] 
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT
		medicalunit_nm AS Jenis_Kegiatan,
		COUNT(1)AS Jumlah
	FROM
		trx_medical_unit mast
	JOIN trx_unitmedis_item a ON a.medicalunit_cd = mast.medicalunit_cd
	AND a.medunit_cd = 'LAB00'
	AND simrke.FN_STRINGTODATE(
		simrke.FN_FORMATDATE(mast.datetime_trx)
	)>= simrke.FN_STRINGTODATE(@pdtDateStart)
	AND simrke.FN_STRINGTODATE(
		simrke.FN_FORMATDATE(mast.datetime_trx)
	)<= simrke.FN_STRINGTODATE(@pdtDateEnd)
	GROUP BY
		medicalunit_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_3_9_PELAYANAN_REHABILITASI_MEDIK(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_3_9_PELAYANAN_REHABILITASI_MEDIK] 
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT  RTRIM(LTRIM(A.medical_note)) as Jenis_Kegiatan,COUNT(A.medical_note) AS Jumlah
	FROM simrke.trx_medical_tindakan A JOIN simrke.trx_medical B ON A.medical_cd = B.medical_cd
	WHERE B.medunit_cd = 'POLIREHAB' 
	AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_in)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(B.datetime_in)) <=simrke.FN_STRINGTODATE(@pdtDateEnd) 
	GROUP BY A.medical_note
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_4_A_PENYAKIT_RAWAT_INAP(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_4_A_PENYAKIT_RAWAT_INAP] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ICD.icd_cd,ICD.icd_nm,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 0 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 6 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age <= 6)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_0To6hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 0 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 6 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age <= 6)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_0To6hari,	
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 7 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 7 AND Pas2.age <= 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_7To28hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 7 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 7 AND Pas2.age <= 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_7To28hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 28 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_28hariTo1th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 28 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_28hariTo1th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 365 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age <= (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_1To4th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 365 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age <= (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_1To4th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (5*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age <= (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_5To14th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (5*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age <= (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_5To14th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (15*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age <= (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_15To24th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (15*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age <= (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_15To24th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (25*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age <= (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_25To44th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (25*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age <= (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_25To44th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (45*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age <= (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_45To64th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (45*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age <= (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_45To64th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365) )
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_lebih65th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365) )
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_lebih65th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE Med2.medical_tp='MEDICAL_TP_02'
					AND Med2.out_tp IS NOT NULL
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
					AND PAS2.birth_date IS NULL AND Pas2.age IS NULL) as kosong,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND Med2.out_tp IS NOT NULL
			AND Pas2.gender_tp='GENDER_TP_01'
			AND ICD2.icd_cd = icd.icd_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_laki,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2 
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND Med2.out_tp IS NOT NULL
			AND Pas2.gender_tp='GENDER_TP_02'
			AND ICD2.icd_cd = icd.icd_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_perempuan,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND (Med2.out_tp <> 'OUT_TP_04' AND Med2.out_tp IS NOT NULL)
			AND ICD2.icd_cd = icd.icd_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_hidup,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND (Med2.out_tp = 'OUT_TP_04' AND Med2.out_tp IS NOT NULL)
			AND ICD2.icd_cd = icd.icd_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_mati,	
COUNT(MR.medical_record_seqno) as total
FROM trx_medical_record MR
LEFT JOIN trx_medical Med ON Med.medical_cd = MR.medical_cd  
LEFT JOIN trx_pasien Pas ON Pas.pasien_cd = MR.pasien_cd
LEFT JOIN trx_icd ICD ON ICD.icd_cd = MR.icd_cd
WHERE med.medical_tp='MEDICAL_TP_02'
AND Med.out_tp IS NOT NULL
AND ICD.icd_cd IS NOT NULL
AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
GROUP BY ICD.icd_cd, ICD.icd_nm
ORDER BY ICD.icd_cd
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_4_B_PENYAKIT_RAWAT_JALAN(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_4_B_PENYAKIT_RAWAT_JALAN] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--Rawat Jalan
SELECT ICD.icd_cd,ICD.icd_nm,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 0 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 6 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age <= 6)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_0To6hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 0 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 6 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age <= 6)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_0To6hari,	
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 7 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 7 AND Pas2.age <= 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_7To28hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 7 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 7 AND Pas2.age <= 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_7To28hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 28 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_28hariTo1th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 28 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_28hariTo1th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 365 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age <= (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_1To4th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 365 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age <= (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_1To4th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (5*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age <= (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_5To14th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (5*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age <= (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_5To14th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (15*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age <= (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_15To24th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (15*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age <= (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_15To24th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (25*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age <= (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_25To44th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (25*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age <= (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_25To44th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (45*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age <= (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_45To64th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (45*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age <= (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_45To64th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365) )
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_01'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as laki_lebih65th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365) )
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND Pas2.gender_tp='GENDER_TP_02'
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as perempuan_lebih65th,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
					LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
					WHERE Med2.medical_tp='MEDICAL_TP_01'
					AND Med2.out_tp IS NOT NULL
					AND ICD2.icd_cd = ICD.icd_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
					AND PAS2.birth_date IS NULL AND Pas2.age IS NULL) as kosong,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND Med2.out_tp IS NOT NULL
			AND Pas2.gender_tp='GENDER_TP_01'
			AND ICD2.icd_cd = icd.icd_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_laki,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2 
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND Med2.out_tp IS NOT NULL
			AND Pas2.gender_tp='GENDER_TP_02'
			AND ICD2.icd_cd = icd.icd_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_perempuan,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND (Med2.out_tp <> 'OUT_TP_04' AND Med2.out_tp IS NOT NULL)
			AND ICD2.icd_cd = icd.icd_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_hidup,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND (Med2.out_tp = 'OUT_TP_04' AND Med2.out_tp IS NOT NULL)
			AND ICD2.icd_cd = icd.icd_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_mati,	
COUNT(MR.medical_record_seqno) as total
FROM trx_medical_record MR
LEFT JOIN trx_medical Med ON Med.medical_cd = MR.medical_cd  
LEFT JOIN trx_pasien Pas ON Pas.pasien_cd = MR.pasien_cd
LEFT JOIN trx_icd ICD ON ICD.icd_cd = MR.icd_cd
WHERE med.medical_tp='MEDICAL_TP_01'
AND Med.out_tp IS NOT NULL
AND ICD.icd_cd IS NOT NULL
AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
GROUP BY ICD.icd_cd, ICD.icd_nm
ORDER BY ICD.icd_cd
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_5_1_PENGUNJUNG_RUMAH_SAKIT(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_5_1_PENGUNJUNG_RUMAH_SAKIT]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN

--JUMLAH KUNJUNGAN PASIEN BARU
SELECT 
	--B.no_rm,
	'PASIEN BARU',
	COUNT
		(CASE WHEN 
			simrke.FN_FORMATDATE(B.register_date)=simrke.FN_FORMATDATE(A.datetime_in) THEN 'Pasien Baru' 
		ELSE NULL END) AS Total
FROM trx_medical as  A JOIN trx_pasien as B ON A.pasien_cd = B.pasien_cd
WHERE A.datetime_in BETWEEN simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(@pdtDateEnd)

UNION

--JUMLAH KUNJUNGAN PASIEN LAMA
SELECT 
	--B.no_rm,
	'PASIEN LAMA',
	COUNT
		(CASE WHEN 
			simrke.FN_FORMATDATE(B.register_date)=simrke.FN_FORMATDATE(A.datetime_in) THEN NULL 
		ELSE 'Pasien Lama' END) AS Total
FROM trx_medical as  A JOIN trx_pasien as B ON A.pasien_cd = B.pasien_cd
WHERE A.datetime_in BETWEEN simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(@pdtDateEnd)

END;

DROP FUNCTION IF EXISTS SP_RPT_RL_5_2_KUNJUNGAN_RAWAT_JALAN(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_5_2_KUNJUNGAN_RAWAT_JALAN]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	SELECT RJ.medunit_nm,COUNT(A.medical_cd) AS total
	FROM trx_medical A 
	JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	WHERE 
	RJ.medicalunit_tp = 'MEDICALUNIT_TP_1'
	--A.medical_tp='MEDICAL_TP_01'
	AND A.datetime_in BETWEEN simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(@pdtDateEnd)
	GROUP BY RJ.medunit_nm
	ORDER BY RJ.medunit_nm
END;

DROP FUNCTION IF EXISTS SP_RPT_RL_5_3_TOP10_PENYAKIT_RINAP(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_5_3_TOP10_PENYAKIT_RINAP]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN

		WITH RL_5_3_MSTR  AS 
		(
				SELECT TOP 10 ICD.icd_cd, ICD.icd_nm, COUNT(*) AS total
				FROM trx_medical MDCL
				JOIN trx_medical_record REC ON REC.medical_cd = MDCL.medical_cd
				JOIN trx_icd ICD on ICD.icd_cd = REC.icd_cd
				WHERE MDCL.medical_tp = 'MEDICAL_TP_02' 
					AND REC.icd_cd IS NOT NULL AND REC.icd_cd <> ''
					AND REC.datetime_record BETWEEN simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(@pdtDateEnd)
				GROUP BY ICD.icd_cd, ICD.icd_nm
				ORDER BY total DESC
		) 
		SELECT MSTR.*, simrke.FN_CHECK_NULL(LAKI_HDP.total_hidup_laki) AS total_hidup_laki, 
			simrke.FN_CHECK_NULL(PEREMPUAN_HDP.total_hidup_perempuan) AS total_hidup_perempuan, 
			simrke.FN_CHECK_NULL(LAKI_MT.total_mati_laki) AS total_mati_laki, 
			simrke.FN_CHECK_NULL(PEREMPUAN_MT.total_mati_perempuan) AS total_mati_perempuan
		FROM RL_5_3_MSTR AS MSTR
		LEFT JOIN 
		(
				SELECT REC.icd_cd, COUNT(*) AS total_hidup_laki
				FROM trx_medical MDCL
				JOIN trx_medical_record REC ON REC.medical_cd = MDCL.medical_cd 
				JOIN trx_pasien PASIEN ON PASIEN.pasien_cd = REC.pasien_cd
				WHERE MDCL.medical_tp = 'MEDICAL_TP_02' 
					AND REC.icd_cd IN 
						(
								SELECT icd_cd FROM RL_5_3_MSTR
						)
					AND PASIEN.gender_tp = 'GENDER_TP_01'
					AND MDCL.out_tp IS NOT NULL
					AND MDCL.out_tp <> 'OUT_TP_04'
					AND REC.datetime_record BETWEEN simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(@pdtDateEnd)
				GROUP BY REC.icd_cd
		) AS LAKI_HDP ON MSTR.icd_cd = LAKI_HDP.icd_cd
		LEFT JOIN 
		(
				SELECT REC.icd_cd, COUNT(*) AS total_hidup_perempuan
				FROM trx_medical MDCL
				JOIN trx_medical_record REC ON REC.medical_cd = MDCL.medical_cd 
				JOIN trx_pasien PASIEN ON PASIEN.pasien_cd = REC.pasien_cd
				WHERE MDCL.medical_tp = 'MEDICAL_TP_02' 
					AND REC.icd_cd IN 
						(
								SELECT icd_cd FROM RL_5_3_MSTR
						)
					AND PASIEN.gender_tp = 'GENDER_TP_02'
					AND MDCL.out_tp IS NOT NULL
					AND MDCL.out_tp <> 'OUT_TP_04'
					AND REC.datetime_record BETWEEN simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(@pdtDateEnd)
				GROUP BY REC.icd_cd
		) AS PEREMPUAN_HDP ON MSTR.icd_cd = PEREMPUAN_HDP.icd_cd
		LEFT JOIN 
		(
				SELECT REC.icd_cd, COUNT(*) AS total_mati_laki
				FROM trx_medical MDCL
				JOIN trx_medical_record REC ON REC.medical_cd = MDCL.medical_cd 
				JOIN trx_pasien PASIEN ON PASIEN.pasien_cd = REC.pasien_cd
				WHERE MDCL.medical_tp = 'MEDICAL_TP_02' 
					AND REC.icd_cd IN 
						(
								SELECT icd_cd FROM RL_5_3_MSTR
						)
					AND PASIEN.gender_tp = 'GENDER_TP_01'
					AND MDCL.out_tp = 'OUT_TP_04'
					AND REC.datetime_record BETWEEN simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(@pdtDateEnd)
				GROUP BY REC.icd_cd
		) AS LAKI_MT ON MSTR.icd_cd = LAKI_MT.icd_cd
		LEFT JOIN 
		(
				SELECT REC.icd_cd, COUNT(*) AS total_mati_perempuan
				FROM trx_medical MDCL
				JOIN trx_medical_record REC ON REC.medical_cd = MDCL.medical_cd 
				JOIN trx_pasien PASIEN ON PASIEN.pasien_cd = REC.pasien_cd
				WHERE MDCL.medical_tp = 'MEDICAL_TP_02' 
					AND REC.icd_cd IN 
						(
								SELECT icd_cd FROM RL_5_3_MSTR
						)
					AND PASIEN.gender_tp = 'GENDER_TP_02'
					AND MDCL.out_tp = 'OUT_TP_04'
					AND REC.datetime_record BETWEEN simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(@pdtDateEnd)
				GROUP BY REC.icd_cd
		) AS PEREMPUAN_MT ON MSTR.icd_cd = PEREMPUAN_MT.icd_cd
		ORDER BY total DESC

END;

DROP FUNCTION IF EXISTS SP_RPT_RL_5_4_TOP10_PENYAKIT_RJALAN(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_RL_5_4_TOP10_PENYAKIT_RJALAN]
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
		WITH RL_5_4_MSTR  AS 
		(
				SELECT TOP 10 ICD.icd_cd, ICD.icd_nm, COUNT(*) AS total
				FROM trx_medical MDCL
				JOIN trx_medical_record REC ON REC.medical_cd = MDCL.medical_cd
				JOIN trx_icd ICD on ICD.icd_cd = REC.icd_cd
				WHERE MDCL.medical_tp = 'MEDICAL_TP_01' 
					AND REC.icd_cd IS NOT NULL AND REC.icd_cd <> ''
					AND REC.datetime_record BETWEEN simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(@pdtDateEnd)
				GROUP BY ICD.icd_cd, ICD.icd_nm
				ORDER BY total DESC
		) 
		SELECT MSTR.*, simrke.FN_CHECK_NULL(LAKI_HDP.total_hidup_laki) AS total_hidup_laki, 
			simrke.FN_CHECK_NULL(PEREMPUAN_HDP.total_hidup_perempuan) AS total_hidup_perempuan, 
			simrke.FN_CHECK_NULL(LAKI_MT.total_mati_laki) AS total_mati_laki, 
			simrke.FN_CHECK_NULL(PEREMPUAN_MT.total_mati_perempuan) AS total_mati_perempuan
		FROM RL_5_4_MSTR AS MSTR
		LEFT JOIN 
		(
				SELECT REC.icd_cd, COUNT(*) AS total_hidup_laki
				FROM trx_medical MDCL
				JOIN trx_medical_record REC ON REC.medical_cd = MDCL.medical_cd 
				JOIN trx_pasien PASIEN ON PASIEN.pasien_cd = REC.pasien_cd
				WHERE MDCL.medical_tp = 'MEDICAL_TP_01' 
					AND REC.icd_cd IN 
						(
								SELECT icd_cd FROM RL_5_4_MSTR
						)
					AND PASIEN.gender_tp = 'GENDER_TP_01'
					AND MDCL.out_tp IS NOT NULL
					AND MDCL.out_tp <> 'OUT_TP_04'
					AND REC.datetime_record BETWEEN simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(@pdtDateEnd)
				GROUP BY REC.icd_cd
		) AS LAKI_HDP ON MSTR.icd_cd = LAKI_HDP.icd_cd
		LEFT JOIN 
		(
				SELECT REC.icd_cd, COUNT(*) AS total_hidup_perempuan
				FROM trx_medical MDCL
				JOIN trx_medical_record REC ON REC.medical_cd = MDCL.medical_cd 
				JOIN trx_pasien PASIEN ON PASIEN.pasien_cd = REC.pasien_cd
				WHERE MDCL.medical_tp = 'MEDICAL_TP_01' 
					AND REC.icd_cd IN 
						(
								SELECT icd_cd FROM RL_5_4_MSTR
						)
					AND PASIEN.gender_tp = 'GENDER_TP_02'
					AND MDCL.out_tp IS NOT NULL
					AND MDCL.out_tp <> 'OUT_TP_04'
					AND REC.datetime_record BETWEEN simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(@pdtDateEnd)
				GROUP BY REC.icd_cd
		) AS PEREMPUAN_HDP ON MSTR.icd_cd = PEREMPUAN_HDP.icd_cd
		LEFT JOIN 
		(
				SELECT REC.icd_cd, COUNT(*) AS total_mati_laki
				FROM trx_medical MDCL
				JOIN trx_medical_record REC ON REC.medical_cd = MDCL.medical_cd 
				JOIN trx_pasien PASIEN ON PASIEN.pasien_cd = REC.pasien_cd
				WHERE MDCL.medical_tp = 'MEDICAL_TP_01' 
					AND REC.icd_cd IN 
						(
								SELECT icd_cd FROM RL_5_4_MSTR
						)
					AND PASIEN.gender_tp = 'GENDER_TP_01'
					AND MDCL.out_tp = 'OUT_TP_04'
					AND REC.datetime_record BETWEEN simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(@pdtDateEnd)
				GROUP BY REC.icd_cd
		) AS LAKI_MT ON MSTR.icd_cd = LAKI_MT.icd_cd
		LEFT JOIN 
		(
				SELECT REC.icd_cd, COUNT(*) AS total_mati_perempuan
				FROM trx_medical MDCL
				JOIN trx_medical_record REC ON REC.medical_cd = MDCL.medical_cd 
				JOIN trx_pasien PASIEN ON PASIEN.pasien_cd = REC.pasien_cd
				WHERE MDCL.medical_tp = 'MEDICAL_TP_01' 
					AND REC.icd_cd IN 
						(
								SELECT icd_cd FROM RL_5_4_MSTR
						)
					AND PASIEN.gender_tp = 'GENDER_TP_02'
					AND MDCL.out_tp = 'OUT_TP_04'
					AND REC.datetime_record BETWEEN simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(@pdtDateEnd)
				GROUP BY REC.icd_cd
		) AS PEREMPUAN_MT ON MSTR.icd_cd = PEREMPUAN_MT.icd_cd
		ORDER BY total DESC

END;

DROP FUNCTION IF EXISTS SP_RPT_SPT_RAWAT_INAP(int,int) CASCADE;
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [simrke].[SP_RPT_SPT_RAWAT_INAP] 
	-- Add the parameters for the stored procedure here
@pintMonth int,
@pintYear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 100 
		ICD.icd_cd,ICD.icd_nm,
		(select note from trx_icd where icd_cd=ICD.icd_cd) as icd_note, 
		
		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_02' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=0 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= 28) as nol_28,
		
		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_02' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) > 28 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= 365) as dua8_1th,

		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_02' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=365 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= (5*365) -1) as satuthn_4thn,

		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_02' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=5*365 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= (15*365)-1 ) as limathn_14thn,

		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_02' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=15*365 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= (25*365)-1 ) as limabelasthn_24thn,

		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_02' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=25*365 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= (45*365)-1 ) as dua5thn_44thn,

		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_02' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=45*365 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= (65*365)-1 ) as empat5thn_64thn,

		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_02' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=65*365) as enam5thn_keatas,
	
		(select COUNT(A2.icd_cd) from ((trx_medical m2 
			JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd)
			JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) 
			JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_02'
			AND ICD2.icd_cd=ICD.icd_cd
			AND p2.gender_tp='GENDER_TP_01') AS total_lk,
		(select COUNT(A2.icd_cd) from ((trx_medical m2 
			JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd)
			JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) 
			JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_02'
			AND ICD2.icd_cd=ICD.icd_cd
			AND p2.gender_tp='GENDER_TP_02') AS total_pr,
		--(select COUNT(A2.icd_cd) from ((trx_medical m2 
		--	JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd)
		--	JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) 
		--	JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
		--	WHERE m2.medical_tp='MEDICAL_TP_02'
		--	AND m2.out_tp<>'OUT_TP_04'
		--	AND ICD2.icd_cd=ICD.icd_cd) AS total_hidup,
		(select COUNT(A2.icd_cd) from ((trx_medical m2 
			JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd)
			JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) 
			JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_02'
			AND m2.out_tp='OUT_TP_04'
			AND ICD2.icd_cd=ICD.icd_cd) AS total_mati,
		count(A.icd_cd) as total
		FROM ((trx_medical m 
		JOIN trx_medical_record A ON m.medical_cd=A.medical_cd)
		JOIN trx_icd ICD ON A.icd_cd=ICD.icd_cd) 
		JOIN trx_pasien p ON m.pasien_cd=p.pasien_cd
		WHERE m.medical_tp='MEDICAL_TP_02'
		AND MONTH(m.datetime_in)=@pintMonth AND YEAR(m.datetime_in)=@pintYear
		GROUP BY ICD.icd_cd,ICD.icd_nm
		ORDER BY total DESC
END;

-- sikat
DROP FUNCTION IF EXISTS SP_RPT_SPT_RAWAT_JALAN(int,int) CASCADE;
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [simrke].[SP_RPT_SPT_RAWAT_JALAN] 
	-- Add the parameters for the stored procedure here
@pintMonth int,
@pintYear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 100 
		ICD.icd_cd,ICD.icd_nm,
		(select note from trx_icd where icd_cd=ICD.icd_cd) as icd_note, 
		
		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_01' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=0 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= 28) as nol_28,
		
		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_01' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) > 28 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= 365) as dua8_1th,

		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_01' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=365 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= (5*365) -1) as satuthn_4thn,

		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_01' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=5*365 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= (15*365)-1 ) as limathn_14thn,

		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_01' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=15*365 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= (25*365)-1 ) as limabelasthn_24thn,

		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_01' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=25*365 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= (45*365)-1 ) as dua5thn_44thn,

		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_01' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=45*365 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) <= (65*365)-1 ) as empat5thn_64thn,

		(select COUNT(A2.icd_cd) as jumlah from ((trx_medical m2 JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd) JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_01' AND ICD2.icd_cd=ICD.icd_cd
			 AND DATEDIFF(DAY,p2.birth_date,m2.datetime_in) >=65*365) as enam5thn_keatas,
	
		(select COUNT(A2.icd_cd) from ((trx_medical m2 
			JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd)
			JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) 
			JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_01'
			AND ICD2.icd_cd=ICD.icd_cd
			AND p2.gender_tp='GENDER_TP_01') AS total_lk,
		(select COUNT(A2.icd_cd) from ((trx_medical m2 
			JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd)
			JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) 
			JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_01'
			AND ICD2.icd_cd=ICD.icd_cd
			AND p2.gender_tp='GENDER_TP_02') AS total_pr,
		--(select COUNT(A2.icd_cd) from ((trx_medical m2 
		--	JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd)
		--	JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) 
		--	JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
		--	WHERE m2.medical_tp='MEDICAL_TP_01'
		--	AND m2.out_tp<>'OUT_TP_04'
		--	AND ICD2.icd_cd=ICD.icd_cd) AS total_hidup,
		(select COUNT(A2.icd_cd) from ((trx_medical m2 
			JOIN trx_medical_record A2 ON m2.medical_cd=A2.medical_cd)
			JOIN trx_icd ICD2 ON A2.icd_cd=ICD2.icd_cd) 
			JOIN trx_pasien p2 ON m2.pasien_cd=p2.pasien_cd
			WHERE m2.medical_tp='MEDICAL_TP_01'
			AND m2.out_tp='OUT_TP_04'
			AND ICD2.icd_cd=ICD.icd_cd) AS total_mati,
		count(A.icd_cd) as total
		FROM ((trx_medical m 
		JOIN trx_medical_record A ON m.medical_cd=A.medical_cd)
		JOIN trx_icd ICD ON A.icd_cd=ICD.icd_cd) 
		JOIN trx_pasien p ON m.pasien_cd=p.pasien_cd
		WHERE m.medical_tp='MEDICAL_TP_01'
		AND MONTH(m.datetime_in)=@pintMonth AND YEAR(m.datetime_in)=@pintYear
		GROUP BY ICD.icd_cd,ICD.icd_nm
		ORDER BY total DESC
END;

DROP FUNCTION IF EXISTS SP_RPT_STP_RAWAT_INAP_MENULAR(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_STP_RAWAT_INAP_MENULAR] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT VW.stp_cd ,VW.group_nm,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 0 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age < 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_0To28Hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 28 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_28HariTo1Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 365 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age < (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_1To4Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (5*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age < (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_5To14Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (15*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age < (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_15To24Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (25*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age < (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_25To44Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (45*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age < (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_45To64Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_lebih65Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE Pas2.birth_date IS NULL AND Pas2.age IS NULL
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as kosong,     
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND Pas2.gender_tp='GENDER_TP_01'
			AND VW2.stp_cd = VW.stp_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_laki, 
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND Pas2.gender_tp='GENDER_TP_02'
			AND VW2.stp_cd = VW.stp_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_perempuan,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND Med2.out_tp = 'OUT_TP_04'
			AND VW2.stp_cd = VW.stp_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_mati,
	COUNT(medical_record_seqno) as total
	FROM trx_medical_record MR
	RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW ON MR.icd_cd = VW.icd_cd
	LEFT JOIN trx_medical Med ON Med.medical_cd = MR.medical_cd
	LEFT JOIN trx_pasien Pas ON Pas.pasien_cd = MR.pasien_cd
	WHERE Med.medical_tp = 'MEDICAL_TP_02'
	AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
	GROUP BY VW.stp_cd, VW.group_nm
	ORDER BY VW.stp_cd
END;

DROP FUNCTION IF EXISTS SP_RPT_STP_RAWAT_INAP_TIDAKMENULAR(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_STP_RAWAT_INAP_TIDAKMENULAR] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT VW.stp_cd ,VW.group_nm,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 0 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age < 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_0To28Hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 28 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_28HariTo1Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 365 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age < (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_1To4Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (5*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age < (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_5To14Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (15*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age < (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_15To24Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (25*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age < (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_25To44Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (45*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age < (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_45To64Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_lebih65Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE Pas2.birth_date IS NULL AND Pas2.age IS NULL
					AND Med2.medical_tp='MEDICAL_TP_02'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as kosong,     
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND Pas2.gender_tp='GENDER_TP_01'
			AND VW2.stp_cd = VW.stp_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_laki, 
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND Pas2.gender_tp='GENDER_TP_02'
			AND VW2.stp_cd = VW.stp_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_perempuan,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			WHERE Med2.medical_tp='MEDICAL_TP_02'
			AND Med2.out_tp = 'OUT_TP_04'
			AND VW2.stp_cd = VW.stp_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_mati,
	COUNT(medical_record_seqno) as total
	FROM trx_medical_record MR
	RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW ON MR.icd_cd = VW.icd_cd
	LEFT JOIN trx_medical Med ON Med.medical_cd = MR.medical_cd
	LEFT JOIN trx_pasien Pas ON Pas.pasien_cd = MR.pasien_cd
	WHERE Med.medical_tp = 'MEDICAL_TP_02'
	AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
	GROUP BY VW.stp_cd, VW.group_nm
	ORDER BY VW.stp_cd
END;

DROP FUNCTION IF EXISTS SP_RPT_STP_RAWAT_JALAN_MENULAR(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_STP_RAWAT_JALAN_MENULAR] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT VW.stp_cd ,VW.group_nm,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 0 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age < 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_0To28Hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 28 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_28HariTo1Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 365 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age < (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_1To4Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (5*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age < (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_5To14Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (15*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age < (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_15To24Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (25*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age < (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_25To44Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (45*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age < (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_45To64Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_lebih65Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE Pas2.birth_date IS NULL AND Pas2.age IS NULL
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as kosong,     
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND Pas2.gender_tp='GENDER_TP_01'
			AND VW2.stp_cd = VW.stp_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_laki, 
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND Pas2.gender_tp='GENDER_TP_02'
			AND VW2.stp_cd = VW.stp_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_perempuan,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND Med2.out_tp = 'OUT_TP_04'
			AND VW2.stp_cd = VW.stp_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_mati,
	COUNT(medical_record_seqno) as total
	FROM trx_medical_record MR
	RIGHT JOIN simrke.VW_TRX_ICD_STP_MENULAR VW ON MR.icd_cd = VW.icd_cd
	LEFT JOIN trx_medical Med ON Med.medical_cd = MR.medical_cd
	LEFT JOIN trx_pasien Pas ON Pas.pasien_cd = MR.pasien_cd
	WHERE Med.medical_tp = 'MEDICAL_TP_01'
	AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
	GROUP BY VW.stp_cd, VW.group_nm
	ORDER BY VW.stp_cd
END;

DROP FUNCTION IF EXISTS SP_RPT_STP_RAWAT_JALAN_TIDAKMENULAR(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_RPT_STP_RAWAT_JALAN_TIDAKMENULAR] 
	-- Add the parameters for the stored procedure here
@pdtDateStart Varchar(10),
@pdtDateEnd Varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT VW.stp_cd ,VW.group_nm,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 0 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 28 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age < 28)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_0To28Hari,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 28 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) < 365 ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age < 365)
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_28HariTo1Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= 365 AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (4*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age < (4*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_1To4Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (5*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (14*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age < (14*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_5To14Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (15*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (24*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age < (24*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_15To24Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (25*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (44*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age < (44*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_25To44Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (45*365) AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) <= (64*365) ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age < (64*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_45To64Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE (
								(PAS2.birth_date IS NOT NULL AND DATEDIFF(DAY,Pas2.birth_date,GETDATE()) >= (65*365)  ) OR
								(PAS2.birth_date IS NULL AND Pas2.age >= (65*365))
							)
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_lebih65Tahun,
				(SELECT COUNT(MR2.medical_record_seqno)
					FROM trx_medical_record MR2
					RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
					LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
					LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					WHERE Pas2.birth_date IS NULL AND Pas2.age IS NULL
					AND Med2.medical_tp='MEDICAL_TP_01'
					AND VW2.stp_cd = VW.stp_cd
					AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as kosong,     
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND Pas2.gender_tp='GENDER_TP_01'
			AND VW2.stp_cd = VW.stp_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_laki, 
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND Pas2.gender_tp='GENDER_TP_02'
			AND VW2.stp_cd = VW.stp_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_perempuan,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW2 ON MR2.icd_cd = VW2.icd_cd
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND Med2.out_tp = 'OUT_TP_04'
			AND VW2.stp_cd = VW.stp_cd
			AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR2.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)) as total_mati,
	COUNT(medical_record_seqno) as total
	FROM trx_medical_record MR
	RIGHT JOIN simrke.VW_TRX_ICD_STP_TIDAKMENULAR VW ON MR.icd_cd = VW.icd_cd
	LEFT JOIN trx_medical Med ON Med.medical_cd = MR.medical_cd
	LEFT JOIN trx_pasien Pas ON Pas.pasien_cd = MR.pasien_cd
	WHERE Med.medical_tp = 'MEDICAL_TP_01'
	AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) >=simrke.FN_STRINGTODATE(@pdtDateStart) AND simrke.FN_STRINGTODATE(simrke.FN_FORMATDATE(MR.datetime_record)) <=simrke.FN_STRINGTODATE(@pdtDateEnd)
	GROUP BY VW.stp_cd, VW.group_nm
	ORDER BY VW.stp_cd
END;

DROP FUNCTION IF EXISTS SP_SAVE_ASURANSI_PASIEN(Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_SAVE_ASURANSI_PASIEN] 
@pstrPasienCd Varchar(20),
@pstrAsuransiCd Varchar(20),
@pstrUserId Varchar(20)
AS
BEGIN
	IF @pstrAsuransiCd = ''
	BEGIN
		UPDATE trx_pasien SET pasien_tp='PASIEN_TP_01', 
		modi_id=@pstrUserId, modi_datetime=GETDATE()
		WHERE pasien_cd=@pstrPasienCd
		
		DELETE FROM trx_pasien_insurance WHERE pasien_cd=@pstrPasienCd
	END
	ELSE
	BEGIN
		UPDATE trx_pasien SET pasien_tp='PASIEN_TP_02', 
		modi_id=@pstrUserId, modi_datetime=GETDATE()
		WHERE pasien_cd=@pstrPasienCd
		
		IF EXISTS(SELECT insurance_cd FROM trx_pasien_insurance
				WHERE pasien_cd=@pstrPasienCd
				AND insurance_cd=@pstrAsuransiCd)
		BEGIN
			UPDATE trx_pasien_insurance
			SET default_st='1'
			WHERE pasien_cd=@pstrPasienCd
			AND insurance_cd=@pstrAsuransiCd
		END	
		ELSE
		BEGIN
			DELETE FROM trx_pasien_insurance WHERE pasien_cd=@pstrPasienCd
			
			INSERT INTO trx_pasien_insurance (pasien_cd,insurance_cd,default_st,modi_id,modi_datetime)
			VALUES (@pstrPasienCd,@pstrAsuransiCd,'1',@pstrUserId,GETDATE())
		END
	END
END;

DROP FUNCTION IF EXISTS SP_SAVE_INVITEM(Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,numeric,numeric,Varchar,numeric,numeric,numeric,numeric,Varchar,Char,Char,Char,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_SAVE_INVITEM] 
@pstrItemCd Varchar(20),
@pstrTypeCd Varchar(20),
@pstrUnitCd Varchar(20),
@pstrItemNm Varchar(100),
@pstrBarcode Varchar(50),
@pstrCurrCd Varchar(1000),
@pnumItemPriceBuy numeric(15),
@pnumItemPrice numeric(15),
@pstrVatTp Varchar(20),
@pnumPpn numeric(5,2),
@pnumReorder numeric(10,2),
@pnumStokMin numeric(10,2),
@pnumStokMax numeric(10,2),
@pstrTarifTp Varchar(20),
@pstrGenericSt Char(1),
@pstrActiveSt Char(1),
@pstrInventorySt Char(1),
@pstrUserId Varchar(20)
AS
BEGIN
	INSERT INTO inv_item_master (item_cd,type_cd,unit_cd,item_nm,
	barcode,currency_cd,
	item_price_buy,item_price,vat_tp,ppn,
	reorder_point,minimum_stock,maximum_stock,
	tariftp_cd,generic_st,active_st,inventory_st,modi_id,modi_datetime) 
	VALUES (@pstrItemCd,@pstrTypeCd,@pstrUnitCd,@pstrItemNm,
	@pstrBarcode,@pstrCurrCd,
	@pnumItemPriceBuy,@pnumItemPrice,@pstrVatTp,@pnumPpn,
	@pnumReorder,@pnumStokMin,@pnumStokMax,
	@pstrTarifTp,@pstrGenericSt,@pstrActiveSt,@pstrInventorySt,@pstrUserId,GETDATE())
	
	DECLARE @strAccountCd Varchar(20)
	--HARDCODE
	SET @strAccountCd = 'AC301'
	--End HARDCODE
	
	--Tarif
	IF EXISTS(SELECT item_cd FROM trx_tarif_inventori
			  WHERE item_cd=@pstrItemCd
			  AND ISNULL(kelas_cd,'')=''
			  AND ISNULL(insurance_cd,'')='')
		UPDATE trx_tarif_inventori SET tarif=@pnumItemPrice
		WHERE item_cd=@pstrItemCd
		AND ISNULL(kelas_cd,'')=''
		AND ISNULL(insurance_cd,'')=''
	ELSE
		INSERT INTO trx_tarif_inventori (item_cd,tarif,account_cd,kelas_cd,insurance_cd,modi_id,modi_datetime) 
		VALUES (@pstrItemCd,@pnumItemPrice,@strAccountCd,'','',@pstrUserId,GETDATE())
	
	--Set stok gudang utama : WHMASTER
	IF EXISTS(SELECT pos_cd FROM inv_pos_inventory WHERE pos_cd='WHMASTER')
	BEGIN
		IF NOT EXISTS(SELECT item_cd FROM inv_pos_item WHERE pos_cd='WHMASTER' AND item_cd=@pstrItemCd)
			INSERT INTO inv_pos_item (pos_cd,item_cd,quantity,modi_id,modi_datetime) VALUES('WHMASTER',@pstrItemCd,0,'admin',GETDATE())
	END
	
	--Set stok gudang farmasi : WHFAR01
	/*IF EXISTS(SELECT pos_cd FROM inv_pos_inventory WHERE pos_cd='WHFAR01')
	BEGIN
		IF NOT EXISTS(SELECT item_cd FROM inv_pos_item WHERE pos_cd='WHFAR01' AND item_cd=@pstrItemCd)
			INSERT INTO inv_pos_item (pos_cd,item_cd,quantity,modi_id,modi_datetime) VALUES('WHFAR01',@pstrItemCd,0,'admin',GETDATE())
	END*/
END;

DROP FUNCTION IF EXISTS SP_SAVE_PASIEN(Varchar,Varchar,Char,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Datetime,int,Varchar,Varchar,Varchar,Varchar,numeric,numeric,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Datetime,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_SAVE_PASIEN] 
@pstrPasienNm Varchar(100),
@pstrNoRM Varchar(10),
@pstrRMSt Char(1),
@pstrPasienTp Varchar(20),
@pstrAddress Varchar(1000),
@pstrProp Varchar(100),
@pstrKab Varchar(100),
@pstrKec Varchar(100),
@pstrKel Varchar(100),
@pstrKodePost Varchar(20),
@pstrBirthPlace Varchar(100),
@pdtBirthDate Datetime,
@pintUmur int,
@pstrPhone Varchar(20),
@pstrMobile Varchar(20),
@pstrEmail Varchar(100),
@pstrBloodTp Varchar(20),
@pnumBerat numeric(5,2),
@pnumTinggi numeric(5,2),
@pstrGenderTp Varchar(20),
@pstrMaritalTp Varchar(20),
@pstrNationCd Varchar(20),
@pstrAgamaCd Varchar(20),
@pstrPekerjaanCd Varchar(20),
@pstrPendidikanCd Varchar(20),
@pstrRasCd Varchar(20),
@pstrIdentitas Varchar(20),
@pstrIdNo Varchar(50),
@pstrAsuransiCd Varchar(20),
@pstrAsuransiNo Varchar(50),
@pstrAsrPerusahaanCd Varchar(20),
@pstrAsrKelasCd Varchar(20),
@pstrAyahNm Varchar(100),
@pstrIbuNm Varchar(100),
@pstrPenanggungNm Varchar(100),
@pstrPenanggungTp Varchar(20),
@pstrPenanggungAddress Varchar(1000),
@pstrPenanggungPhone Varchar(20),
@pstrPenanggungBirth Datetime,
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @strPasienCd Varchar(20)
	DECLARE @strNoRM Varchar(10)
	
	SET @strPasienCd = (SELECT simrke.FN_GET_PASIENCD(GETDATE()))
	
	--SET @strNoRM = (SELECT simrke.FN_GET_NORM())
	IF @pstrRMSt = '1'
	BEGIN
		IF @pstrNoRM = ''
		BEGIN
			SET @strNoRM = (SELECT simrke.FN_GET_NORM())
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT TOP 1 no_rm FROM trx_pasien WHERE no_rm=@pstrNoRM)
			BEGIN
				--Refresh No RM
				SET @strNoRM = (SELECT simrke.FN_GET_NORM())
				
				--Error No RM duplikat, stop proses
				/*ROLLBACK TRANSACTION
				RAISERROR('No RM duplikat',16,1)
				RETURN*/
			END	
			ELSE
				SET @strNoRM = @pstrNoRM
		END
		
		SET @strNoRM = simrke.FN_FORMAT_STRING(@strNoRM,'0',8)
	END
	ELSE
	BEGIN
		SET @strNoRM = ''
	END
	
	SELECT @strPasienCd AS pasien_cd
	
	INSERT INTO trx_pasien (pasien_cd,no_rm,pasien_tp,pasien_nm,
	address,region_prop,region_kab,region_kec,region_kel,postcode,
	phone,mobile,email,birth_place,birth_date,age,
	blood_tp,weight,height,gender_tp,marital_tp,
	nation_cd,religion_cd,occupation_cd,education_cd,race_cd,
	register_date,register_st,modi_id,modi_datetime) 
	VALUES (@strPasienCd,@strNoRM,@pstrPasienTp,@pstrPasienNm,
	@pstrAddress,@pstrProp,@pstrKab,@pstrKec,@pstrKel,@pstrKodePost,
	@pstrPhone,@pstrMobile,@pstrEmail,@pstrBirthPlace,@pdtBirthDate,@pintUmur,
	@pstrBloodTp,@pnumBerat,@pnumTinggi,@pstrGenderTp,@pstrMaritalTp,
	@pstrNationCd,@pstrAgamaCd,@pstrPekerjaanCd,@pstrPendidikanCd,@pstrRasCd,
	GETDATE(),'1',@pstrUserId,GETDATE())
	
	IF @pstrIdentitas IS NOT NULL
	BEGIN
		INSERT INTO trx_pasien_identity (pasien_cd,identity_tp,id_no,modi_id,modi_datetime)
		VALUES (@strPasienCd,@pstrIdentitas,@pstrIdNo,@pstrUserId,GETDATE())
	END
	
	IF @pstrAyahNm <> ''
	BEGIN
		INSERT INTO trx_pasien_family (pasien_cd,family_tp,family_nm,modi_id,modi_datetime)
		VALUES (@strPasienCd,'FAMILY_TP_01',@pstrAyahNm,@pstrUserId,GETDATE())
	END
	IF @pstrIbuNm <> ''
	BEGIN
		INSERT INTO trx_pasien_family (pasien_cd,family_tp,family_nm,modi_id,modi_datetime)
		VALUES (@strPasienCd,'FAMILY_TP_02',@pstrIbuNm,@pstrUserId,GETDATE())
	END
	IF @pstrPenanggungNm <> ''
	BEGIN
		IF @pstrPenanggungTp IN ('FAMILY_TP_01','FAMILY_TP_02')
			SET @pstrPenanggungTp = 'FAMILY_TP_00'
		
		INSERT INTO trx_pasien_family (pasien_cd,family_tp,family_nm,address,phone,birth_date,modi_id,modi_datetime)
		VALUES (@strPasienCd,@pstrPenanggungTp,@pstrPenanggungNm,@pstrPenanggungAddress,@pstrPenanggungPhone,@pstrPenanggungBirth,@pstrUserId,GETDATE())
	END
	
	IF @pstrAsuransiCd IS NOT NULL
	BEGIN
		INSERT INTO trx_pasien_insurance (pasien_cd,insurance_cd,insurance_no,standar_kelas_cd,default_st,modi_id,modi_datetime)
		VALUES (@strPasienCd,@pstrAsuransiCd,@pstrAsuransiNo,@pstrAsrKelasCd,'1',@pstrUserId,GETDATE())
	END
END;

DROP FUNCTION IF EXISTS SP_SAVE_RESEP(Varchar,Datetime,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_SAVE_RESEP] 
@pstrMedicalCd Varchar(10),
@pdtResepDate Datetime,
@pstrResepNo Varchar(50),
@pstrResep Varchar(1000),
@pstrDrCd Varchar(20),
@pstrDrNm Varchar(100),
@pstrPasienCd Varchar(20),
@pstrPasienNm Varchar(100),
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @strKelasCd varchar(20)
	SELECT @strKelasCd=KMR.kelas_cd
	FROM trx_medical A 
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd 
	WHERE A.medical_cd=@pstrMedicalCd
	
	INSERT INTO trx_medical_resep (medical_cd,resep_date,resep_no,resep,
	dr_cd,dr_nm,pasien_cd,pasien_nm,kelas_cd,
	proses_st,modi_id,modi_datetime)
	VALUES (@pstrMedicalCd,@pdtResepDate,@pstrResepNo,@pstrResep,
	@pstrDrCd,@pstrDrNm,@pstrPasienCd,@pstrPasienNm,@strKelasCd,
	'0',@pstrUserId,GETDATE())
	
	SELECT MAX(medical_resep_seqno) AS medical_resep_seqno FROM trx_medical_resep
	WHERE medical_cd=@pstrMedicalCd
END;

DROP FUNCTION IF EXISTS SP_SAVE_RESEP_BYREF(Bigint,Varchar,Datetime,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_SAVE_RESEP_BYREF] 
@pintRefSeqno Bigint,
@pstrMedicalCd Varchar(10),
@pdtResepDate Datetime,
@pstrResepNo Varchar(50),
@pstrResep Varchar(1000),
@pstrDrCd Varchar(20),
@pstrDrNm Varchar(100),
@pstrPasienCd Varchar(20),
@pstrPasienNm Varchar(100),
@pstrUserId Varchar(20)
AS
BEGIN
	IF @pstrMedicalCd = ''
	BEGIN
		INSERT INTO trx_medical_resep (medical_cd,ref_proses_seqno,resep_date,resep_no,resep,
		dr_cd,dr_nm,pasien_cd,pasien_nm,
		proses_st,modi_id,modi_datetime)
		VALUES (NULL,@pintRefSeqno,@pdtResepDate,@pstrResepNo,@pstrResep,
		@pstrDrCd,@pstrDrNm,@pstrPasienCd,@pstrPasienNm,
		'0',@pstrUserId,GETDATE())
	END
	ELSE
	BEGIN
		DECLARE @strKelasCd varchar(20)
		SELECT @strKelasCd=KMR.kelas_cd
		FROM trx_medical A 
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd 
		WHERE A.medical_cd=@pstrMedicalCd
	
		INSERT INTO trx_medical_resep (medical_cd,ref_proses_seqno,resep_date,resep_no,resep,
		dr_cd,dr_nm,pasien_cd,pasien_nm,kelas_cd,
		proses_st,modi_id,modi_datetime)
		VALUES (@pstrMedicalCd,@pintRefSeqno,@pdtResepDate,@pstrResepNo,@pstrResep,
		@pstrDrCd,@pstrDrNm,@pstrPasienCd,@pstrPasienNm,@strKelasCd,
		'0',@pstrUserId,GETDATE())
	END
	
	SELECT MAX(medical_resep_seqno) AS medical_resep_seqno FROM trx_medical_resep
	WHERE ref_proses_seqno=@pintRefSeqno
END;

DROP FUNCTION IF EXISTS SP_SAVE_RESEP_CUSTOMPRICE(bigint,Varchar,Varchar,Numeric,Numeric,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_SAVE_RESEP_CUSTOMPRICE] 
@pstrMedicalResepSeqno bigint,
@pstrItemCd Varchar(20),
@pstrDataNm Varchar(1000),
@pnumQuantity Numeric(5,2),
@pnumPrice Numeric(15),
@pstrInfo01 Varchar(100),
@pstrResepTp Varchar(20),
@pstrUserId Varchar(20)
AS
BEGIN
	INSERT INTO trx_resep_data (medical_resep_seqno,item_cd,data_nm,
	quantity,price,info_01,resep_tp,
	modi_id,modi_datetime) 
	VALUES (@pstrMedicalResepSeqno,@pstrItemCd,@pstrDataNm,
	@pnumQuantity,@pnumPrice,@pstrInfo01,@pstrResepTp,
	@pstrUserId,GETDATE())
						 
	--SELECT MAX(resep_seqno) AS resep_seqno FROM trx_resep_data
	--WHERE medical_resep_seqno=@pstrMedicalResepSeqno
END;

DROP FUNCTION IF EXISTS SP_SAVE_RESEP_DATA(bigint,Varchar,Varchar,Numeric,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_SAVE_RESEP_DATA] 
@pstrMedicalResepSeqno bigint,
@pstrItemCd Varchar(20),
@pstrDataNm Varchar(1000),
@pnumQuantity Numeric(5,2),
@pstrInfo01 Varchar(100),
@pstrResepTp Varchar(20),
@pstrUserId Varchar(20)
AS
BEGIN
	INSERT INTO trx_resep_data (medical_resep_seqno,item_cd,data_nm,
	quantity,info_01,resep_tp,
	modi_id,modi_datetime) 
	VALUES (@pstrMedicalResepSeqno,@pstrItemCd,@pstrDataNm,
	@pnumQuantity,@pstrInfo01,@pstrResepTp,
	@pstrUserId,GETDATE())
						 
	SELECT MAX(resep_seqno) AS resep_seqno FROM trx_resep_data
	WHERE medical_resep_seqno=@pstrMedicalResepSeqno
END;

DROP FUNCTION IF EXISTS SP_SAVE_RESEP_HS(Bigint) CASCADE;
CREATE PROCEDURE [simrke].[SP_SAVE_RESEP_HS]
@pstrMedicalResepSeqno Bigint
AS
BEGIN
	IF NOT EXISTS(SELECT medical_resep_seqno
				FROM trx_medical_resep_hs
				WHERE medical_resep_seqno=@pstrMedicalResepSeqno)
	BEGIN
		INSERT INTO trx_medical_resep_hs (medical_resep_seqno,medical_cd,dr_cd,dr_nm,pasien_cd,pasien_nm,
		resep_date,resep_no,resep,proses_st,proses_date,info_01,info_02,ref_proses_seqno,
		modi_id,modi_datetime)
		SELECT medical_resep_seqno,medical_cd,dr_cd,dr_nm,pasien_cd,pasien_nm,
		resep_date,resep_no,resep,proses_st,proses_date,info_01,info_02,ref_proses_seqno,
		modi_id,GETDATE()
		FROM trx_medical_resep
		WHERE medical_resep_seqno=@pstrMedicalResepSeqno
		
		INSERT INTO trx_resep_data_hs (medical_resep_seqno,item_cd,data_nm,quantity,resep_tp,info_01,info_02,modi_id,modi_datetime)
		SELECT medical_resep_seqno,item_cd,data_nm,quantity,resep_tp,info_01,info_02,modi_id,GETDATE() FROM trx_resep_data
		WHERE medical_resep_seqno=@pstrMedicalResepSeqno
	END
END;

DROP FUNCTION IF EXISTS SP_SAVE_RESEP_SALE(Datetime,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_SAVE_RESEP_SALE] 
@pdtResepDate Datetime,
@pstrResepNo Varchar(50),
@pstrResep Varchar(1000),
@pstrDrCd Varchar(20),
@pstrDrNm Varchar(100),
@pstrPasienCd Varchar(20),
@pstrPasienNm Varchar(100),
@pstrAlamat Varchar(1000),
@pstrUserId Varchar(20)
AS
BEGIN
	INSERT INTO trx_medical_resep (resep_date,resep_no,resep,
	dr_cd,dr_nm,pasien_cd,pasien_nm,info_01,
	proses_st,modi_id,modi_datetime)
	VALUES (@pdtResepDate,@pstrResepNo,@pstrResep,
	@pstrDrCd,@pstrDrNm,@pstrPasienCd,@pstrPasienNm,@pstrAlamat,
	'0',@pstrUserId,GETDATE())
	
	SELECT MAX(medical_resep_seqno) AS medical_resep_seqno FROM trx_medical_resep
	WHERE medical_cd IS NULL
END;

DROP FUNCTION IF EXISTS SP_SAVE_RETUR(Varchar,Datetime,Varchar,Varchar,bigint,bigint,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_SAVE_RETUR] 
@pstrMedicalCd Varchar(10),
@pdtReturDate Datetime,
@pstrPasienCd Varchar(20),
@pstrPasienNm Varchar(100),
@pstrMedicalResepSeqno bigint,
@pstrMedicalAlkesSeqno bigint,
@pstrUserId Varchar(20)
AS
BEGIN
	INSERT INTO trx_resep_retur (medical_cd,retur_date,
	pasien_cd,pasien_nm,ref_medical_resep_seqno,ref_medical_alkes_seqno,
	modi_id,modi_datetime)
	VALUES (@pstrMedicalCd,@pdtReturDate,
	@pstrPasienCd,@pstrPasienNm,@pstrMedicalResepSeqno,@pstrMedicalAlkesSeqno,
	@pstrUserId,GETDATE())
	
	SELECT MAX(retur_seqno) AS retur_seqno FROM trx_resep_retur
	WHERE medical_cd=@pstrMedicalCd
END;

DROP FUNCTION IF EXISTS SP_SETTLEMENT_EXCEPTION(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_SETTLEMENT_EXCEPTION] 
@pstrMedicalCd Varchar(10),
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @strMedicalTp varchar(20)
	DECLARE @strEmergencySt char(1)
	DECLARE @strPasienCd varchar(20)
	DECLARE @strRegisterSt char(1)
	
	SELECT @strMedicalTp=A.medical_tp,@strEmergencySt=A.emergency_st,
	@strPasienCd=A.pasien_cd,@strRegisterSt=B.register_st
	FROM trx_medical A 
	JOIN trx_pasien B ON A.pasien_cd=B.pasien_cd 
	WHERE A.medical_cd=@pstrMedicalCd
	
	/*--Biaya pendaftaran pasien lama--*/
	IF @strRegisterSt <> '1'
	BEGIN
		--HARDCODE
		UPDATE trx_medical_settlement SET amount=2500
		WHERE account_cd='AC003'
		AND payment_st='PAYMENT_ST_0'
		AND medical_cd=@pstrMedicalCd
		--End HARDCODE
	END
	
	/*--Biaya dokter plus emergency--*/
	IF @strMedicalTp = 'MEDICAL_TP_01'
	--Rawat Jalan
	BEGIN
		IF @strEmergencySt = '1'
			--HARDCODE
			UPDATE trx_medical_settlement SET amount=50000
			WHERE account_cd='AC101'
			AND payment_st='PAYMENT_ST_0'
			AND medical_cd=@pstrMedicalCd
			--End HARDCODE
	END
	
	/*--Biaya bidan/fisioterapi--*/
	DECLARE @strDrCd varchar(20)
	DECLARE @strParamedisTp varchar(20)
	IF @strMedicalTp = 'MEDICAL_TP_01'
	--Rawat Jalan
	BEGIN
		SELECT @strDrCd=B.dr_cd,@strParamedisTp=B.paramedis_tp
		FROM trx_medical_settlement A 
		JOIN trx_dokter B ON A.data_cd=B.dr_cd 
		WHERE A.medical_cd=@pstrMedicalCd
		
		--Biaya bidan
		IF @strParamedisTp = 'PARAMEDIS_TP_05'
		BEGIN
			--HARDCODE
			UPDATE trx_medical_settlement SET amount=10000
			WHERE account_cd='AC101'
			AND payment_st='PAYMENT_ST_0'
			AND data_cd=@strDrCd
			AND medical_cd=@pstrMedicalCd
			--End HARDCODE
		END
		--Biaya fisioterapi
		IF @strParamedisTp = 'PARAMEDIS_TP_07'
		BEGIN
			--HARDCODE
			UPDATE trx_medical_settlement SET amount=0
			WHERE account_cd='AC101'
			AND payment_st='PAYMENT_ST_0'
			AND data_cd=@strDrCd
			AND medical_cd=@pstrMedicalCd
			--End HARDCODE
		END
	END
END;

DROP FUNCTION IF EXISTS SP_SETTLEMENT_SHARE(Varchar,Varchar) CASCADE;
CREATE PROCEDURE [simrke].[SP_SETTLEMENT_SHARE] 
@pstrMedicalCd Varchar(10),
@pstrUserId Varchar(20)
AS
BEGIN
	DECLARE @strMedicalTp varchar(20)
	DECLARE @strPasienCd varchar(20)
	DECLARE @strKelasCd varchar(20)
	DECLARE @strAsuransiCd varchar(20)
	DECLARE @strAsuransiKelas varchar(20)
	
	DECLARE @dtDateIn datetime
	DECLARE @dtDateStart datetime
	DECLARE @dtDateEnd datetime
	DECLARE @strKelasTrx varchar(20)
	DECLARE @strTarifTp varchar(20)
	DECLARE @numAmountTotal numeric
	DECLARE @numAmountAsuransi numeric
	DECLARE @numAmountPasien numeric
	DECLARE @intRefSeqno bigint
	
	DECLARE @numAmountTarif numeric
	DECLARE @numDefaultTarif numeric
	DECLARE @intTotalDay int
	
	SELECT @strMedicalTp=A.medical_tp,@strPasienCd=A.pasien_cd,
	@dtDateIn=A.datetime_in,@strKelasCd=D.kelas_cd,
	@strAsuransiCd=C.insurance_cd,@strAsuransiKelas=C.standar_kelas_cd
	FROM trx_medical A 
	JOIN trx_pasien B ON A.pasien_cd=B.pasien_cd 
	LEFT JOIN trx_pasien_insurance C ON A.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_ruang D ON A.ruang_cd=D.ruang_cd 
	WHERE A.medical_cd=@pstrMedicalCd
	
	IF @strKelasCd IS NULL SET @strKelasCd = ''
	IF @strAsuransiCd IS NULL SET @strAsuransiCd = ''
	IF @strAsuransiKelas IS NULL SET @strAsuransiKelas = ''
	
	IF @strAsuransiCd <> ''
	BEGIN
		--Rawat Inap
		IF @strMedicalTp = 'MEDICAL_TP_02'
		--TARIF KELAS
		BEGIN
			SET @strTarifTp = 'TARIF_TP_03'
			
			SELECT @numDefaultTarif=A.tarif
			FROM trx_tarif_kelas A
			WHERE A.kelas_cd=@strAsuransiKelas
			AND (A.insurance_cd=@strAsuransiCd OR A.insurance_cd='')
			
			--aktif kamar
			IF EXISTS(SELECT TOP 1 A.datetime_end
				FROM trx_medical_ruang A
				WHERE A.medical_cd=@pstrMedicalCd
				ORDER BY A.datetime_start DESC)
			BEGIN	
				SET @dtDateIn = (SELECT TOP 1 A.datetime_end
								FROM trx_medical_ruang A
								WHERE A.medical_cd=@pstrMedicalCd
								ORDER BY A.datetime_start DESC)
			END
				
			SELECT @numAmountTarif=A.tarif
			FROM trx_tarif_kelas A
			WHERE A.kelas_cd=@strKelasCd
			AND (A.insurance_cd=@strAsuransiCd OR A.insurance_cd='')
			
			SELECT @intTotalDay=CASE WHEN DATEDIFF(DAY,@dtDateIn,GETDATE())<=0 THEN 1 ELSE DATEDIFF(DAY,@dtDateIn,GETDATE())+1 END
			FROM trx_medical A
			JOIN trx_ruang B ON A.ruang_cd=B.ruang_cd
			JOIN trx_kelas C ON B.kelas_cd=C.kelas_cd
			WHERE A.medical_cd=@pstrMedicalCd
			
			IF @strAsuransiKelas = @strKelasCd
			BEGIN
				SET @numAmountTotal = @numAmountTarif * @intTotalDay
				SET @numAmountAsuransi = @numAmountTotal
				SET @numAmountPasien = 0
			END
			ELSE
			BEGIN
				IF @numAmountTarif < @numDefaultTarif SET @numAmountTarif = @numDefaultTarif
				
				SET @numAmountTotal = @numAmountTarif * @intTotalDay
				SET @numAmountAsuransi = @numDefaultTarif * @intTotalDay
				SET @numAmountPasien = @numAmountTotal - (@numDefaultTarif * @intTotalDay)
			END
			
			INSERT INTO trx_settlement_share (medical_cd,tarif_tp,
			amount_total,amount_asuransi,amount_pasien,
			modi_id,modi_datetime)
			VALUES (@pstrMedicalCd,@strTarifTp,
			@numAmountTotal,@numAmountAsuransi,@numAmountPasien,
			@pstrUserId,GETDATE())
			--aktif kamar
			
			--history kamar
			DECLARE cursorData CURSOR FOR
			SELECT A.datetime_start,A.datetime_end,B.kelas_cd,A.seq_no
			FROM trx_medical_ruang A
			JOIN trx_ruang B ON A.ruang_cd=B.ruang_cd
			WHERE A.medical_cd=@pstrMedicalCd
			ORDER BY A.datetime_start
			
			OPEN cursorData
			FETCH NEXT FROM cursorData
			INTO @dtDateStart,@dtDateEnd,@strKelasTrx,@intRefSeqno
			WHILE (@@FETCH_STATUS = 0)
			BEGIN
				SET @intTotalDay = DATEDIFF(DAY,@dtDateStart,@dtDateEnd)
				IF @intTotalDay = 0 SET @intTotalDay = 1
				
				SELECT @numAmountTarif=A.tarif
				FROM trx_tarif_kelas A
				WHERE A.kelas_cd=@strKelasTrx
				AND (A.insurance_cd=@strAsuransiCd OR A.insurance_cd='')
				
				IF @strAsuransiKelas = @strKelasTrx
				BEGIN
					SET @numAmountTotal = @numAmountTarif * @intTotalDay
					SET @numAmountAsuransi = @numAmountTotal
					SET @numAmountPasien = 0
				END
				ELSE
				BEGIN
					IF @numAmountTarif < @numDefaultTarif SET @numAmountTarif = @numDefaultTarif
					
					SET @numAmountTotal = @numAmountTarif * @intTotalDay
					SET @numAmountAsuransi = @numDefaultTarif * @intTotalDay
					SET @numAmountPasien = @numAmountTotal - (@numDefaultTarif * @intTotalDay)
				END
				
				INSERT INTO trx_settlement_share (medical_cd,tarif_tp,
				amount_total,amount_asuransi,amount_pasien,ref_seqno,
				modi_id,modi_datetime)
				VALUES (@pstrMedicalCd,@strTarifTp,
				@numAmountTotal,@numAmountAsuransi,@numAmountPasien,@intRefSeqno,
				@pstrUserId,GETDATE())
				
				FETCH NEXT FROM cursorData
				INTO @dtDateStart,@dtDateEnd,@strKelasTrx,@intRefSeqno
			END
			CLOSE cursorData
			DEALLOCATE cursorData
			--end history kamar
		END
	END
END;
