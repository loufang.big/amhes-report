DROP FUNCTION public.sp_rpt_rl_1_2_indikator_pelayanan_rumah_sakit(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_1_2_indikator_pelayanan_rumah_sakit(p_datestart character varying, p_dateend character varying);
DROP FUNCTION public.sp_rpt_rl_1_3_fasilitas_tempat_tidur_rawat_inap(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
drop FUNCTION public.sp_rpt_rl_2_ketenagaan();
DROP FUNCTION public.sp_rpt_rl_3_10_kegiatan_pelayanan_khusus(p_datestart character varying, p_dateend character varying);
DROP FUNCTION public.sp_rpt_rl_3_10_kegiatan_pelayanan_khusus(p_datestart timestamp without time zone, p_dateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_3_11_kegiatan_kesehatan_jiwa(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_3_14_kegiatan_rujukan(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_3_15_cara_bayar(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_3_1_kegiatan_pelayanan_rawat_inap(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_3_2_kunjungan_rawat_darurat(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_3_3_kegiatan_kesehatan_gigi_mulut(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_3_4_kegiatan_kebidanan(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_3_5_kegiatan_perinatologi(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_3_6_kegiatan_pembedahan(p_datestart character varying, p_dateend character varying);
DROP FUNCTION public.sp_rpt_rl_3_6_kegiatan_pembedahan(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_3_7_kegiatan_radiologi(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_3_7_kegiatan_radiologi(p_datestart character varying, p_dateend character varying);
DROP FUNCTION public.sp_rpt_rl_3_8_pemeriksaan_laboratorium(p_datestart character varying, p_dateend character varying);
DROP FUNCTION public.sp_rpt_rl_3_8_pemeriksaan_laboratorium(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_3_9_pelayanan_rehabilitasi_medik(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_3_9_pelayanan_rehabilitasi_medik(p_datestart character varying, p_dateend character varying);
DROP FUNCTION public.sp_rpt_rl_4_a_penyakit_rawat_inap(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_4_b_penyakit_rawat_jalan(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
DROP FUNCTION public.sp_rpt_rl_5_2_kunjungan_rawat_jalan(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);

DROP FUNCTION public.FN_RPT_TOTAL_PASIENKELAS (p_kelasCd Varchar(20),p_dateStart varchar,p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.FN_RPT_TOTAL_PASIENKELAS (p_kelasCd Varchar(20),p_dateStart varchar,p_dateEnd varchar)
RETURNS Bigint
AS
$$
   	DECLARE v_intResult Int;
BEGIN
	SELECT 
	COUNT(A.medical_cd) into v_intResult
	FROM trx_medical A, trx_ruang B
	WHERE A.ruang_cd=B.ruang_cd
	AND B.kelas_cd=p_kelasCd
	AND (to_char(A.datetime_in, 'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd 
	or to_char(A.datetime_out, 'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd);

	RETURN v_intResult;
END;
$$ 
LANGUAGE plpgsql;

DROP FUNCTION public.FN_RPT_TOTAL_HARIRAWAT (p_kelasCd Varchar(10),p_dateStart varchar,p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.FN_RPT_TOTAL_HARIRAWAT (p_kelasCd Varchar(10),p_dateStart varchar,p_dateEnd varchar)
RETURNS Bigint
AS
$$
   	DECLARE v_intResult Int;
BEGIN	
		SELECT SUM(fn_datediff_day(to_char(A.datetime_in, 'yyyy-mm-dd'),to_char(A.datetime_out, 'yyyy-mm-dd')) + 1) into v_intResult
		FROM trx_medical A, trx_ruang B
		WHERE A.ruang_cd=B.ruang_cd
		AND B.kelas_cd=p_kelasCd
		AND (A.datetime_in BETWEEN p_dateStart::timestamp AND p_dateEnd::timestamp  OR
		A.datetime_out BETWEEN p_dateStart::timestamp  AND p_dateEnd::timestamp);
		        		  				  
	RETURN v_intResult;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION public.FN_INTERVAL_TO_INTEGER(p_interval interval);
 CREATE OR REPLACE FUNCTION public.FN_INTERVAL_TO_INTEGER(p_interval interval)
 RETURNS Int
 as
 $$
 DECLARE v_intResult Int;
 BEGIN
 	SELECT coalesce(extract(epoch from p_interval), 0) into v_intResult;
 	Return v_intResult;
 END;
 $$ 
 LANGUAGE plpgsql;

DROP FUNCTION public.fn_last_day_month (p_date Varchar);
CREATE OR REPLACE FUNCTION public.fn_last_day_month (p_date Varchar)
RETURNS varchar
AS
$$
   	DECLARE v_intResult varchar;
BEGIN	
	SELECT (date_trunc('MONTH', p_date::timestamp) + INTERVAL '1 MONTH - 1 day')::DATE;    		  				  
	RETURN v_intResult::varchar;
END;
$$ LANGUAGE plpgsql;


DROP FUNCTION public.FN_RPT_TOTAL_DEATH (p_KelasCd Varchar,p_DateStart varchar,p_DateEnd varchar);
CREATE OR REPLACE FUNCTION public.FN_RPT_TOTAL_DEATH (p_KelasCd Varchar,p_DateStart varchar,p_DateEnd varchar)
RETURNS Int
AS
$$
	DECLARE v_intResult Int;
BEGIN
	SELECT 
	SUM(fn_datediff_day(A.datetime_in::varchar,A.datetime_out::varchar) + 1) into v_intResult
	FROM trx_medical A, trx_ruang B
	WHERE A.ruang_cd=B.ruang_cd
	AND A.out_tp IN ('OUT_TP_11','OUT_TP_12')
	AND B.kelas_cd=p_KelasCd
	AND (to_char(A.datetime_in,'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd OR
		to_char(A.datetime_out,'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd);
	        		  				  
	RETURN v_intResult;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION public.SP_RPT_GET_BOR(p_Month int,p_Year int);
CREATE OR REPLACE FUNCTION public.SP_RPT_GET_BOR(p_Month int,p_Year int) 
RETURNS table(
	kelas_cd varchar,
	kelas_nm varchar,
	total_kamar Bigint,
	total_pasien Bigint,
	total_harirawat Bigint,
	day_month integer
)
AS $$
	DECLARE v_dtDateStart varchar;
 	v_dtDateEnd varchar;
BEGIN
	v_dtDateStart := concat(p_Year::Varchar, '-', LPAD(p_Month::varchar,2,'0')::Varchar, '-', '01')::varchar;
	v_dtDateEnd := (date_trunc('MONTH', v_dtDateStart::timestamp) + INTERVAL '1 MONTH - 1 day')::DATE;

	-- execute fn_last_day_month(v_dtDateStart) into v_dtDateEnd;
	
	RETURN QUERY SELECT 
	A.kelas_cd,
	A.kelas_nm,
	COUNT(B.ruang_cd) AS total_kamar,
	COALESCE(public.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd,v_dtDateStart::varchar,v_dtDateEnd::varchar),0) AS total_pasien,
	COALESCE(public.FN_RPT_TOTAL_HARIRAWAT(A.kelas_cd,v_dtDateStart,v_dtDateEnd),0) AS total_harirawat,
	to_char(v_dtDateEnd::timestamp, 'dd')::integer AS day_month
	FROM trx_kelas A 
	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd AND COALESCE(B.ruang_tp,'')<>'1'
	GROUP BY A.kelas_cd,A.kelas_nm
	ORDER BY A.kelas_nm;
END;
$$ LANGUAGE plpgsql;

select * from public.SP_RPT_GET_BOR(2, 2017);

DROP FUNCTION public.SP_RPT_GET_NDR (p_Month int,p_Year int);
CREATE OR REPLACE FUNCTION public.SP_RPT_GET_NDR (p_Month int,p_Year int) 
RETURNS table(
	kelas_cd varchar,
	kelas_nm varchar,
	total_kamar Bigint,
	total_pasien Bigint,
	total_meninggal int,
	day_month integer
)
AS $$
	DECLARE v_dtDateStart Timestamp(3);
	v_dtDateEnd Timestamp(3);
BEGIN
	v_dtDateStart := concat(p_Year::Varchar, '-', LPAD(p_Month::varchar,2,'0')::Varchar, '-', '01')::varchar;
	v_dtDateEnd := (date_trunc('MONTH', v_dtDateStart::timestamp) + INTERVAL '1 MONTH - 1 day')::DATE;
	-- execute fn_last_day_month(v_dtDateStart) into v_dtDateEnd;
	RETURN QUERY SELECT 
	A.kelas_cd,
	A.kelas_nm,
	COUNT(B.ruang_cd) AS total_kamar,
	COALESCE(public.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd,v_dtDateStart::varchar,v_dtDateEnd::varchar),0) AS total_pasien,
	COALESCE(public.FN_RPT_TOTAL_DEATH_OUTTP11(A.kelas_cd,v_dtDateStart,v_dtDateEnd),0) AS total_meninggal,
	to_char(v_dtDateEnd::timestamp, 'dd')::integer AS day_month
	FROM trx_kelas A 
	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd AND COALESCE(B.ruang_tp,'')<>'1'
	GROUP BY A.kelas_cd,A.kelas_nm
	ORDER BY A.kelas_nm;
END;
$$ LANGUAGE plpgsql;

select * from public.SP_RPT_GET_NDR(2, 2017);

DROP FUNCTION public.SP_RPT_GET_GDR (p_Month int, p_Year int);
CREATE OR REPLACE FUNCTION public.SP_RPT_GET_GDR (p_Month int, p_Year int) 
RETURNS table(
	kelas_cd varchar,
	kelas_nm varchar,
	total_kamar Bigint,
	total_pasien Bigint,
	total_meninggal int,
	day_month integer
)
AS $$
	DECLARE v_dtDateStart Timestamp(3);
	v_dtDateEnd Timestamp(3);
BEGIN
	v_dtDateStart := concat(p_Year::Varchar, '-', LPAD(p_Month::varchar,2,'0')::Varchar, '-', '01')::Timestamp;
	v_dtDateEnd := (date_trunc('MONTH', v_dtDateStart::timestamp) + INTERVAL '1 MONTH - 1 day')::DATE;
	
	RETURN QUERY SELECT 
	A.kelas_cd,
	A.kelas_nm,
	COUNT(B.ruang_cd) AS total_kamar,
	COALESCE(public.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd, v_dtDateStart::varchar,v_dtDateEnd::varchar),0) AS total_pasien,
	COALESCE(public.FN_RPT_TOTAL_DEATH(A.kelas_cd,v_dtDateStart::varchar,v_dtDateEnd::varchar),0) AS total_meninggal,
	to_char(v_dtDateEnd::timestamp, 'dd')::integer AS day_month
	FROM trx_kelas A 
	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd AND COALESCE(B.ruang_tp,'')<>'1'
	GROUP BY A.kelas_cd,A.kelas_nm
	ORDER BY A.kelas_nm;
END;
$$ 
LANGUAGE plpgsql;

select * from public.SP_RPT_GET_GDR(2, 2017);


DROP FUNCTION public.FN_RPT_TOTAL_DEATH_OUTTP11 (p_KelasCd Varchar(10),p_DateStart varchar(3),p_DateEnd varchar(3));
CREATE OR REPLACE FUNCTION public.FN_RPT_TOTAL_DEATH_OUTTP11 (p_KelasCd Varchar(10),p_DateStart varchar(3),p_DateEnd varchar(3))
RETURNS Int
AS
$$
	DECLARE v_intResult Int;
	-- SELECT FN_INTERVAL_TO_INTEGER(SUM(a.datetime_out::varchar - a.datetime_in::varchar)) + 1 into v_intResult
BEGIN
	SELECT FN_INTERVAL_TO_INTEGER(SUM(a.datetime_out::timestamp - a.datetime_in::timestamp)) into v_intResult
	FROM trx_medical A, trx_ruang B
	WHERE A.ruang_cd=B.ruang_cd
	AND A.out_tp='OUT_TP_11'
	AND B.kelas_cd=p_KelasCd
	AND (A.datetime_in BETWEEN p_dateStart::timestamp AND p_dateEnd::timestamp  OR
		A.datetime_out BETWEEN p_dateStart::timestamp  AND p_dateEnd::timestamp);   

	RETURN v_intResult;
END;
$$ 
LANGUAGE plpgsql;

DROP FUNCTION public.sp_rpt_rl_1_2_indikator_pelayanan_rumah_sakit(p_dateStart varchar, p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.sp_rpt_rl_1_2_indikator_pelayanan_rumah_sakit(p_dateStart varchar, p_dateEnd varchar)
 RETURNS TABLE(kelas_cd character varying, kelas_nm character varying, total_kamar bigint, total_pasien bigint, total_harirawat bigint, total_meninggal int, day_month integer)
AS $function$
BEGIN
	RETURN QUERY 
	SELECT 
	A.kelas_cd,
	A.kelas_nm,
	COUNT(B.ruang_cd) AS total_kamar,
	coalesce(public.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd,p_dateStart,p_dateEnd), 0) AS total_pasien,
	coalesce(public.FN_RPT_TOTAL_HARIRAWAT(A.kelas_cd,p_dateStart,p_dateEnd), 0) AS total_harirawat,
	coalesce(public.FN_RPT_TOTAL_DEATH_OUTTP11(A.kelas_cd,p_dateStart::varchar,p_dateEnd::varchar), 0) AS total_meninggal,
	to_char(p_dateEnd::timestamp, 'dd')::integer AS day_month
	FROM trx_kelas A 
	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd 
	GROUP BY A.kelas_cd,A.kelas_nm
	ORDER BY A.kelas_nm;
END;
$function$
 LANGUAGE plpgsql;

SELECT * from public.sp_rpt_rl_1_2_indikator_pelayanan_rumah_sakit('2017-01-01', '2017-12-31');

DROP FUNCTION public.sp_rpt_rl_2_ketenagaan();
CREATE OR REPLACE FUNCTION public.sp_rpt_rl_2_ketenagaan()
 RETURNS TABLE(paramedis_cd character varying, paramedis_nm character varying, paramedis_tp character varying, tipe_nm character varying)
AS $function$
BEGIN
	RETURN QUERY SELECT 
	A.paramedis_cd, 
	A.paramedis_nm, 
	A.paramedis_tp, 
	COMCD.code_nm AS tipe_nm
	FROM trx_paramedis A
	LEFT JOIN com_code COMCD On A.paramedis_tp=COMCD.com_cd
	ORDER BY COMCD.code_nm, A.paramedis_nm;
END;
$function$
LANGUAGE plpgsql;

SELECT * from public.sp_rpt_rl_2_ketenagaan();

DROP FUNCTION public.SP_RPT_RL_3_8_PEMERIKSAAN_LABORATORIUM (p_dateStart varchar,p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.SP_RPT_RL_3_8_PEMERIKSAAN_LABORATORIUM (p_dateStart varchar,p_dateEnd varchar)
RETURNS table(
	Jenis_Kegiatan varchar(100),
	Jumlah bigint
	)
AS $$
BEGIN
	RETURN QUERY SELECT
		medicalunit_nm AS Jenis_Kegiatan,
		COUNT(1)AS Jumlah
	FROM
		trx_medical_unit mast
	JOIN trx_unitmedis_item a ON a.medicalunit_cd = mast.medicalunit_cd
	AND a.medunit_cd = 'LAB00'
	AND mast.datetime_trx >=  p_dateStart::timestamp
	AND mast.datetime_trx <=  p_dateEnd::timestamp
	GROUP BY
		medicalunit_nm;
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_RL_3_8_PEMERIKSAAN_LABORATORIUM('2017-01-01', '2017-12-31');

DROP FUNCTION public.SP_RPT_RL_3_7_KEGIATAN_RADIOLOGI (p_dateStart varchar,p_dateEnd varchar) ;
CREATE OR REPLACE FUNCTION public.SP_RPT_RL_3_7_KEGIATAN_RADIOLOGI (p_dateStart varchar,p_dateEnd varchar) 
RETURNS table(
	Jenis_Kegiatan varchar(100),
	Jumlah bigint
	)
AS $$
BEGIN
	RETURN QUERY SELECT
		medicalunit_nm AS Jenis_Kegiatan,
		COUNT(1)AS Jumlah
	FROM
		trx_medical_unit mast
	JOIN trx_unitmedis_item a ON a.medicalunit_cd = mast.medicalunit_cd
	AND a.medunit_cd = 'RADIO00'
	AND mast.datetime_trx >=  p_dateStart::timestamp
	AND mast.datetime_trx <=  p_dateEnd::timestamp
	GROUP BY
		medicalunit_nm;
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_RL_3_7_KEGIATAN_RADIOLOGI('2017-01-01', '2017-12-31');


DROP FUNCTION public.SP_RPT_RL_3_6_KEGIATAN_PEMBEDAHAN (p_dateStart varchar,p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.SP_RPT_RL_3_6_KEGIATAN_PEMBEDAHAN (p_dateStart varchar,p_dateEnd varchar) 
RETURNS table(
	Jenis_Kegiatan text,
	Jumlah bigint
	)
AS $$
BEGIN
	RETURN QUERY SELECT
		RTRIM(LTRIM(C.treatment_nm)) AS Jenis_Kegiatan,
		COUNT(1)AS Jumlah
	FROM trx_medical_tindakan A
	JOIN trx_medical B ON A.medical_cd = B.medical_cd
	JOIN trx_tindakan C ON A.treatment_cd = C.treatment_cd
	WHERE
		B.medunit_cd = 'POLISPB'
	AND B.datetime_in >=  p_dateStart::timestamp
	AND B.datetime_in <=  p_dateEnd::timestamp
	GROUP BY
		C.treatment_nm;
END;
$$ 
LANGUAGE plpgsql;

SELECT * from public.SP_RPT_RL_3_6_KEGIATAN_PEMBEDAHAN('2017-01-01', '2017-12-31');

DROP FUNCTION public.sp_rpt_rl_3_9_pelayanan_rehabilitasi_medik(p_dateStart varchar, p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.sp_rpt_rl_3_9_pelayanan_rehabilitasi_medik(p_dateStart varchar, p_dateEnd varchar)
 RETURNS TABLE(jenis_kegiatan text, jumlah bigint)
AS $function$
BEGIN
	return query SELECT  RTRIM(LTRIM(A.medical_note)) as Jenis_Kegiatan,COUNT(A.medical_note) AS Jumlah
	FROM public.trx_medical_tindakan A JOIN public.trx_medical B ON A.medical_cd = B.medical_cd
	WHERE B.medunit_cd = 'POLIREHAB' 
	AND B.datetime_in >= p_dateStart::timestamp
	AND B.datetime_in <= p_dateEnd::timestamp
	GROUP BY A.medical_note;
END;
$function$
LANGUAGE plpgsql;

SELECT * from public.sp_rpt_rl_3_9_pelayanan_rehabilitasi_medik('2017-01-01', '2017-12-31');

DROP FUNCTION public.sp_rpt_rl_3_10_kegiatan_pelayanan_khusus(p_dateStart varchar, p_dateEnd varchar);
CREATE OR REPLACE FUNCTION public.sp_rpt_rl_3_10_kegiatan_pelayanan_khusus(p_dateStart varchar, p_dateEnd varchar)
 RETURNS TABLE(jenis_kegiatan text, jumlah bigint)
AS $function$
BEGIN
	return Query SELECT 
	RTRIM(LTRIM(A.medical_note)) as Jenis_Kegiatan,
	COUNT(A.medical_note) AS Jumlah
	FROM trx_medical_tindakan A 
	JOIN trx_medical B ON A.medical_cd = B.medical_cd
	WHERE  B.datetime_in >= p_dateStart::timestamp
	AND B.datetime_in <= p_dateEnd::timestamp
	OR A.medical_note LIKE '%elektro%'
	OR A.medical_note LIKE '%kardiographi%'
	OR A.medical_note LIKE '%myographi%' 
	OR A.medical_note LIKE '%echo%'
	OR A.medical_note LIKE '%cardiographi%'
	OR A.medical_note LIKE '%endoskopi%'
	OR A.medical_note LIKE '%hemodialisa%'
	OR A.medical_note LIKE '%densometri%'
	OR A.medical_note LIKE '%tulang%'
	OR A.medical_note LIKE '%densometri tulang%'
	OR A.medical_note LIKE '%pungsi%'
	OR A.medical_note LIKE '%spirometri%'
	OR A.medical_note LIKE '%kulit%'
	OR A.medical_note LIKE '%alergi%'
	OR A.medical_note LIKE '%histamin%'
	OR A.medical_note LIKE '%topometri%'
	OR A.medical_note LIKE '%akupuntur%'
	OR A.medical_note LIKE '%hiperbarik%'
	OR A.medical_note LIKE '%herbal%'
	OR A.medical_note LIKE '%jamu%'
	GROUP BY A.medical_note;
END;
$function$
LANGUAGE plpgsql;

SELECT * from public.sp_rpt_rl_3_10_kegiatan_pelayanan_khusus('2017-01-01', '2017-12-31');

DROP FUNCTION public.sp_rpt_rl_3_11_kegiatan_kesehatan_jiwa(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone);
CREATE OR REPLACE FUNCTION public.sp_rpt_rl_3_11_kegiatan_kesehatan_jiwa(p_pdtdatestart timestamp without time zone, p_pdtdateend timestamp without time zone)
 RETURNS TABLE(jenis_kegiatan text, jumlah integer)
AS $function$
BEGIN
	return QUERY SELECT  
	RTRIM(LTRIM(A.medical_note)) as Jenis_Kegiatan,
	COUNT(A.medical_note) AS Jumlah
	FROM public.trx_medical_tindakan A JOIN public.trx_medical B ON A.medical_cd = B.medical_cd
	WHERE B.medunit_cd = 'POLISPJW' 
	AND B.datetime_in >= p_pdtDateStart AND B.datetime_in <= p_pdtDateEnd 
	GROUP BY A.medical_note;
END;
$function$
LANGUAGE plpgsql;

SELECT * from public.sp_rpt_rl_3_11_kegiatan_kesehatan_jiwa('2017-01-01', '2017-12-31');