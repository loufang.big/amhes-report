 create view vw_pasien_rawat_jalan as
 SELECT trx_medical.medical_cd AS primekey,
    com_code.code_nm AS jenis_rawat,
    trx_unit_medis.medunit_nm AS kamar,
    "right"(trx_pasien.no_rm::text, 8)::character varying AS no_rm,
    trx_dokter.dr_nm,
    concat(trx_pasien.pasien_nm, ' ', trx_pasien.middle_nm, ' ', trx_pasien.last_nm) AS nama_pasien,
    to_char(trx_medical.datetime_in, 'dd-mm-yyyy'::text) AS tanggal_masuk,
    tipe_pasien.code_nm AS jns_pasien
   FROM trx_medical
     JOIN trx_pasien ON trx_medical.pasien_cd::text = trx_pasien.pasien_cd::text
     LEFT JOIN trx_dokter ON trx_medical.dr_cd::text = trx_dokter.dr_cd::text
     LEFT JOIN trx_unit_medis ON trx_unit_medis.medunit_cd::text = trx_medical.medunit_cd::text
     LEFT JOIN com_code ON com_code.com_cd::text = trx_medical.medical_tp::text
     LEFT JOIN trx_medical_ruang ON trx_medical_ruang.medical_cd::text = trx_medical.medical_cd::text
     LEFT JOIN trx_ruang ON trx_ruang.ruang_cd::text = trx_medical.ruang_cd::text
     LEFT JOIN com_code tipe_pasien ON trx_pasien.pasien_tp::text = tipe_pasien.com_cd::text
  WHERE trx_medical.medical_tp::text = 'MEDICAL_TP_01'::text AND trx_medical.medical_root_cd IS NULL AND trx_medical.medical_trx_st::text <> 'MEDICAL_TRX_ST_1'::text;

  create view vw_report_hermina_master as
     SELECT pas.pasien_cd,
    med.medical_cd,
    concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
    jk.code_nm AS jenis_kelamin,
    pas.birth_date,
    med.medical_tp,
    med.visit_tp,
    jaminan.insurance_cd,
    bangsal.bangsal_cd AS ruang_perawatan,
    med.dr_cd,
    dr.spesialis_cd,
    kelas.kelas_cd,
    COALESCE(( SELECT rm.icd_cd
           FROM trx_medical_record rm
          WHERE rm.medical_cd::text = med.medical_cd::text AND rm.rm_tp::text = 'RM_TP_1'::text), ( SELECT rm.icd_cd
           FROM trx_medical_record rm
          WHERE rm.medical_cd::text = med.medical_cd::text AND rm.rm_tp::text <> 'RM_TP_1'::text AND rm.datetime_record = (( SELECT max(rm2.datetime_record) AS max
                   FROM trx_medical_record rm2
                  WHERE rm.medical_cd::text = rm2.medical_cd::text)))) AS diagnosa,
    med.datetime_in,
    med.datetime_out,
    med.reff_tp,
    med.referensi_cd,
    med.out_tp,
    med.medical_trx_st
   FROM trx_medical med
     JOIN trx_pasien pas ON med.pasien_cd::text = pas.pasien_cd::text
     LEFT JOIN trx_ruang ruang ON ruang.ruang_cd::text = med.ruang_cd::text
     LEFT JOIN trx_kelas kelas ON kelas.kelas_cd::text = ruang.kelas_cd::text
     LEFT JOIN trx_dokter dr ON dr.dr_cd::text = med.dr_cd::text
     LEFT JOIN com_code jk ON jk.com_cd::text = pas.gender_tp::text
     LEFT JOIN com_code mtp ON mtp.com_cd::text = med.medical_tp::text
     LEFT JOIN trx_pasien_insurance jaminan ON jaminan.pasien_cd::text = med.pasien_cd::text
     LEFT JOIN trx_bangsal bangsal ON bangsal.bangsal_cd::text = ruang.bangsal_cd::text;

     create view vw_pasien_today as
         SELECT pasien.medical_cd,
    pasien.pasien_cd,
    pasien.pasien_nm,
    pasien.no_rm,
    pasien.birth_place,
    pasien.birth_date,
    pasien.blood_tp,
    pasien.height,
    pasien.weight,
    pasien.religion_tp,
    pasien.gender_tp,
    pasien.marital_tp,
    pasien.status
   FROM ( SELECT a.medical_cd,
            b.pasien_cd,
            to_char(a.datetime_in, 'DD-MM-YYYY'::text) AS tgl_masuk,
            b.pasien_nm,
            b.no_rm,
            b.birth_place,
            b.height,
            b.weight,
            to_char(b.birth_date::timestamp with time zone, 'DD-MM-YYYY'::text) AS birth_date,
            dar.code_nm AS blood_tp,
            agama.code_nm AS religion_tp,
            jk.code_nm AS gender_tp,
            mar.code_nm AS marital_tp,
            'RWI'::text AS status
           FROM trx_medical a
             JOIN trx_pasien b ON a.pasien_cd::text = b.pasien_cd::text
             JOIN com_code c ON a.medical_tp::text = c.com_cd::text
             LEFT JOIN com_code dar ON b.blood_tp::text = dar.com_cd::text
             LEFT JOIN com_code agama ON b.religion_cd::text = agama.com_cd::text
             LEFT JOIN com_code jk ON b.gender_tp::text = jk.com_cd::text
             LEFT JOIN com_code mar ON b.marital_tp::text = mar.com_cd::text
          WHERE a.medical_tp::text = 'MEDICAL_TP_01'::text
        UNION
         SELECT a.medical_cd,
            b.pasien_cd,
            to_char(a.datetime_in, 'DD-MM-YYYY'::text) AS tgl_masuk,
            b.pasien_nm,
            b.no_rm,
            b.birth_place,
            b.height,
            b.weight,
            to_char(b.birth_date::timestamp with time zone, 'DD-MM-YYYY'::text) AS birth_date,
            dar.code_nm AS blood_tp,
            agama.code_nm AS religion_tp,
            jk.code_nm AS gender_tp,
            mar.code_nm AS marital_tp,
            'RWJ'::text AS status
           FROM trx_medical a
             JOIN trx_pasien b ON a.pasien_cd::text = b.pasien_cd::text
             JOIN com_code c ON a.medical_tp::text = c.com_cd::text
             LEFT JOIN com_code dar ON b.blood_tp::text = dar.com_cd::text
             LEFT JOIN com_code agama ON b.religion_cd::text = agama.com_cd::text
             LEFT JOIN com_code jk ON b.gender_tp::text = jk.com_cd::text
             LEFT JOIN com_code mar ON b.marital_tp::text = mar.com_cd::text
          WHERE a.medical_tp::text = 'MEDICAL_TP_02'::text) pasien
  WHERE to_char(pasien.tgl_masuk::date::timestamp with time zone, 'YYYY-MM-DD'::text) = to_char(now(), 'YYYY-MM-DD'::text);

  create view vw_pasien_month as 
     SELECT pasien.medical_cd,
    pasien.pasien_cd,
    pasien.pasien_nm,
    pasien.no_rm,
    pasien.birth_place,
    pasien.birth_date,
    pasien.blood_tp,
    pasien.height,
    pasien.weight,
    pasien.religion_tp,
    pasien.gender_tp,
    pasien.marital_tp,
    pasien.status
   FROM ( SELECT a.medical_cd,
            b.pasien_cd,
            to_char(a.datetime_in, 'DD-MM-YYYY'::text) AS tgl_masuk,
            b.pasien_nm,
            b.no_rm,
            b.birth_place,
            b.height,
            b.weight,
            to_char(b.birth_date::timestamp with time zone, 'DD-MM-YYYY'::text) AS birth_date,
            dar.code_nm AS blood_tp,
            agama.code_nm AS religion_tp,
            jk.code_nm AS gender_tp,
            mar.code_nm AS marital_tp,
            'RWI'::text AS status
           FROM trx_medical a
             JOIN trx_pasien b ON a.pasien_cd::text = b.pasien_cd::text
             JOIN com_code c ON a.medical_tp::text = c.com_cd::text
             LEFT JOIN com_code dar ON b.blood_tp::text = dar.com_cd::text
             LEFT JOIN com_code agama ON b.religion_cd::text = agama.com_cd::text
             LEFT JOIN com_code jk ON b.gender_tp::text = jk.com_cd::text
             LEFT JOIN com_code mar ON b.marital_tp::text = mar.com_cd::text
          WHERE a.medical_tp::text = 'MEDICAL_TP_01'::text
        UNION
         SELECT a.medical_cd,
            b.pasien_cd,
            to_char(a.datetime_in, 'DD-MM-YYYY'::text) AS tgl_masuk,
            b.pasien_nm,
            b.no_rm,
            b.birth_place,
            b.height,
            b.weight,
            to_char(b.birth_date::timestamp with time zone, 'DD-MM-YYYY'::text) AS birth_date,
            dar.code_nm AS blood_tp,
            agama.code_nm AS religion_tp,
            jk.code_nm AS gender_tp,
            mar.code_nm AS marital_tp,
            'RWJ'::text AS status
           FROM trx_medical a
             JOIN trx_pasien b ON a.pasien_cd::text = b.pasien_cd::text
             JOIN com_code c ON a.medical_tp::text = c.com_cd::text
             LEFT JOIN com_code dar ON b.blood_tp::text = dar.com_cd::text
             LEFT JOIN com_code agama ON b.religion_cd::text = agama.com_cd::text
             LEFT JOIN com_code jk ON b.gender_tp::text = jk.com_cd::text
             LEFT JOIN com_code mar ON b.marital_tp::text = mar.com_cd::text
          WHERE a.medical_tp::text = 'MEDICAL_TP_02'::text) pasien
  WHERE to_char(pasien.tgl_masuk::date::timestamp with time zone, 'YYYY-MM'::text) = to_char(now(), 'YYYY-MM'::text);