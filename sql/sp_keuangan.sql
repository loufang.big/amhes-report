DROP FUNCTION SP_RPT_GET_KLAIMTRX_BYDATE (p_dateStart Varchar(10), p_dateEnd Varchar(10)); 
CREATE OR REPLACE FUNCTION SP_RPT_GET_KLAIMTRX_BYDATE (p_dateStart Varchar(10), p_dateEnd Varchar(10)) 
RETURNS TABLE(
	medical_tp Varchar,
	medical_tp_nm Varchar,
	trx_datetime text,
	pasien_nm text,
	no_rm Varchar,
	address Varchar,
	insurance_nm Varchar,
	dr_nm Varchar,
	medunit_nm Varchar,
	ruang_nm Varchar,
	kelas_nm Varchar,
	medical_data text
)
AS $$
BEGIN
	Return Query SELECT 
	A.medical_tp,
	COM.code_nm AS medical_tp_nm,
	to_char(A.datetime_in, 'DD-MM-YYYY') AS trx_datetime,
	concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
	PAS.no_rm,
	PAS.address,
	INS.insurance_nm,
	DR.dr_nm,
	RJ.medunit_nm,
	KMR.ruang_nm,
	KLS.kelas_nm,
	MR.medical_data
	FROM trx_medical A 
	LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
	JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
	JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
	JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
	LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
	LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
	LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
	LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
	LEFT JOIN trx_medical_record MR ON A.medical_cd=MR.medical_cd
	JOIN trx_settlement SETT ON A.medical_cd=SETT.medical_cd
	WHERE 
	SETT.invoice_date>=p_dateStart::timestamp  AND SETT.invoice_date<=p_dateEnd::timestamp 
	AND 
	SETT.payment_st='PAYMENT_ST_1'
	ORDER BY A.medical_tp,INS.insurance_nm,SETT.invoice_date;
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_GET_KLAIMTRX_BYDATE('2017-01-01', '2017-12-31');

SELECT 'SP_RPT_GET_KLAIMTRX_BYDATE';

DROP FUNCTION SP_RPT_GET_TRXACCOUNT_BYDATE (p_dateStart Varchar(10), p_dateEnd Varchar(10)); 
CREATE OR REPLACE FUNCTION SP_RPT_GET_TRXACCOUNT_BYDATE (p_dateStart Varchar(10), p_dateEnd Varchar(10)) 
RETURNS TABLE(
	invoice_no Varchar,
	insurance_nm Varchar,
	pasien_nm text,
	no_rm Varchar,
	account_nm Varchar,
	amount numeric
)
AS $$
BEGIN
	RETURN QUERY SELECT 
	A.invoice_no,
	INS.insurance_nm,
	concat(c.pasien_nm, ' ', c.middle_nm, ' ', c.last_nm) AS pasien_nm,
	C.no_rm,
	ACC.account_nm,
	B.amount
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	JOIN com_account ACC ON B.account_cd=ACC.account_cd
	LEFT JOIN trx_insurance INS ON A.insurance_cd=INS.insurance_cd
	JOIN trx_pasien C ON A.pasien_cd=C.pasien_cd
	WHERE A.invoice_date>=p_dateStart::timestamp  AND A.invoice_date<=p_dateEnd::timestamp 
	AND A.payment_st='PAYMENT_ST_1'
	UNION
	SELECT 
	A.invoice_no,
	'' as insurance_nm,
	'PENDAPATAN LAIN' as pasien_nm,
	'' as no_rm,
	ACC.account_nm,B.amount
	FROM trx_settlement A 
	JOIN trx_settlement_account B ON A.settlement_no=B.settlement_no
	JOIN com_account ACC ON B.account_cd=ACC.account_cd
	WHERE A.medical_cd IS NULL
	AND A.invoice_date>=p_dateStart::timestamp  AND A.invoice_date<=p_dateEnd::timestamp 
	and A.payment_st='PAYMENT_ST_1'
	ORDER BY invoice_no;
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_GET_TRXACCOUNT_BYDATE('2017-01-01', '2017-12-31');
SELECT 'SP_RPT_GET_TRXACCOUNT_BYDATE';

DROP FUNCTION FN_RPT_GET_TRXJM_PARAMEDIS (p_paramedisCd Varchar(20),p_treatmentCd Varchar(20),p_medicalCd Varchar(10));
CREATE OR REPLACE FUNCTION FN_RPT_GET_TRXJM_PARAMEDIS (p_paramedisCd Varchar(20),p_treatmentCd Varchar(20),p_medicalCd Varchar(10))
RETURNS Numeric(18,0)
AS
$$
   	DECLARE v_result Numeric(18,0);
	DECLARE	v_medicalTp varchar(20);
	DECLARE	v_kelasCd varchar(20);
	DECLARE	v_asuransiCd varchar(20);
BEGIN
	SELECT 
	A.medical_tp,
	D.kelas_cd,
	C.insurance_cd 
	INTO 
	v_medicalTp, 
	v_kelasCd, 
	v_asuransiCd
	FROM trx_medical A
	LEFT JOIN trx_pasien_insurance C ON A.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_ruang D ON A.ruang_cd=D.ruang_cd 
	WHERE A.medical_cd=p_medicalCd;
	
	IF EXISTS (SELECT COALESCE(A.tarif_item,0)
			  FROM trx_tariftp_item A
			  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
			  JOIN trx_tarif_tindakan C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
			  WHERE C.treatment_cd=p_treatmentCd
			  AND A.tarif_tp='TARIF_TP_01'
			  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_04' AND COALESCE(D.kelas_cd,'')=v_kelasCd)
			  ORDER BY A.trx_tarif_seqno DESC limit 1
			  ) THEN
		SELECT COALESCE(A.tarif_item,0) into v_result
		FROM trx_tariftp_item A
		JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
		JOIN trx_tarif_tindakan C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
		WHERE C.treatment_cd=p_treatmentCd
		AND A.tarif_tp='TARIF_TP_01'
		AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_04' AND COALESCE(D.kelas_cd,'')=v_kelasCd)
		ORDER BY A.trx_tarif_seqno DESC limit 1
		;
	ELSE
		v_result = 0;
	END IF;
	
	RETURN v_result;
END;
$$ LANGUAGE plpgsql;

SELECT 'FN_RPT_GET_TRXJM_PARAMEDIS';

DROP FUNCTION SP_RPT_GET_TRXJM_BYDATE_ORDERPARAMEDIS (p_dateStart Varchar(10), p_dateEnd Varchar(10), p_paramedisCd Varchar(20)); 
CREATE OR REPLACE FUNCTION SP_RPT_GET_TRXJM_BYDATE_ORDERPARAMEDIS (p_dateStart Varchar(10), p_dateEnd Varchar(10), p_paramedisCd Varchar(20)) 
RETURNS TABLE(
	medical_cd varchar,
	medical_tp varchar,
	medical_tp_nm varchar,
	datetime_in text,
	pasien_nm text,
	no_rm varchar,
	insurance_nm varchar,
	dr_nm varchar,
	paramedis_cd varchar,
	paramedis_nm varchar,
	medunit_nm varchar,
	ruang_nm varchar,
	kelas_nm varchar,
	datetime_trx text,
	treatment_nm varchar,
	jm_paramedis numeric
)
AS $$
BEGIN
	IF p_paramedisCd = '' THEN 
	/*--Semua--*/
		RETURN QUERY SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		DR.dr_nm,
		MP.paramedis_cd,
		PRM.paramedis_nm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN to_char(MTK.datetime_trx, 'DD-MM-YYYY') ELSE to_char(A.datetime_in, 'DD-MM-YYYY')  END AS datetime_trx,
		TK.treatment_nm,
		public.FN_RPT_GET_TRXJM_PARAMEDIS(MP.paramedis_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_paramedis
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_medical_paramedis MP ON MTK.medical_tindakan_seqno=MP.ref_proses_seqno
		JOIN trx_paramedis PRM ON MP.paramedis_cd=PRM.paramedis_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=p_dateStart::timestamp  AND A.datetime_in<=p_dateEnd::timestamp 
		UNION ALL
		SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		DR.dr_nm,
		MP.paramedis_cd,
		PRM.paramedis_nm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN to_char(MTK.datetime_trx, 'DD-MM-YYYY') ELSE to_char(A.datetime_in, 'DD-MM-YYYY')  END AS datetime_trx,
		TK.treatment_nm,
		public.FN_RPT_GET_TRXJM_PARAMEDIS(MP.paramedis_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_paramedis
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_medical_paramedis MP ON MTK.medical_tindakan_seqno=MP.ref_proses_seqno
		JOIN trx_paramedis PRM ON MP.paramedis_cd=PRM.paramedis_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND MTK.datetime_trx>=p_dateStart::timestamp  AND MTK.datetime_trx<=p_dateEnd::timestamp 
		ORDER BY paramedis_nm,medical_tp,datetime_trx,datetime_in;
	ELSE
	/*--Per paramedis--*/
		RETURN QUERY SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		DR.dr_nm,
		MP.paramedis_cd,
		PRM.paramedis_nm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN to_char(MTK.datetime_trx, 'DD-MM-YYYY') ELSE to_char(A.datetime_in, 'DD-MM-YYYY')  END AS datetime_trx,
		TK.treatment_nm,
		public.FN_RPT_GET_TRXJM_PARAMEDIS(MP.paramedis_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_paramedis
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_medical_paramedis MP ON MTK.medical_tindakan_seqno=MP.ref_proses_seqno
		JOIN trx_paramedis PRM ON MP.paramedis_cd=PRM.paramedis_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND MP.paramedis_cd=p_paramedisCd
		AND A.datetime_in>=p_dateStart::timestamp  AND A.datetime_in<=p_dateEnd::timestamp 
		UNION ALL
		SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		DR.dr_nm,
		MP.paramedis_cd,
		PRM.paramedis_nm,
		RJ.medunit_nm,
		KMR.ruang_nm,KLS.kelas_nm,
		CASE WHEN MTK.datetime_trx IS NOT NULL THEN to_char(MTK.datetime_trx, 'DD-MM-YYYY') ELSE to_char(A.datetime_in, 'DD-MM-YYYY')  END AS datetime_trx,
		TK.treatment_nm,
		public.FN_RPT_GET_TRXJM_PARAMEDIS(MP.paramedis_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_paramedis
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_medical_paramedis MP ON MTK.medical_tindakan_seqno=MP.ref_proses_seqno
		JOIN trx_paramedis PRM ON MP.paramedis_cd=PRM.paramedis_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND MP.paramedis_cd=p_paramedisCd
		AND MTK.datetime_trx>=p_dateStart::timestamp  AND MTK.datetime_trx<=p_dateEnd::timestamp 
		ORDER BY paramedis_nm,medical_tp,datetime_trx,datetime_in;
	END IF;
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_GET_TRXJM_BYDATE_ORDERPARAMEDIS('2017-01-01', '2017-12-31', '');
SELECT 'SP_RPT_GET_TRXJM_BYDATE_ORDERPARAMEDIS';

DROP FUNCTION FN_RPT_GET_TRXJM_UNIT (p_treatmentCd Varchar(20),p_medicalCd Varchar(10));
CREATE OR REPLACE FUNCTION FN_RPT_GET_TRXJM_UNIT (p_treatmentCd Varchar(20),p_medicalCd Varchar(10))
RETURNS Numeric(18,0)
AS
$$
   	DECLARE v_result Numeric(18,0);	
	DECLARE v_medicalTp varchar(20);
	DECLARE v_kelasCd varchar(20);
	DECLARE v_asuransiCd varchar(20);
BEGIN
	SELECT A.medical_tp,
	D.kelas_cd,C.insurance_cd INTO v_medicalTp, v_kelasCd, v_asuransiCd
	FROM trx_medical A
	LEFT JOIN trx_pasien_insurance C ON A.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_ruang D ON A.ruang_cd=D.ruang_cd 
	WHERE A.medical_cd=p_medicalCd;
	
	IF v_kelasCd IS NULL THEN 
		v_kelasCd = '' ; 
	END IF;
	IF v_asuransiCd IS NULL THEN 
		v_asuransiCd = ''; 
	END IF;
	
	IF EXISTS (SELECT COALESCE(A.tarif_item,0)
			  FROM trx_tariftp_item A
			  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
			  JOIN trx_tarif_tindakan C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
			  WHERE C.treatment_cd=p_treatmentCd
			  AND A.tarif_tp='TARIF_TP_01'
			  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_04' AND COALESCE(D.kelas_cd,'')=v_kelasCd)
			  ORDER BY A.trx_tarif_seqno DESC limit 1
			  ) THEN
		 SELECT COALESCE(A.tarif_item,0) into v_result
					  FROM trx_tariftp_item A
					  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
					  JOIN trx_tarif_tindakan C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
					  WHERE C.treatment_cd=p_treatmentCd
					  AND A.tarif_tp='TARIF_TP_01'
					  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_04' AND COALESCE(D.kelas_cd,'')=v_kelasCd)
					  ORDER BY A.trx_tarif_seqno DESC limit 1
					  ;
	ELSE
		v_result = 0;
	END IF;
	
	RETURN v_result;
END;
$$ 
LANGUAGE plpgsql;

SELECT 'FN_RPT_GET_TRXJM_UNIT';

DROP FUNCTION FN_RPT_GET_TRXJM_UNIT_UNITMEDIS (p_medicalunitCd Varchar(20),p_medicalCd Varchar(10));
CREATE OR REPLACE FUNCTION FN_RPT_GET_TRXJM_UNIT_UNITMEDIS (p_medicalunitCd Varchar(20),p_medicalCd Varchar(10))
RETURNS Numeric(18,0)
AS
$$
	DECLARE v_result Numeric(18,0);
	DECLARE v_medicalTp varchar(20);
	DECLARE v_kelasCd varchar(20);
	DECLARE v_asuransiCd varchar(20);
BEGIN

	
	SELECT 
	A.medical_tp,
	D.kelas_cd,
	C.insurance_cd 
	INTO 
	v_medicalTp, 
	v_kelasCd, 
	v_asuransiCd
	FROM trx_medical A
	LEFT JOIN trx_pasien_insurance C ON A.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_ruang D ON A.ruang_cd=D.ruang_cd 
	WHERE A.medical_cd=p_medicalCd;
	
	IF v_kelasCd IS NULL THEN v_kelasCd = '';
	END IF;
	IF v_asuransiCd IS NULL THEN v_asuransiCd = '';
	END IF;
	
	IF EXISTS (SELECT COALESCE(A.tarif_item,0) into v_result
			  FROM trx_tariftp_item A
			  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
			  JOIN trx_tarif_unitmedis C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
			  WHERE C.medicalunit_cd=p_medicalunitCd
			  AND A.tarif_tp='TARIF_TP_01'
			  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_04' AND COALESCE(D.kelas_cd,'')=v_kelasCd)
			  ORDER BY A.trx_tarif_seqno DESC limit 1
			  ) THEN
		 SELECT COALESCE(A.tarif_item,0)
					  FROM trx_tariftp_item A
					  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
					  JOIN trx_tarif_unitmedis C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
					  WHERE C.medicalunit_cd=p_medicalunitCd
					  AND A.tarif_tp='TARIF_TP_01'
					  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_04' AND COALESCE(D.kelas_cd,'')=v_kelasCd)
		 			  ORDER BY A.trx_tarif_seqno DESC limit 1
					  ;
	ELSE
		v_result = 0;
	END IF;
	
	RETURN v_result;
END;
$$ LANGUAGE plpgsql;

SELECT 'FN_RPT_GET_TRXJM_UNIT_UNITMEDIS';

DROP FUNCTION SP_RPT_GET_TRXJM_BYDATE_ORDERUNIT (p_dateStart Varchar(10), p_dateEnd Varchar(10), p_medunitCd Varchar(20)); 
CREATE OR REPLACE FUNCTION SP_RPT_GET_TRXJM_BYDATE_ORDERUNIT (p_dateStart Varchar(10), p_dateEnd Varchar(10), p_medunitCd Varchar(20)) 
RETURNS TABLE(
	medical_cd varchar,
	medical_tp varchar,
	medical_tp_nm varchar,
	datetime_in text,
	pasien_nm text,
	no_rm varchar,
	insurance_nm varchar,
	unit_nm varchar,
	ruang_nm varchar,
	kelas_nm varchar,
	datetime_trx text,
	treatment_nm varchar,
	jm_paramedis numeric
)
AS $$
BEGIN
	IF p_medunitCd = '' THEN 
	/*--Semua--*/
		RETURN QUERY SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		CASE WHEN COALESCE(MTK.medunit_cd,'') <> '' THEN RUNIT.medunit_nm ELSE RJ.medunit_nm END AS unit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		COALESCE(to_char(MTK.datetime_trx, 'DD-MM-YYYY'),to_char(A.datetime_in, 'DD-MM-YYYY')) AS datetime_trx,
		TK.treatment_nm,
		public.FN_RPT_GET_TRXJM_UNIT(MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_unit
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_unit_medis RUNIT ON MTK.medunit_cd=RUNIT.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=p_dateStart::timestamp  AND A.datetime_in<=p_dateEnd::timestamp 
		UNION ALL
		SELECT 
		A.medical_cd,
		A.medical_tp,COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		CASE WHEN COALESCE(MTK.medunit_cd,'') <> '' THEN RUNIT.medunit_nm ELSE BNG.bangsal_nm END AS unit_nm,
		KMR.ruang_nm,KLS.kelas_nm,
		COALESCE(to_char(MTK.datetime_trx, 'DD-MM-YYYY'),to_char(A.datetime_in, 'DD-MM-YYYY')) AS datetime_trx,
		TK.treatment_nm,
		public.FN_RPT_GET_TRXJM_UNIT(MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_unit
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_unit_medis RUNIT ON MTK.medunit_cd=RUNIT.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND MTK.datetime_trx>=p_dateStart::timestamp  AND MTK.datetime_trx<=p_dateEnd::timestamp 
		--Laboratorium/Radiologi
		UNION ALL
		SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		UNIT.medunit_nm AS unit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		COALESCE(to_char(MUNIT.datetime_trx, 'DD-MM-YYYY'),to_char(A.datetime_in, 'DD-MM-YYYY')) AS datetime_trx,
		MUITEM.medicalunit_nm AS treatment_nm,
		public.FN_RPT_GET_TRXJM_UNIT_UNITMEDIS(MUNIT.medicalunit_cd,A.medical_cd) AS jm_unit
		FROM trx_medical A
		JOIN trx_medical_unit MUNIT ON A.medical_cd=MUNIT.medical_cd
		JOIN trx_unitmedis_item MUITEM ON MUNIT.medicalunit_cd=MUITEM.medicalunit_cd
		JOIN trx_unit_medis UNIT ON MUITEM.medunit_cd=UNIT.medunit_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		AND MUNIT.datetime_trx>=p_dateStart::timestamp  AND MUNIT.datetime_trx<=p_dateEnd::timestamp 
		--End Laboratorium/Radiologi (unit medis)
		ORDER BY unit_nm,medical_tp,datetime_trx,datetime_in;
	ELSE
	/*--Per unit--*/
		RETURN QUERY SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		CASE WHEN COALESCE(MTK.medunit_cd,'') <> '' THEN RUNIT.medunit_nm ELSE RJ.medunit_nm END AS unit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		COALESCE(to_char(MTK.datetime_trx, 'DD-MM-YYYY'),to_char(A.datetime_in, 'DD-MM-YYYY')) AS datetime_trx,
		TK.treatment_nm,
		public.FN_RPT_GET_TRXJM_UNIT(MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_unit
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_unit_medis RUNIT ON MTK.medunit_cd=RUNIT.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND RJ.medunit_cd=p_medunitCd
		AND A.datetime_in>=p_dateStart::timestamp  AND A.datetime_in<=p_dateEnd::timestamp 
		UNION ALL
		SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		CASE WHEN COALESCE(MTK.medunit_cd,'') <> '' THEN RUNIT.medunit_nm ELSE BNG.bangsal_nm END AS unit_nm,
		KMR.ruang_nm,KLS.kelas_nm,
		COALESCE(to_char(MTK.datetime_trx, 'DD-MM-YYYY'),to_char(A.datetime_in, 'DD-MM-YYYY')) AS datetime_trx,
		TK.treatment_nm,
		public.FN_RPT_GET_TRXJM_UNIT(MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_unit
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_unit_medis RUNIT ON MTK.medunit_cd=RUNIT.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		LEFT JOIN trx_bangsal BNG ON KMR.bangsal_cd=BNG.bangsal_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND BNG.bangsal_cd=p_medunitCd
		AND MTK.datetime_trx>=p_dateStart::timestamp  AND MTK.datetime_trx<=p_dateEnd::timestamp 
		--Laboratorium/Radiologi
		UNION ALL
		SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		UNIT.medunit_nm AS unit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		COALESCE(to_char(MUNIT.datetime_trx, 'DD-MM-YYYY'),to_char(A.datetime_in, 'DD-MM-YYYY')) AS datetime_trx,
		MUITEM.medicalunit_nm AS treatment_nm,
		public.FN_RPT_GET_TRXJM_UNIT_UNITMEDIS(MUNIT.medicalunit_cd,A.medical_cd) AS jm_unit
		FROM trx_medical A
		JOIN trx_medical_unit MUNIT ON A.medical_cd=MUNIT.medical_cd
		JOIN trx_unitmedis_item MUITEM ON MUNIT.medicalunit_cd=MUITEM.medicalunit_cd
		JOIN trx_unit_medis UNIT ON MUITEM.medunit_cd=UNIT.medunit_cd AND UNIT.medunit_cd=p_medunitCd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		AND MUNIT.datetime_trx>=p_dateStart::timestamp  AND MUNIT.datetime_trx<=p_dateEnd::timestamp 
		--End Laboratorium/Radiologi (unit medis)
		ORDER BY unit_nm,medical_tp,datetime_trx,datetime_in;
	END IF;
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_GET_TRXJM_BYDATE_ORDERUNIT('2017-01-01', '2017-12-31', '');
SELECT 'SP_RPT_GET_TRXJM_BYDATE_ORDERUNIT';

DROP FUNCTION FN_RPT_GET_TRXJM_DOKTER (p_drCd Varchar(20),p_treatmentCd Varchar(20),p_medicalCd Varchar(10));
CREATE OR REPLACE FUNCTION FN_RPT_GET_TRXJM_DOKTER (p_drCd Varchar(20),p_treatmentCd Varchar(20),p_medicalCd Varchar(10))
RETURNS Numeric(18,0)
AS
$$
   	DECLARE v_result Numeric(18,0);
	DECLARE v_spesialisCd varchar(20);
	DECLARE v_medicalTp varchar(20);
	DECLARE v_kelasCd varchar(20);
	DECLARE v_asuransiCd varchar(20);
	DECLARE v_emergencySt char(1);
BEGIN
	SELECT spesialis_cd INTO v_spesialisCd
	FROM trx_dokter
	WHERE dr_cd=p_drCd;
	
	SELECT 
	A.medical_tp,
	A.emergency_st,
	D.kelas_cd,
	C.insurance_cd 
	INTO 
	v_medicalTp, 
	v_emergencySt, 
	v_kelasCd, 
	v_asuransiCd
	FROM trx_medical A
	LEFT JOIN trx_pasien_insurance C ON A.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_ruang D ON A.ruang_cd=D.ruang_cd 
	WHERE A.medical_cd=p_medicalCd;
	
	IF v_kelasCd IS NULL THEN v_kelasCd = '';
	END IF;
	IF v_asuransiCd IS NULL THEN v_asuransiCd = '';
	END IF;
	
	IF p_treatmentCd <> ''
	THEN
		IF v_spesialisCd = ''
		THEN
			IF EXISTS (SELECT COALESCE(A.tarif_item,0)
					  FROM trx_tariftp_item A
					  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
					  JOIN trx_tarif_tindakan C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
					  WHERE C.treatment_cd=p_treatmentCd
					  AND A.tarif_tp='TARIF_TP_01'
					  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
					  ORDER BY A.trx_tarif_seqno DESC limit 1
					  ) THEN
				 SELECT COALESCE(A.tarif_item,0) into v_result
							  FROM trx_tariftp_item A
							  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
							  JOIN trx_tarif_tindakan C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
							  WHERE C.treatment_cd=p_treatmentCd
							  AND A.tarif_tp='TARIF_TP_01'
							  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
							  ORDER BY A.trx_tarif_seqno DESC limit 1
							  ;
			ELSE
				 SELECT COALESCE(A.tarif_item,0) into v_result
							  FROM trx_tariftp_item A
							  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
							  JOIN trx_tarif_tindakan C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
							  WHERE C.treatment_cd=p_treatmentCd
							  AND A.tarif_tp='TARIF_TP_01'
							  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
							  ORDER BY A.trx_tarif_seqno DESC limit 1
							  ;
			END IF;
		ELSE
			IF EXISTS (SELECT COALESCE(A.tarif_item,0)
					  FROM trx_tariftp_item A
					  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
					  JOIN trx_tarif_tindakan C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
					  WHERE C.treatment_cd=p_treatmentCd
					  AND A.tarif_tp='TARIF_TP_01'
					  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
					  ORDER BY A.trx_tarif_seqno DESC limit 1
					  ) THEN
				 SELECT COALESCE(A.tarif_item,0) into v_result
							  FROM trx_tariftp_item A
							  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
							  JOIN trx_tarif_tindakan C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
							  WHERE C.treatment_cd=p_treatmentCd
							  AND A.tarif_tp='TARIF_TP_01'
							  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
							  ORDER BY A.trx_tarif_seqno DESC limit 1
							  ;
			ELSE
				 SELECT COALESCE(A.tarif_item,0) into v_result
							  FROM trx_tariftp_item A
							  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
							  JOIN trx_tarif_tindakan C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
							  WHERE C.treatment_cd=p_treatmentCd
							  AND A.tarif_tp='TARIF_TP_01'
							  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
							  ORDER BY A.trx_tarif_seqno DESC limit 1
							  ;
			END IF;
		END IF;

	ELSE
		IF v_spesialisCd = ''
		THEN
			 SELECT COALESCE(A.tarif,0) into v_result
							FROM trx_tarif_paramedis A
							WHERE A.paramedis_tp='PARAMEDIS_TP_01'
							/*--AND (A.kelas_cd='' AND A.insurance_cd='')--*/
							order by A.seq_no DESC limit 1
							;
			--HARDCODE
			--SET @numResult = 15000
			--End HARDCODE				
		ELSE
			 SELECT COALESCE(A.tarif,0) into v_result
							FROM trx_tarif_paramedis A
							WHERE A.paramedis_tp='PARAMEDIS_TP_02'
							/*--AND (A.kelas_cd='' AND A.insurance_cd='')--*/
							order by A.seq_no DESC limit 1
							;
			--HARDCODE
			--SET @numResult = 25000
			--End HARDCODE				
		END IF;
		
		/*--Biaya dokter emergency--*/
		IF v_medicalTp = 'MEDICAL_TP_01'
		THEN
			IF v_emergencySt = '1'
			THEN
				 SELECT COALESCE(A.tarif,0) into v_result
							FROM trx_tarif_paramedis A
							WHERE A.paramedis_tp='PARAMEDIS_TP_02'
							/*--AND (A.kelas_cd='' AND A.insurance_cd='')--*/
							order by A.seq_no DESC limit 1
							;
				--HARDCODE
				--SET @numResult = 25000
				--End HARDCODE
			END IF;
		END IF;
	END IF;
	
	RETURN v_result;
END;
$$ LANGUAGE plpgsql;

SELECT 'FN_RPT_GET_TRXJM_DOKTER';

DROP FUNCTION FN_RPT_GET_TRXJM_DOKTER_UNITMEDIS (p_drCd Varchar(20),p_medicalunitCd Varchar(20),p_medicalCd Varchar(10));
CREATE OR REPLACE FUNCTION FN_RPT_GET_TRXJM_DOKTER_UNITMEDIS (p_drCd Varchar(20),p_medicalunitCd Varchar(20),p_medicalCd Varchar(10))
RETURNS Numeric(18,0)
AS
$$
   	DECLARE v_result Numeric(18,0);
	DECLARE v_spesialisCd varchar(20);
	DECLARE v_medicalTp varchar(20);
	DECLARE v_kelasCd varchar(20);
	DECLARE v_asuransiCd varchar(20);
BEGIN
	SELECT spesialis_cd INTO v_spesialisCd
	FROM trx_dokter
	WHERE dr_cd=p_drCd;
	
	SELECT A.medical_tp,D.kelas_cd,C.insurance_cd INTO v_medicalTp, v_kelasCd, v_asuransiCd
	FROM trx_medical A
	LEFT JOIN trx_pasien_insurance C ON A.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_ruang D ON A.ruang_cd=D.ruang_cd 
	WHERE A.medical_cd=p_medicalCd;
	
	IF v_kelasCd IS NULL THEN v_kelasCd = '';
	END IF;
	IF v_asuransiCd IS NULL THEN v_asuransiCd = '';
	END IF;
	
	IF p_medicalunitCd <> ''
	THEN
		IF v_spesialisCd = ''
		THEN
			IF EXISTS (SELECT COALESCE(A.tarif_item,0) into v_result
					  FROM trx_tariftp_item A
					  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
					  JOIN trx_tarif_unitmedis C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
					  WHERE C.medicalunit_cd=p_medicalunitCd
					  AND A.tarif_tp='TARIF_TP_01'
					  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
							  ORDER BY A.trx_tarif_seqno DESC limit 1
					  ) THEN
				 SELECT COALESCE(A.tarif_item,0) into v_result
							  FROM trx_tariftp_item A
							  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
							  JOIN trx_tarif_unitmedis C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
							  WHERE C.medicalunit_cd=p_medicalunitCd
							  AND A.tarif_tp='TARIF_TP_01'
							  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
							  ORDER BY A.trx_tarif_seqno DESC limit 1
							  ;
			ELSE
				 SELECT COALESCE(A.tarif_item,0) into v_result
							  FROM trx_tariftp_item A
							  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
							  JOIN trx_tarif_unitmedis C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
							  WHERE C.medicalunit_cd=p_medicalunitCd
							  AND A.tarif_tp='TARIF_TP_01'
							  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
							  ORDER BY A.trx_tarif_seqno DESC limit 1
							  ;
			END IF;
		ELSE
			IF EXISTS (SELECT COALESCE(A.tarif_item,0) into v_result
					  FROM trx_tariftp_item A
					  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
					  JOIN trx_tarif_unitmedis C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
					  WHERE C.medicalunit_cd=p_medicalunitCd
					  AND A.tarif_tp='TARIF_TP_01'
					  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
					  ORDER BY A.trx_tarif_seqno DESC limit 1
					  ) THEN
				 SELECT COALESCE(A.tarif_item,0) into v_result
							  FROM trx_tariftp_item A
							  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
							  JOIN trx_tarif_unitmedis C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
							  WHERE C.medicalunit_cd=p_medicalunitCd
							  AND A.tarif_tp='TARIF_TP_01'
							  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
							  ORDER BY A.trx_tarif_seqno DESC limit 1
							  ;
			ELSE
				 SELECT COALESCE(A.tarif_item,0) into v_result
							  FROM trx_tariftp_item A
							  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
							  JOIN trx_tarif_unitmedis C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
							  WHERE C.medicalunit_cd=p_medicalunitCd
							  AND A.tarif_tp='TARIF_TP_01'
							  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
							  ORDER BY A.trx_tarif_seqno DESC limit 1
							  ;
			END IF;
		END IF;

	END IF;
	
	RETURN v_result;
END;
$$ LANGUAGE plpgsql;

SELECT 'FN_RPT_GET_TRXJM_DOKTER_UNITMEDIS';

DROP FUNCTION FN_RPT_GET_TRXJM_DOKTER_VISIT (p_drCd Varchar(20),p_trxDrTp Varchar(20),p_medicalCd Varchar(10));
CREATE OR REPLACE FUNCTION FN_RPT_GET_TRXJM_DOKTER_VISIT (p_drCd Varchar(20),p_trxDrTp Varchar(20),p_medicalCd Varchar(10))
RETURNS Numeric(18,0)
AS
$$
   	DECLARE v_result Numeric(18,0);
	DECLARE v_spesialisCd varchar(20);
	DECLARE v_medicalTp varchar(20);
	DECLARE v_kelasCd varchar(20);
	DECLARE v_asuransiCd varchar(20);
	DECLARE v_emergencySt char(1);
BEGIN
	SELECT spesialis_cd INTO v_spesialisCd
	FROM trx_dokter
	WHERE dr_cd=p_drCd;
	
	SELECT 
	A.medical_tp,
	A.emergency_st,
	D.kelas_cd,
	C.insurance_cd 
	INTO 
	v_medicalTp, 
	v_emergencySt, 
	v_kelasCd, 
	v_asuransiCd
	FROM trx_medical A
	LEFT JOIN trx_pasien_insurance C ON A.pasien_cd=C.pasien_cd AND C.default_st='1'
	LEFT JOIN trx_ruang D ON A.ruang_cd=D.ruang_cd 
	WHERE A.medical_cd=p_medicalCd;
	
	IF v_kelasCd IS NULL THEN v_kelasCd := '';
	END IF;
	IF v_asuransiCd IS NULL THEN v_asuransiCd := '';
	END IF;
	
	IF p_trxDrTp = 'TRXDR_TP_1'
	THEN
		IF v_spesialisCd = ''
		THEN
			 SELECT  COALESCE(A.tarif_item,0) into v_result
							  FROM trx_tariftp_item A
							  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
							  JOIN trx_tarif_tindakan C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
							  WHERE C.treatment_cd='TRJM001'
							  AND A.tarif_tp='TARIF_TP_01'
							  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
							  ORDER BY A.trx_tarif_seqno DESC limit 1
							  ;
		ELSE
			 SELECT  COALESCE(A.tarif_item,0) into v_result
							  FROM trx_tariftp_item A
							  JOIN trx_tarif_tp B ON A.tariftp_no=B.tariftp_no
							  JOIN trx_tarif_tindakan C ON B.trx_tarif_seqno=C.seq_no AND COALESCE(C.kelas_cd,'')=v_kelasCd
							  WHERE C.treatment_cd='TRJM002'
							  AND A.tarif_tp='TARIF_TP_01'
							  AND A.trx_tarif_seqno=(SELECT seq_no FROM trx_tarif_paramedis D WHERE D.paramedis_tp='PARAMEDIS_TP_01' ORDER BY seq_no DESC limit 1 /*--AND ISNULL(D.kelas_cd,'')=@strKelasCd--*/)
							  ORDER BY A.trx_tarif_seqno DESC limit 1
							  ;
		END IF;

	END IF;
	
	RETURN v_result;
END;
$$ LANGUAGE plpgsql;

SELECT 'FN_RPT_GET_TRXJM_DOKTER_VISIT';

DROP FUNCTION SP_RPT_GET_TRXJM_BYDATE_ORDERDR (p_dateStart Varchar(10),p_dateEnd Varchar(10),p_drCd Varchar(20)); 
CREATE OR REPLACE FUNCTION SP_RPT_GET_TRXJM_BYDATE_ORDERDR (p_dateStart Varchar(10),p_dateEnd Varchar(10),p_drCd Varchar(20)) 
RETURNS TABLE(
	medical_cd varchar,
	medical_tp varchar,
	medical_tp_nm varchar,
	datetime_in text,
	pasien_nm text,
	no_rm varchar,
	insurance_nm varchar,
	dr_nm varchar,
	medunit_nm varchar,
	ruang_nm varchar,
	kelas_nm varchar,
	datetime_trx text,
	treatment_nm varchar,
	jm_paramedis numeric
)
AS $$
BEGIN
	IF p_drCd = '' THEN 
	/*--Semua--*/
		RETURN QUERY SELECT 
		A.medical_cd,
		A.medical_tp,COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		--public.FN_FORMATDATE(A.datetime_in) AS trx_datetime,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		DR.dr_nm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		COALESCE(to_char(MTK.datetime_trx, 'DD-MM-YYYY'), to_char(A.datetime_in,'DD-MM-YYYY')) AS datetime_trx,
		TK.treatment_nm,
		public.FN_RPT_GET_TRXJM_DOKTER(MTK.dr_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_dr
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MTK.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=p_dateStart::timestamp  AND A.datetime_in<=p_dateEnd::timestamp 
		UNION ALL
		SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		DR.dr_nm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		COALESCE(to_char(MTK.datetime_trx, 'DD-MM-YYYY'), to_char(A.datetime_in,'DD-MM-YYYY')) AS datetime_trx,
		TK.treatment_nm,
		public.FN_RPT_GET_TRXJM_DOKTER(MTK.dr_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_dr
		FROM trx_medical A
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MTK.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND MTK.datetime_trx>=p_dateStart::timestamp  AND MTK.datetime_trx<=p_dateEnd::timestamp 
		--Non tindakan
		--pemeriksaan rawat jalan
		/*UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		A.datetime_in AS datetime_trx,'' AS treatment_nm,
		public.FN_RPT_GET_TRXJM_DOKTER(A.dr_cd,'',A.medical_cd) AS jm_dr
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=public.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=public.FN_STRINGTODATE(@pdtDateEnd)*/
		--mutasi rawat inap
		--HARDCODE
		--15000
		--End HARDCODE
		/*
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		A.datetime_in AS datetime_trx,'MUTASI PASIEN' AS treatment_nm,
		15000 AS jm_dr
		FROM trx_medical A
		JOIN trx_medical MROOT ON A.medical_cd=MROOT.medical_root_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=public.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=public.FN_STRINGTODATE(@pdtDateEnd)
		*/
		--End Non tindakan
		--Visit dokter
		UNION ALL
		SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		DR.dr_nm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		COALESCE(to_char(MDR.datetime_trx, 'DD-MM-YYYY'), to_char(A.datetime_in,'DD-MM-YYYY')) AS datetime_trx,
		CASE WHEN COALESCE(DR.spesialis_cd,'')<>'' THEN 'Visit Dokter Spesialis' ELSE 'Visit Dokter Umum' END AS treatment_nm,
		public.FN_RPT_GET_TRXJM_DOKTER_VISIT(MDR.dr_cd,'TRXDR_TP_1',A.medical_cd) AS jm_dr
		FROM trx_medical A
		JOIN trx_medical_dokter MDR ON A.medical_cd=MDR.medical_cd AND trx_dr_tp='TRXDR_TP_1' AND MDR.datetime_trx>=p_dateStart::timestamp  AND MDR.datetime_trx<=p_dateEnd::timestamp 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MDR.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		--End Visit dokter
		--Laboratorium/Radiologi (unit medis)
		UNION ALL
		SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		DR.dr_nm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		COALESCE(to_char(MUNIT.datetime_trx, 'DD-MM-YYYY'), to_char(A.datetime_in,'DD-MM-YYYY')) AS datetime_trx,
		MUITEM.medicalunit_nm AS treatment_nm,
		public.FN_RPT_GET_TRXJM_DOKTER_UNITMEDIS(MUNIT.dr2_cd,MUNIT.medicalunit_cd,A.medical_cd) AS jm_dr
		FROM trx_medical A
		JOIN trx_medical_unit MUNIT ON A.medical_cd=MUNIT.medical_cd AND MUNIT.datetime_trx>=p_dateStart::timestamp  AND MUNIT.datetime_trx<=p_dateEnd::timestamp 
		JOIN trx_unitmedis_item MUITEM ON MUNIT.medicalunit_cd=MUITEM.medicalunit_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MUNIT.dr2_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		--End Laboratorium/Radiologi (unit medis)
		ORDER BY dr_nm,medical_tp,datetime_trx,datetime_in;
	ELSE
	/*--Per dokter--*/
		RETURN QUERY SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		DR.dr_nm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		COALESCE(to_char(MTK.datetime_trx, 'DD-MM-YYYY'), to_char(A.datetime_in,'DD-MM-YYYY')) AS datetime_trx,
		TK.treatment_nm,
		public.FN_RPT_GET_TRXJM_DOKTER(MTK.dr_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_dr
		FROM trx_medical A 
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MTK.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL AND DR.dr_cd=p_drCd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=p_dateStart::timestamp  AND A.datetime_in<=p_dateEnd::timestamp 
		UNION ALL
		SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		DR.dr_nm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		COALESCE(to_char(MTK.datetime_trx, 'DD-MM-YYYY'), to_char(A.datetime_in,'DD-MM-YYYY')) AS datetime_trx,
		TK.treatment_nm,
		public.FN_RPT_GET_TRXJM_DOKTER(MTK.dr_cd,MTK.treatment_cd,A.medical_cd) * MTK.quantity AS jm_dr
		FROM trx_medical A
		JOIN trx_medical_tindakan MTK ON A.medical_cd=MTK.medical_cd
		JOIN trx_tindakan TK ON MTK.treatment_cd=TK.treatment_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MTK.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL AND DR.dr_cd=p_drCd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_02'
		AND MTK.datetime_trx>=p_dateStart::timestamp  AND MTK.datetime_trx<=p_dateEnd::timestamp 
		--Non tindakan
		--pemeriksaan rawat jalan
		/*UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		A.datetime_in AS datetime_trx,'' AS treatment_nm,
		public.FN_RPT_GET_TRXJM_DOKTER(A.dr_cd,'',A.medical_cd) AS jm_dr
		FROM trx_medical A 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL AND DR.dr_cd=@pstrDrCd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=public.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=public.FN_STRINGTODATE(@pdtDateEnd)*/
		--mutasi rawat inap
		--HARDCODE
		--15000
		--End HARDCODE
		/*
		UNION ALL
		SELECT A.medical_cd,A.medical_tp,COM.code_nm AS medical_tp_nm,A.datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,PAS.no_rm,INS.insurance_nm,
		DR.dr_nm,RJ.medunit_nm,KMR.ruang_nm,KLS.kelas_nm,
		A.datetime_in AS datetime_trx,'MUTASI PASIEN' AS treatment_nm,
		15000 AS jm_dr
		FROM trx_medical A
		JOIN trx_medical MROOT ON A.medical_cd=MROOT.medical_root_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON A.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL AND DR.dr_cd=@pstrDrCd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		WHERE A.medical_tp='MEDICAL_TP_01'
		AND A.datetime_in>=public.FN_STRINGTODATE(@pdtDateStart) AND A.datetime_in<=public.FN_STRINGTODATE(@pdtDateEnd)
		*/
		--End Non tindakan
		--Visit dokter
		UNION ALL
		SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		DR.dr_nm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		COALESCE(to_char(MDR.datetime_trx, 'DD-MM-YYYY'), to_char(A.datetime_in,'DD-MM-YYYY')) AS datetime_trx,
		CASE WHEN COALESCE(DR.spesialis_cd,'')<>'' THEN 'Visit Dokter Spesialis' ELSE 'Visit Dokter Umum' END AS treatment_nm,
		public.FN_RPT_GET_TRXJM_DOKTER_VISIT(MDR.dr_cd,'TRXDR_TP_1',A.medical_cd) AS jm_dr
		FROM trx_medical A
		JOIN trx_medical_dokter MDR ON A.medical_cd=MDR.medical_cd AND trx_dr_tp='TRXDR_TP_1' AND MDR.datetime_trx>=p_dateStart::timestamp  AND MDR.datetime_trx<=p_dateEnd::timestamp 
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MDR.dr_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL AND DR.dr_cd=p_drCd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		--End Visit dokter
		--Laboratorium/Radiologi (unit medis)
		UNION ALL
		SELECT 
		A.medical_cd,
		A.medical_tp,
		COM.code_nm AS medical_tp_nm,
		to_char(A.datetime_in, 'DD-MM-YYYY') as datetime_in,
		concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
		PAS.no_rm,
		INS.insurance_nm,
		DR.dr_nm,
		RJ.medunit_nm,
		KMR.ruang_nm,
		KLS.kelas_nm,
		COALESCE(to_char(MUNIT.datetime_trx, 'DD-MM-YYYY'), to_char(A.datetime_in,'DD-MM-YYYY')) AS datetime_trx,
		MUITEM.medicalunit_nm AS treatment_nm,
		public.FN_RPT_GET_TRXJM_DOKTER_UNITMEDIS(MUNIT.dr2_cd,MUNIT.medicalunit_cd,A.medical_cd) AS jm_dr
		FROM trx_medical A
		JOIN trx_medical_unit MUNIT ON A.medical_cd=MUNIT.medical_cd AND MUNIT.datetime_trx>=p_dateStart::timestamp  AND MUNIT.datetime_trx<=p_dateEnd::timestamp 
		JOIN trx_unitmedis_item MUITEM ON MUNIT.medicalunit_cd=MUITEM.medicalunit_cd
		LEFT JOIN com_code COM ON A.medical_tp=COM.com_cd
		JOIN trx_pasien PAS ON A.pasien_cd=PAS.pasien_cd
		LEFT JOIN trx_pasien_insurance PASINS ON PAS.pasien_cd=PASINS.pasien_cd AND PASINS.default_st='1'
		LEFT JOIN trx_insurance INS ON PASINS.insurance_cd=INS.insurance_cd
		JOIN trx_dokter DR ON MUNIT.dr2_cd=DR.dr_cd AND DR.dr_nm IS NOT NULL AND DR.dr_cd=p_drCd
		LEFT JOIN trx_unit_medis RJ ON A.medunit_cd=RJ.medunit_cd
		LEFT JOIN trx_ruang KMR ON A.ruang_cd=KMR.ruang_cd
		LEFT JOIN trx_kelas KLS ON KMR.kelas_cd=KLS.kelas_cd
		--End Laboratorium/Radiologi (unit medis)
		ORDER BY dr_nm,medical_tp,datetime_trx,datetime_in;
	END IF;
END;
$$ LANGUAGE plpgsql;

SELECT * from public.SP_RPT_GET_TRXJM_BYDATE_ORDERDR('2017-01-01', '2017-12-31', '');
SELECT 'SP_RPT_GET_TRXJM_BYDATE_ORDERDR';