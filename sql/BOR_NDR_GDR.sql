-- DROP FUNCTION public.FN_RPT_TOTAL_PASIENKELAS (p_kelasCd Varchar(20),p_dateStart varchar,p_dateEnd varchar);
-- CREATE OR REPLACE FUNCTION public.FN_RPT_TOTAL_PASIENKELAS (p_kelasCd Varchar(20),p_dateStart varchar,p_dateEnd varchar)
-- RETURNS Bigint
-- AS
-- $$
--    	DECLARE v_intResult Int;
-- BEGIN
-- 	SELECT 
-- 	COUNT(A.medical_cd) into v_intResult
-- 	FROM trx_medical A, trx_ruang B
-- 	WHERE A.ruang_cd=B.ruang_cd
-- 	AND B.kelas_cd=p_kelasCd
-- 	AND (to_char(A.datetime_in, 'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd 
-- 	or to_char(A.datetime_out, 'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd);

-- 	RETURN v_intResult;
-- END;
-- $$ 
-- LANGUAGE plpgsql;

-- DROP FUNCTION public.FN_RPT_TOTAL_HARIRAWAT (p_kelasCd Varchar(10),p_dateStart varchar,p_dateEnd varchar);
-- CREATE OR REPLACE FUNCTION public.FN_RPT_TOTAL_HARIRAWAT (p_kelasCd Varchar(10),p_dateStart varchar,p_dateEnd varchar)
-- RETURNS Bigint
-- AS
-- $$
--    	DECLARE v_intResult Int;
-- BEGIN	
-- 		SELECT SUM(fn_datediff_day(to_char(A.datetime_in, 'yyyy-mm-dd'),to_char(A.datetime_out, 'yyyy-mm-dd')) + 1) into v_intResult
-- 		FROM trx_medical A, trx_ruang B
-- 		WHERE A.ruang_cd=B.ruang_cd
-- 		AND B.kelas_cd=p_kelasCd
-- 		AND (A.datetime_in BETWEEN p_dateStart::timestamp AND p_dateEnd::timestamp  OR
-- 		A.datetime_out BETWEEN p_dateStart::timestamp  AND p_dateEnd::timestamp);
		        		  				  
-- 	RETURN v_intResult;
-- END;
-- $$ LANGUAGE plpgsql;

-- DROP FUNCTION public.FN_INTERVAL_TO_INTEGER(p_interval interval);
--  CREATE OR REPLACE FUNCTION public.FN_INTERVAL_TO_INTEGER(p_interval interval)
--  RETURNS Int
--  as
--  $$
--  DECLARE v_intResult Int;
--  BEGIN
--  	SELECT coalesce(extract(epoch from p_interval), 0) into v_intResult;
--  	Return v_intResult;
--  END;
--  $$ 
--  LANGUAGE plpgsql;

-- DROP FUNCTION public.fn_last_day_month (p_date Varchar);
-- CREATE OR REPLACE FUNCTION public.fn_last_day_month (p_date Varchar)
-- RETURNS varchar
-- AS
-- $$
--    	DECLARE v_intResult varchar;
-- BEGIN	
-- 	SELECT (date_trunc('MONTH', p_date::timestamp) + INTERVAL '1 MONTH - 1 day')::DATE;    		  				  
-- 	RETURN v_intResult::varchar;
-- END;
-- $$ LANGUAGE plpgsql;

-- DROP FUNCTION public.FN_RPT_TOTAL_DEATH_OUTTP11 (p_KelasCd Varchar(10),p_DateStart Timestamp(3),p_DateEnd Timestamp(3));
-- CREATE OR REPLACE FUNCTION public.FN_RPT_TOTAL_DEATH_OUTTP11 (p_KelasCd Varchar(10),p_DateStart Timestamp(3),p_DateEnd Timestamp(3))
-- RETURNS Int
-- AS
-- $$
-- 	DECLARE v_intResult Int;
-- 	-- SELECT FN_INTERVAL_TO_INTEGER(SUM(a.datetime_out::timestamp - a.datetime_in::timestamp)) + 1 into v_intResult
-- BEGIN
-- 	SELECT FN_INTERVAL_TO_INTEGER(SUM(a.datetime_out::timestamp - a.datetime_in::timestamp)) into v_intResult
-- 	FROM trx_medical A, trx_ruang B
-- 	WHERE A.ruang_cd=B.ruang_cd
-- 	AND A.out_tp='OUT_TP_11'
-- 	AND B.kelas_cd=p_KelasCd
-- 	AND (A.datetime_in BETWEEN p_dateStart::timestamp AND p_dateEnd::timestamp  OR
-- 		A.datetime_out BETWEEN p_dateStart::timestamp  AND p_dateEnd::timestamp);   

-- 	RETURN v_intResult;
-- END;
-- $$ 
-- LANGUAGE plpgsql;


-- DROP FUNCTION public.FN_RPT_TOTAL_DEATH (p_KelasCd Varchar,p_DateStart varchar,p_DateEnd varchar);
-- CREATE OR REPLACE FUNCTION public.FN_RPT_TOTAL_DEATH (p_KelasCd Varchar,p_DateStart varchar,p_DateEnd varchar)
-- RETURNS Int
-- AS
-- $$
-- 	DECLARE v_intResult Int;
-- BEGIN
-- 	SELECT 
-- 	SUM(fn_datediff_day(A.datetime_in::varchar,A.datetime_out::varchar) + 1) into v_intResult
-- 	FROM trx_medical A, trx_ruang B
-- 	WHERE A.ruang_cd=B.ruang_cd
-- 	AND A.out_tp IN ('OUT_TP_11','OUT_TP_12')
-- 	AND B.kelas_cd=p_KelasCd
-- 	AND (to_char(A.datetime_in,'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd OR
-- 		to_char(A.datetime_out,'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd);
	        		  				  
-- 	RETURN v_intResult;
-- END;
-- $$ LANGUAGE plpgsql;

-- DROP FUNCTION public.SP_RPT_GET_BOR(p_Month int,p_Year int);
-- CREATE OR REPLACE FUNCTION public.SP_RPT_GET_BOR(p_Month int,p_Year int) 
-- RETURNS table(
-- 	kelas_cd varchar,
-- 	kelas_nm varchar,
-- 	total_kamar Bigint,
-- 	total_pasien Bigint,
-- 	total_harirawat Bigint,
-- 	day_month integer
-- )
-- AS $$
-- 	DECLARE v_dtDateStart varchar;
--  	v_dtDateEnd varchar;
-- BEGIN
-- 	v_dtDateStart := concat(p_Year::Varchar, '-', LPAD(p_Month::varchar,2,'0')::Varchar, '-', '01')::varchar;
-- 	v_dtDateEnd := (date_trunc('MONTH', v_dtDateStart::timestamp) + INTERVAL '1 MONTH - 1 day')::DATE;

-- 	-- execute fn_last_day_month(v_dtDateStart) into v_dtDateEnd;
	
-- 	RETURN QUERY SELECT 
-- 	A.kelas_cd,
-- 	A.kelas_nm,
-- 	COUNT(B.ruang_cd) AS total_kamar,
-- 	COALESCE(public.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd,v_dtDateStart::varchar,v_dtDateEnd::varchar),0) AS total_pasien,
-- 	COALESCE(public.FN_RPT_TOTAL_HARIRAWAT(A.kelas_cd,v_dtDateStart,v_dtDateEnd),0) AS total_harirawat,
-- 	to_char(v_dtDateEnd::timestamp, 'dd')::integer AS day_month
-- 	FROM trx_kelas A 
-- 	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd AND COALESCE(B.ruang_tp,'')<>'1'
-- 	GROUP BY A.kelas_cd,A.kelas_nm
-- 	ORDER BY A.kelas_nm;
-- END;
-- $$ LANGUAGE plpgsql;

-- select * from public.SP_RPT_GET_BOR(2, 2017);

-- DROP FUNCTION public.SP_RPT_GET_NDR (p_Month int,p_Year int);
-- CREATE OR REPLACE FUNCTION public.SP_RPT_GET_NDR (p_Month int,p_Year int) 
-- RETURNS table(
-- 	kelas_cd varchar,
-- 	kelas_nm varchar,
-- 	total_kamar Bigint,
-- 	total_pasien Bigint,
-- 	total_meninggal int,
-- 	day_month integer
-- )
-- AS $$
-- 	DECLARE v_dtDateStart Timestamp(3);
-- 	v_dtDateEnd Timestamp(3);
-- BEGIN
-- 	v_dtDateStart := concat(p_Year::Varchar, '-', LPAD(p_Month::varchar,2,'0')::Varchar, '-', '01')::varchar;
-- 	v_dtDateEnd := (date_trunc('MONTH', v_dtDateStart::timestamp) + INTERVAL '1 MONTH - 1 day')::DATE;
-- 	-- execute fn_last_day_month(v_dtDateStart) into v_dtDateEnd;
-- 	RETURN QUERY SELECT 
-- 	A.kelas_cd,
-- 	A.kelas_nm,
-- 	COUNT(B.ruang_cd) AS total_kamar,
-- 	COALESCE(public.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd,v_dtDateStart::varchar,v_dtDateEnd::varchar),0) AS total_pasien,
-- 	COALESCE(public.FN_RPT_TOTAL_DEATH_OUTTP11(A.kelas_cd,v_dtDateStart,v_dtDateEnd),0) AS total_meninggal,
-- 	to_char(v_dtDateEnd::timestamp, 'dd')::integer AS day_month
-- 	FROM trx_kelas A 
-- 	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd AND COALESCE(B.ruang_tp,'')<>'1'
-- 	GROUP BY A.kelas_cd,A.kelas_nm
-- 	ORDER BY A.kelas_nm;
-- END;
-- $$ LANGUAGE plpgsql;

-- select * from public.SP_RPT_GET_NDR(2, 2017);

-- DROP FUNCTION public.SP_RPT_GET_GDR (p_Month int, p_Year int);
-- CREATE OR REPLACE FUNCTION public.SP_RPT_GET_GDR (p_Month int, p_Year int) 
-- RETURNS table(
-- 	kelas_cd varchar,
-- 	kelas_nm varchar,
-- 	total_kamar Bigint,
-- 	total_pasien Bigint,
-- 	total_meninggal int,
-- 	day_month integer
-- )
-- AS $$
-- 	DECLARE v_dtDateStart Timestamp(3);
-- 	v_dtDateEnd Timestamp(3);
-- BEGIN
-- 	v_dtDateStart := concat(p_Year::Varchar, '-', LPAD(p_Month::varchar,2,'0')::Varchar, '-', '01')::Timestamp;
-- 	v_dtDateEnd := (date_trunc('MONTH', v_dtDateStart::timestamp) + INTERVAL '1 MONTH - 1 day')::DATE;
	
-- 	RETURN QUERY SELECT 
-- 	A.kelas_cd,
-- 	A.kelas_nm,
-- 	COUNT(B.ruang_cd) AS total_kamar,
-- 	COALESCE(public.FN_RPT_TOTAL_PASIENKELAS(A.kelas_cd, v_dtDateStart::varchar,v_dtDateEnd::varchar),0) AS total_pasien,
-- 	COALESCE(public.FN_RPT_TOTAL_DEATH(A.kelas_cd,v_dtDateStart::varchar,v_dtDateEnd::varchar),0) AS total_meninggal,
-- 	to_char(v_dtDateEnd::timestamp, 'dd')::integer AS day_month
-- 	FROM trx_kelas A 
-- 	JOIN trx_ruang B ON A.kelas_cd=B.kelas_cd AND COALESCE(B.ruang_tp,'')<>'1'
-- 	GROUP BY A.kelas_cd,A.kelas_nm
-- 	ORDER BY A.kelas_nm;
-- END;
-- $$ 
-- LANGUAGE plpgsql;

-- select * from public.SP_RPT_GET_GDR(2, 2017);

/*fungsi ambil jumlah pasien per bangsal berdasarkan kelas*/
drop FUNCTION public.fn_rpt_total_pasienbangsal(p_bangsalcd character varying, p_datestart character varying, p_dateend character varying);
CREATE OR REPLACE FUNCTION public.fn_rpt_total_pasienbangsal(p_bangsalcd character varying, p_kelascd character varying,p_datestart character varying, p_dateend character varying)
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
   	DECLARE v_intResult Int;
BEGIN
	SELECT 
	COUNT(A.medical_cd) into v_intResult
	FROM trx_medical A, trx_ruang B
	WHERE A.ruang_cd=B.ruang_cd
	and b.bangsal_cd=p_bangsalcd
	and b.kelas_cd=p_kelascd
	AND (to_char(A.datetime_in, 'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd 
	or to_char(A.datetime_out, 'yyyy-mm-dd') BETWEEN p_dateStart AND p_dateEnd);

	RETURN v_intResult;
END;
$function$


/*new bor*/
SELECT 
bangsal.bangsal_nm,
kelas.kelas_nm,
(select count(*) from trx_ruang where bangsal_cd=bangsal.bangsal_cd and kelas_cd=kelas.kelas_cd) as total_ruang,
COALESCE(public.FN_RPT_TOTAL_PASIENBANGSAL(bangsal.bangsal_cd,kelas.kelas_cd,'2017-03-01'::varchar,'2017-03-31'::varchar),0) AS total_pasien,
COALESCE(public.FN_RPT_TOTAL_HARIRAWAT(ruang.kelas_cd,'2017-03-01','2017-03-31'),0) AS total_harirawat,
to_char('2017-03-31'::timestamp, 'dd')::integer AS day_month
FROM trx_bangsal bangsal
left join trx_ruang ruang ON bangsal.bangsal_cd=ruang.ruang_cd AND COALESCE(ruang.ruang_tp,'')<>'1'
join trx_kelas kelas on 1=1
GROUP BY bangsal.bangsal_cd , ruang.kelas_cd ,kelas.kelas_nm,kelas.kelas_cd
ORDER BY bangsal.bangsal_nm, kelas.kelas_nm;

/*new gdr*/
SELECT 
bangsal.bangsal_nm,
kelas.kelas_nm,
(select count(*) from trx_ruang where bangsal_cd=bangsal.bangsal_cd and kelas_cd=kelas.kelas_cd) as total_ruang,
COALESCE(public.FN_RPT_TOTAL_PASIENBANGSAL(bangsal.bangsal_cd,kelas.kelas_cd,'2017-03-01'::varchar,'2017-03-31'::varchar),0) AS total_pasien,
COALESCE(public.FN_RPT_TOTAL_DEATH(ruang.kelas_cd,'2017-03-01','2017-03-31'),0) AS total_meninggal,
to_char('2017-03-31'::timestamp, 'dd')::integer AS day_month
FROM trx_bangsal bangsal
left join trx_ruang ruang ON bangsal.bangsal_cd=ruang.ruang_cd AND COALESCE(ruang.ruang_tp,'')<>'1'
join trx_kelas kelas on 1=1
GROUP BY bangsal.bangsal_cd , ruang.kelas_cd ,kelas.kelas_nm,kelas.kelas_cd
ORDER BY bangsal.bangsal_nm, kelas.kelas_nm;

/*new ndr*/
SELECT 
bangsal.bangsal_nm,
kelas.kelas_nm,
(select count(*) from trx_ruang where bangsal_cd=bangsal.bangsal_cd and kelas_cd=kelas.kelas_cd) as total_ruang,
COALESCE(public.FN_RPT_TOTAL_PASIENBANGSAL(bangsal.bangsal_cd,kelas.kelas_cd,'2017-03-01'::varchar,'2017-03-31'::varchar),0) AS total_pasien,
COALESCE(public.FN_RPT_TOTAL_DEATH_OUTTP11(ruang.kelas_cd,'2017-03-01','2017-03-31'),0) AS total_meninggal,
to_char('2017-03-31'::timestamp, 'dd')::integer AS day_month
FROM trx_bangsal bangsal
left join trx_ruang ruang ON bangsal.bangsal_cd=ruang.ruang_cd AND COALESCE(ruang.ruang_tp,'')<>'1'
join trx_kelas kelas on 1=1
GROUP BY bangsal.bangsal_cd , ruang.kelas_cd ,kelas.kelas_nm,kelas.kelas_cd
ORDER BY bangsal.bangsal_nm, kelas.kelas_nm;

/*diagnosa rawat jalan*/
select 
icd.icd_cd,
icd_nm,
count(*) as jumlah
from trx_medical_record mr
join trx_medical med on med.medical_cd=mr.medical_cd and med.medical_tp='MEDICAL_TP_01' -- rawat jalan
left join trx_icd as icd on icd.icd_cd=mr.icd_cd 
	and mr.case_tp='RM_TP_1' 
	and to_char(datetime_record, 'MM')='01' --bulan
	and to_char(datetime_record,'YYYY') ='2017' -- tahun
group by icd.icd_cd, icd_nm
order by count(*) desc
limit 20;

/*diagnosa rawat inap*/
select 
icd.icd_cd,
icd_nm,
count(*) as jumlah
from trx_medical_record mr
join trx_medical med on med.medical_cd=mr.medical_cd and med.medical_tp='MEDICAL_TP_02' -- rawat jalan
left join trx_icd as icd on icd.icd_cd=mr.icd_cd 
	and mr.case_tp='RM_TP_1' 
	and to_char(datetime_record, 'MM')='01' --bulan
	and to_char(datetime_record,'YYYY') ='2017' -- tahun
group by icd.icd_cd, icd_nm
order by count(*) desc
limit 20;