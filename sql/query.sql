SELECT 
		ICD.icd_cd,
		ICD.icd_nm,
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= 0 AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= 6 ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age <= 6)
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_01'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN 'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as laki_0To6hari,

		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= 0 AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= 6 ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= 0 AND Pas2.age <= 6)
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_02'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN 'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as perempuan_0To6hari,

		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= 7 AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= 28 ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= 7 AND Pas2.age <= 28)
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_01'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as laki_7To28hari,

		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= 7 AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= 28 ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= 7 AND Pas2.age <= 28)
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_02'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as perempuan_7To28hari,

		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= 28 AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= 365 ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age <= 365)
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_01'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as laki_28hariTo1th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= 28 AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= 365 ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= 28 AND Pas2.age <= 365)
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_02'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as perempuan_28hariTo1th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= 365 AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= (365*4) ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age <= (365*4))
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_01'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as laki_1To4th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= 365 AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= (365*4) ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= 365 AND Pas2.age <= (365*4))
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_02'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as perempuan_1To4th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= (5*365) AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= (365*14) ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age <= (365*14))
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_01'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as laki_5To14th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= (5*365) AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= (365*14) ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= (5*365) AND Pas2.age <= (365*14))
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_02'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as perempuan_5To14th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= (15*365) AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= (365*24) ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age <= (365*24))
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_01'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as laki_15To24th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= (15*365) AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= (365*24) ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= (15*365) AND Pas2.age <= (365*24))
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_02'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as perempuan_15To24th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= (25*365) AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= (365*44) ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age <= (365*44))
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_01'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as laki_25To44th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= (25*365) AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= (365*44) ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= (25*365) AND Pas2.age <= (365*44))
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_02'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as perempuan_25To44th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
				--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= (45*365) AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= (365*64) ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age <= (365*64))
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_01'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as laki_45To64th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= (45*365) AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) <= (365*64) ) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= (45*365) AND Pas2.age <= (365*64))
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_02'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as perempuan_45To64th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= (65*365)) OR
					(PAS2.birth_date IS NULL AND Pas2.age >= (65*365))
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_01'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as laki_lebih65th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE 
				(
					(PAS2.birth_date IS NOT NULL AND fn_date_diff_day(Pas2.birth_date::timestamp,NOW()::timestamp) >= (65*365))OR
					(PAS2.birth_date IS NULL AND Pas2.age >= (65*365))
				)
				AND Med2.medical_tp='MEDICAL_TP_01'
				AND Med2.out_tp IS NOT NULL
				AND Pas2.gender_tp='GENDER_TP_02'
				AND ICD2.icd_cd = ICD.icd_cd
				AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
				AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
				AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as perempuan_lebih65th,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
					--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND Med2.out_tp IS NOT NULL
			AND ICD2.icd_cd = ICD.icd_cd
			AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
			AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
			AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
			AND PAS2.birth_date IS NULL AND Pas2.age IS NULL
		) as kosong,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd  
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND Med2.out_tp IS NOT NULL
			AND Pas2.gender_tp='GENDER_TP_01'
			AND ICD2.icd_cd = ICD.icd_cd
			AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
			AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
			AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as total_laki,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2 
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND Med2.out_tp IS NOT NULL
			AND Pas2.gender_tp='GENDER_TP_02'
			AND ICD2.icd_cd = ICD.icd_cd
			AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
			AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
			AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as total_perempuan,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND (Med2.out_tp <> 'OUT_TP_04' AND Med2.out_tp IS NOT NULL)
			AND ICD2.icd_cd = ICD.icd_cd
			AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
			AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
			AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as total_hidup,
		
		(SELECT COUNT(MR2.medical_record_seqno)
			FROM trx_medical_record MR2
			LEFT JOIN trx_medical Med2 ON Med2.medical_cd = MR2.medical_cd 
			LEFT JOIN trx_pasien Pas2 ON Pas2.pasien_cd = MR2.pasien_cd
			--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW2 ON VW2.icd_cd = MR2.icd_cd
			LEFT JOIN trx_icd ICD2 ON ICD2.icd_cd = MR2.icd_cd
			WHERE Med2.medical_tp='MEDICAL_TP_01'
			AND (Med2.out_tp = 'OUT_TP_04' AND Med2.out_tp IS NOT NULL)
			AND ICD2.icd_cd = ICD.icd_cd
			AND (ICD2.icd_cd BETWEEN  'A00' AND 'A99')
			AND (MR2.datetime_record)::timestamp >= '2017-01-01'::timestamp
			AND (MR2.datetime_record)::timestamp <= '2017-04-01'::timestamp
		) as total_mati,	
		
		COUNT(MR.medical_record_seqno) as total
	FROM trx_medical_record MR
	LEFT JOIN trx_medical Med ON Med.medical_cd = MR.medical_cd  
	LEFT JOIN trx_pasien Pas ON Pas.pasien_cd = MR.pasien_cd
	--LEFT JOIN simrke.VW_TRX_ICD_MOBIRDITAS VW ON VW.icd_cd = MR.icd_cd
	LEFT JOIN trx_icd ICD ON ICD.icd_cd = MR.icd_cd
	WHERE med.medical_tp='MEDICAL_TP_01'
	AND Med.out_tp IS NOT NULL
	AND ICD.icd_cd IS NOT NULL
	AND (ICD.icd_cd BETWEEN  'A00' AND 'A99')
	AND (MR.datetime_record)::timestamp >= '2017-01-01'::timestamp
	AND (MR.datetime_record)::timestamp <= '2017-04-01'::timestamp
	GROUP BY ICD.icd_cd, ICD.icd_nm
	ORDER BY ICD.icd_cd;