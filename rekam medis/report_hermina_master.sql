drop view vw_report_hermina_master;
create view vw_report_hermina_master as 
 SELECT pas.pasien_cd,
 med.medical_cd,
    concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
    jk.code_nm AS jenis_kelamin,
    pas.birth_date as birth_date,
    med.medical_tp AS medical_tp,
    med.visit_tp as visit_tp,
	jaminan.insurance_cd as insurance_cd,
	bangsal.bangsal_cd as ruang_perawatan,
    med.dr_cd as dr_cd,
    dr.spesialis_cd as spesialis_cd,
    kelas.kelas_cd,
    coalesce(
		(select icd_cd 
		from trx_medical_record rm
		where rm.medical_cd=med.medical_cd
		and	rm.rm_tp='RM_TP_1'), 
		(select icd_cd 
		from trx_medical_record rm
		where rm.medical_cd=med.medical_cd
		and	rm.rm_tp != 'RM_TP_1'
		and rm.datetime_record=(select max(datetime_record) 
						from trx_medical_record rm2 
						where rm.medical_cd=rm2.medical_cd
						))
	) as diagnosa,
    med.datetime_in,
    med.datetime_out,
    med.reff_tp,
    med.referensi_cd,
    med.out_tp as out_tp,
    med.medical_trx_st
   FROM trx_medical med
     JOIN trx_pasien pas ON med.pasien_cd::text = pas.pasien_cd::text
     LEFT JOIN trx_ruang ruang ON ruang.ruang_cd::text = med.ruang_cd::text
     LEFT JOIN trx_kelas kelas ON kelas.kelas_cd::text = ruang.kelas_cd::text
     LEFT JOIN trx_dokter dr ON dr.dr_cd::text = med.dr_cd::text
     LEFT JOIN com_code jk ON jk.com_cd::text = pas.gender_tp::text
     LEFT JOIN com_code mtp ON mtp.com_cd::text = med.medical_tp::text
     left join trx_pasien_insurance jaminan on jaminan.pasien_cd=med.pasien_cd
     left join trx_bangsal bangsal on bangsal.bangsal_cd=ruang.bangsal_cd;

select * from vw_report_hermina_master;

/*laporan hari perawatan per spesialis*/

select 
pasien_nm,
extract(year FROM age(master.birth_date)) as umur,
jenis_kelamin,
coalesce(jaminan.insurance_nm, 'PRIBADI') as insurance_nm,
kelas.kelas_nm as kelas_nm,
dokter.dr_nm as dr_nm,
spesialis.spesialis_nm as spesialis_nm, 
datetime_out::timestamp - datetime_in::timestamp as hari -- update
from public.sp_rpt_hermina_master() as master
left join trx_insurance as jaminan on jaminan.insurance_cd=master.insurance_cd
left join trx_kelas kelas on kelas.kelas_cd=master.kelas_cd
left join trx_dokter dokter on dokter.dr_cd=master.dr_cd
left join trx_spesialis as spesialis on spesialis.spesialis_cd=master.spesialis_cd
where master.medical_tp='MEDICAL_TP_02';

/*hari perawatan terdiri dari per dokter*/

-- bisa jadi sama kayak laporan hari perawatan per spesialis

/*pasien rawat inap menjadi laporan rp 1*/

/*laporan pasien pulang rawat inap*/
select 
pasien_nm,
extract(year FROM age(master.birth_date)) as umur,
jenis_kelamin,
coalesce(jaminan.insurance_nm, 'PRIBADI') as insurance_nm,
kelas.kelas_nm as kelas_nm,
dokter.dr_nm as dr_nm,
icd.icd_nm as diagnosa,
spesialis.spesialis_nm as spesialis_nm,
out.code_nm as out_tp
from public.vw_report_hermina_master as master
left join trx_icd icd on icd.icd_cd=master.diagnosa
left join com_code out on master.out_tp=out.com_cd
left join trx_insurance as jaminan on jaminan.insurance_cd=master.insurance_cd
left join trx_kelas kelas on kelas.kelas_cd=master.kelas_cd
left join trx_dokter dokter on dokter.dr_cd=master.dr_cd
left join trx_spesialis as spesialis on spesialis.spesialis_cd=master.spesialis_cd
where master.medical_tp='MEDICAL_TP_02'
and medical_trx_st='MEDICAL_TRX_ST_1';

/*rujukan*/

select 
pasien_nm,
extract(year FROM age(master.birth_date)) as umur,
jenis_kelamin,
coalesce(jaminan.insurance_nm, 'PRIBADI') as insurance_nm,
kelas.kelas_nm as kelas_nm,
dokter.dr_nm as dr_nm,
icd.icd_nm as diagnosa,
spesialis.spesialis_nm as spesialis_nm,
out.code_nm as out_tp,
reff.referensi_nm as referensi_nm
from public.vw_report_hermina_master as master
left join trx_icd icd on icd.icd_cd=master.diagnosa
left join com_code out on master.out_tp=out.com_cd
left join trx_insurance as jaminan on jaminan.insurance_cd=master.insurance_cd
left join trx_kelas kelas on kelas.kelas_cd=master.kelas_cd
left join trx_dokter dokter on dokter.dr_cd=master.dr_cd
left join trx_spesialis as spesialis on spesialis.spesialis_cd=master.spesialis_cd
join trx_referensi reff on reff.referensi_cd=master.referensi_cd;

/*diagnosa besar ke kecil*/

select 
mr.icd_cd as icd_cd,
icd.icd_nm as icd_nm,
--sp.spesialis_nm,
count(*) as jumlah
from trx_medical_record mr
join trx_medical med on med.medical_cd=mr.medical_cd
join trx_icd icd on mr.icd_cd=icd.icd_cd
--left join trx_dokter dr on med.dr_cd=dr.dr_cd
--left join trx_spesialis sp on sp.spesialis_cd=dr.spesialis_cd
where med.medical_tp='MEDICAL_TP_01'
group by mr.icd_cd, icd.icd_nm
order by jumlah desc;

/*pasien per dokter per tindakan*/


/*20 penyakit tertinggi*/
select 
icd.icd_cd,
icd_nm,
count(*) as jumlah
from trx_medical_record mr
left join trx_icd as icd on icd.icd_cd=mr.icd_cd 
	and mr.case_tp='RM_TP_1' 
	and to_char(datetime_record, 'MM')='03' 
	and to_char(datetime_record,'YYYY') ='2017'
group by icd.icd_cd, icd_nm
order by count(*) desc
limit 20;

/*rawat jalan bulanan*/
select 
dr.dr_cd,
dr.dr_nm,
med.medunit_cd,
case when dr.sep is not null then 'V' else 'X' end as sep,
case when dr.str is not null then 'V' else 'X' end as str,
bulan.bulan_cd,
bulan.bulan_nm,
count(med.medical_cd) as total
from trx_dokter dr
left join com_bulan bulan on 1=1
left join trx_medical med on dr.dr_cd=med.dr_cd 
	and med.medical_tp='MEDICAL_TP_01' 
	and to_char(med.datetime_in, 'YYYY') = '2017' 
	and to_char(med.datetime_in, 'MM') = bulan.bulan_cd
--where med.medunit_cd='POLIGIGI'
group by dr.dr_cd,dr.dr_nm, bulan.bulan_cd, bulan.bulan_nm, med.medunit_cd
order by dr.dr_cd, dr.dr_nm, bulan.bulan_cd;

/*Rawat inap bulanan*/
select 
dr.dr_cd,
dr.dr_nm,
med.medunit_cd,
case when dr.sep is not null then 'V' else 'X' end as sep,
case when dr.str is not null then 'V' else 'X' end as str,
bulan.bulan_cd,
bulan.bulan_nm,
count(med.medical_cd) as total
from trx_dokter dr
left join com_bulan bulan on 1=1
left join trx_medical med on dr.dr_cd=med.dr_cd 
	and med.medical_tp='MEDICAL_TP_01' 
	and to_char(med.datetime_in, 'YYYY') = '2017' 
	and to_char(med.datetime_in, 'MM') = bulan.bulan_cd
--where med.medunit_cd='POLIGIGI'
group by dr.dr_cd,dr.dr_nm, bulan.bulan_cd, bulan.bulan_nm, med.medunit_cd
order by dr.dr_cd, dr.dr_nm, bulan.bulan_cd;

