DROP FUNCTION public.sp_rpt_bor_per_kelas(p_Year varchar, p_Bangsal varchar);
CREATE OR REPLACE FUNCTION public.sp_rpt_bor_per_kelas(p_Year varchar, p_Bangsal varchar)
RETURNS TABLE
    (
     bangsal_nm varchar,
     kelas_nm varchar,
     bulan_nm varchar,
     last_date text,
     tt bigint,
     total_hari_rawat bigint
    )
AS $function$
BEGIN
    RETURN QUERY
    SELECT 
      bangsal.bangsal_nm,
      kelas.kelas_nm,
      bulan.bulan_nm,
      to_char(fn_last_day_month(concat(to_char(now(), 'YYYY'),'-',bulan.bulan_cd,'-01'))::timestamp, 'DD') as last_date,
      (select count(*) from trx_ruang where kelas_cd=kelas.kelas_cd and bangsal_cd=bangsal.bangsal_cd) as TT,
      fn_rpt_total_harirawat(kelas.kelas_cd, p_Year||'-01-01'::varchar, p_Year||'-12-31'::varchar) as total_hari_rawat
    FROM 
      trx_bangsal bangsal
      left join trx_ruang ruang ON bangsal.bangsal_cd=ruang.ruang_cd AND COALESCE(ruang.ruang_tp,'')<>'1'
      join trx_kelas kelas on 1=1
      left join com_bulan bulan on 1=1
    WHERE 
      bangsal.bangsal_cd = p_Bangsal
    GROUP BY 
      bangsal.bangsal_cd , ruang.kelas_cd ,kelas.kelas_nm,kelas.kelas_cd,bulan.bulan_cd, bulan.bulan_nm
    ORDER BY 
      bangsal.bangsal_nm, kelas.kelas_nm,bulan.bulan_cd;
END;
$function$
LANGUAGE plpgsql;

SELECT * FROM public.sp_rpt_bor_per_kelas('2017','RAnak');