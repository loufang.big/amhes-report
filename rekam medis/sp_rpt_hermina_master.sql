DROP FUNCTION public.sp_rpt_hermina_master();
CREATE OR REPLACE FUNCTION public.sp_rpt_hermina_master()
RETURNS TABLE
    (
        pasien_cd character varying,
        medical_cd character varying,
        pasien_nm text,
        jenis_kelamin character varying,
        birth_date date,
        medical_tp character varying,
        visit_tp character varying,
        insurance_cd character varying,
        ruang_perawatan character varying,
        dr_cd character varying,
        spesialis_cd character varying,
        kelas_cd character varying,
        diagnosa character varying,
        datetime_in timestamp,
        datetime_out timestamp,
        reff_tp character varying,
        referensi_cd character varying,
        out_tp character varying,
        medical_trx_st character varying
    )
AS $function$
BEGIN
    RETURN QUERY
    SELECT 
        pas.pasien_cd,
        med.medical_cd,
        concat(pas.pasien_nm, ' ', pas.middle_nm, ' ', pas.last_nm) AS pasien_nm,
        jk.code_nm AS jenis_kelamin,
        pas.birth_date as birth_date,
        med.medical_tp AS medical_tp,
        med.visit_tp as visit_tp,
        jaminan.insurance_cd as insurance_cd,
        bangsal.bangsal_cd as ruang_perawatan,
        med.dr_cd as dr_cd,
        dr.spesialis_cd as spesialis_cd,
        kelas.kelas_cd,
        coalesce(
            (select icd_cd 
            from trx_medical_record rm
            where rm.medical_cd=med.medical_cd
            and rm.rm_tp='RM_TP_1'), 
            (select icd_cd 
            from trx_medical_record rm
            where rm.medical_cd=med.medical_cd
            and rm.rm_tp != 'RM_TP_1'
            and rm.datetime_record=(select max(datetime_record) 
                            from trx_medical_record rm2 
                            where rm.medical_cd=rm2.medical_cd
                            )
                            limit 1)
        ) as diagnosa,
        med.datetime_in,
        med.datetime_out,
        med.reff_tp,
        med.referensi_cd,
        med.out_tp as out_tp,
        med.medical_trx_st
    FROM 
        trx_medical med
        JOIN trx_pasien pas ON med.pasien_cd::text = pas.pasien_cd::text
        LEFT JOIN trx_ruang ruang ON ruang.ruang_cd::text = med.ruang_cd::text
        LEFT JOIN trx_kelas kelas ON kelas.kelas_cd::text = ruang.kelas_cd::text
        LEFT JOIN trx_dokter dr ON dr.dr_cd::text = med.dr_cd::text
        LEFT JOIN com_code jk ON jk.com_cd::text = pas.gender_tp::text
        LEFT JOIN com_code mtp ON mtp.com_cd::text = med.medical_tp::text
        left join trx_pasien_insurance jaminan on jaminan.pasien_cd=med.pasien_cd
        left join trx_bangsal bangsal on bangsal.bangsal_cd=ruang.bangsal_cd;
END;
$function$
LANGUAGE plpgsql;

SELECT * FROM public.sp_rpt_hermina_master();