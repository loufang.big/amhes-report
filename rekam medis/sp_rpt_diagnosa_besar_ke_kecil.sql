DROP FUNCTION public.sp_rpt_diagnosa_besar_ke_kecil();
CREATE OR REPLACE FUNCTION public.sp_rpt_diagnosa_besar_ke_kecil()
RETURNS TABLE
    (
      icd_cd varchar,
      icd_nm varchar,
      jumlah bigint
    )
AS $function$
BEGIN
    RETURN QUERY
    select 
      mr.icd_cd as icd_cd,
      icd.icd_nm as icd_nm,
      --sp.spesialis_nm,
      count(*) as jumlah
    from 
      trx_medical_record mr
      join trx_medical med on med.medical_cd=mr.medical_cd
      join trx_icd icd on mr.icd_cd=icd.icd_cd
      --left join trx_dokter dr on med.dr_cd=dr.dr_cd
      --left join trx_spesialis sp on sp.spesialis_cd=dr.spesialis_cd
    where 
      med.medical_tp='MEDICAL_TP_01'
      group by mr.icd_cd, icd.icd_nm
      order by jumlah desc;
END;
$function$
LANGUAGE plpgsql;

SELECT * FROM public.sp_rpt_diagnosa_besar_ke_kecil();