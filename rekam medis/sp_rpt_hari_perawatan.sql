DROP FUNCTION public.sp_rpt_hari_perawatan();
CREATE OR REPLACE FUNCTION public.sp_rpt_hari_perawatan()
RETURNS TABLE
    (
       pasien_nm text,
       umur float,
       jenis_kelamin varchar,
       insurance_nm varchar,
       kelas_nm varchar,
       dr_nm varchar,
       spesialis_nm varchar,
       hari interval
    )
AS $function$
BEGIN
    RETURN QUERY
    select 
      master.pasien_nm,
      extract(year FROM age(master.birth_date)) as umur,
      master.jenis_kelamin,
      coalesce(jaminan.insurance_nm, 'PRIBADI') as insurance_nm,
      kelas.kelas_nm as kelas_nm,
      dokter.dr_nm as dr_nm,
      spesialis.spesialis_nm as spesialis_nm, 
      datetime_out::timestamp - datetime_in::timestamp as hari -- update
    from 
      public.sp_rpt_hermina_master() as master
      left join trx_insurance as jaminan on jaminan.insurance_cd=master.insurance_cd
      left join trx_kelas kelas on kelas.kelas_cd=master.kelas_cd
      left join trx_dokter dokter on dokter.dr_cd=master.dr_cd
      left join trx_spesialis as spesialis on spesialis.spesialis_cd=master.spesialis_cd
    where 
      master.medical_tp='MEDICAL_TP_02';

END;
$function$
LANGUAGE plpgsql;

SELECT * FROM public.sp_rpt_hari_perawatan();